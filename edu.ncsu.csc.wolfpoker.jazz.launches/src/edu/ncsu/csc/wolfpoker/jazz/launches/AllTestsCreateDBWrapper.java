/*******************************************************************************
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2007. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp. 
 *******************************************************************************/
package edu.ncsu.csc.wolfpoker.jazz.launches;

import com.ibm.team.repository.service.tests.AllTestsCreateDB;

/**
 * Exposes <code>AllTestsCreateDB</code> so we can easily refer to it in a
 * launch file. <code>AllTestsCreateDB</code> is a JUnit test that builds a
 * repository database based on the components it can find in the workspace and
 * target platform.
 */
public class AllTestsCreateDBWrapper extends AllTestsCreateDB {

}
