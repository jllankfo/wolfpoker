/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import com.ibm.team.repository.common.IContributorHandle;

import com.ibm.team.repository.common.model.impl.SimpleItemImpl;

import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Session History</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl#getDate <em>Date</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl#getHistoryType <em>History Type</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl#getUser <em>User</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl#getOldValue <em>Old Value</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl#getNewValue <em>New Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SessionHistoryImpl extends SimpleItemImpl implements
		SessionHistory {
	/**
	 * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected int ALL_FLAGS = 0;

	/**
	 * The default value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected static final long DATE_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getDate() <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDate()
	 * @generated
	 * @ordered
	 */
	protected long date = DATE_EDEFAULT;

	/**
	 * The flag representing whether the Date attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int DATE_ESETFLAG = 1 << 11;

	/**
	 * The default value of the '{@link #getHistoryType() <em>History Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistoryType()
	 * @generated
	 * @ordered
	 */
	protected static final String HISTORY_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistoryType() <em>History Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistoryType()
	 * @generated
	 * @ordered
	 */
	protected String historyType = HISTORY_TYPE_EDEFAULT;

	/**
	 * The flag representing whether the History Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int HISTORY_TYPE_ESETFLAG = 1 << 12;

	/**
	 * The cached value of the '{@link #getUser() <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUser()
	 * @generated
	 * @ordered
	 */
	protected IContributorHandle user;

	/**
	 * The flag representing whether the User reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int USER_ESETFLAG = 1 << 13;

	/**
	 * The default value of the '{@link #getOldValue() <em>Old Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldValue()
	 * @generated
	 * @ordered
	 */
	protected static final String OLD_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOldValue() <em>Old Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldValue()
	 * @generated
	 * @ordered
	 */
	protected String oldValue = OLD_VALUE_EDEFAULT;

	/**
	 * The flag representing whether the Old Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int OLD_VALUE_ESETFLAG = 1 << 14;

	/**
	 * The default value of the '{@link #getNewValue() <em>New Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewValue()
	 * @generated
	 * @ordered
	 */
	protected static final String NEW_VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNewValue() <em>New Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewValue()
	 * @generated
	 * @ordered
	 */
	protected String newValue = NEW_VALUE_EDEFAULT;

	/**
	 * The flag representing whether the New Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int NEW_VALUE_ESETFLAG = 1 << 15;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int EOFFSET_CORRECTION = JazzPackage.Literals.SESSION_HISTORY
			.getFeatureID(JazzPackage.Literals.SESSION_HISTORY__DATE)
			- JazzPackage.SESSION_HISTORY__DATE;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SessionHistoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return JazzPackage.Literals.SESSION_HISTORY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getDate() {
		return date;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDate(long newDate) {
		long oldDate = date;
		date = newDate;
		boolean oldDateESet = (ALL_FLAGS & DATE_ESETFLAG) != 0;
		ALL_FLAGS |= DATE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION_HISTORY__DATE + EOFFSET_CORRECTION,
					oldDate, date, !oldDateESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDate() {
		long oldDate = date;
		boolean oldDateESet = (ALL_FLAGS & DATE_ESETFLAG) != 0;
		date = DATE_EDEFAULT;
		ALL_FLAGS &= ~DATE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION_HISTORY__DATE + EOFFSET_CORRECTION,
					oldDate, DATE_EDEFAULT, oldDateESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDate() {
		return (ALL_FLAGS & DATE_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHistoryType() {
		return historyType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistoryType(String newHistoryType) {
		String oldHistoryType = historyType;
		historyType = newHistoryType;
		boolean oldHistoryTypeESet = (ALL_FLAGS & HISTORY_TYPE_ESETFLAG) != 0;
		ALL_FLAGS |= HISTORY_TYPE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION_HISTORY__HISTORY_TYPE
							+ EOFFSET_CORRECTION, oldHistoryType, historyType,
					!oldHistoryTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistoryType() {
		String oldHistoryType = historyType;
		boolean oldHistoryTypeESet = (ALL_FLAGS & HISTORY_TYPE_ESETFLAG) != 0;
		historyType = HISTORY_TYPE_EDEFAULT;
		ALL_FLAGS &= ~HISTORY_TYPE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION_HISTORY__HISTORY_TYPE
							+ EOFFSET_CORRECTION, oldHistoryType,
					HISTORY_TYPE_EDEFAULT, oldHistoryTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistoryType() {
		return (ALL_FLAGS & HISTORY_TYPE_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IContributorHandle getUser() {
		if (user != null && ((EObject) user).eIsProxy()) {
			InternalEObject oldUser = (InternalEObject) user;
			user = (IContributorHandle) eResolveProxy(oldUser);
			if (user != oldUser) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JazzPackage.SESSION_HISTORY__USER
									+ EOFFSET_CORRECTION, oldUser, user));
			}
		}
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IContributorHandle basicGetUser() {
		return user;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUser(IContributorHandle newUser) {
		IContributorHandle oldUser = user;
		user = newUser;
		boolean oldUserESet = (ALL_FLAGS & USER_ESETFLAG) != 0;
		ALL_FLAGS |= USER_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION_HISTORY__USER + EOFFSET_CORRECTION,
					oldUser, user, !oldUserESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUser() {
		IContributorHandle oldUser = user;
		boolean oldUserESet = (ALL_FLAGS & USER_ESETFLAG) != 0;
		user = null;
		ALL_FLAGS &= ~USER_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION_HISTORY__USER + EOFFSET_CORRECTION,
					oldUser, null, oldUserESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUser() {
		return (ALL_FLAGS & USER_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOldValue() {
		return oldValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOldValue(String newOldValue) {
		String oldOldValue = oldValue;
		oldValue = newOldValue;
		boolean oldOldValueESet = (ALL_FLAGS & OLD_VALUE_ESETFLAG) != 0;
		ALL_FLAGS |= OLD_VALUE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					JazzPackage.SESSION_HISTORY__OLD_VALUE + EOFFSET_CORRECTION,
					oldOldValue, oldValue, !oldOldValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOldValue() {
		String oldOldValue = oldValue;
		boolean oldOldValueESet = (ALL_FLAGS & OLD_VALUE_ESETFLAG) != 0;
		oldValue = OLD_VALUE_EDEFAULT;
		ALL_FLAGS &= ~OLD_VALUE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.UNSET,
					JazzPackage.SESSION_HISTORY__OLD_VALUE + EOFFSET_CORRECTION,
					oldOldValue, OLD_VALUE_EDEFAULT, oldOldValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOldValue() {
		return (ALL_FLAGS & OLD_VALUE_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNewValue(String newNewValue) {
		String oldNewValue = newValue;
		newValue = newNewValue;
		boolean oldNewValueESet = (ALL_FLAGS & NEW_VALUE_ESETFLAG) != 0;
		ALL_FLAGS |= NEW_VALUE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					JazzPackage.SESSION_HISTORY__NEW_VALUE + EOFFSET_CORRECTION,
					oldNewValue, newValue, !oldNewValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNewValue() {
		String oldNewValue = newValue;
		boolean oldNewValueESet = (ALL_FLAGS & NEW_VALUE_ESETFLAG) != 0;
		newValue = NEW_VALUE_EDEFAULT;
		ALL_FLAGS &= ~NEW_VALUE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.UNSET,
					JazzPackage.SESSION_HISTORY__NEW_VALUE + EOFFSET_CORRECTION,
					oldNewValue, NEW_VALUE_EDEFAULT, oldNewValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNewValue() {
		return (ALL_FLAGS & NEW_VALUE_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.SESSION_HISTORY__DATE:
			return new Long(getDate());
		case JazzPackage.SESSION_HISTORY__HISTORY_TYPE:
			return getHistoryType();
		case JazzPackage.SESSION_HISTORY__USER:
			if (resolve)
				return getUser();
			return basicGetUser();
		case JazzPackage.SESSION_HISTORY__OLD_VALUE:
			return getOldValue();
		case JazzPackage.SESSION_HISTORY__NEW_VALUE:
			return getNewValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.SESSION_HISTORY__DATE:
			setDate(((Long) newValue).longValue());
			return;
		case JazzPackage.SESSION_HISTORY__HISTORY_TYPE:
			setHistoryType((String) newValue);
			return;
		case JazzPackage.SESSION_HISTORY__USER:
			setUser((IContributorHandle) newValue);
			return;
		case JazzPackage.SESSION_HISTORY__OLD_VALUE:
			setOldValue((String) newValue);
			return;
		case JazzPackage.SESSION_HISTORY__NEW_VALUE:
			setNewValue((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.SESSION_HISTORY__DATE:
			unsetDate();
			return;
		case JazzPackage.SESSION_HISTORY__HISTORY_TYPE:
			unsetHistoryType();
			return;
		case JazzPackage.SESSION_HISTORY__USER:
			unsetUser();
			return;
		case JazzPackage.SESSION_HISTORY__OLD_VALUE:
			unsetOldValue();
			return;
		case JazzPackage.SESSION_HISTORY__NEW_VALUE:
			unsetNewValue();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.SESSION_HISTORY__DATE:
			return isSetDate();
		case JazzPackage.SESSION_HISTORY__HISTORY_TYPE:
			return isSetHistoryType();
		case JazzPackage.SESSION_HISTORY__USER:
			return isSetUser();
		case JazzPackage.SESSION_HISTORY__OLD_VALUE:
			return isSetOldValue();
		case JazzPackage.SESSION_HISTORY__NEW_VALUE:
			return isSetNewValue();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class baseClass) {
		if (baseClass == SessionHistoryHandle.class) {
			switch (baseFeatureID) {
			default:
				return -1;
			}
		}
		if (baseClass == SessionHistory.class) {
			switch (baseFeatureID - EOFFSET_CORRECTION) {
			case JazzPackage.SESSION_HISTORY__DATE:
				return JazzPackage.SESSION_HISTORY__DATE + EOFFSET_CORRECTION;
			case JazzPackage.SESSION_HISTORY__HISTORY_TYPE:
				return JazzPackage.SESSION_HISTORY__HISTORY_TYPE
						+ EOFFSET_CORRECTION;
			case JazzPackage.SESSION_HISTORY__USER:
				return JazzPackage.SESSION_HISTORY__USER + EOFFSET_CORRECTION;
			case JazzPackage.SESSION_HISTORY__OLD_VALUE:
				return JazzPackage.SESSION_HISTORY__OLD_VALUE
						+ EOFFSET_CORRECTION;
			case JazzPackage.SESSION_HISTORY__NEW_VALUE:
				return JazzPackage.SESSION_HISTORY__NEW_VALUE
						+ EOFFSET_CORRECTION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (date: "); //$NON-NLS-1$
		if ((ALL_FLAGS & DATE_ESETFLAG) != 0)
			result.append(date);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", historyType: "); //$NON-NLS-1$
		if ((ALL_FLAGS & HISTORY_TYPE_ESETFLAG) != 0)
			result.append(historyType);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", oldValue: "); //$NON-NLS-1$
		if ((ALL_FLAGS & OLD_VALUE_ESETFLAG) != 0)
			result.append(oldValue);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", newValue: "); //$NON-NLS-1$
		if ((ALL_FLAGS & NEW_VALUE_ESETFLAG) != 0)
			result.append(newValue);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(')');
		return result.toString();
	}

} //SessionHistoryImpl
