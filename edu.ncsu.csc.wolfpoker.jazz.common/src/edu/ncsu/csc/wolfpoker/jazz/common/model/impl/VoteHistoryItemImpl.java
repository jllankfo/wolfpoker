/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import com.ibm.team.repository.common.UUID;

import com.ibm.team.repository.common.model.impl.HelperImpl;

import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Vote;
import edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vote History Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteHistoryItemImpl#getSession <em>Session</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteHistoryItemImpl#getVotes <em>Votes</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteHistoryItemImpl#getVoteRound <em>Vote Round</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VoteHistoryItemImpl extends HelperImpl implements VoteHistoryItem {
	/**
	 * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected int ALL_FLAGS = 0;

	/**
	 * The default value of the '{@link #getSession() <em>Session</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSession()
	 * @generated
	 * @ordered
	 */
	protected static final UUID SESSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSession() <em>Session</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSession()
	 * @generated
	 * @ordered
	 */
	protected UUID session = SESSION_EDEFAULT;

	/**
	 * The flag representing whether the Session attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int SESSION_ESETFLAG = 1 << 1;

	/**
	 * The cached value of the '{@link #getVotes() <em>Votes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVotes()
	 * @generated
	 * @ordered
	 */
	protected EList votes;

	/**
	 * The default value of the '{@link #getVoteRound() <em>Vote Round</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoteRound()
	 * @generated
	 * @ordered
	 */
	protected static final int VOTE_ROUND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getVoteRound() <em>Vote Round</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoteRound()
	 * @generated
	 * @ordered
	 */
	protected int voteRound = VOTE_ROUND_EDEFAULT;

	/**
	 * The flag representing whether the Vote Round attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int VOTE_ROUND_ESETFLAG = 1 << 2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int EOFFSET_CORRECTION = JazzPackage.Literals.VOTE_HISTORY_ITEM
			.getFeatureID(JazzPackage.Literals.VOTE_HISTORY_ITEM__SESSION)
			- JazzPackage.VOTE_HISTORY_ITEM__SESSION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VoteHistoryItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return JazzPackage.Literals.VOTE_HISTORY_ITEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UUID getSession() {
		return session;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSession(UUID newSession) {
		UUID oldSession = session;
		session = newSession;
		boolean oldSessionESet = (ALL_FLAGS & SESSION_ESETFLAG) != 0;
		ALL_FLAGS |= SESSION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					JazzPackage.VOTE_HISTORY_ITEM__SESSION + EOFFSET_CORRECTION,
					oldSession, session, !oldSessionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSession() {
		UUID oldSession = session;
		boolean oldSessionESet = (ALL_FLAGS & SESSION_ESETFLAG) != 0;
		session = SESSION_EDEFAULT;
		ALL_FLAGS &= ~SESSION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.UNSET,
					JazzPackage.VOTE_HISTORY_ITEM__SESSION + EOFFSET_CORRECTION,
					oldSession, SESSION_EDEFAULT, oldSessionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSession() {
		return (ALL_FLAGS & SESSION_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getVotes() {
		if (votes == null) {
			votes = new EObjectContainmentEList.Unsettable(Vote.class, this,
					JazzPackage.VOTE_HISTORY_ITEM__VOTES + EOFFSET_CORRECTION);
		}
		return votes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVotes() {
		if (votes != null)
			((InternalEList.Unsettable) votes).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVotes() {
		return votes != null && ((InternalEList.Unsettable) votes).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getVoteRound() {
		return voteRound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVoteRound(int newVoteRound) {
		int oldVoteRound = voteRound;
		voteRound = newVoteRound;
		boolean oldVoteRoundESet = (ALL_FLAGS & VOTE_ROUND_ESETFLAG) != 0;
		ALL_FLAGS |= VOTE_ROUND_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.VOTE_HISTORY_ITEM__VOTE_ROUND
							+ EOFFSET_CORRECTION, oldVoteRound, voteRound,
					!oldVoteRoundESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVoteRound() {
		int oldVoteRound = voteRound;
		boolean oldVoteRoundESet = (ALL_FLAGS & VOTE_ROUND_ESETFLAG) != 0;
		voteRound = VOTE_ROUND_EDEFAULT;
		ALL_FLAGS &= ~VOTE_ROUND_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.VOTE_HISTORY_ITEM__VOTE_ROUND
							+ EOFFSET_CORRECTION, oldVoteRound,
					VOTE_ROUND_EDEFAULT, oldVoteRoundESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVoteRound() {
		return (ALL_FLAGS & VOTE_ROUND_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE_HISTORY_ITEM__VOTES:
			return ((InternalEList) getVotes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE_HISTORY_ITEM__SESSION:
			return getSession();
		case JazzPackage.VOTE_HISTORY_ITEM__VOTES:
			return getVotes();
		case JazzPackage.VOTE_HISTORY_ITEM__VOTE_ROUND:
			return new Integer(getVoteRound());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE_HISTORY_ITEM__SESSION:
			setSession((UUID) newValue);
			return;
		case JazzPackage.VOTE_HISTORY_ITEM__VOTES:
			getVotes().clear();
			getVotes().addAll((Collection) newValue);
			return;
		case JazzPackage.VOTE_HISTORY_ITEM__VOTE_ROUND:
			setVoteRound(((Integer) newValue).intValue());
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE_HISTORY_ITEM__SESSION:
			unsetSession();
			return;
		case JazzPackage.VOTE_HISTORY_ITEM__VOTES:
			unsetVotes();
			return;
		case JazzPackage.VOTE_HISTORY_ITEM__VOTE_ROUND:
			unsetVoteRound();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE_HISTORY_ITEM__SESSION:
			return isSetSession();
		case JazzPackage.VOTE_HISTORY_ITEM__VOTES:
			return isSetVotes();
		case JazzPackage.VOTE_HISTORY_ITEM__VOTE_ROUND:
			return isSetVoteRound();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class baseClass) {
		if (baseClass == VoteHistoryItem.class) {
			switch (baseFeatureID - EOFFSET_CORRECTION) {
			case JazzPackage.VOTE_HISTORY_ITEM__SESSION:
				return JazzPackage.VOTE_HISTORY_ITEM__SESSION
						+ EOFFSET_CORRECTION;
			case JazzPackage.VOTE_HISTORY_ITEM__VOTES:
				return JazzPackage.VOTE_HISTORY_ITEM__VOTES
						+ EOFFSET_CORRECTION;
			case JazzPackage.VOTE_HISTORY_ITEM__VOTE_ROUND:
				return JazzPackage.VOTE_HISTORY_ITEM__VOTE_ROUND
						+ EOFFSET_CORRECTION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (session: "); //$NON-NLS-1$
		if ((ALL_FLAGS & SESSION_ESETFLAG) != 0)
			result.append(session);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", voteRound: "); //$NON-NLS-1$
		if ((ALL_FLAGS & VOTE_ROUND_ESETFLAG) != 0)
			result.append(voteRound);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(')');
		return result.toString();
	}

} //VoteHistoryItemImpl
