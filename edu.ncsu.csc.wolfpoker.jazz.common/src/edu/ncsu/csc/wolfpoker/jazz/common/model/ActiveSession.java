/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import com.ibm.team.repository.common.model.SimpleItem;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Active Session</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getAssociatedSession <em>Associated Session</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getJoinedParticipants <em>Joined Participants</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemUnderDiscussion <em>Item Under Discussion</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemsCompleted <em>Items Completed</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getLastOperation <em>Last Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getActiveSession()
 * @model
 * @generated
 */
public interface ActiveSession extends SimpleItem, ActiveSessionHandle {
	/**
	 * Returns the value of the '<em><b>Associated Session</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Associated Session</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Associated Session</em>' reference.
	 * @see #isSetAssociatedSession()
	 * @see #unsetAssociatedSession()
	 * @see #setAssociatedSession(SessionHandle)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getActiveSession_AssociatedSession()
	 * @model unsettable="true"
	 *        annotation="queryableProperty unique='false'"
	 * @generated
	 */
	SessionHandle getAssociatedSession();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getAssociatedSession <em>Associated Session</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Associated Session</em>' reference.
	 * @see #isSetAssociatedSession()
	 * @see #unsetAssociatedSession()
	 * @see #getAssociatedSession()
	 * @generated
	 */
	void setAssociatedSession(SessionHandle value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getAssociatedSession <em>Associated Session</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAssociatedSession()
	 * @see #getAssociatedSession()
	 * @see #setAssociatedSession(SessionHandle)
	 * @generated
	 */
	void unsetAssociatedSession();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getAssociatedSession <em>Associated Session</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Associated Session</em>' reference is set.
	 * @see #unsetAssociatedSession()
	 * @see #getAssociatedSession()
	 * @see #setAssociatedSession(SessionHandle)
	 * @generated
	 */
	boolean isSetAssociatedSession();

	/**
	 * Returns the value of the '<em><b>Joined Participants</b></em>' reference list.
	 * The list contents are of type {@link com.ibm.team.repository.common.IContributorHandle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Joined Participants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Joined Participants</em>' reference list.
	 * @see #isSetJoinedParticipants()
	 * @see #unsetJoinedParticipants()
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getActiveSession_JoinedParticipants()
	 * @model type="com.ibm.team.repository.common.model.ContributorHandleFacade" unsettable="true"
	 * @generated
	 */
	List getJoinedParticipants();

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getJoinedParticipants <em>Joined Participants</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetJoinedParticipants()
	 * @see #getJoinedParticipants()
	 * @generated
	 */
	void unsetJoinedParticipants();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getJoinedParticipants <em>Joined Participants</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Joined Participants</em>' reference list is set.
	 * @see #unsetJoinedParticipants()
	 * @see #getJoinedParticipants()
	 * @generated
	 */
	boolean isSetJoinedParticipants();

	/**
	 * Returns the value of the '<em><b>Item Under Discussion</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item Under Discussion</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item Under Discussion</em>' reference.
	 * @see #isSetItemUnderDiscussion()
	 * @see #unsetItemUnderDiscussion()
	 * @see #setItemUnderDiscussion(ItemForEstimation)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getActiveSession_ItemUnderDiscussion()
	 * @model unsettable="true"
	 * @generated
	 */
	ItemForEstimation getItemUnderDiscussion();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemUnderDiscussion <em>Item Under Discussion</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Item Under Discussion</em>' reference.
	 * @see #isSetItemUnderDiscussion()
	 * @see #unsetItemUnderDiscussion()
	 * @see #getItemUnderDiscussion()
	 * @generated
	 */
	void setItemUnderDiscussion(ItemForEstimation value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemUnderDiscussion <em>Item Under Discussion</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetItemUnderDiscussion()
	 * @see #getItemUnderDiscussion()
	 * @see #setItemUnderDiscussion(ItemForEstimation)
	 * @generated
	 */
	void unsetItemUnderDiscussion();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemUnderDiscussion <em>Item Under Discussion</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Item Under Discussion</em>' reference is set.
	 * @see #unsetItemUnderDiscussion()
	 * @see #getItemUnderDiscussion()
	 * @see #setItemUnderDiscussion(ItemForEstimation)
	 * @generated
	 */
	boolean isSetItemUnderDiscussion();

	/**
	 * Returns the value of the '<em><b>Items Completed</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Items Completed</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Items Completed</em>' containment reference list.
	 * @see #isSetItemsCompleted()
	 * @see #unsetItemsCompleted()
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getActiveSession_ItemsCompleted()
	 * @model type="edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation" containment="true" unsettable="true"
	 * @generated
	 */
	List getItemsCompleted();

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemsCompleted <em>Items Completed</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetItemsCompleted()
	 * @see #getItemsCompleted()
	 * @generated
	 */
	void unsetItemsCompleted();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemsCompleted <em>Items Completed</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Items Completed</em>' containment reference list is set.
	 * @see #unsetItemsCompleted()
	 * @see #getItemsCompleted()
	 * @generated
	 */
	boolean isSetItemsCompleted();

	/**
	 * Returns the value of the '<em><b>Last Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Operation</em>' attribute.
	 * @see #isSetLastOperation()
	 * @see #unsetLastOperation()
	 * @see #setLastOperation(int)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getActiveSession_LastOperation()
	 * @model unsettable="true"
	 * @generated
	 */
	int getLastOperation();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getLastOperation <em>Last Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Operation</em>' attribute.
	 * @see #isSetLastOperation()
	 * @see #unsetLastOperation()
	 * @see #getLastOperation()
	 * @generated
	 */
	void setLastOperation(int value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getLastOperation <em>Last Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLastOperation()
	 * @see #getLastOperation()
	 * @see #setLastOperation(int)
	 * @generated
	 */
	void unsetLastOperation();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getLastOperation <em>Last Operation</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Last Operation</em>' attribute is set.
	 * @see #unsetLastOperation()
	 * @see #getLastOperation()
	 * @see #setLastOperation(int)
	 * @generated
	 */
	boolean isSetLastOperation();

} // ActiveSession
