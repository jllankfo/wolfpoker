package edu.ncsu.csc.wolfpoker.jazz.common;

import com.ibm.team.repository.common.IItemType;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.repository.common.transport.IParameterWrapper;
import com.ibm.team.repository.common.transport.ITeamModelledRestService;
import com.ibm.team.workitem.common.model.IWorkItem;

import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public interface IWolfPokerService extends ITeamModelledRestService {

	public static IItemType PLANNING_POKER_SESSION_TYPE = IItemType.IRegistry.INSTANCE.getItemType(JazzPackage.eINSTANCE
				.getSession().getName(), JazzPackage.eNS_URI);
	
	public interface SessionHistoryChanges {
		public static final String NAME = "Name";
		public static final String CREATED = "Created";
		public static final String COORDINATOR = "Session Coordinator";
		public static final String DESCRIPTION = "Description";
		public static final String PARTICIPANTS = "Participants";
		public static final String ITEMS = "Work Items";
		public static final String VOTE = "Vote";
	}
	
	public interface LastActiveSessionOperation {
		final int CREATED = 0;
		final int VOTE_STARTED=1;
		final int  PARTICIPANT_JOINED=2;
		final int PARTICIPANT_LEFT=3;
		final int  REVOTE=4;
		final int VOTE=5;
		final int VOTE_SAVED=6;
		final int SHOW_RESULT=7;
		final int VOTECANCEL = 8;
		final int VOTEGAMECANCEL = 9;
	}
	
	Session[] getAllSessionsByProjectArea(SessionsByProjectAreaParam param) throws TeamRepositoryException;
	
	IWorkItem[] getWorkItemsByIteration(UUIDParam iterationUUID)
			throws TeamRepositoryException;
	
	Session postNewSession(SessionParameters params)
			throws TeamRepositoryException;

	void postDeleteSession(UUIDParam params) throws TeamRepositoryException;

	Session postSaveSession(SaveSessionParameters params)
			throws TeamRepositoryException;
	
	ActiveSession postStartSession(UUIDParam params) throws TeamRepositoryException;

	ActiveSession postJoinSession(UUIDParam params) throws TeamRepositoryException;
	
	void postLeaveSession(UUIDParam params) throws TeamRepositoryException;
	
	void postStartVoting(StoryVoteParam params) throws TeamRepositoryException;
	
	void postShowResult(StoryVoteParam params) throws TeamRepositoryException;
	
	boolean postVote(VoteParam params) throws TeamRepositoryException;
	
	boolean postVoteGameCancel(VoteParam params) throws TeamRepositoryException;
	
	boolean postVoteCancel(VoteParam params) throws TeamRepositoryException;
	
	void postSaveVote(VoteParam params) throws TeamRepositoryException;
	
	void postStartRevoting(StoryVoteParam params) throws TeamRepositoryException;
	
	boolean postSaveActiveSession(UUIDParam params) throws TeamRepositoryException;
	
	void postCancelActiveSession(UUIDParam params) throws TeamRepositoryException;
	
	boolean postArchiveSession(UUIDParam params) throws TeamRepositoryException;
	
	public static final class StoryVoteParam implements IParameterWrapper {
		public String activeSessionUUID;
		public String workItemUUID;
	}
	
	public static final class VoteParam implements IParameterWrapper {
		public String activeSessionUUID;
		public String workItemUUID;
		public int vote;
		public String oldVote;
	}
	
	public static final class SessionParameters implements IParameterWrapper {
		public String name;
		public String[] participantsUserID;
		public String createrUserID;
		public String sessionCoordinatorUserID;
		public String description;
		public String[] itemsUUID;
		public String projectAreaName;
		public int cardsValuelowerBound;
		public int cardsValueUpperBound;
		
		//Additional items for session history
		public int vote;
		public String oldVote;
	}

	public static final class SaveSessionParameters implements
			IParameterWrapper {
		public String name;
		public String[] participantsUserID;
		public String createrUserID;
		public String sessionCoordinatorUserID;
		public String description;
		public String[] itemsID;
		public String projectArea;
		public String sessionID;
		public int cardsValuelowerBound;
		public int cardsValueUpperBound;
	}

	public static final class UUIDParam implements IParameterWrapper {
		public String uuid;
	}
	
	public static final class SessionsByProjectAreaParam implements IParameterWrapper {
		public String projectName;
	}
}