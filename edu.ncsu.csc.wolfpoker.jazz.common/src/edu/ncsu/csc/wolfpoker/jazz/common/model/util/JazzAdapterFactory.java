/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.util;

import com.ibm.team.repository.common.IAuditable;
import com.ibm.team.repository.common.IAuditableHandle;
import com.ibm.team.repository.common.IHelper;
import com.ibm.team.repository.common.IItem;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.repository.common.IManagedItem;
import com.ibm.team.repository.common.IManagedItemHandle;
import com.ibm.team.repository.common.ISimpleItem;
import com.ibm.team.repository.common.ISimpleItemHandle;

import com.ibm.team.repository.common.model.Auditable;
import com.ibm.team.repository.common.model.AuditableHandle;
import com.ibm.team.repository.common.model.Helper;
import com.ibm.team.repository.common.model.Item;
import com.ibm.team.repository.common.model.ItemHandle;
import com.ibm.team.repository.common.model.ManagedItem;
import com.ibm.team.repository.common.model.ManagedItemHandle;
import com.ibm.team.repository.common.model.SimpleItem;
import com.ibm.team.repository.common.model.SimpleItemHandle;

import edu.ncsu.csc.wolfpoker.jazz.common.model.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage
 * @generated
 */
public class JazzAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static JazzPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JazzAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = JazzPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JazzSwitch modelSwitch = new JazzSwitch() {
		public Object caseSession(Session object) {
			return createSessionAdapter();
		}

		public Object caseSessionHandle(SessionHandle object) {
			return createSessionHandleAdapter();
		}

		public Object caseSessionHistory(SessionHistory object) {
			return createSessionHistoryAdapter();
		}

		public Object caseSessionHistoryHandle(SessionHistoryHandle object) {
			return createSessionHistoryHandleAdapter();
		}

		public Object caseActiveSession(ActiveSession object) {
			return createActiveSessionAdapter();
		}

		public Object caseActiveSessionHandle(ActiveSessionHandle object) {
			return createActiveSessionHandleAdapter();
		}

		public Object caseVote(Vote object) {
			return createVoteAdapter();
		}

		public Object caseItemForEstimation(ItemForEstimation object) {
			return createItemForEstimationAdapter();
		}

		public Object caseVoteHistoryItem(VoteHistoryItem object) {
			return createVoteHistoryItemAdapter();
		}

		public Object caseItemHandleFacade(IItemHandle object) {
			return createItemHandleFacadeAdapter();
		}

		public Object caseItemHandle(ItemHandle object) {
			return createItemHandleAdapter();
		}

		public Object caseItemFacade(IItem object) {
			return createItemFacadeAdapter();
		}

		public Object caseItem(Item object) {
			return createItemAdapter();
		}

		public Object caseManagedItemHandleFacade(IManagedItemHandle object) {
			return createManagedItemHandleFacadeAdapter();
		}

		public Object caseManagedItemHandle(ManagedItemHandle object) {
			return createManagedItemHandleAdapter();
		}

		public Object caseManagedItemFacade(IManagedItem object) {
			return createManagedItemFacadeAdapter();
		}

		public Object caseManagedItem(ManagedItem object) {
			return createManagedItemAdapter();
		}

		public Object caseAuditableHandleFacade(IAuditableHandle object) {
			return createAuditableHandleFacadeAdapter();
		}

		public Object caseAuditableHandle(AuditableHandle object) {
			return createAuditableHandleAdapter();
		}

		public Object caseAuditableFacade(IAuditable object) {
			return createAuditableFacadeAdapter();
		}

		public Object caseAuditable(Auditable object) {
			return createAuditableAdapter();
		}

		public Object caseSimpleItemHandleFacade(ISimpleItemHandle object) {
			return createSimpleItemHandleFacadeAdapter();
		}

		public Object caseSimpleItemHandle(SimpleItemHandle object) {
			return createSimpleItemHandleAdapter();
		}

		public Object caseSimpleItemFacade(ISimpleItem object) {
			return createSimpleItemFacadeAdapter();
		}

		public Object caseSimpleItem(SimpleItem object) {
			return createSimpleItemAdapter();
		}

		public Object caseHelperFacade(IHelper object) {
			return createHelperFacadeAdapter();
		}

		public Object caseHelper(Helper object) {
			return createHelperAdapter();
		}

		public Object defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	public Adapter createAdapter(Notifier target) {
		return (Adapter) modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session <em>Session</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session
	 * @generated
	 */
	public Adapter createSessionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHandle <em>Session Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHandle
	 * @generated
	 */
	public Adapter createSessionHandleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory <em>Session History</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory
	 * @generated
	 */
	public Adapter createSessionHistoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle <em>Session History Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle
	 * @generated
	 */
	public Adapter createSessionHistoryHandleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession <em>Active Session</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession
	 * @generated
	 */
	public Adapter createActiveSessionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSessionHandle <em>Active Session Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSessionHandle
	 * @generated
	 */
	public Adapter createActiveSessionHandleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote <em>Vote</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Vote
	 * @generated
	 */
	public Adapter createVoteAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation <em>Item For Estimation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation
	 * @generated
	 */
	public Adapter createItemForEstimationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem <em>Vote History Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem
	 * @generated
	 */
	public Adapter createVoteHistoryItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.IItemHandle <em>Item Handle Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.IItemHandle
	 * @generated
	 */
	public Adapter createItemHandleFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.ItemHandle <em>Item Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.ItemHandle
	 * @generated
	 */
	public Adapter createItemHandleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.IItem <em>Item Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.IItem
	 * @generated
	 */
	public Adapter createItemFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.Item <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.Item
	 * @generated
	 */
	public Adapter createItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.IManagedItemHandle <em>Managed Item Handle Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.IManagedItemHandle
	 * @generated
	 */
	public Adapter createManagedItemHandleFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.ManagedItemHandle <em>Managed Item Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.ManagedItemHandle
	 * @generated
	 */
	public Adapter createManagedItemHandleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.IManagedItem <em>Managed Item Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.IManagedItem
	 * @generated
	 */
	public Adapter createManagedItemFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.ManagedItem <em>Managed Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.ManagedItem
	 * @generated
	 */
	public Adapter createManagedItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.IAuditableHandle <em>Auditable Handle Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.IAuditableHandle
	 * @generated
	 */
	public Adapter createAuditableHandleFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.AuditableHandle <em>Auditable Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.AuditableHandle
	 * @generated
	 */
	public Adapter createAuditableHandleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.IAuditable <em>Auditable Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.IAuditable
	 * @generated
	 */
	public Adapter createAuditableFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.Auditable <em>Auditable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.Auditable
	 * @generated
	 */
	public Adapter createAuditableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.ISimpleItemHandle <em>Simple Item Handle Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.ISimpleItemHandle
	 * @generated
	 */
	public Adapter createSimpleItemHandleFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.SimpleItemHandle <em>Simple Item Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.SimpleItemHandle
	 * @generated
	 */
	public Adapter createSimpleItemHandleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.ISimpleItem <em>Simple Item Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.ISimpleItem
	 * @generated
	 */
	public Adapter createSimpleItemFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.SimpleItem <em>Simple Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.SimpleItem
	 * @generated
	 */
	public Adapter createSimpleItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.IHelper <em>Helper Facade</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.IHelper
	 * @generated
	 */
	public Adapter createHelperFacadeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.ibm.team.repository.common.model.Helper <em>Helper</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.ibm.team.repository.common.model.Helper
	 * @generated
	 */
	public Adapter createHelperAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //JazzAdapterFactory
