/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import com.ibm.team.repository.common.IContributorHandle;

import com.ibm.team.repository.common.model.Helper;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vote</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getParticipant <em>Participant</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getVote <em>Vote</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getVote()
 * @model
 * @generated
 */
public interface Vote extends Helper {
	/**
	 * Returns the value of the '<em><b>Participant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participant</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participant</em>' reference.
	 * @see #isSetParticipant()
	 * @see #unsetParticipant()
	 * @see #setParticipant(IContributorHandle)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getVote_Participant()
	 * @model type="com.ibm.team.repository.common.model.ContributorHandleFacade" unsettable="true"
	 * @generated
	 */
	IContributorHandle getParticipant();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getParticipant <em>Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Participant</em>' reference.
	 * @see #isSetParticipant()
	 * @see #unsetParticipant()
	 * @see #getParticipant()
	 * @generated
	 */
	void setParticipant(IContributorHandle value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getParticipant <em>Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParticipant()
	 * @see #getParticipant()
	 * @see #setParticipant(IContributorHandle)
	 * @generated
	 */
	void unsetParticipant();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getParticipant <em>Participant</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Participant</em>' reference is set.
	 * @see #unsetParticipant()
	 * @see #getParticipant()
	 * @see #setParticipant(IContributorHandle)
	 * @generated
	 */
	boolean isSetParticipant();

	/**
	 * Returns the value of the '<em><b>Vote</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vote</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vote</em>' attribute.
	 * @see #isSetVote()
	 * @see #unsetVote()
	 * @see #setVote(int)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getVote_Vote()
	 * @model unsettable="true"
	 * @generated
	 */
	int getVote();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getVote <em>Vote</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vote</em>' attribute.
	 * @see #isSetVote()
	 * @see #unsetVote()
	 * @see #getVote()
	 * @generated
	 */
	void setVote(int value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getVote <em>Vote</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVote()
	 * @see #getVote()
	 * @see #setVote(int)
	 * @generated
	 */
	void unsetVote();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getVote <em>Vote</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Vote</em>' attribute is set.
	 * @see #unsetVote()
	 * @see #getVote()
	 * @see #setVote(int)
	 * @generated
	 */
	boolean isSetVote();

} // Vote
