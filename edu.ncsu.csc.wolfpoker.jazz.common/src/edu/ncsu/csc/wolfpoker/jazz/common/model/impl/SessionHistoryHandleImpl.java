/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import com.ibm.team.repository.common.model.impl.SimpleItemHandleImpl;

import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Session History Handle</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class SessionHistoryHandleImpl extends SimpleItemHandleImpl implements
		SessionHistoryHandle {
	/**
	 * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected int ALL_FLAGS = 0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SessionHistoryHandleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return JazzPackage.Literals.SESSION_HISTORY_HANDLE;
	}

} //SessionHistoryHandleImpl
