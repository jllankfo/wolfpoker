package edu.ncsu.csc.wolfpoker.jazz.common.model.query.impl;

import com.ibm.team.repository.common.internal.querypath.AbstractQueryPathModel;
import com.ibm.team.repository.common.internal.querypath.IQueryPath;
import com.ibm.team.repository.common.model.query.impl.SimpleItemQueryModelImpl;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseActiveSessionQueryModel.ActiveSessionQueryModel;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseActiveSessionQueryModel.ManyActiveSessionQueryModel;
import java.util.List;
import java.util.Map;

/**
 * @generated
 */
public class ActiveSessionQueryModelImpl extends SimpleItemQueryModelImpl implements ManyActiveSessionQueryModel, ActiveSessionQueryModel {

	/** @generated */
	private SessionQueryModelImpl associatedSession;

 	/**
     * @param anExtent the parent query element
     * @param aField the name of the parent's feature that this model represents
	 * @generated
	 */
	public ActiveSessionQueryModelImpl(IQueryPath anExtent, String aField) {
		super(anExtent,  aField);
		_implementation.setItemType("ActiveSession", "edu.ncsu.csc.wolfpoker.jazz"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
 	 * @generated
	 */
	public SessionQueryModelImpl associatedSession() {	
		synchronized (this) {
			if (associatedSession == null) { 
				associatedSession = new SessionQueryModelImpl(_implementation, "associatedSession"); //$NON-NLS-1$	
			}				
			return associatedSession;	
		}
	}

	/**
	 * @generated
	 * @Override
	 */
	protected void initProperties(List allFieldNames, List allReferenceNames, Map allFields) {
		super.initProperties(allFieldNames, allReferenceNames, allFields);
           
		allReferenceNames.add("associatedSession"); //$NON-NLS-1$
	}	

	/**
	 * @generated
	 * @Override
	 */
	protected AbstractQueryPathModel getReference(String name) { 
		if ("associatedSession".equals(name)) return associatedSession(); //$NON-NLS-1$
		return super.getReference(name);
	}

}