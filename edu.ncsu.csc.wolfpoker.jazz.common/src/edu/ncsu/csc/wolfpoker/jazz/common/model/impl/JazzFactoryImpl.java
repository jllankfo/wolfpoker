/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import edu.ncsu.csc.wolfpoker.jazz.common.model.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JazzFactoryImpl extends EFactoryImpl implements JazzFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static JazzFactory init() {
		try {
			JazzFactory theJazzFactory = (JazzFactory) EPackage.Registry.INSTANCE
					.getEFactory("edu.ncsu.csc.wolfpoker.jazz"); //$NON-NLS-1$ 
			if (theJazzFactory != null) {
				return theJazzFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new JazzFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JazzFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case JazzPackage.SESSION:
			return (EObject) createSession();
		case JazzPackage.SESSION_HANDLE:
			return (EObject) createSessionHandle();
		case JazzPackage.SESSION_HISTORY:
			return (EObject) createSessionHistory();
		case JazzPackage.SESSION_HISTORY_HANDLE:
			return (EObject) createSessionHistoryHandle();
		case JazzPackage.ACTIVE_SESSION:
			return (EObject) createActiveSession();
		case JazzPackage.ACTIVE_SESSION_HANDLE:
			return (EObject) createActiveSessionHandle();
		case JazzPackage.VOTE:
			return (EObject) createVote();
		case JazzPackage.ITEM_FOR_ESTIMATION:
			return (EObject) createItemForEstimation();
		case JazzPackage.VOTE_HISTORY_ITEM:
			return (EObject) createVoteHistoryItem();
		default:
			throw new IllegalArgumentException(
					"The class '" + eClass.getName() + "' is not a valid classifier"); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Session createSession() {
		SessionImpl session = new SessionImpl();
		return session;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionHandle createSessionHandle() {
		SessionHandleImpl sessionHandle = new SessionHandleImpl();
		return sessionHandle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionHistory createSessionHistory() {
		SessionHistoryImpl sessionHistory = new SessionHistoryImpl();
		return sessionHistory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionHistoryHandle createSessionHistoryHandle() {
		SessionHistoryHandleImpl sessionHistoryHandle = new SessionHistoryHandleImpl();
		return sessionHistoryHandle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveSession createActiveSession() {
		ActiveSessionImpl activeSession = new ActiveSessionImpl();
		return activeSession;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActiveSessionHandle createActiveSessionHandle() {
		ActiveSessionHandleImpl activeSessionHandle = new ActiveSessionHandleImpl();
		return activeSessionHandle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Vote createVote() {
		VoteImpl vote = new VoteImpl();
		return vote;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ItemForEstimation createItemForEstimation() {
		ItemForEstimationImpl itemForEstimation = new ItemForEstimationImpl();
		return itemForEstimation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VoteHistoryItem createVoteHistoryItem() {
		VoteHistoryItemImpl voteHistoryItem = new VoteHistoryItemImpl();
		return voteHistoryItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JazzPackage getJazzPackage() {
		return (JazzPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	public static JazzPackage getPackage() {
		return JazzPackage.eINSTANCE;
	}

} //JazzFactoryImpl
