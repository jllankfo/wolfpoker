/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage
 * @generated
 */
public interface JazzFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JazzFactory eINSTANCE = edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzFactoryImpl
			.init();

	/**
	 * Returns a new object of class '<em>Session</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session</em>'.
	 * @generated
	 */
	Session createSession();

	/**
	 * Returns a new object of class '<em>Session Handle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session Handle</em>'.
	 * @generated
	 */
	SessionHandle createSessionHandle();

	/**
	 * Returns a new object of class '<em>Session History</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session History</em>'.
	 * @generated
	 */
	SessionHistory createSessionHistory();

	/**
	 * Returns a new object of class '<em>Session History Handle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Session History Handle</em>'.
	 * @generated
	 */
	SessionHistoryHandle createSessionHistoryHandle();

	/**
	 * Returns a new object of class '<em>Active Session</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Session</em>'.
	 * @generated
	 */
	ActiveSession createActiveSession();

	/**
	 * Returns a new object of class '<em>Active Session Handle</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Active Session Handle</em>'.
	 * @generated
	 */
	ActiveSessionHandle createActiveSessionHandle();

	/**
	 * Returns a new object of class '<em>Vote</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vote</em>'.
	 * @generated
	 */
	Vote createVote();

	/**
	 * Returns a new object of class '<em>Item For Estimation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Item For Estimation</em>'.
	 * @generated
	 */
	ItemForEstimation createItemForEstimation();

	/**
	 * Returns a new object of class '<em>Vote History Item</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vote History Item</em>'.
	 * @generated
	 */
	VoteHistoryItem createVoteHistoryItem();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	JazzPackage getJazzPackage();

} //JazzFactory
