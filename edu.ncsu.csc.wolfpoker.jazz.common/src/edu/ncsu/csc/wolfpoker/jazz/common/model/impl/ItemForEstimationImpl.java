/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import com.ibm.team.repository.common.IItemHandle;

import com.ibm.team.repository.common.model.impl.HelperImpl;

import edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation;
import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Vote;
import edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Item For Estimation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl#getItem <em>Item</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl#getCurrentVotes <em>Current Votes</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl#getVoteHistory <em>Vote History</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl#getSavedVote <em>Saved Vote</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl#getRoundNo <em>Round No</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ItemForEstimationImpl extends HelperImpl implements
		ItemForEstimation {
	/**
	 * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected int ALL_FLAGS = 0;

	/**
	 * The cached value of the '{@link #getItem() <em>Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItem()
	 * @generated
	 * @ordered
	 */
	protected IItemHandle item;

	/**
	 * The flag representing whether the Item reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int ITEM_ESETFLAG = 1 << 1;

	/**
	 * The cached value of the '{@link #getCurrentVotes() <em>Current Votes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentVotes()
	 * @generated
	 * @ordered
	 */
	protected EList currentVotes;

	/**
	 * The cached value of the '{@link #getVoteHistory() <em>Vote History</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoteHistory()
	 * @generated
	 * @ordered
	 */
	protected EList voteHistory;

	/**
	 * The default value of the '{@link #getSavedVote() <em>Saved Vote</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSavedVote()
	 * @generated
	 * @ordered
	 */
	protected static final int SAVED_VOTE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSavedVote() <em>Saved Vote</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSavedVote()
	 * @generated
	 * @ordered
	 */
	protected int savedVote = SAVED_VOTE_EDEFAULT;

	/**
	 * The flag representing whether the Saved Vote attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int SAVED_VOTE_ESETFLAG = 1 << 2;

	/**
	 * The default value of the '{@link #getRoundNo() <em>Round No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoundNo()
	 * @generated
	 * @ordered
	 */
	protected static final int ROUND_NO_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRoundNo() <em>Round No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoundNo()
	 * @generated
	 * @ordered
	 */
	protected int roundNo = ROUND_NO_EDEFAULT;

	/**
	 * The flag representing whether the Round No attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int ROUND_NO_ESETFLAG = 1 << 3;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int EOFFSET_CORRECTION = JazzPackage.Literals.ITEM_FOR_ESTIMATION
			.getFeatureID(JazzPackage.Literals.ITEM_FOR_ESTIMATION__ITEM)
			- JazzPackage.ITEM_FOR_ESTIMATION__ITEM;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ItemForEstimationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return JazzPackage.Literals.ITEM_FOR_ESTIMATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IItemHandle getItem() {
		if (item != null && ((EObject) item).eIsProxy()) {
			InternalEObject oldItem = (InternalEObject) item;
			item = (IItemHandle) eResolveProxy(oldItem);
			if (item != oldItem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JazzPackage.ITEM_FOR_ESTIMATION__ITEM
									+ EOFFSET_CORRECTION, oldItem, item));
			}
		}
		return item;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IItemHandle basicGetItem() {
		return item;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setItem(IItemHandle newItem) {
		IItemHandle oldItem = item;
		item = newItem;
		boolean oldItemESet = (ALL_FLAGS & ITEM_ESETFLAG) != 0;
		ALL_FLAGS |= ITEM_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.ITEM_FOR_ESTIMATION__ITEM + EOFFSET_CORRECTION,
					oldItem, item, !oldItemESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetItem() {
		IItemHandle oldItem = item;
		boolean oldItemESet = (ALL_FLAGS & ITEM_ESETFLAG) != 0;
		item = null;
		ALL_FLAGS &= ~ITEM_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.ITEM_FOR_ESTIMATION__ITEM + EOFFSET_CORRECTION,
					oldItem, null, oldItemESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetItem() {
		return (ALL_FLAGS & ITEM_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getCurrentVotes() {
		if (currentVotes == null) {
			currentVotes = new EObjectContainmentEList.Unsettable(Vote.class,
					this, JazzPackage.ITEM_FOR_ESTIMATION__CURRENT_VOTES
							+ EOFFSET_CORRECTION);
		}
		return currentVotes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCurrentVotes() {
		if (currentVotes != null)
			((InternalEList.Unsettable) currentVotes).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCurrentVotes() {
		return currentVotes != null
				&& ((InternalEList.Unsettable) currentVotes).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getVoteHistory() {
		if (voteHistory == null) {
			voteHistory = new EObjectResolvingEList.Unsettable(
					VoteHistoryItem.class, this,
					JazzPackage.ITEM_FOR_ESTIMATION__VOTE_HISTORY
							+ EOFFSET_CORRECTION);
		}
		return voteHistory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVoteHistory() {
		if (voteHistory != null)
			((InternalEList.Unsettable) voteHistory).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVoteHistory() {
		return voteHistory != null
				&& ((InternalEList.Unsettable) voteHistory).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSavedVote() {
		return savedVote;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSavedVote(int newSavedVote) {
		int oldSavedVote = savedVote;
		savedVote = newSavedVote;
		boolean oldSavedVoteESet = (ALL_FLAGS & SAVED_VOTE_ESETFLAG) != 0;
		ALL_FLAGS |= SAVED_VOTE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.ITEM_FOR_ESTIMATION__SAVED_VOTE
							+ EOFFSET_CORRECTION, oldSavedVote, savedVote,
					!oldSavedVoteESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSavedVote() {
		int oldSavedVote = savedVote;
		boolean oldSavedVoteESet = (ALL_FLAGS & SAVED_VOTE_ESETFLAG) != 0;
		savedVote = SAVED_VOTE_EDEFAULT;
		ALL_FLAGS &= ~SAVED_VOTE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.ITEM_FOR_ESTIMATION__SAVED_VOTE
							+ EOFFSET_CORRECTION, oldSavedVote,
					SAVED_VOTE_EDEFAULT, oldSavedVoteESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSavedVote() {
		return (ALL_FLAGS & SAVED_VOTE_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRoundNo() {
		return roundNo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoundNo(int newRoundNo) {
		int oldRoundNo = roundNo;
		roundNo = newRoundNo;
		boolean oldRoundNoESet = (ALL_FLAGS & ROUND_NO_ESETFLAG) != 0;
		ALL_FLAGS |= ROUND_NO_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.ITEM_FOR_ESTIMATION__ROUND_NO
							+ EOFFSET_CORRECTION, oldRoundNo, roundNo,
					!oldRoundNoESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetRoundNo() {
		int oldRoundNo = roundNo;
		boolean oldRoundNoESet = (ALL_FLAGS & ROUND_NO_ESETFLAG) != 0;
		roundNo = ROUND_NO_EDEFAULT;
		ALL_FLAGS &= ~ROUND_NO_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.ITEM_FOR_ESTIMATION__ROUND_NO
							+ EOFFSET_CORRECTION, oldRoundNo,
					ROUND_NO_EDEFAULT, oldRoundNoESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetRoundNo() {
		return (ALL_FLAGS & ROUND_NO_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ITEM_FOR_ESTIMATION__CURRENT_VOTES:
			return ((InternalEList) getCurrentVotes()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ITEM_FOR_ESTIMATION__ITEM:
			if (resolve)
				return getItem();
			return basicGetItem();
		case JazzPackage.ITEM_FOR_ESTIMATION__CURRENT_VOTES:
			return getCurrentVotes();
		case JazzPackage.ITEM_FOR_ESTIMATION__VOTE_HISTORY:
			return getVoteHistory();
		case JazzPackage.ITEM_FOR_ESTIMATION__SAVED_VOTE:
			return new Integer(getSavedVote());
		case JazzPackage.ITEM_FOR_ESTIMATION__ROUND_NO:
			return new Integer(getRoundNo());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ITEM_FOR_ESTIMATION__ITEM:
			setItem((IItemHandle) newValue);
			return;
		case JazzPackage.ITEM_FOR_ESTIMATION__CURRENT_VOTES:
			getCurrentVotes().clear();
			getCurrentVotes().addAll((Collection) newValue);
			return;
		case JazzPackage.ITEM_FOR_ESTIMATION__VOTE_HISTORY:
			getVoteHistory().clear();
			getVoteHistory().addAll((Collection) newValue);
			return;
		case JazzPackage.ITEM_FOR_ESTIMATION__SAVED_VOTE:
			setSavedVote(((Integer) newValue).intValue());
			return;
		case JazzPackage.ITEM_FOR_ESTIMATION__ROUND_NO:
			setRoundNo(((Integer) newValue).intValue());
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ITEM_FOR_ESTIMATION__ITEM:
			unsetItem();
			return;
		case JazzPackage.ITEM_FOR_ESTIMATION__CURRENT_VOTES:
			unsetCurrentVotes();
			return;
		case JazzPackage.ITEM_FOR_ESTIMATION__VOTE_HISTORY:
			unsetVoteHistory();
			return;
		case JazzPackage.ITEM_FOR_ESTIMATION__SAVED_VOTE:
			unsetSavedVote();
			return;
		case JazzPackage.ITEM_FOR_ESTIMATION__ROUND_NO:
			unsetRoundNo();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ITEM_FOR_ESTIMATION__ITEM:
			return isSetItem();
		case JazzPackage.ITEM_FOR_ESTIMATION__CURRENT_VOTES:
			return isSetCurrentVotes();
		case JazzPackage.ITEM_FOR_ESTIMATION__VOTE_HISTORY:
			return isSetVoteHistory();
		case JazzPackage.ITEM_FOR_ESTIMATION__SAVED_VOTE:
			return isSetSavedVote();
		case JazzPackage.ITEM_FOR_ESTIMATION__ROUND_NO:
			return isSetRoundNo();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class baseClass) {
		if (baseClass == ItemForEstimation.class) {
			switch (baseFeatureID - EOFFSET_CORRECTION) {
			case JazzPackage.ITEM_FOR_ESTIMATION__ITEM:
				return JazzPackage.ITEM_FOR_ESTIMATION__ITEM
						+ EOFFSET_CORRECTION;
			case JazzPackage.ITEM_FOR_ESTIMATION__CURRENT_VOTES:
				return JazzPackage.ITEM_FOR_ESTIMATION__CURRENT_VOTES
						+ EOFFSET_CORRECTION;
			case JazzPackage.ITEM_FOR_ESTIMATION__VOTE_HISTORY:
				return JazzPackage.ITEM_FOR_ESTIMATION__VOTE_HISTORY
						+ EOFFSET_CORRECTION;
			case JazzPackage.ITEM_FOR_ESTIMATION__SAVED_VOTE:
				return JazzPackage.ITEM_FOR_ESTIMATION__SAVED_VOTE
						+ EOFFSET_CORRECTION;
			case JazzPackage.ITEM_FOR_ESTIMATION__ROUND_NO:
				return JazzPackage.ITEM_FOR_ESTIMATION__ROUND_NO
						+ EOFFSET_CORRECTION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (savedVote: "); //$NON-NLS-1$
		if ((ALL_FLAGS & SAVED_VOTE_ESETFLAG) != 0)
			result.append(savedVote);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", roundNo: "); //$NON-NLS-1$
		if ((ALL_FLAGS & ROUND_NO_ESETFLAG) != 0)
			result.append(roundNo);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(')');
		return result.toString();
	}

} //ItemForEstimationImpl
