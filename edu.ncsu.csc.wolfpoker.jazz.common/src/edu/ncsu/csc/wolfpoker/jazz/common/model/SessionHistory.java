/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import com.ibm.team.repository.common.IContributorHandle;

import com.ibm.team.repository.common.model.SimpleItem;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Session History</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getDate <em>Date</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getHistoryType <em>History Type</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getUser <em>User</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getOldValue <em>Old Value</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getNewValue <em>New Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSessionHistory()
 * @model
 * @generated
 */
public interface SessionHistory extends SimpleItem, SessionHistoryHandle {
	/**
	 * Returns the value of the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date</em>' attribute.
	 * @see #isSetDate()
	 * @see #unsetDate()
	 * @see #setDate(long)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSessionHistory_Date()
	 * @model unsettable="true"
	 * @generated
	 */
	long getDate();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date</em>' attribute.
	 * @see #isSetDate()
	 * @see #unsetDate()
	 * @see #getDate()
	 * @generated
	 */
	void setDate(long value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getDate <em>Date</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDate()
	 * @see #getDate()
	 * @see #setDate(long)
	 * @generated
	 */
	void unsetDate();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getDate <em>Date</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Date</em>' attribute is set.
	 * @see #unsetDate()
	 * @see #getDate()
	 * @see #setDate(long)
	 * @generated
	 */
	boolean isSetDate();

	/**
	 * Returns the value of the '<em><b>History Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>History Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>History Type</em>' attribute.
	 * @see #isSetHistoryType()
	 * @see #unsetHistoryType()
	 * @see #setHistoryType(String)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSessionHistory_HistoryType()
	 * @model unsettable="true"
	 * @generated
	 */
	String getHistoryType();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getHistoryType <em>History Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>History Type</em>' attribute.
	 * @see #isSetHistoryType()
	 * @see #unsetHistoryType()
	 * @see #getHistoryType()
	 * @generated
	 */
	void setHistoryType(String value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getHistoryType <em>History Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHistoryType()
	 * @see #getHistoryType()
	 * @see #setHistoryType(String)
	 * @generated
	 */
	void unsetHistoryType();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getHistoryType <em>History Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>History Type</em>' attribute is set.
	 * @see #unsetHistoryType()
	 * @see #getHistoryType()
	 * @see #setHistoryType(String)
	 * @generated
	 */
	boolean isSetHistoryType();

	/**
	 * Returns the value of the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' reference.
	 * @see #isSetUser()
	 * @see #unsetUser()
	 * @see #setUser(IContributorHandle)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSessionHistory_User()
	 * @model type="com.ibm.team.repository.common.model.ContributorHandleFacade" unsettable="true"
	 * @generated
	 */
	IContributorHandle getUser();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getUser <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' reference.
	 * @see #isSetUser()
	 * @see #unsetUser()
	 * @see #getUser()
	 * @generated
	 */
	void setUser(IContributorHandle value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getUser <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUser()
	 * @see #getUser()
	 * @see #setUser(IContributorHandle)
	 * @generated
	 */
	void unsetUser();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getUser <em>User</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>User</em>' reference is set.
	 * @see #unsetUser()
	 * @see #getUser()
	 * @see #setUser(IContributorHandle)
	 * @generated
	 */
	boolean isSetUser();

	/**
	 * Returns the value of the '<em><b>Old Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Old Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Old Value</em>' attribute.
	 * @see #isSetOldValue()
	 * @see #unsetOldValue()
	 * @see #setOldValue(String)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSessionHistory_OldValue()
	 * @model unsettable="true"
	 * @generated
	 */
	String getOldValue();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getOldValue <em>Old Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Old Value</em>' attribute.
	 * @see #isSetOldValue()
	 * @see #unsetOldValue()
	 * @see #getOldValue()
	 * @generated
	 */
	void setOldValue(String value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getOldValue <em>Old Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOldValue()
	 * @see #getOldValue()
	 * @see #setOldValue(String)
	 * @generated
	 */
	void unsetOldValue();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getOldValue <em>Old Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Old Value</em>' attribute is set.
	 * @see #unsetOldValue()
	 * @see #getOldValue()
	 * @see #setOldValue(String)
	 * @generated
	 */
	boolean isSetOldValue();

	/**
	 * Returns the value of the '<em><b>New Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>New Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>New Value</em>' attribute.
	 * @see #isSetNewValue()
	 * @see #unsetNewValue()
	 * @see #setNewValue(String)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSessionHistory_NewValue()
	 * @model unsettable="true"
	 * @generated
	 */
	String getNewValue();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getNewValue <em>New Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>New Value</em>' attribute.
	 * @see #isSetNewValue()
	 * @see #unsetNewValue()
	 * @see #getNewValue()
	 * @generated
	 */
	void setNewValue(String value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getNewValue <em>New Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNewValue()
	 * @see #getNewValue()
	 * @see #setNewValue(String)
	 * @generated
	 */
	void unsetNewValue();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getNewValue <em>New Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>New Value</em>' attribute is set.
	 * @see #unsetNewValue()
	 * @see #getNewValue()
	 * @see #setNewValue(String)
	 * @generated
	 */
	boolean isSetNewValue();

} // SessionHistory
