/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import com.ibm.team.repository.common.model.RepositoryPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzFactory
 * @model kind="package"
 *        annotation="teamPackage clientBasePackage='edu.ncsu.csc.wolfpoker' clientPackageSuffix='common.model'"
 * @generated
 */
public interface JazzPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "jazz"; //$NON-NLS-1$

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "edu.ncsu.csc.wolfpoker.jazz"; //$NON-NLS-1$

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "jazz"; //$NON-NLS-1$

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JazzPackage eINSTANCE = edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl <em>Session</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getSession()
	 * @generated
	 */
	int SESSION = 0;

	/**
	 * The feature id for the '<em><b>State Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__STATE_ID = RepositoryPackage.AUDITABLE__STATE_ID;

	/**
	 * The feature id for the '<em><b>Item Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__ITEM_ID = RepositoryPackage.AUDITABLE__ITEM_ID;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__ORIGIN = RepositoryPackage.AUDITABLE__ORIGIN;

	/**
	 * The feature id for the '<em><b>Immutable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__IMMUTABLE = RepositoryPackage.AUDITABLE__IMMUTABLE;

	/**
	 * The feature id for the '<em><b>Context Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__CONTEXT_ID = RepositoryPackage.AUDITABLE__CONTEXT_ID;

	/**
	 * The feature id for the '<em><b>Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__MODIFIED = RepositoryPackage.AUDITABLE__MODIFIED;

	/**
	 * The feature id for the '<em><b>Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__MODIFIED_BY = RepositoryPackage.AUDITABLE__MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Working Copy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__WORKING_COPY = RepositoryPackage.AUDITABLE__WORKING_COPY;

	/**
	 * The feature id for the '<em><b>String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__STRING_EXTENSIONS = RepositoryPackage.AUDITABLE__STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Int Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__INT_EXTENSIONS = RepositoryPackage.AUDITABLE__INT_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Boolean Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__BOOLEAN_EXTENSIONS = RepositoryPackage.AUDITABLE__BOOLEAN_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Timestamp Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__TIMESTAMP_EXTENSIONS = RepositoryPackage.AUDITABLE__TIMESTAMP_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Long Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__LONG_EXTENSIONS = RepositoryPackage.AUDITABLE__LONG_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Large String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__LARGE_STRING_EXTENSIONS = RepositoryPackage.AUDITABLE__LARGE_STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Medium String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__MEDIUM_STRING_EXTENSIONS = RepositoryPackage.AUDITABLE__MEDIUM_STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Big Decimal Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__BIG_DECIMAL_EXTENSIONS = RepositoryPackage.AUDITABLE__BIG_DECIMAL_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Merge Predecessor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__MERGE_PREDECESSOR = RepositoryPackage.AUDITABLE__MERGE_PREDECESSOR;

	/**
	 * The feature id for the '<em><b>Working Copy Predecessor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__WORKING_COPY_PREDECESSOR = RepositoryPackage.AUDITABLE__WORKING_COPY_PREDECESSOR;

	/**
	 * The feature id for the '<em><b>Working Copy Merge Predecessor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__WORKING_COPY_MERGE_PREDECESSOR = RepositoryPackage.AUDITABLE__WORKING_COPY_MERGE_PREDECESSOR;

	/**
	 * The feature id for the '<em><b>Predecessor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__PREDECESSOR = RepositoryPackage.AUDITABLE__PREDECESSOR;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__TEXT = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Participants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__PARTICIPANTS = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Created By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__CREATED_BY = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Session Coordinator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__SESSION_COORDINATOR = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Date Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__DATE_CREATED = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Date Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__DATE_LAST_MODIFIED = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__DESCRIPTION = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Items</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__ITEMS = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Project Area</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__PROJECT_AREA = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Session History</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__SESSION_HISTORY = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__ACTIVE = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Archived</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__ARCHIVED = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Cards Value Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__CARDS_VALUE_LOWER_BOUND = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Cards Value Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION__CARDS_VALUE_UPPER_BOUND = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 13;

	/**
	 * The number of structural features of the '<em>Session</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_FEATURE_COUNT = RepositoryPackage.AUDITABLE_FEATURE_COUNT + 14;

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHandleImpl <em>Session Handle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHandleImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getSessionHandle()
	 * @generated
	 */
	int SESSION_HANDLE = 1;

	/**
	 * The feature id for the '<em><b>State Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HANDLE__STATE_ID = RepositoryPackage.AUDITABLE_HANDLE__STATE_ID;

	/**
	 * The feature id for the '<em><b>Item Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HANDLE__ITEM_ID = RepositoryPackage.AUDITABLE_HANDLE__ITEM_ID;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HANDLE__ORIGIN = RepositoryPackage.AUDITABLE_HANDLE__ORIGIN;

	/**
	 * The feature id for the '<em><b>Immutable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HANDLE__IMMUTABLE = RepositoryPackage.AUDITABLE_HANDLE__IMMUTABLE;

	/**
	 * The number of structural features of the '<em>Session Handle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HANDLE_FEATURE_COUNT = RepositoryPackage.AUDITABLE_HANDLE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl <em>Session History</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getSessionHistory()
	 * @generated
	 */
	int SESSION_HISTORY = 2;

	/**
	 * The feature id for the '<em><b>State Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__STATE_ID = RepositoryPackage.SIMPLE_ITEM__STATE_ID;

	/**
	 * The feature id for the '<em><b>Item Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__ITEM_ID = RepositoryPackage.SIMPLE_ITEM__ITEM_ID;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__ORIGIN = RepositoryPackage.SIMPLE_ITEM__ORIGIN;

	/**
	 * The feature id for the '<em><b>Immutable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__IMMUTABLE = RepositoryPackage.SIMPLE_ITEM__IMMUTABLE;

	/**
	 * The feature id for the '<em><b>Context Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__CONTEXT_ID = RepositoryPackage.SIMPLE_ITEM__CONTEXT_ID;

	/**
	 * The feature id for the '<em><b>Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__MODIFIED = RepositoryPackage.SIMPLE_ITEM__MODIFIED;

	/**
	 * The feature id for the '<em><b>Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__MODIFIED_BY = RepositoryPackage.SIMPLE_ITEM__MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Working Copy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__WORKING_COPY = RepositoryPackage.SIMPLE_ITEM__WORKING_COPY;

	/**
	 * The feature id for the '<em><b>String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__STRING_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Int Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__INT_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__INT_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Boolean Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__BOOLEAN_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__BOOLEAN_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Timestamp Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__TIMESTAMP_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__TIMESTAMP_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Long Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__LONG_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__LONG_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Large String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__LARGE_STRING_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__LARGE_STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Medium String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__MEDIUM_STRING_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__MEDIUM_STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Big Decimal Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__BIG_DECIMAL_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__BIG_DECIMAL_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Predecessor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__PREDECESSOR = RepositoryPackage.SIMPLE_ITEM__PREDECESSOR;

	/**
	 * The feature id for the '<em><b>Date</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__DATE = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>History Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__HISTORY_TYPE = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__USER = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Old Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__OLD_VALUE = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>New Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY__NEW_VALUE = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Session History</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY_FEATURE_COUNT = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryHandleImpl <em>Session History Handle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryHandleImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getSessionHistoryHandle()
	 * @generated
	 */
	int SESSION_HISTORY_HANDLE = 3;

	/**
	 * The feature id for the '<em><b>State Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY_HANDLE__STATE_ID = RepositoryPackage.SIMPLE_ITEM_HANDLE__STATE_ID;

	/**
	 * The feature id for the '<em><b>Item Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY_HANDLE__ITEM_ID = RepositoryPackage.SIMPLE_ITEM_HANDLE__ITEM_ID;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY_HANDLE__ORIGIN = RepositoryPackage.SIMPLE_ITEM_HANDLE__ORIGIN;

	/**
	 * The feature id for the '<em><b>Immutable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY_HANDLE__IMMUTABLE = RepositoryPackage.SIMPLE_ITEM_HANDLE__IMMUTABLE;

	/**
	 * The number of structural features of the '<em>Session History Handle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SESSION_HISTORY_HANDLE_FEATURE_COUNT = RepositoryPackage.SIMPLE_ITEM_HANDLE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl <em>Active Session</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getActiveSession()
	 * @generated
	 */
	int ACTIVE_SESSION = 4;

	/**
	 * The feature id for the '<em><b>State Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__STATE_ID = RepositoryPackage.SIMPLE_ITEM__STATE_ID;

	/**
	 * The feature id for the '<em><b>Item Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__ITEM_ID = RepositoryPackage.SIMPLE_ITEM__ITEM_ID;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__ORIGIN = RepositoryPackage.SIMPLE_ITEM__ORIGIN;

	/**
	 * The feature id for the '<em><b>Immutable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__IMMUTABLE = RepositoryPackage.SIMPLE_ITEM__IMMUTABLE;

	/**
	 * The feature id for the '<em><b>Context Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__CONTEXT_ID = RepositoryPackage.SIMPLE_ITEM__CONTEXT_ID;

	/**
	 * The feature id for the '<em><b>Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__MODIFIED = RepositoryPackage.SIMPLE_ITEM__MODIFIED;

	/**
	 * The feature id for the '<em><b>Modified By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__MODIFIED_BY = RepositoryPackage.SIMPLE_ITEM__MODIFIED_BY;

	/**
	 * The feature id for the '<em><b>Working Copy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__WORKING_COPY = RepositoryPackage.SIMPLE_ITEM__WORKING_COPY;

	/**
	 * The feature id for the '<em><b>String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__STRING_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Int Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__INT_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__INT_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Boolean Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__BOOLEAN_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__BOOLEAN_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Timestamp Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__TIMESTAMP_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__TIMESTAMP_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Long Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__LONG_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__LONG_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Large String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__LARGE_STRING_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__LARGE_STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Medium String Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__MEDIUM_STRING_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__MEDIUM_STRING_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Big Decimal Extensions</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__BIG_DECIMAL_EXTENSIONS = RepositoryPackage.SIMPLE_ITEM__BIG_DECIMAL_EXTENSIONS;

	/**
	 * The feature id for the '<em><b>Predecessor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__PREDECESSOR = RepositoryPackage.SIMPLE_ITEM__PREDECESSOR;

	/**
	 * The feature id for the '<em><b>Associated Session</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__ASSOCIATED_SESSION = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Joined Participants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__JOINED_PARTICIPANTS = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Item Under Discussion</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__ITEM_UNDER_DISCUSSION = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Items Completed</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__ITEMS_COMPLETED = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Last Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION__LAST_OPERATION = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Active Session</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION_FEATURE_COUNT = RepositoryPackage.SIMPLE_ITEM_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionHandleImpl <em>Active Session Handle</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionHandleImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getActiveSessionHandle()
	 * @generated
	 */
	int ACTIVE_SESSION_HANDLE = 5;

	/**
	 * The feature id for the '<em><b>State Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION_HANDLE__STATE_ID = RepositoryPackage.SIMPLE_ITEM_HANDLE__STATE_ID;

	/**
	 * The feature id for the '<em><b>Item Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION_HANDLE__ITEM_ID = RepositoryPackage.SIMPLE_ITEM_HANDLE__ITEM_ID;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION_HANDLE__ORIGIN = RepositoryPackage.SIMPLE_ITEM_HANDLE__ORIGIN;

	/**
	 * The feature id for the '<em><b>Immutable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION_HANDLE__IMMUTABLE = RepositoryPackage.SIMPLE_ITEM_HANDLE__IMMUTABLE;

	/**
	 * The number of structural features of the '<em>Active Session Handle</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTIVE_SESSION_HANDLE_FEATURE_COUNT = RepositoryPackage.SIMPLE_ITEM_HANDLE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteImpl <em>Vote</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getVote()
	 * @generated
	 */
	int VOTE = 6;

	/**
	 * The feature id for the '<em><b>Internal Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE__INTERNAL_ID = RepositoryPackage.HELPER__INTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Participant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE__PARTICIPANT = RepositoryPackage.HELPER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vote</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE__VOTE = RepositoryPackage.HELPER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Vote</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE_FEATURE_COUNT = RepositoryPackage.HELPER_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl <em>Item For Estimation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getItemForEstimation()
	 * @generated
	 */
	int ITEM_FOR_ESTIMATION = 7;

	/**
	 * The feature id for the '<em><b>Internal Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_FOR_ESTIMATION__INTERNAL_ID = RepositoryPackage.HELPER__INTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_FOR_ESTIMATION__ITEM = RepositoryPackage.HELPER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Current Votes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_FOR_ESTIMATION__CURRENT_VOTES = RepositoryPackage.HELPER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Vote History</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_FOR_ESTIMATION__VOTE_HISTORY = RepositoryPackage.HELPER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Saved Vote</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_FOR_ESTIMATION__SAVED_VOTE = RepositoryPackage.HELPER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Round No</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_FOR_ESTIMATION__ROUND_NO = RepositoryPackage.HELPER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Item For Estimation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITEM_FOR_ESTIMATION_FEATURE_COUNT = RepositoryPackage.HELPER_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteHistoryItemImpl <em>Vote History Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteHistoryItemImpl
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getVoteHistoryItem()
	 * @generated
	 */
	int VOTE_HISTORY_ITEM = 8;

	/**
	 * The feature id for the '<em><b>Internal Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE_HISTORY_ITEM__INTERNAL_ID = RepositoryPackage.HELPER__INTERNAL_ID;

	/**
	 * The feature id for the '<em><b>Session</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE_HISTORY_ITEM__SESSION = RepositoryPackage.HELPER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Votes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE_HISTORY_ITEM__VOTES = RepositoryPackage.HELPER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Vote Round</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE_HISTORY_ITEM__VOTE_ROUND = RepositoryPackage.HELPER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Vote History Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VOTE_HISTORY_ITEM_FEATURE_COUNT = RepositoryPackage.HELPER_FEATURE_COUNT + 3;

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session <em>Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session
	 * @generated
	 */
	EClass getSession();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getText()
	 * @see #getSession()
	 * @generated
	 */
	EAttribute getSession_Text();

	/**
	 * Returns the meta object for the reference list '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getParticipants <em>Participants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Participants</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getParticipants()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_Participants();

	/**
	 * Returns the meta object for the reference '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCreatedBy <em>Created By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Created By</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCreatedBy()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_CreatedBy();

	/**
	 * Returns the meta object for the reference '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionCoordinator <em>Session Coordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Session Coordinator</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionCoordinator()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_SessionCoordinator();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateCreated <em>Date Created</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date Created</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateCreated()
	 * @see #getSession()
	 * @generated
	 */
	EAttribute getSession_DateCreated();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateLastModified <em>Date Last Modified</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date Last Modified</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateLastModified()
	 * @see #getSession()
	 * @generated
	 */
	EAttribute getSession_DateLastModified();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDescription()
	 * @see #getSession()
	 * @generated
	 */
	EAttribute getSession_Description();

	/**
	 * Returns the meta object for the reference list '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getItems <em>Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Items</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getItems()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_Items();

	/**
	 * Returns the meta object for the reference '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getProjectArea <em>Project Area</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Project Area</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getProjectArea()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_ProjectArea();

	/**
	 * Returns the meta object for the reference list '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionHistory <em>Session History</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Session History</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionHistory()
	 * @see #getSession()
	 * @generated
	 */
	EReference getSession_SessionHistory();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isActive <em>Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Active</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isActive()
	 * @see #getSession()
	 * @generated
	 */
	EAttribute getSession_Active();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isArchived <em>Archived</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Archived</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isArchived()
	 * @see #getSession()
	 * @generated
	 */
	EAttribute getSession_Archived();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueLowerBound <em>Cards Value Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cards Value Lower Bound</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueLowerBound()
	 * @see #getSession()
	 * @generated
	 */
	EAttribute getSession_CardsValueLowerBound();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueUpperBound <em>Cards Value Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cards Value Upper Bound</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueUpperBound()
	 * @see #getSession()
	 * @generated
	 */
	EAttribute getSession_CardsValueUpperBound();

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHandle <em>Session Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session Handle</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHandle
	 * @generated
	 */
	EClass getSessionHandle();

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory <em>Session History</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session History</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory
	 * @generated
	 */
	EClass getSessionHistory();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getDate <em>Date</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getDate()
	 * @see #getSessionHistory()
	 * @generated
	 */
	EAttribute getSessionHistory_Date();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getHistoryType <em>History Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>History Type</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getHistoryType()
	 * @see #getSessionHistory()
	 * @generated
	 */
	EAttribute getSessionHistory_HistoryType();

	/**
	 * Returns the meta object for the reference '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getUser()
	 * @see #getSessionHistory()
	 * @generated
	 */
	EReference getSessionHistory_User();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getOldValue <em>Old Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Old Value</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getOldValue()
	 * @see #getSessionHistory()
	 * @generated
	 */
	EAttribute getSessionHistory_OldValue();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getNewValue <em>New Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>New Value</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory#getNewValue()
	 * @see #getSessionHistory()
	 * @generated
	 */
	EAttribute getSessionHistory_NewValue();

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle <em>Session History Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Session History Handle</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle
	 * @generated
	 */
	EClass getSessionHistoryHandle();

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession <em>Active Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Session</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession
	 * @generated
	 */
	EClass getActiveSession();

	/**
	 * Returns the meta object for the reference '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getAssociatedSession <em>Associated Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Associated Session</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getAssociatedSession()
	 * @see #getActiveSession()
	 * @generated
	 */
	EReference getActiveSession_AssociatedSession();

	/**
	 * Returns the meta object for the reference list '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getJoinedParticipants <em>Joined Participants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Joined Participants</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getJoinedParticipants()
	 * @see #getActiveSession()
	 * @generated
	 */
	EReference getActiveSession_JoinedParticipants();

	/**
	 * Returns the meta object for the reference '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemUnderDiscussion <em>Item Under Discussion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Item Under Discussion</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemUnderDiscussion()
	 * @see #getActiveSession()
	 * @generated
	 */
	EReference getActiveSession_ItemUnderDiscussion();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemsCompleted <em>Items Completed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Items Completed</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getItemsCompleted()
	 * @see #getActiveSession()
	 * @generated
	 */
	EReference getActiveSession_ItemsCompleted();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getLastOperation <em>Last Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Operation</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession#getLastOperation()
	 * @see #getActiveSession()
	 * @generated
	 */
	EAttribute getActiveSession_LastOperation();

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSessionHandle <em>Active Session Handle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Active Session Handle</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSessionHandle
	 * @generated
	 */
	EClass getActiveSessionHandle();

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote <em>Vote</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vote</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Vote
	 * @generated
	 */
	EClass getVote();

	/**
	 * Returns the meta object for the reference '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getParticipant <em>Participant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Participant</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getParticipant()
	 * @see #getVote()
	 * @generated
	 */
	EReference getVote_Participant();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getVote <em>Vote</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vote</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.Vote#getVote()
	 * @see #getVote()
	 * @generated
	 */
	EAttribute getVote_Vote();

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation <em>Item For Estimation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Item For Estimation</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation
	 * @generated
	 */
	EClass getItemForEstimation();

	/**
	 * Returns the meta object for the reference '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getItem <em>Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Item</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getItem()
	 * @see #getItemForEstimation()
	 * @generated
	 */
	EReference getItemForEstimation_Item();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getCurrentVotes <em>Current Votes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Current Votes</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getCurrentVotes()
	 * @see #getItemForEstimation()
	 * @generated
	 */
	EReference getItemForEstimation_CurrentVotes();

	/**
	 * Returns the meta object for the reference list '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getVoteHistory <em>Vote History</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Vote History</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getVoteHistory()
	 * @see #getItemForEstimation()
	 * @generated
	 */
	EReference getItemForEstimation_VoteHistory();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getSavedVote <em>Saved Vote</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Saved Vote</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getSavedVote()
	 * @see #getItemForEstimation()
	 * @generated
	 */
	EAttribute getItemForEstimation_SavedVote();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getRoundNo <em>Round No</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Round No</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getRoundNo()
	 * @see #getItemForEstimation()
	 * @generated
	 */
	EAttribute getItemForEstimation_RoundNo();

	/**
	 * Returns the meta object for class '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem <em>Vote History Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vote History Item</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem
	 * @generated
	 */
	EClass getVoteHistoryItem();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getSession <em>Session</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Session</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getSession()
	 * @see #getVoteHistoryItem()
	 * @generated
	 */
	EAttribute getVoteHistoryItem_Session();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVotes <em>Votes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Votes</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVotes()
	 * @see #getVoteHistoryItem()
	 * @generated
	 */
	EReference getVoteHistoryItem_Votes();

	/**
	 * Returns the meta object for the attribute '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVoteRound <em>Vote Round</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vote Round</em>'.
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVoteRound()
	 * @see #getVoteHistoryItem()
	 * @generated
	 */
	EAttribute getVoteHistoryItem_VoteRound();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JazzFactory getJazzFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl <em>Session</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getSession()
		 * @generated
		 */
		EClass SESSION = eINSTANCE.getSession();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION__TEXT = eINSTANCE.getSession_Text();

		/**
		 * The meta object literal for the '<em><b>Participants</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__PARTICIPANTS = eINSTANCE.getSession_Participants();

		/**
		 * The meta object literal for the '<em><b>Created By</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__CREATED_BY = eINSTANCE.getSession_CreatedBy();

		/**
		 * The meta object literal for the '<em><b>Session Coordinator</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__SESSION_COORDINATOR = eINSTANCE
				.getSession_SessionCoordinator();

		/**
		 * The meta object literal for the '<em><b>Date Created</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION__DATE_CREATED = eINSTANCE.getSession_DateCreated();

		/**
		 * The meta object literal for the '<em><b>Date Last Modified</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION__DATE_LAST_MODIFIED = eINSTANCE
				.getSession_DateLastModified();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION__DESCRIPTION = eINSTANCE.getSession_Description();

		/**
		 * The meta object literal for the '<em><b>Items</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__ITEMS = eINSTANCE.getSession_Items();

		/**
		 * The meta object literal for the '<em><b>Project Area</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__PROJECT_AREA = eINSTANCE.getSession_ProjectArea();

		/**
		 * The meta object literal for the '<em><b>Session History</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION__SESSION_HISTORY = eINSTANCE
				.getSession_SessionHistory();

		/**
		 * The meta object literal for the '<em><b>Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION__ACTIVE = eINSTANCE.getSession_Active();

		/**
		 * The meta object literal for the '<em><b>Archived</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION__ARCHIVED = eINSTANCE.getSession_Archived();

		/**
		 * The meta object literal for the '<em><b>Cards Value Lower Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION__CARDS_VALUE_LOWER_BOUND = eINSTANCE
				.getSession_CardsValueLowerBound();

		/**
		 * The meta object literal for the '<em><b>Cards Value Upper Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION__CARDS_VALUE_UPPER_BOUND = eINSTANCE
				.getSession_CardsValueUpperBound();

		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHandleImpl <em>Session Handle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHandleImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getSessionHandle()
		 * @generated
		 */
		EClass SESSION_HANDLE = eINSTANCE.getSessionHandle();

		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl <em>Session History</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getSessionHistory()
		 * @generated
		 */
		EClass SESSION_HISTORY = eINSTANCE.getSessionHistory();

		/**
		 * The meta object literal for the '<em><b>Date</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_HISTORY__DATE = eINSTANCE.getSessionHistory_Date();

		/**
		 * The meta object literal for the '<em><b>History Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_HISTORY__HISTORY_TYPE = eINSTANCE
				.getSessionHistory_HistoryType();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SESSION_HISTORY__USER = eINSTANCE.getSessionHistory_User();

		/**
		 * The meta object literal for the '<em><b>Old Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_HISTORY__OLD_VALUE = eINSTANCE
				.getSessionHistory_OldValue();

		/**
		 * The meta object literal for the '<em><b>New Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SESSION_HISTORY__NEW_VALUE = eINSTANCE
				.getSessionHistory_NewValue();

		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryHandleImpl <em>Session History Handle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionHistoryHandleImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getSessionHistoryHandle()
		 * @generated
		 */
		EClass SESSION_HISTORY_HANDLE = eINSTANCE.getSessionHistoryHandle();

		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl <em>Active Session</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getActiveSession()
		 * @generated
		 */
		EClass ACTIVE_SESSION = eINSTANCE.getActiveSession();

		/**
		 * The meta object literal for the '<em><b>Associated Session</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SESSION__ASSOCIATED_SESSION = eINSTANCE
				.getActiveSession_AssociatedSession();

		/**
		 * The meta object literal for the '<em><b>Joined Participants</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SESSION__JOINED_PARTICIPANTS = eINSTANCE
				.getActiveSession_JoinedParticipants();

		/**
		 * The meta object literal for the '<em><b>Item Under Discussion</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SESSION__ITEM_UNDER_DISCUSSION = eINSTANCE
				.getActiveSession_ItemUnderDiscussion();

		/**
		 * The meta object literal for the '<em><b>Items Completed</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTIVE_SESSION__ITEMS_COMPLETED = eINSTANCE
				.getActiveSession_ItemsCompleted();

		/**
		 * The meta object literal for the '<em><b>Last Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTIVE_SESSION__LAST_OPERATION = eINSTANCE
				.getActiveSession_LastOperation();

		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionHandleImpl <em>Active Session Handle</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionHandleImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getActiveSessionHandle()
		 * @generated
		 */
		EClass ACTIVE_SESSION_HANDLE = eINSTANCE.getActiveSessionHandle();

		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteImpl <em>Vote</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getVote()
		 * @generated
		 */
		EClass VOTE = eINSTANCE.getVote();

		/**
		 * The meta object literal for the '<em><b>Participant</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VOTE__PARTICIPANT = eINSTANCE.getVote_Participant();

		/**
		 * The meta object literal for the '<em><b>Vote</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VOTE__VOTE = eINSTANCE.getVote_Vote();

		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl <em>Item For Estimation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ItemForEstimationImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getItemForEstimation()
		 * @generated
		 */
		EClass ITEM_FOR_ESTIMATION = eINSTANCE.getItemForEstimation();

		/**
		 * The meta object literal for the '<em><b>Item</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEM_FOR_ESTIMATION__ITEM = eINSTANCE
				.getItemForEstimation_Item();

		/**
		 * The meta object literal for the '<em><b>Current Votes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEM_FOR_ESTIMATION__CURRENT_VOTES = eINSTANCE
				.getItemForEstimation_CurrentVotes();

		/**
		 * The meta object literal for the '<em><b>Vote History</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITEM_FOR_ESTIMATION__VOTE_HISTORY = eINSTANCE
				.getItemForEstimation_VoteHistory();

		/**
		 * The meta object literal for the '<em><b>Saved Vote</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ITEM_FOR_ESTIMATION__SAVED_VOTE = eINSTANCE
				.getItemForEstimation_SavedVote();

		/**
		 * The meta object literal for the '<em><b>Round No</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ITEM_FOR_ESTIMATION__ROUND_NO = eINSTANCE
				.getItemForEstimation_RoundNo();

		/**
		 * The meta object literal for the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteHistoryItemImpl <em>Vote History Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteHistoryItemImpl
		 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.impl.JazzPackageImpl#getVoteHistoryItem()
		 * @generated
		 */
		EClass VOTE_HISTORY_ITEM = eINSTANCE.getVoteHistoryItem();

		/**
		 * The meta object literal for the '<em><b>Session</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VOTE_HISTORY_ITEM__SESSION = eINSTANCE
				.getVoteHistoryItem_Session();

		/**
		 * The meta object literal for the '<em><b>Votes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VOTE_HISTORY_ITEM__VOTES = eINSTANCE
				.getVoteHistoryItem_Votes();

		/**
		 * The meta object literal for the '<em><b>Vote Round</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VOTE_HISTORY_ITEM__VOTE_ROUND = eINSTANCE
				.getVoteHistoryItem_VoteRound();

	}

} //JazzPackage
