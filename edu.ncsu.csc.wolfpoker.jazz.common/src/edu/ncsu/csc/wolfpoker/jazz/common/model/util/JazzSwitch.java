/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.util;

import com.ibm.team.repository.common.IAuditable;
import com.ibm.team.repository.common.IAuditableHandle;
import com.ibm.team.repository.common.IHelper;
import com.ibm.team.repository.common.IItem;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.repository.common.IManagedItem;
import com.ibm.team.repository.common.IManagedItemHandle;
import com.ibm.team.repository.common.ISimpleItem;
import com.ibm.team.repository.common.ISimpleItemHandle;

import com.ibm.team.repository.common.model.Auditable;
import com.ibm.team.repository.common.model.AuditableHandle;
import com.ibm.team.repository.common.model.Helper;
import com.ibm.team.repository.common.model.Item;
import com.ibm.team.repository.common.model.ItemHandle;
import com.ibm.team.repository.common.model.ManagedItem;
import com.ibm.team.repository.common.model.ManagedItemHandle;
import com.ibm.team.repository.common.model.SimpleItem;
import com.ibm.team.repository.common.model.SimpleItemHandle;

import edu.ncsu.csc.wolfpoker.jazz.common.model.*;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage
 * @generated
 */
public class JazzSwitch {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static JazzPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JazzSwitch() {
		if (modelPackage == null) {
			modelPackage = JazzPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public Object doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		} else {
			List eSuperTypes = theEClass.getESuperTypes();
			return eSuperTypes.isEmpty() ? defaultCase(theEObject) : doSwitch(
					(EClass) eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected Object doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case JazzPackage.SESSION: {
			Session session = (Session) theEObject;
			Object result = caseSession(session);
			if (result == null)
				result = caseAuditable(session);
			if (result == null)
				result = caseSessionHandle(session);
			if (result == null)
				result = caseManagedItem(session);
			if (result == null)
				result = caseAuditableHandle(session);
			if (result == null)
				result = caseAuditableFacade(session);
			if (result == null)
				result = caseItem(session);
			if (result == null)
				result = caseManagedItemHandle(session);
			if (result == null)
				result = caseManagedItemFacade(session);
			if (result == null)
				result = caseAuditableHandleFacade(session);
			if (result == null)
				result = caseItemHandle(session);
			if (result == null)
				result = caseItemFacade(session);
			if (result == null)
				result = caseManagedItemHandleFacade(session);
			if (result == null)
				result = caseItemHandleFacade(session);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case JazzPackage.SESSION_HANDLE: {
			SessionHandle sessionHandle = (SessionHandle) theEObject;
			Object result = caseSessionHandle(sessionHandle);
			if (result == null)
				result = caseAuditableHandle(sessionHandle);
			if (result == null)
				result = caseManagedItemHandle(sessionHandle);
			if (result == null)
				result = caseAuditableHandleFacade(sessionHandle);
			if (result == null)
				result = caseItemHandle(sessionHandle);
			if (result == null)
				result = caseManagedItemHandleFacade(sessionHandle);
			if (result == null)
				result = caseItemHandleFacade(sessionHandle);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case JazzPackage.SESSION_HISTORY: {
			SessionHistory sessionHistory = (SessionHistory) theEObject;
			Object result = caseSessionHistory(sessionHistory);
			if (result == null)
				result = caseSimpleItem(sessionHistory);
			if (result == null)
				result = caseSessionHistoryHandle(sessionHistory);
			if (result == null)
				result = caseManagedItem(sessionHistory);
			if (result == null)
				result = caseSimpleItemHandle(sessionHistory);
			if (result == null)
				result = caseSimpleItemFacade(sessionHistory);
			if (result == null)
				result = caseItem(sessionHistory);
			if (result == null)
				result = caseManagedItemHandle(sessionHistory);
			if (result == null)
				result = caseManagedItemFacade(sessionHistory);
			if (result == null)
				result = caseSimpleItemHandleFacade(sessionHistory);
			if (result == null)
				result = caseItemHandle(sessionHistory);
			if (result == null)
				result = caseItemFacade(sessionHistory);
			if (result == null)
				result = caseManagedItemHandleFacade(sessionHistory);
			if (result == null)
				result = caseItemHandleFacade(sessionHistory);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case JazzPackage.SESSION_HISTORY_HANDLE: {
			SessionHistoryHandle sessionHistoryHandle = (SessionHistoryHandle) theEObject;
			Object result = caseSessionHistoryHandle(sessionHistoryHandle);
			if (result == null)
				result = caseSimpleItemHandle(sessionHistoryHandle);
			if (result == null)
				result = caseManagedItemHandle(sessionHistoryHandle);
			if (result == null)
				result = caseSimpleItemHandleFacade(sessionHistoryHandle);
			if (result == null)
				result = caseItemHandle(sessionHistoryHandle);
			if (result == null)
				result = caseManagedItemHandleFacade(sessionHistoryHandle);
			if (result == null)
				result = caseItemHandleFacade(sessionHistoryHandle);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case JazzPackage.ACTIVE_SESSION: {
			ActiveSession activeSession = (ActiveSession) theEObject;
			Object result = caseActiveSession(activeSession);
			if (result == null)
				result = caseSimpleItem(activeSession);
			if (result == null)
				result = caseActiveSessionHandle(activeSession);
			if (result == null)
				result = caseManagedItem(activeSession);
			if (result == null)
				result = caseSimpleItemHandle(activeSession);
			if (result == null)
				result = caseSimpleItemFacade(activeSession);
			if (result == null)
				result = caseItem(activeSession);
			if (result == null)
				result = caseManagedItemHandle(activeSession);
			if (result == null)
				result = caseManagedItemFacade(activeSession);
			if (result == null)
				result = caseSimpleItemHandleFacade(activeSession);
			if (result == null)
				result = caseItemHandle(activeSession);
			if (result == null)
				result = caseItemFacade(activeSession);
			if (result == null)
				result = caseManagedItemHandleFacade(activeSession);
			if (result == null)
				result = caseItemHandleFacade(activeSession);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case JazzPackage.ACTIVE_SESSION_HANDLE: {
			ActiveSessionHandle activeSessionHandle = (ActiveSessionHandle) theEObject;
			Object result = caseActiveSessionHandle(activeSessionHandle);
			if (result == null)
				result = caseSimpleItemHandle(activeSessionHandle);
			if (result == null)
				result = caseManagedItemHandle(activeSessionHandle);
			if (result == null)
				result = caseSimpleItemHandleFacade(activeSessionHandle);
			if (result == null)
				result = caseItemHandle(activeSessionHandle);
			if (result == null)
				result = caseManagedItemHandleFacade(activeSessionHandle);
			if (result == null)
				result = caseItemHandleFacade(activeSessionHandle);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case JazzPackage.VOTE: {
			Vote vote = (Vote) theEObject;
			Object result = caseVote(vote);
			if (result == null)
				result = caseHelper(vote);
			if (result == null)
				result = caseHelperFacade(vote);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case JazzPackage.ITEM_FOR_ESTIMATION: {
			ItemForEstimation itemForEstimation = (ItemForEstimation) theEObject;
			Object result = caseItemForEstimation(itemForEstimation);
			if (result == null)
				result = caseHelper(itemForEstimation);
			if (result == null)
				result = caseHelperFacade(itemForEstimation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case JazzPackage.VOTE_HISTORY_ITEM: {
			VoteHistoryItem voteHistoryItem = (VoteHistoryItem) theEObject;
			Object result = caseVoteHistoryItem(voteHistoryItem);
			if (result == null)
				result = caseHelper(voteHistoryItem);
			if (result == null)
				result = caseHelperFacade(voteHistoryItem);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSession(Session object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session Handle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session Handle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSessionHandle(SessionHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session History</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session History</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSessionHistory(SessionHistory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Session History Handle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Session History Handle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSessionHistoryHandle(SessionHistoryHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Session</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Session</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseActiveSession(ActiveSession object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Active Session Handle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Active Session Handle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseActiveSessionHandle(ActiveSessionHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vote</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vote</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseVote(Vote object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Item For Estimation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Item For Estimation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseItemForEstimation(ItemForEstimation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vote History Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vote History Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseVoteHistoryItem(VoteHistoryItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Item Handle Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Item Handle Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseItemHandleFacade(IItemHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Item Handle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Item Handle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseItemHandle(ItemHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Item Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Item Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseItemFacade(IItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseItem(Item object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Managed Item Handle Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Managed Item Handle Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseManagedItemHandleFacade(IManagedItemHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Managed Item Handle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Managed Item Handle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseManagedItemHandle(ManagedItemHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Managed Item Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Managed Item Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseManagedItemFacade(IManagedItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Managed Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Managed Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseManagedItem(ManagedItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Auditable Handle Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Auditable Handle Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAuditableHandleFacade(IAuditableHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Auditable Handle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Auditable Handle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAuditableHandle(AuditableHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Auditable Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Auditable Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAuditableFacade(IAuditable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Auditable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Auditable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseAuditable(Auditable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Item Handle Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Item Handle Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSimpleItemHandleFacade(ISimpleItemHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Item Handle</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Item Handle</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSimpleItemHandle(SimpleItemHandle object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Item Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Item Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSimpleItemFacade(ISimpleItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Simple Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Simple Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseSimpleItem(SimpleItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Helper Facade</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Helper Facade</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseHelperFacade(IHelper object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Helper</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Helper</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public Object caseHelper(Helper object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public Object defaultCase(EObject object) {
		return null;
	}

} //JazzSwitch
