package edu.ncsu.csc.wolfpoker.jazz.common.model.query.impl;

import com.ibm.team.repository.common.internal.querypath.AbstractQueryPathModel;
import com.ibm.team.repository.common.internal.querypath.IQueryPath;
import com.ibm.team.repository.common.internal.querypath.StringField;
import com.ibm.team.repository.common.model.query.impl.AuditableQueryModelImpl;
import com.ibm.team.repository.common.model.query.impl.ContributorQueryModelImpl;
import com.ibm.team.repository.common.model.query.impl.ItemHandleQueryModelImpl;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseSessionQueryModel.ManySessionQueryModel;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseSessionQueryModel.SessionQueryModel;
import java.util.List;
import java.util.Map;

/**
 * @generated
 */
public class SessionQueryModelImpl extends AuditableQueryModelImpl implements ManySessionQueryModel, SessionQueryModel {

	/** @generated */
	private StringField text;
	/** @generated */
	private ContributorQueryModelImpl createdBy;
	/** @generated */
	private ContributorQueryModelImpl sessionCoordinator;
	/** @generated */
	private StringField description;
	/** @generated */
	private ItemHandleQueryModelImpl items;
	/** @generated */
	private ItemHandleQueryModelImpl projectArea;

 	/**
     * @param anExtent the parent query element
     * @param aField the name of the parent's feature that this model represents
	 * @generated
	 */
	public SessionQueryModelImpl(IQueryPath anExtent, String aField) {
		super(anExtent,  aField);
		_implementation.setItemType("Session", "edu.ncsu.csc.wolfpoker.jazz"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
 	 * @generated
	 */
	public StringField text() {
		return text;
	}

	/**
 	 * @generated
	 */
	public ContributorQueryModelImpl createdBy() {	
		synchronized (this) {
			if (createdBy == null) { 
				createdBy = new ContributorQueryModelImpl(_implementation, "createdBy"); //$NON-NLS-1$	
			}				
			return createdBy;	
		}
	}

	/**
 	 * @generated
	 */
	public ContributorQueryModelImpl sessionCoordinator() {	
		synchronized (this) {
			if (sessionCoordinator == null) { 
				sessionCoordinator = new ContributorQueryModelImpl(_implementation, "sessionCoordinator"); //$NON-NLS-1$	
			}				
			return sessionCoordinator;	
		}
	}

	/**
 	 * @generated
	 */
	public StringField description() {
		return description;
	}

	/**
 	 * @generated
	 */
	public ItemHandleQueryModelImpl items() {	
		synchronized (this) {
			if (items == null) { 
				items = new ItemHandleQueryModelImpl(_implementation, "items"); //$NON-NLS-1$
				getImplementation(items).setSingleValueRef(false);	
			}				
			return items;	
		}
	}

	/**
 	 * @generated
	 */
	public ItemHandleQueryModelImpl projectArea() {	
		synchronized (this) {
			if (projectArea == null) { 
				projectArea = new ItemHandleQueryModelImpl(_implementation, "projectArea"); //$NON-NLS-1$	
			}				
			return projectArea;	
		}
	}

	/**
	 * @generated
	 * @Override
	 */
	protected void initProperties(List allFieldNames, List allReferenceNames, Map allFields) {
		super.initProperties(allFieldNames, allReferenceNames, allFields);

		text = new StringField(_implementation, "text"); //$NON-NLS-1$
		allFieldNames.add("text"); //$NON-NLS-1$
		allFields.put("text", text); //$NON-NLS-1$           
		allReferenceNames.add("createdBy"); //$NON-NLS-1$           
		allReferenceNames.add("sessionCoordinator"); //$NON-NLS-1$
		description = new StringField(_implementation, "description"); //$NON-NLS-1$
		allFieldNames.add("description"); //$NON-NLS-1$
		allFields.put("description", description); //$NON-NLS-1$           
		allReferenceNames.add("items"); //$NON-NLS-1$           
		allReferenceNames.add("projectArea"); //$NON-NLS-1$
	}	

	/**
	 * @generated
	 * @Override
	 */
	protected AbstractQueryPathModel getReference(String name) { 
		if ("createdBy".equals(name)) return createdBy(); //$NON-NLS-1$ 
		if ("sessionCoordinator".equals(name)) return sessionCoordinator(); //$NON-NLS-1$ 
		if ("items".equals(name)) return items(); //$NON-NLS-1$ 
		if ("projectArea".equals(name)) return projectArea(); //$NON-NLS-1$
		return super.getReference(name);
	}

}