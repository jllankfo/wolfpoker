/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import com.ibm.team.repository.common.IContributorHandle;

import com.ibm.team.repository.common.model.impl.HelperImpl;

import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Vote;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vote</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteImpl#getParticipant <em>Participant</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.VoteImpl#getVote <em>Vote</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VoteImpl extends HelperImpl implements Vote {
	/**
	 * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected int ALL_FLAGS = 0;

	/**
	 * The cached value of the '{@link #getParticipant() <em>Participant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipant()
	 * @generated
	 * @ordered
	 */
	protected IContributorHandle participant;

	/**
	 * The flag representing whether the Participant reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int PARTICIPANT_ESETFLAG = 1 << 1;

	/**
	 * The default value of the '{@link #getVote() <em>Vote</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVote()
	 * @generated
	 * @ordered
	 */
	protected static final int VOTE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getVote() <em>Vote</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVote()
	 * @generated
	 * @ordered
	 */
	protected int vote = VOTE_EDEFAULT;

	/**
	 * The flag representing whether the Vote attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int VOTE_ESETFLAG = 1 << 2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int EOFFSET_CORRECTION = JazzPackage.Literals.VOTE
			.getFeatureID(JazzPackage.Literals.VOTE__PARTICIPANT)
			- JazzPackage.VOTE__PARTICIPANT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VoteImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return JazzPackage.Literals.VOTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IContributorHandle getParticipant() {
		if (participant != null && ((EObject) participant).eIsProxy()) {
			InternalEObject oldParticipant = (InternalEObject) participant;
			participant = (IContributorHandle) eResolveProxy(oldParticipant);
			if (participant != oldParticipant) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JazzPackage.VOTE__PARTICIPANT + EOFFSET_CORRECTION,
							oldParticipant, participant));
			}
		}
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IContributorHandle basicGetParticipant() {
		return participant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParticipant(IContributorHandle newParticipant) {
		IContributorHandle oldParticipant = participant;
		participant = newParticipant;
		boolean oldParticipantESet = (ALL_FLAGS & PARTICIPANT_ESETFLAG) != 0;
		ALL_FLAGS |= PARTICIPANT_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.VOTE__PARTICIPANT + EOFFSET_CORRECTION,
					oldParticipant, participant, !oldParticipantESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetParticipant() {
		IContributorHandle oldParticipant = participant;
		boolean oldParticipantESet = (ALL_FLAGS & PARTICIPANT_ESETFLAG) != 0;
		participant = null;
		ALL_FLAGS &= ~PARTICIPANT_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.VOTE__PARTICIPANT + EOFFSET_CORRECTION,
					oldParticipant, null, oldParticipantESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetParticipant() {
		return (ALL_FLAGS & PARTICIPANT_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getVote() {
		return vote;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVote(int newVote) {
		int oldVote = vote;
		vote = newVote;
		boolean oldVoteESet = (ALL_FLAGS & VOTE_ESETFLAG) != 0;
		ALL_FLAGS |= VOTE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.VOTE__VOTE + EOFFSET_CORRECTION, oldVote, vote,
					!oldVoteESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetVote() {
		int oldVote = vote;
		boolean oldVoteESet = (ALL_FLAGS & VOTE_ESETFLAG) != 0;
		vote = VOTE_EDEFAULT;
		ALL_FLAGS &= ~VOTE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.VOTE__VOTE + EOFFSET_CORRECTION, oldVote,
					VOTE_EDEFAULT, oldVoteESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetVote() {
		return (ALL_FLAGS & VOTE_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE__PARTICIPANT:
			if (resolve)
				return getParticipant();
			return basicGetParticipant();
		case JazzPackage.VOTE__VOTE:
			return new Integer(getVote());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE__PARTICIPANT:
			setParticipant((IContributorHandle) newValue);
			return;
		case JazzPackage.VOTE__VOTE:
			setVote(((Integer) newValue).intValue());
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE__PARTICIPANT:
			unsetParticipant();
			return;
		case JazzPackage.VOTE__VOTE:
			unsetVote();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.VOTE__PARTICIPANT:
			return isSetParticipant();
		case JazzPackage.VOTE__VOTE:
			return isSetVote();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class baseClass) {
		if (baseClass == Vote.class) {
			switch (baseFeatureID - EOFFSET_CORRECTION) {
			case JazzPackage.VOTE__PARTICIPANT:
				return JazzPackage.VOTE__PARTICIPANT + EOFFSET_CORRECTION;
			case JazzPackage.VOTE__VOTE:
				return JazzPackage.VOTE__VOTE + EOFFSET_CORRECTION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (vote: "); //$NON-NLS-1$
		if ((ALL_FLAGS & VOTE_ESETFLAG) != 0)
			result.append(vote);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(')');
		return result.toString();
	}

} //VoteImpl
