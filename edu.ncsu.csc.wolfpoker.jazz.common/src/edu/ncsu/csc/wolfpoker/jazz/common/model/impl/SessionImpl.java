/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.repository.common.model.impl.AuditableImpl;

import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHandle;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Session</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getText <em>Text</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getParticipants <em>Participants</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getCreatedBy <em>Created By</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getSessionCoordinator <em>Session Coordinator</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getDateCreated <em>Date Created</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getDateLastModified <em>Date Last Modified</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getItems <em>Items</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getProjectArea <em>Project Area</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getSessionHistory <em>Session History</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#isActive <em>Active</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#isArchived <em>Archived</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getCardsValueLowerBound <em>Cards Value Lower Bound</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.SessionImpl#getCardsValueUpperBound <em>Cards Value Upper Bound</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SessionImpl extends AuditableImpl implements Session {
	/**
	 * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected int ALL_FLAGS = 0;

	/**
	 * The default value of the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
	protected static final String TEXT_EDEFAULT = "yadda"; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getText() <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getText()
	 * @generated
	 * @ordered
	 */
	protected String text = TEXT_EDEFAULT;

	/**
	 * The flag representing whether the Text attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int TEXT_ESETFLAG = 1 << 14;

	/**
	 * The cached value of the '{@link #getParticipants() <em>Participants</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParticipants()
	 * @generated
	 * @ordered
	 */
	protected EList<IContributor> participants;

	/**
	 * The cached value of the '{@link #getCreatedBy() <em>Created By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreatedBy()
	 * @generated
	 * @ordered
	 */
	protected IContributorHandle createdBy;

	/**
	 * The flag representing whether the Created By reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int CREATED_BY_ESETFLAG = 1 << 15;

	/**
	 * The cached value of the '{@link #getSessionCoordinator() <em>Session Coordinator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSessionCoordinator()
	 * @generated
	 * @ordered
	 */
	protected IContributorHandle sessionCoordinator;

	/**
	 * The flag representing whether the Session Coordinator reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int SESSION_COORDINATOR_ESETFLAG = 1 << 16;

	/**
	 * The default value of the '{@link #getDateCreated() <em>Date Created</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateCreated()
	 * @generated
	 * @ordered
	 */
	protected static final long DATE_CREATED_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getDateCreated() <em>Date Created</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateCreated()
	 * @generated
	 * @ordered
	 */
	protected long dateCreated = DATE_CREATED_EDEFAULT;

	/**
	 * The flag representing whether the Date Created attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int DATE_CREATED_ESETFLAG = 1 << 17;

	/**
	 * The default value of the '{@link #getDateLastModified() <em>Date Last Modified</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateLastModified()
	 * @generated
	 * @ordered
	 */
	protected static final long DATE_LAST_MODIFIED_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getDateLastModified() <em>Date Last Modified</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateLastModified()
	 * @generated
	 * @ordered
	 */
	protected long dateLastModified = DATE_LAST_MODIFIED_EDEFAULT;

	/**
	 * The flag representing whether the Date Last Modified attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int DATE_LAST_MODIFIED_ESETFLAG = 1 << 18;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = "yadda"; //$NON-NLS-1$

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The flag representing whether the Description attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int DESCRIPTION_ESETFLAG = 1 << 19;

	/**
	 * The cached value of the '{@link #getItems() <em>Items</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItems()
	 * @generated
	 * @ordered
	 */
	protected EList items;

	/**
	 * The cached value of the '{@link #getProjectArea() <em>Project Area</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProjectArea()
	 * @generated
	 * @ordered
	 */
	protected IItemHandle projectArea;

	/**
	 * The flag representing whether the Project Area reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int PROJECT_AREA_ESETFLAG = 1 << 20;

	/**
	 * The cached value of the '{@link #getSessionHistory() <em>Session History</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSessionHistory()
	 * @generated
	 * @ordered
	 */
	protected EList sessionHistory;

	/**
	 * The default value of the '{@link #isActive() <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActive()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ACTIVE_EDEFAULT = false;

	/**
	 * The flag representing the value of the '{@link #isActive() <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isActive()
	 * @generated
	 * @ordered
	 */
	protected static final int ACTIVE_EFLAG = 1 << 21;

	/**
	 * The flag representing whether the Active attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int ACTIVE_ESETFLAG = 1 << 22;

	/**
	 * The default value of the '{@link #isArchived() <em>Archived</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArchived()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ARCHIVED_EDEFAULT = false;

	/**
	 * The flag representing the value of the '{@link #isArchived() <em>Archived</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isArchived()
	 * @generated
	 * @ordered
	 */
	protected static final int ARCHIVED_EFLAG = 1 << 23;

	/**
	 * The flag representing whether the Archived attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int ARCHIVED_ESETFLAG = 1 << 24;

	/**
	 * The default value of the '{@link #getCardsValueLowerBound() <em>Cards Value Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardsValueLowerBound()
	 * @generated
	 * @ordered
	 */
	protected static final int CARDS_VALUE_LOWER_BOUND_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCardsValueLowerBound() <em>Cards Value Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardsValueLowerBound()
	 * @generated
	 * @ordered
	 */
	protected int cardsValueLowerBound = CARDS_VALUE_LOWER_BOUND_EDEFAULT;

	/**
	 * The flag representing whether the Cards Value Lower Bound attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int CARDS_VALUE_LOWER_BOUND_ESETFLAG = 1 << 25;

	/**
	 * The default value of the '{@link #getCardsValueUpperBound() <em>Cards Value Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardsValueUpperBound()
	 * @generated
	 * @ordered
	 */
	protected static final int CARDS_VALUE_UPPER_BOUND_EDEFAULT = 100;

	/**
	 * The cached value of the '{@link #getCardsValueUpperBound() <em>Cards Value Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCardsValueUpperBound()
	 * @generated
	 * @ordered
	 */
	protected int cardsValueUpperBound = CARDS_VALUE_UPPER_BOUND_EDEFAULT;

	/**
	 * The flag representing whether the Cards Value Upper Bound attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int CARDS_VALUE_UPPER_BOUND_ESETFLAG = 1 << 26;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int EOFFSET_CORRECTION = JazzPackage.Literals.SESSION
			.getFeatureID(JazzPackage.Literals.SESSION__TEXT)
			- JazzPackage.SESSION__TEXT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SessionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return JazzPackage.Literals.SESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getText() {
		return text;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setText(String newText) {
		String oldText = text;
		text = newText;
		boolean oldTextESet = (ALL_FLAGS & TEXT_ESETFLAG) != 0;
		ALL_FLAGS |= TEXT_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__TEXT + EOFFSET_CORRECTION, oldText,
					text, !oldTextESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetText() {
		String oldText = text;
		boolean oldTextESet = (ALL_FLAGS & TEXT_ESETFLAG) != 0;
		text = TEXT_EDEFAULT;
		ALL_FLAGS &= ~TEXT_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__TEXT + EOFFSET_CORRECTION, oldText,
					TEXT_EDEFAULT, oldTextESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetText() {
		return (ALL_FLAGS & TEXT_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List<IContributor> getParticipants() {
		if (participants == null) {
			participants = new EObjectResolvingEList.Unsettable<IContributor>(
					IContributorHandle.class, this,
					JazzPackage.SESSION__PARTICIPANTS + EOFFSET_CORRECTION);
		}
		return participants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetParticipants() {
		if (participants != null)
			((InternalEList.Unsettable) participants).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetParticipants() {
		return participants != null
				&& ((InternalEList.Unsettable) participants).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IContributorHandle getCreatedBy() {
		if (createdBy != null && ((EObject) createdBy).eIsProxy()) {
			InternalEObject oldCreatedBy = (InternalEObject) createdBy;
			createdBy = (IContributorHandle) eResolveProxy(oldCreatedBy);
			if (createdBy != oldCreatedBy) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JazzPackage.SESSION__CREATED_BY
									+ EOFFSET_CORRECTION, oldCreatedBy,
							createdBy));
			}
		}
		return createdBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IContributorHandle basicGetCreatedBy() {
		return createdBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCreatedBy(IContributorHandle newCreatedBy) {
		IContributorHandle oldCreatedBy = createdBy;
		createdBy = newCreatedBy;
		boolean oldCreatedByESet = (ALL_FLAGS & CREATED_BY_ESETFLAG) != 0;
		ALL_FLAGS |= CREATED_BY_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__CREATED_BY + EOFFSET_CORRECTION,
					oldCreatedBy, createdBy, !oldCreatedByESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCreatedBy() {
		IContributorHandle oldCreatedBy = createdBy;
		boolean oldCreatedByESet = (ALL_FLAGS & CREATED_BY_ESETFLAG) != 0;
		createdBy = null;
		ALL_FLAGS &= ~CREATED_BY_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__CREATED_BY + EOFFSET_CORRECTION,
					oldCreatedBy, null, oldCreatedByESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCreatedBy() {
		return (ALL_FLAGS & CREATED_BY_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IContributorHandle getSessionCoordinator() {
		if (sessionCoordinator != null
				&& ((EObject) sessionCoordinator).eIsProxy()) {
			InternalEObject oldSessionCoordinator = (InternalEObject) sessionCoordinator;
			sessionCoordinator = (IContributorHandle) eResolveProxy(oldSessionCoordinator);
			if (sessionCoordinator != oldSessionCoordinator) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JazzPackage.SESSION__SESSION_COORDINATOR
									+ EOFFSET_CORRECTION,
							oldSessionCoordinator, sessionCoordinator));
			}
		}
		return sessionCoordinator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IContributorHandle basicGetSessionCoordinator() {
		return sessionCoordinator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSessionCoordinator(IContributorHandle newSessionCoordinator) {
		IContributorHandle oldSessionCoordinator = sessionCoordinator;
		sessionCoordinator = newSessionCoordinator;
		boolean oldSessionCoordinatorESet = (ALL_FLAGS & SESSION_COORDINATOR_ESETFLAG) != 0;
		ALL_FLAGS |= SESSION_COORDINATOR_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__SESSION_COORDINATOR
							+ EOFFSET_CORRECTION, oldSessionCoordinator,
					sessionCoordinator, !oldSessionCoordinatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSessionCoordinator() {
		IContributorHandle oldSessionCoordinator = sessionCoordinator;
		boolean oldSessionCoordinatorESet = (ALL_FLAGS & SESSION_COORDINATOR_ESETFLAG) != 0;
		sessionCoordinator = null;
		ALL_FLAGS &= ~SESSION_COORDINATOR_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__SESSION_COORDINATOR
							+ EOFFSET_CORRECTION, oldSessionCoordinator, null,
					oldSessionCoordinatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSessionCoordinator() {
		return (ALL_FLAGS & SESSION_COORDINATOR_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getDateCreated() {
		return dateCreated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateCreated(long newDateCreated) {
		long oldDateCreated = dateCreated;
		dateCreated = newDateCreated;
		boolean oldDateCreatedESet = (ALL_FLAGS & DATE_CREATED_ESETFLAG) != 0;
		ALL_FLAGS |= DATE_CREATED_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__DATE_CREATED + EOFFSET_CORRECTION,
					oldDateCreated, dateCreated, !oldDateCreatedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDateCreated() {
		long oldDateCreated = dateCreated;
		boolean oldDateCreatedESet = (ALL_FLAGS & DATE_CREATED_ESETFLAG) != 0;
		dateCreated = DATE_CREATED_EDEFAULT;
		ALL_FLAGS &= ~DATE_CREATED_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__DATE_CREATED + EOFFSET_CORRECTION,
					oldDateCreated, DATE_CREATED_EDEFAULT, oldDateCreatedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDateCreated() {
		return (ALL_FLAGS & DATE_CREATED_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getDateLastModified() {
		return dateLastModified;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateLastModified(long newDateLastModified) {
		long oldDateLastModified = dateLastModified;
		dateLastModified = newDateLastModified;
		boolean oldDateLastModifiedESet = (ALL_FLAGS & DATE_LAST_MODIFIED_ESETFLAG) != 0;
		ALL_FLAGS |= DATE_LAST_MODIFIED_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__DATE_LAST_MODIFIED
							+ EOFFSET_CORRECTION, oldDateLastModified,
					dateLastModified, !oldDateLastModifiedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDateLastModified() {
		long oldDateLastModified = dateLastModified;
		boolean oldDateLastModifiedESet = (ALL_FLAGS & DATE_LAST_MODIFIED_ESETFLAG) != 0;
		dateLastModified = DATE_LAST_MODIFIED_EDEFAULT;
		ALL_FLAGS &= ~DATE_LAST_MODIFIED_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__DATE_LAST_MODIFIED
							+ EOFFSET_CORRECTION, oldDateLastModified,
					DATE_LAST_MODIFIED_EDEFAULT, oldDateLastModifiedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDateLastModified() {
		return (ALL_FLAGS & DATE_LAST_MODIFIED_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		boolean oldDescriptionESet = (ALL_FLAGS & DESCRIPTION_ESETFLAG) != 0;
		ALL_FLAGS |= DESCRIPTION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__DESCRIPTION + EOFFSET_CORRECTION,
					oldDescription, description, !oldDescriptionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDescription() {
		String oldDescription = description;
		boolean oldDescriptionESet = (ALL_FLAGS & DESCRIPTION_ESETFLAG) != 0;
		description = DESCRIPTION_EDEFAULT;
		ALL_FLAGS &= ~DESCRIPTION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__DESCRIPTION + EOFFSET_CORRECTION,
					oldDescription, DESCRIPTION_EDEFAULT, oldDescriptionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDescription() {
		return (ALL_FLAGS & DESCRIPTION_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getItems() {
		if (items == null) {
			items = new EObjectResolvingEList.Unsettable(IItemHandle.class,
					this, JazzPackage.SESSION__ITEMS + EOFFSET_CORRECTION);
		}
		return items;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetItems() {
		if (items != null)
			((InternalEList.Unsettable) items).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetItems() {
		return items != null && ((InternalEList.Unsettable) items).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IItemHandle getProjectArea() {
		if (projectArea != null && ((EObject) projectArea).eIsProxy()) {
			InternalEObject oldProjectArea = (InternalEObject) projectArea;
			projectArea = (IItemHandle) eResolveProxy(oldProjectArea);
			if (projectArea != oldProjectArea) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JazzPackage.SESSION__PROJECT_AREA
									+ EOFFSET_CORRECTION, oldProjectArea,
							projectArea));
			}
		}
		return projectArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IItemHandle basicGetProjectArea() {
		return projectArea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProjectArea(IItemHandle newProjectArea) {
		IItemHandle oldProjectArea = projectArea;
		projectArea = newProjectArea;
		boolean oldProjectAreaESet = (ALL_FLAGS & PROJECT_AREA_ESETFLAG) != 0;
		ALL_FLAGS |= PROJECT_AREA_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__PROJECT_AREA + EOFFSET_CORRECTION,
					oldProjectArea, projectArea, !oldProjectAreaESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetProjectArea() {
		IItemHandle oldProjectArea = projectArea;
		boolean oldProjectAreaESet = (ALL_FLAGS & PROJECT_AREA_ESETFLAG) != 0;
		projectArea = null;
		ALL_FLAGS &= ~PROJECT_AREA_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__PROJECT_AREA + EOFFSET_CORRECTION,
					oldProjectArea, null, oldProjectAreaESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProjectArea() {
		return (ALL_FLAGS & PROJECT_AREA_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getSessionHistory() {
		if (sessionHistory == null) {
			sessionHistory = new EObjectResolvingEList.Unsettable(
					SessionHistoryHandle.class, this,
					JazzPackage.SESSION__SESSION_HISTORY + EOFFSET_CORRECTION);
		}
		return sessionHistory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSessionHistory() {
		if (sessionHistory != null)
			((InternalEList.Unsettable) sessionHistory).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSessionHistory() {
		return sessionHistory != null
				&& ((InternalEList.Unsettable) sessionHistory).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isActive() {
		return (ALL_FLAGS & ACTIVE_EFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActive(boolean newActive) {
		boolean oldActive = (ALL_FLAGS & ACTIVE_EFLAG) != 0;
		if (newActive)
			ALL_FLAGS |= ACTIVE_EFLAG;
		else
			ALL_FLAGS &= ~ACTIVE_EFLAG;
		boolean oldActiveESet = (ALL_FLAGS & ACTIVE_ESETFLAG) != 0;
		ALL_FLAGS |= ACTIVE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__ACTIVE + EOFFSET_CORRECTION,
					oldActive, newActive, !oldActiveESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetActive() {
		boolean oldActive = (ALL_FLAGS & ACTIVE_EFLAG) != 0;
		boolean oldActiveESet = (ALL_FLAGS & ACTIVE_ESETFLAG) != 0;
		if (ACTIVE_EDEFAULT)
			ALL_FLAGS |= ACTIVE_EFLAG;
		else
			ALL_FLAGS &= ~ACTIVE_EFLAG;
		ALL_FLAGS &= ~ACTIVE_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__ACTIVE + EOFFSET_CORRECTION,
					oldActive, ACTIVE_EDEFAULT, oldActiveESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetActive() {
		return (ALL_FLAGS & ACTIVE_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isArchived() {
		return (ALL_FLAGS & ARCHIVED_EFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArchived(boolean newArchived) {
		boolean oldArchived = (ALL_FLAGS & ARCHIVED_EFLAG) != 0;
		if (newArchived)
			ALL_FLAGS |= ARCHIVED_EFLAG;
		else
			ALL_FLAGS &= ~ARCHIVED_EFLAG;
		boolean oldArchivedESet = (ALL_FLAGS & ARCHIVED_ESETFLAG) != 0;
		ALL_FLAGS |= ARCHIVED_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__ARCHIVED + EOFFSET_CORRECTION,
					oldArchived, newArchived, !oldArchivedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetArchived() {
		boolean oldArchived = (ALL_FLAGS & ARCHIVED_EFLAG) != 0;
		boolean oldArchivedESet = (ALL_FLAGS & ARCHIVED_ESETFLAG) != 0;
		if (ARCHIVED_EDEFAULT)
			ALL_FLAGS |= ARCHIVED_EFLAG;
		else
			ALL_FLAGS &= ~ARCHIVED_EFLAG;
		ALL_FLAGS &= ~ARCHIVED_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__ARCHIVED + EOFFSET_CORRECTION,
					oldArchived, ARCHIVED_EDEFAULT, oldArchivedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetArchived() {
		return (ALL_FLAGS & ARCHIVED_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCardsValueLowerBound() {
		return cardsValueLowerBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCardsValueLowerBound(int newCardsValueLowerBound) {
		int oldCardsValueLowerBound = cardsValueLowerBound;
		cardsValueLowerBound = newCardsValueLowerBound;
		boolean oldCardsValueLowerBoundESet = (ALL_FLAGS & CARDS_VALUE_LOWER_BOUND_ESETFLAG) != 0;
		ALL_FLAGS |= CARDS_VALUE_LOWER_BOUND_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__CARDS_VALUE_LOWER_BOUND
							+ EOFFSET_CORRECTION, oldCardsValueLowerBound,
					cardsValueLowerBound, !oldCardsValueLowerBoundESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCardsValueLowerBound() {
		int oldCardsValueLowerBound = cardsValueLowerBound;
		boolean oldCardsValueLowerBoundESet = (ALL_FLAGS & CARDS_VALUE_LOWER_BOUND_ESETFLAG) != 0;
		cardsValueLowerBound = CARDS_VALUE_LOWER_BOUND_EDEFAULT;
		ALL_FLAGS &= ~CARDS_VALUE_LOWER_BOUND_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__CARDS_VALUE_LOWER_BOUND
							+ EOFFSET_CORRECTION, oldCardsValueLowerBound,
					CARDS_VALUE_LOWER_BOUND_EDEFAULT,
					oldCardsValueLowerBoundESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCardsValueLowerBound() {
		return (ALL_FLAGS & CARDS_VALUE_LOWER_BOUND_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCardsValueUpperBound() {
		return cardsValueUpperBound;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCardsValueUpperBound(int newCardsValueUpperBound) {
		int oldCardsValueUpperBound = cardsValueUpperBound;
		cardsValueUpperBound = newCardsValueUpperBound;
		boolean oldCardsValueUpperBoundESet = (ALL_FLAGS & CARDS_VALUE_UPPER_BOUND_ESETFLAG) != 0;
		ALL_FLAGS |= CARDS_VALUE_UPPER_BOUND_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.SESSION__CARDS_VALUE_UPPER_BOUND
							+ EOFFSET_CORRECTION, oldCardsValueUpperBound,
					cardsValueUpperBound, !oldCardsValueUpperBoundESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCardsValueUpperBound() {
		int oldCardsValueUpperBound = cardsValueUpperBound;
		boolean oldCardsValueUpperBoundESet = (ALL_FLAGS & CARDS_VALUE_UPPER_BOUND_ESETFLAG) != 0;
		cardsValueUpperBound = CARDS_VALUE_UPPER_BOUND_EDEFAULT;
		ALL_FLAGS &= ~CARDS_VALUE_UPPER_BOUND_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.SESSION__CARDS_VALUE_UPPER_BOUND
							+ EOFFSET_CORRECTION, oldCardsValueUpperBound,
					CARDS_VALUE_UPPER_BOUND_EDEFAULT,
					oldCardsValueUpperBoundESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCardsValueUpperBound() {
		return (ALL_FLAGS & CARDS_VALUE_UPPER_BOUND_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.SESSION__TEXT:
			return getText();
		case JazzPackage.SESSION__PARTICIPANTS:
			return getParticipants();
		case JazzPackage.SESSION__CREATED_BY:
			if (resolve)
				return getCreatedBy();
			return basicGetCreatedBy();
		case JazzPackage.SESSION__SESSION_COORDINATOR:
			if (resolve)
				return getSessionCoordinator();
			return basicGetSessionCoordinator();
		case JazzPackage.SESSION__DATE_CREATED:
			return new Long(getDateCreated());
		case JazzPackage.SESSION__DATE_LAST_MODIFIED:
			return new Long(getDateLastModified());
		case JazzPackage.SESSION__DESCRIPTION:
			return getDescription();
		case JazzPackage.SESSION__ITEMS:
			return getItems();
		case JazzPackage.SESSION__PROJECT_AREA:
			if (resolve)
				return getProjectArea();
			return basicGetProjectArea();
		case JazzPackage.SESSION__SESSION_HISTORY:
			return getSessionHistory();
		case JazzPackage.SESSION__ACTIVE:
			return isActive() ? Boolean.TRUE : Boolean.FALSE;
		case JazzPackage.SESSION__ARCHIVED:
			return isArchived() ? Boolean.TRUE : Boolean.FALSE;
		case JazzPackage.SESSION__CARDS_VALUE_LOWER_BOUND:
			return new Integer(getCardsValueLowerBound());
		case JazzPackage.SESSION__CARDS_VALUE_UPPER_BOUND:
			return new Integer(getCardsValueUpperBound());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.SESSION__TEXT:
			setText((String) newValue);
			return;
		case JazzPackage.SESSION__PARTICIPANTS:
			getParticipants().clear();
			getParticipants().addAll((Collection) newValue);
			return;
		case JazzPackage.SESSION__CREATED_BY:
			setCreatedBy((IContributorHandle) newValue);
			return;
		case JazzPackage.SESSION__SESSION_COORDINATOR:
			setSessionCoordinator((IContributorHandle) newValue);
			return;
		case JazzPackage.SESSION__DATE_CREATED:
			setDateCreated(((Long) newValue).longValue());
			return;
		case JazzPackage.SESSION__DATE_LAST_MODIFIED:
			setDateLastModified(((Long) newValue).longValue());
			return;
		case JazzPackage.SESSION__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case JazzPackage.SESSION__ITEMS:
			getItems().clear();
			getItems().addAll((Collection) newValue);
			return;
		case JazzPackage.SESSION__PROJECT_AREA:
			setProjectArea((IItemHandle) newValue);
			return;
		case JazzPackage.SESSION__SESSION_HISTORY:
			getSessionHistory().clear();
			getSessionHistory().addAll((Collection) newValue);
			return;
		case JazzPackage.SESSION__ACTIVE:
			setActive(((Boolean) newValue).booleanValue());
			return;
		case JazzPackage.SESSION__ARCHIVED:
			setArchived(((Boolean) newValue).booleanValue());
			return;
		case JazzPackage.SESSION__CARDS_VALUE_LOWER_BOUND:
			setCardsValueLowerBound(((Integer) newValue).intValue());
			return;
		case JazzPackage.SESSION__CARDS_VALUE_UPPER_BOUND:
			setCardsValueUpperBound(((Integer) newValue).intValue());
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.SESSION__TEXT:
			unsetText();
			return;
		case JazzPackage.SESSION__PARTICIPANTS:
			unsetParticipants();
			return;
		case JazzPackage.SESSION__CREATED_BY:
			unsetCreatedBy();
			return;
		case JazzPackage.SESSION__SESSION_COORDINATOR:
			unsetSessionCoordinator();
			return;
		case JazzPackage.SESSION__DATE_CREATED:
			unsetDateCreated();
			return;
		case JazzPackage.SESSION__DATE_LAST_MODIFIED:
			unsetDateLastModified();
			return;
		case JazzPackage.SESSION__DESCRIPTION:
			unsetDescription();
			return;
		case JazzPackage.SESSION__ITEMS:
			unsetItems();
			return;
		case JazzPackage.SESSION__PROJECT_AREA:
			unsetProjectArea();
			return;
		case JazzPackage.SESSION__SESSION_HISTORY:
			unsetSessionHistory();
			return;
		case JazzPackage.SESSION__ACTIVE:
			unsetActive();
			return;
		case JazzPackage.SESSION__ARCHIVED:
			unsetArchived();
			return;
		case JazzPackage.SESSION__CARDS_VALUE_LOWER_BOUND:
			unsetCardsValueLowerBound();
			return;
		case JazzPackage.SESSION__CARDS_VALUE_UPPER_BOUND:
			unsetCardsValueUpperBound();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.SESSION__TEXT:
			return isSetText();
		case JazzPackage.SESSION__PARTICIPANTS:
			return isSetParticipants();
		case JazzPackage.SESSION__CREATED_BY:
			return isSetCreatedBy();
		case JazzPackage.SESSION__SESSION_COORDINATOR:
			return isSetSessionCoordinator();
		case JazzPackage.SESSION__DATE_CREATED:
			return isSetDateCreated();
		case JazzPackage.SESSION__DATE_LAST_MODIFIED:
			return isSetDateLastModified();
		case JazzPackage.SESSION__DESCRIPTION:
			return isSetDescription();
		case JazzPackage.SESSION__ITEMS:
			return isSetItems();
		case JazzPackage.SESSION__PROJECT_AREA:
			return isSetProjectArea();
		case JazzPackage.SESSION__SESSION_HISTORY:
			return isSetSessionHistory();
		case JazzPackage.SESSION__ACTIVE:
			return isSetActive();
		case JazzPackage.SESSION__ARCHIVED:
			return isSetArchived();
		case JazzPackage.SESSION__CARDS_VALUE_LOWER_BOUND:
			return isSetCardsValueLowerBound();
		case JazzPackage.SESSION__CARDS_VALUE_UPPER_BOUND:
			return isSetCardsValueUpperBound();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class baseClass) {
		if (baseClass == SessionHandle.class) {
			switch (baseFeatureID) {
			default:
				return -1;
			}
		}
		if (baseClass == Session.class) {
			switch (baseFeatureID - EOFFSET_CORRECTION) {
			case JazzPackage.SESSION__TEXT:
				return JazzPackage.SESSION__TEXT + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__PARTICIPANTS:
				return JazzPackage.SESSION__PARTICIPANTS + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__CREATED_BY:
				return JazzPackage.SESSION__CREATED_BY + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__SESSION_COORDINATOR:
				return JazzPackage.SESSION__SESSION_COORDINATOR
						+ EOFFSET_CORRECTION;
			case JazzPackage.SESSION__DATE_CREATED:
				return JazzPackage.SESSION__DATE_CREATED + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__DATE_LAST_MODIFIED:
				return JazzPackage.SESSION__DATE_LAST_MODIFIED
						+ EOFFSET_CORRECTION;
			case JazzPackage.SESSION__DESCRIPTION:
				return JazzPackage.SESSION__DESCRIPTION + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__ITEMS:
				return JazzPackage.SESSION__ITEMS + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__PROJECT_AREA:
				return JazzPackage.SESSION__PROJECT_AREA + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__SESSION_HISTORY:
				return JazzPackage.SESSION__SESSION_HISTORY
						+ EOFFSET_CORRECTION;
			case JazzPackage.SESSION__ACTIVE:
				return JazzPackage.SESSION__ACTIVE + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__ARCHIVED:
				return JazzPackage.SESSION__ARCHIVED + EOFFSET_CORRECTION;
			case JazzPackage.SESSION__CARDS_VALUE_LOWER_BOUND:
				return JazzPackage.SESSION__CARDS_VALUE_LOWER_BOUND
						+ EOFFSET_CORRECTION;
			case JazzPackage.SESSION__CARDS_VALUE_UPPER_BOUND:
				return JazzPackage.SESSION__CARDS_VALUE_UPPER_BOUND
						+ EOFFSET_CORRECTION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (text: "); //$NON-NLS-1$
		if ((ALL_FLAGS & TEXT_ESETFLAG) != 0)
			result.append(text);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", dateCreated: "); //$NON-NLS-1$
		if ((ALL_FLAGS & DATE_CREATED_ESETFLAG) != 0)
			result.append(dateCreated);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", dateLastModified: "); //$NON-NLS-1$
		if ((ALL_FLAGS & DATE_LAST_MODIFIED_ESETFLAG) != 0)
			result.append(dateLastModified);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", description: "); //$NON-NLS-1$
		if ((ALL_FLAGS & DESCRIPTION_ESETFLAG) != 0)
			result.append(description);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", active: "); //$NON-NLS-1$
		if ((ALL_FLAGS & ACTIVE_ESETFLAG) != 0)
			result.append((ALL_FLAGS & ACTIVE_EFLAG) != 0);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", archived: "); //$NON-NLS-1$
		if ((ALL_FLAGS & ARCHIVED_ESETFLAG) != 0)
			result.append((ALL_FLAGS & ARCHIVED_EFLAG) != 0);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", cardsValueLowerBound: "); //$NON-NLS-1$
		if ((ALL_FLAGS & CARDS_VALUE_LOWER_BOUND_ESETFLAG) != 0)
			result.append(cardsValueLowerBound);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(", cardsValueUpperBound: "); //$NON-NLS-1$
		if ((ALL_FLAGS & CARDS_VALUE_UPPER_BOUND_ESETFLAG) != 0)
			result.append(cardsValueUpperBound);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(')');
		return result.toString();
	}

} //SessionImpl
