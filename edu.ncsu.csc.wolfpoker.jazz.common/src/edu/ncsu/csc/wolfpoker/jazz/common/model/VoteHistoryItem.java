/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import com.ibm.team.repository.common.UUID;

import com.ibm.team.repository.common.model.Helper;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vote History Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getSession <em>Session</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVotes <em>Votes</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVoteRound <em>Vote Round</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getVoteHistoryItem()
 * @model
 * @generated
 */
public interface VoteHistoryItem extends Helper {
	/**
	 * Returns the value of the '<em><b>Session</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Session</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Session</em>' attribute.
	 * @see #isSetSession()
	 * @see #unsetSession()
	 * @see #setSession(UUID)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getVoteHistoryItem_Session()
	 * @model unsettable="true" dataType="com.ibm.team.repository.common.model.UUID"
	 * @generated
	 */
	UUID getSession();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getSession <em>Session</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Session</em>' attribute.
	 * @see #isSetSession()
	 * @see #unsetSession()
	 * @see #getSession()
	 * @generated
	 */
	void setSession(UUID value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getSession <em>Session</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSession()
	 * @see #getSession()
	 * @see #setSession(UUID)
	 * @generated
	 */
	void unsetSession();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getSession <em>Session</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Session</em>' attribute is set.
	 * @see #unsetSession()
	 * @see #getSession()
	 * @see #setSession(UUID)
	 * @generated
	 */
	boolean isSetSession();

	/**
	 * Returns the value of the '<em><b>Votes</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Votes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Votes</em>' containment reference list.
	 * @see #isSetVotes()
	 * @see #unsetVotes()
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getVoteHistoryItem_Votes()
	 * @model type="edu.ncsu.csc.wolfpoker.jazz.common.model.Vote" containment="true" unsettable="true"
	 * @generated
	 */
	List getVotes();

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVotes <em>Votes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVotes()
	 * @see #getVotes()
	 * @generated
	 */
	void unsetVotes();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVotes <em>Votes</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Votes</em>' containment reference list is set.
	 * @see #unsetVotes()
	 * @see #getVotes()
	 * @generated
	 */
	boolean isSetVotes();

	/**
	 * Returns the value of the '<em><b>Vote Round</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vote Round</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vote Round</em>' attribute.
	 * @see #isSetVoteRound()
	 * @see #unsetVoteRound()
	 * @see #setVoteRound(int)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getVoteHistoryItem_VoteRound()
	 * @model unsettable="true"
	 * @generated
	 */
	int getVoteRound();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVoteRound <em>Vote Round</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vote Round</em>' attribute.
	 * @see #isSetVoteRound()
	 * @see #unsetVoteRound()
	 * @see #getVoteRound()
	 * @generated
	 */
	void setVoteRound(int value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVoteRound <em>Vote Round</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVoteRound()
	 * @see #getVoteRound()
	 * @see #setVoteRound(int)
	 * @generated
	 */
	void unsetVoteRound();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem#getVoteRound <em>Vote Round</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Vote Round</em>' attribute is set.
	 * @see #unsetVoteRound()
	 * @see #getVoteRound()
	 * @see #setVoteRound(int)
	 * @generated
	 */
	boolean isSetVoteRound();

} // VoteHistoryItem
