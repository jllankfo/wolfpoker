/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import com.ibm.team.repository.common.IItemHandle;

import com.ibm.team.repository.common.model.Helper;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Item For Estimation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getItem <em>Item</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getCurrentVotes <em>Current Votes</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getVoteHistory <em>Vote History</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getSavedVote <em>Saved Vote</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getRoundNo <em>Round No</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getItemForEstimation()
 * @model
 * @generated
 */
public interface ItemForEstimation extends Helper {
	/**
	 * Returns the value of the '<em><b>Item</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Item</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Item</em>' reference.
	 * @see #isSetItem()
	 * @see #unsetItem()
	 * @see #setItem(IItemHandle)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getItemForEstimation_Item()
	 * @model type="com.ibm.team.repository.common.model.ItemHandleFacade" unsettable="true"
	 * @generated
	 */
	IItemHandle getItem();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getItem <em>Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Item</em>' reference.
	 * @see #isSetItem()
	 * @see #unsetItem()
	 * @see #getItem()
	 * @generated
	 */
	void setItem(IItemHandle value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getItem <em>Item</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetItem()
	 * @see #getItem()
	 * @see #setItem(IItemHandle)
	 * @generated
	 */
	void unsetItem();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getItem <em>Item</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Item</em>' reference is set.
	 * @see #unsetItem()
	 * @see #getItem()
	 * @see #setItem(IItemHandle)
	 * @generated
	 */
	boolean isSetItem();

	/**
	 * Returns the value of the '<em><b>Current Votes</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ncsu.csc.wolfpoker.jazz.common.model.Vote}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Votes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Votes</em>' containment reference list.
	 * @see #isSetCurrentVotes()
	 * @see #unsetCurrentVotes()
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getItemForEstimation_CurrentVotes()
	 * @model type="edu.ncsu.csc.wolfpoker.jazz.common.model.Vote" containment="true" unsettable="true"
	 * @generated
	 */
	List getCurrentVotes();

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getCurrentVotes <em>Current Votes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCurrentVotes()
	 * @see #getCurrentVotes()
	 * @generated
	 */
	void unsetCurrentVotes();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getCurrentVotes <em>Current Votes</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Current Votes</em>' containment reference list is set.
	 * @see #unsetCurrentVotes()
	 * @see #getCurrentVotes()
	 * @generated
	 */
	boolean isSetCurrentVotes();

	/**
	 * Returns the value of the '<em><b>Vote History</b></em>' reference list.
	 * The list contents are of type {@link edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vote History</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vote History</em>' reference list.
	 * @see #isSetVoteHistory()
	 * @see #unsetVoteHistory()
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getItemForEstimation_VoteHistory()
	 * @model type="edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem" unsettable="true"
	 * @generated
	 */
	List getVoteHistory();

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getVoteHistory <em>Vote History</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetVoteHistory()
	 * @see #getVoteHistory()
	 * @generated
	 */
	void unsetVoteHistory();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getVoteHistory <em>Vote History</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Vote History</em>' reference list is set.
	 * @see #unsetVoteHistory()
	 * @see #getVoteHistory()
	 * @generated
	 */
	boolean isSetVoteHistory();

	/**
	 * Returns the value of the '<em><b>Saved Vote</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Saved Vote</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Saved Vote</em>' attribute.
	 * @see #isSetSavedVote()
	 * @see #unsetSavedVote()
	 * @see #setSavedVote(int)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getItemForEstimation_SavedVote()
	 * @model unsettable="true"
	 * @generated
	 */
	int getSavedVote();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getSavedVote <em>Saved Vote</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Saved Vote</em>' attribute.
	 * @see #isSetSavedVote()
	 * @see #unsetSavedVote()
	 * @see #getSavedVote()
	 * @generated
	 */
	void setSavedVote(int value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getSavedVote <em>Saved Vote</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSavedVote()
	 * @see #getSavedVote()
	 * @see #setSavedVote(int)
	 * @generated
	 */
	void unsetSavedVote();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getSavedVote <em>Saved Vote</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Saved Vote</em>' attribute is set.
	 * @see #unsetSavedVote()
	 * @see #getSavedVote()
	 * @see #setSavedVote(int)
	 * @generated
	 */
	boolean isSetSavedVote();

	/**
	 * Returns the value of the '<em><b>Round No</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Round No</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Round No</em>' attribute.
	 * @see #isSetRoundNo()
	 * @see #unsetRoundNo()
	 * @see #setRoundNo(int)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getItemForEstimation_RoundNo()
	 * @model unsettable="true"
	 * @generated
	 */
	int getRoundNo();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getRoundNo <em>Round No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Round No</em>' attribute.
	 * @see #isSetRoundNo()
	 * @see #unsetRoundNo()
	 * @see #getRoundNo()
	 * @generated
	 */
	void setRoundNo(int value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getRoundNo <em>Round No</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRoundNo()
	 * @see #getRoundNo()
	 * @see #setRoundNo(int)
	 * @generated
	 */
	void unsetRoundNo();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation#getRoundNo <em>Round No</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Round No</em>' attribute is set.
	 * @see #unsetRoundNo()
	 * @see #getRoundNo()
	 * @see #setRoundNo(int)
	 * @generated
	 */
	boolean isSetRoundNo();

} // ItemForEstimation
