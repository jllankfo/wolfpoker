package edu.ncsu.csc.wolfpoker.jazz.common.model.query;

import com.ibm.team.repository.common.model.query.BaseSimpleItemQueryModel;
import com.ibm.team.repository.common.query.ast.IManyItemQueryModel;
import com.ibm.team.repository.common.query.ast.ISingleItemQueryModel;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseSessionQueryModel.SessionQueryModel;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.impl.ActiveSessionQueryModelImpl;

/**
 * Defines the query model shared by both single and many valued ActiveSession references. 
 *
 * @generated
 * @NoImplement Clients must not implement this interface.
 */
public interface BaseActiveSessionQueryModel extends BaseSimpleItemQueryModel {

	/** 
	 * Defines the query model for many valued ActiveSession references.
	 * 
	 * @generated
	 * @NoImplement Clients must not implement this interface.
	 */
	public interface ManyActiveSessionQueryModel extends BaseActiveSessionQueryModel, IManyItemQueryModel { /* For strict type checking */ };

    /** 
     * Defines the query model for single valued ActiveSession references.
     *
	 * @generated
	 * @NoImplement Clients must not implement this interface.
	 */
	public interface ActiveSessionQueryModel extends BaseActiveSessionQueryModel, ISingleItemQueryModel {
		/**
		 * The root of ActiveSession queries.
		 * 
 	 	 * @generated
	 	 */
		public static final ActiveSessionQueryModel ROOT = new ActiveSessionQueryModelImpl(null, null);
	};

	/**
	 * Navigates into the <code>associatedSession</code> feature and returns the 
	 * query model at that path.
	 *
	 * @return the query model associated with the <code>associatedSession</code> feature
 	 * @generated
	 */
	public SessionQueryModel associatedSession(); 

}