package edu.ncsu.csc.wolfpoker.jazz.common.model.query;

import com.ibm.team.repository.common.model.query.BaseAuditableQueryModel;
import com.ibm.team.repository.common.model.query.BaseContributorQueryModel.ContributorQueryModel;
import com.ibm.team.repository.common.query.ast.IManyItemQueryModel;
import com.ibm.team.repository.common.query.ast.ISingleItemQueryModel;
import com.ibm.team.repository.common.query.ast.IStringField;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.impl.SessionQueryModelImpl;

/**
 * Defines the query model shared by both single and many valued Session references. 
 *
 * @generated
 * @NoImplement Clients must not implement this interface.
 */
public interface BaseSessionQueryModel extends BaseAuditableQueryModel {

	/** 
	 * Defines the query model for many valued Session references.
	 * 
	 * @generated
	 * @NoImplement Clients must not implement this interface.
	 */
	public interface ManySessionQueryModel extends BaseSessionQueryModel, IManyItemQueryModel { /* For strict type checking */ };

    /** 
     * Defines the query model for single valued Session references.
     *
	 * @generated
	 * @NoImplement Clients must not implement this interface.
	 */
	public interface SessionQueryModel extends BaseSessionQueryModel, ISingleItemQueryModel {
		/**
		 * The root of Session queries.
		 * 
 	 	 * @generated
	 	 */
		public static final SessionQueryModel ROOT = new SessionQueryModelImpl(null, null);
	};

	/**
	 * Navigates into the <code>text</code> feature and returns the 
	 * query model at that path.
	 *
	 * @return the query model associated with the <code>text</code> feature
 	 * @generated
	 */
	public IStringField text(); 

	/**
	 * Navigates into the <code>createdBy</code> feature and returns the 
	 * query model at that path.
	 *
	 * @return the query model associated with the <code>createdBy</code> feature
 	 * @generated
	 */
	public ContributorQueryModel createdBy(); 

	/**
	 * Navigates into the <code>sessionCoordinator</code> feature and returns the 
	 * query model at that path.
	 *
	 * @return the query model associated with the <code>sessionCoordinator</code> feature
 	 * @generated
	 */
	public ContributorQueryModel sessionCoordinator(); 

	/**
	 * Navigates into the <code>description</code> feature and returns the 
	 * query model at that path.
	 *
	 * @return the query model associated with the <code>description</code> feature
 	 * @generated
	 */
	public IStringField description(); 

	/**
	 * Navigates into the <code>items</code> feature and returns the 
	 * query model at that path.
	 *
	 * @return the query model associated with the <code>items</code> feature
 	 * @generated
	 */
	public ManyItemHandleQueryModel items(); 

	/**
	 * Navigates into the <code>projectArea</code> feature and returns the 
	 * query model at that path.
	 *
	 * @return the query model associated with the <code>projectArea</code> feature
 	 * @generated
	 */
	public ItemHandleQueryModel projectArea(); 

}