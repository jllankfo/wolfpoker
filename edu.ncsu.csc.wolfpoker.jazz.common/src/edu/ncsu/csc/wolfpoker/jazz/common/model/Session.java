/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import java.util.List;

import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.repository.common.model.Auditable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Session</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getText <em>Text</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getParticipants <em>Participants</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCreatedBy <em>Created By</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionCoordinator <em>Session Coordinator</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateCreated <em>Date Created</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateLastModified <em>Date Last Modified</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDescription <em>Description</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getItems <em>Items</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getProjectArea <em>Project Area</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionHistory <em>Session History</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isActive <em>Active</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isArchived <em>Archived</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueLowerBound <em>Cards Value Lower Bound</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueUpperBound <em>Cards Value Upper Bound</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession()
 * @model
 * @generated
 */
public interface Session extends Auditable, SessionHandle {
	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * The default value is <code>"yadda"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #isSetText()
	 * @see #unsetText()
	 * @see #setText(String)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_Text()
	 * @model default="yadda" unsettable="true"
	 *        annotation="queryableProperty unique='false'"
	 *        annotation="teamAttribute dbStringSize='SMALL'"
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #isSetText()
	 * @see #unsetText()
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetText()
	 * @see #getText()
	 * @see #setText(String)
	 * @generated
	 */
	void unsetText();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getText <em>Text</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Text</em>' attribute is set.
	 * @see #unsetText()
	 * @see #getText()
	 * @see #setText(String)
	 * @generated
	 */
	boolean isSetText();

	/**
	 * Returns the value of the '<em><b>Participants</b></em>' reference list.
	 * The list contents are of type {@link com.ibm.team.repository.common.IContributorHandle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Participants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Participants</em>' reference list.
	 * @see #isSetParticipants()
	 * @see #unsetParticipants()
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_Participants()
	 * @model type="com.ibm.team.repository.common.model.ContributorHandleFacade" unsettable="true"
	 * @generated
	 */
	List<IContributor> getParticipants();

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getParticipants <em>Participants</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParticipants()
	 * @see #getParticipants()
	 * @generated
	 */
	void unsetParticipants();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getParticipants <em>Participants</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Participants</em>' reference list is set.
	 * @see #unsetParticipants()
	 * @see #getParticipants()
	 * @generated
	 */
	boolean isSetParticipants();

	/**
	 * Returns the value of the '<em><b>Created By</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Created By</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Created By</em>' reference.
	 * @see #isSetCreatedBy()
	 * @see #unsetCreatedBy()
	 * @see #setCreatedBy(IContributorHandle)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_CreatedBy()
	 * @model type="com.ibm.team.repository.common.model.ContributorHandleFacade" unsettable="true"
	 *        annotation="queryableProperty unique='false'"
	 * @generated
	 */
	IContributorHandle getCreatedBy();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCreatedBy <em>Created By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Created By</em>' reference.
	 * @see #isSetCreatedBy()
	 * @see #unsetCreatedBy()
	 * @see #getCreatedBy()
	 * @generated
	 */
	void setCreatedBy(IContributorHandle value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCreatedBy <em>Created By</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCreatedBy()
	 * @see #getCreatedBy()
	 * @see #setCreatedBy(IContributorHandle)
	 * @generated
	 */
	void unsetCreatedBy();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCreatedBy <em>Created By</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Created By</em>' reference is set.
	 * @see #unsetCreatedBy()
	 * @see #getCreatedBy()
	 * @see #setCreatedBy(IContributorHandle)
	 * @generated
	 */
	boolean isSetCreatedBy();

	/**
	 * Returns the value of the '<em><b>Session Coordinator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Session Coordinator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Session Coordinator</em>' reference.
	 * @see #isSetSessionCoordinator()
	 * @see #unsetSessionCoordinator()
	 * @see #setSessionCoordinator(IContributorHandle)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_SessionCoordinator()
	 * @model type="com.ibm.team.repository.common.model.ContributorHandleFacade" unsettable="true"
	 *        annotation="queryableProperty unique='false'"
	 * @generated
	 */
	IContributorHandle getSessionCoordinator();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionCoordinator <em>Session Coordinator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Session Coordinator</em>' reference.
	 * @see #isSetSessionCoordinator()
	 * @see #unsetSessionCoordinator()
	 * @see #getSessionCoordinator()
	 * @generated
	 */
	void setSessionCoordinator(IContributorHandle value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionCoordinator <em>Session Coordinator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSessionCoordinator()
	 * @see #getSessionCoordinator()
	 * @see #setSessionCoordinator(IContributorHandle)
	 * @generated
	 */
	void unsetSessionCoordinator();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionCoordinator <em>Session Coordinator</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Session Coordinator</em>' reference is set.
	 * @see #unsetSessionCoordinator()
	 * @see #getSessionCoordinator()
	 * @see #setSessionCoordinator(IContributorHandle)
	 * @generated
	 */
	boolean isSetSessionCoordinator();

	/**
	 * Returns the value of the '<em><b>Date Created</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Created</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Created</em>' attribute.
	 * @see #isSetDateCreated()
	 * @see #unsetDateCreated()
	 * @see #setDateCreated(long)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_DateCreated()
	 * @model unsettable="true"
	 * @generated
	 */
	long getDateCreated();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateCreated <em>Date Created</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Created</em>' attribute.
	 * @see #isSetDateCreated()
	 * @see #unsetDateCreated()
	 * @see #getDateCreated()
	 * @generated
	 */
	void setDateCreated(long value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateCreated <em>Date Created</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDateCreated()
	 * @see #getDateCreated()
	 * @see #setDateCreated(long)
	 * @generated
	 */
	void unsetDateCreated();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateCreated <em>Date Created</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Date Created</em>' attribute is set.
	 * @see #unsetDateCreated()
	 * @see #getDateCreated()
	 * @see #setDateCreated(long)
	 * @generated
	 */
	boolean isSetDateCreated();

	/**
	 * Returns the value of the '<em><b>Date Last Modified</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date Last Modified</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date Last Modified</em>' attribute.
	 * @see #isSetDateLastModified()
	 * @see #unsetDateLastModified()
	 * @see #setDateLastModified(long)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_DateLastModified()
	 * @model unsettable="true"
	 * @generated
	 */
	long getDateLastModified();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateLastModified <em>Date Last Modified</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date Last Modified</em>' attribute.
	 * @see #isSetDateLastModified()
	 * @see #unsetDateLastModified()
	 * @see #getDateLastModified()
	 * @generated
	 */
	void setDateLastModified(long value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateLastModified <em>Date Last Modified</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDateLastModified()
	 * @see #getDateLastModified()
	 * @see #setDateLastModified(long)
	 * @generated
	 */
	void unsetDateLastModified();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDateLastModified <em>Date Last Modified</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Date Last Modified</em>' attribute is set.
	 * @see #unsetDateLastModified()
	 * @see #getDateLastModified()
	 * @see #setDateLastModified(long)
	 * @generated
	 */
	boolean isSetDateLastModified();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * The default value is <code>"yadda"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #isSetDescription()
	 * @see #unsetDescription()
	 * @see #setDescription(String)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_Description()
	 * @model default="yadda" unsettable="true"
	 *        annotation="queryableProperty unique='false'"
	 *        annotation="teamAttribute dbStringSize='SMALL'"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #isSetDescription()
	 * @see #unsetDescription()
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDescription()
	 * @see #getDescription()
	 * @see #setDescription(String)
	 * @generated
	 */
	void unsetDescription();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getDescription <em>Description</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Description</em>' attribute is set.
	 * @see #unsetDescription()
	 * @see #getDescription()
	 * @see #setDescription(String)
	 * @generated
	 */
	boolean isSetDescription();

	/**
	 * Returns the value of the '<em><b>Items</b></em>' reference list.
	 * The list contents are of type {@link com.ibm.team.repository.common.IItemHandle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Items</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Items</em>' reference list.
	 * @see #isSetItems()
	 * @see #unsetItems()
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_Items()
	 * @model type="com.ibm.team.repository.common.model.ItemHandleFacade" unsettable="true"
	 *        annotation="queryableProperty unique='false'"
	 * @generated
	 */
	List getItems();

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getItems <em>Items</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetItems()
	 * @see #getItems()
	 * @generated
	 */
	void unsetItems();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getItems <em>Items</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Items</em>' reference list is set.
	 * @see #unsetItems()
	 * @see #getItems()
	 * @generated
	 */
	boolean isSetItems();

	/**
	 * Returns the value of the '<em><b>Project Area</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project Area</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project Area</em>' reference.
	 * @see #isSetProjectArea()
	 * @see #unsetProjectArea()
	 * @see #setProjectArea(IItemHandle)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_ProjectArea()
	 * @model type="com.ibm.team.repository.common.model.ItemHandleFacade" unsettable="true"
	 *        annotation="queryableProperty unique='false'"
	 * @generated
	 */
	IItemHandle getProjectArea();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getProjectArea <em>Project Area</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project Area</em>' reference.
	 * @see #isSetProjectArea()
	 * @see #unsetProjectArea()
	 * @see #getProjectArea()
	 * @generated
	 */
	void setProjectArea(IItemHandle value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getProjectArea <em>Project Area</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProjectArea()
	 * @see #getProjectArea()
	 * @see #setProjectArea(IItemHandle)
	 * @generated
	 */
	void unsetProjectArea();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getProjectArea <em>Project Area</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Project Area</em>' reference is set.
	 * @see #unsetProjectArea()
	 * @see #getProjectArea()
	 * @see #setProjectArea(IItemHandle)
	 * @generated
	 */
	boolean isSetProjectArea();

	/**
	 * Returns the value of the '<em><b>Session History</b></em>' reference list.
	 * The list contents are of type {@link edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Session History</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Session History</em>' reference list.
	 * @see #isSetSessionHistory()
	 * @see #unsetSessionHistory()
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_SessionHistory()
	 * @model type="edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle" unsettable="true"
	 * @generated
	 */
	List getSessionHistory();

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionHistory <em>Session History</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSessionHistory()
	 * @see #getSessionHistory()
	 * @generated
	 */
	void unsetSessionHistory();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getSessionHistory <em>Session History</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Session History</em>' reference list is set.
	 * @see #unsetSessionHistory()
	 * @see #getSessionHistory()
	 * @generated
	 */
	boolean isSetSessionHistory();

	/**
	 * Returns the value of the '<em><b>Active</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Active</em>' attribute.
	 * @see #isSetActive()
	 * @see #unsetActive()
	 * @see #setActive(boolean)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_Active()
	 * @model default="false" unique="false" unsettable="true" ordered="false"
	 * @generated
	 */
	boolean isActive();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isActive <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Active</em>' attribute.
	 * @see #isSetActive()
	 * @see #unsetActive()
	 * @see #isActive()
	 * @generated
	 */
	void setActive(boolean value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isActive <em>Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetActive()
	 * @see #isActive()
	 * @see #setActive(boolean)
	 * @generated
	 */
	void unsetActive();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isActive <em>Active</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Active</em>' attribute is set.
	 * @see #unsetActive()
	 * @see #isActive()
	 * @see #setActive(boolean)
	 * @generated
	 */
	boolean isSetActive();

	/**
	 * Returns the value of the '<em><b>Archived</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Archived</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Archived</em>' attribute.
	 * @see #isSetArchived()
	 * @see #unsetArchived()
	 * @see #setArchived(boolean)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_Archived()
	 * @model default="false" unique="false" unsettable="true" ordered="false"
	 * @generated
	 */
	boolean isArchived();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isArchived <em>Archived</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Archived</em>' attribute.
	 * @see #isSetArchived()
	 * @see #unsetArchived()
	 * @see #isArchived()
	 * @generated
	 */
	void setArchived(boolean value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isArchived <em>Archived</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetArchived()
	 * @see #isArchived()
	 * @see #setArchived(boolean)
	 * @generated
	 */
	void unsetArchived();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#isArchived <em>Archived</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Archived</em>' attribute is set.
	 * @see #unsetArchived()
	 * @see #isArchived()
	 * @see #setArchived(boolean)
	 * @generated
	 */
	boolean isSetArchived();

	/**
	 * Returns the value of the '<em><b>Cards Value Lower Bound</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cards Value Lower Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cards Value Lower Bound</em>' attribute.
	 * @see #isSetCardsValueLowerBound()
	 * @see #unsetCardsValueLowerBound()
	 * @see #setCardsValueLowerBound(int)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_CardsValueLowerBound()
	 * @model default="0" unsettable="true"
	 * @generated
	 */
	int getCardsValueLowerBound();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueLowerBound <em>Cards Value Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cards Value Lower Bound</em>' attribute.
	 * @see #isSetCardsValueLowerBound()
	 * @see #unsetCardsValueLowerBound()
	 * @see #getCardsValueLowerBound()
	 * @generated
	 */
	void setCardsValueLowerBound(int value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueLowerBound <em>Cards Value Lower Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCardsValueLowerBound()
	 * @see #getCardsValueLowerBound()
	 * @see #setCardsValueLowerBound(int)
	 * @generated
	 */
	void unsetCardsValueLowerBound();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueLowerBound <em>Cards Value Lower Bound</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Cards Value Lower Bound</em>' attribute is set.
	 * @see #unsetCardsValueLowerBound()
	 * @see #getCardsValueLowerBound()
	 * @see #setCardsValueLowerBound(int)
	 * @generated
	 */
	boolean isSetCardsValueLowerBound();

	/**
	 * Returns the value of the '<em><b>Cards Value Upper Bound</b></em>' attribute.
	 * The default value is <code>"100"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cards Value Upper Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cards Value Upper Bound</em>' attribute.
	 * @see #isSetCardsValueUpperBound()
	 * @see #unsetCardsValueUpperBound()
	 * @see #setCardsValueUpperBound(int)
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSession_CardsValueUpperBound()
	 * @model default="100" unsettable="true"
	 * @generated
	 */
	int getCardsValueUpperBound();

	/**
	 * Sets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueUpperBound <em>Cards Value Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cards Value Upper Bound</em>' attribute.
	 * @see #isSetCardsValueUpperBound()
	 * @see #unsetCardsValueUpperBound()
	 * @see #getCardsValueUpperBound()
	 * @generated
	 */
	void setCardsValueUpperBound(int value);

	/**
	 * Unsets the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueUpperBound <em>Cards Value Upper Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCardsValueUpperBound()
	 * @see #getCardsValueUpperBound()
	 * @see #setCardsValueUpperBound(int)
	 * @generated
	 */
	void unsetCardsValueUpperBound();

	/**
	 * Returns whether the value of the '{@link edu.ncsu.csc.wolfpoker.jazz.common.model.Session#getCardsValueUpperBound <em>Cards Value Upper Bound</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Cards Value Upper Bound</em>' attribute is set.
	 * @see #unsetCardsValueUpperBound()
	 * @see #getCardsValueUpperBound()
	 * @see #setCardsValueUpperBound(int)
	 * @generated
	 */
	boolean isSetCardsValueUpperBound();

} // Session
