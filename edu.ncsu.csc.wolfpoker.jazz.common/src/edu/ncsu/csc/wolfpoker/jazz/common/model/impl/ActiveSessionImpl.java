/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import com.ibm.team.repository.common.IContributorHandle;

import com.ibm.team.repository.common.model.impl.SimpleItemImpl;

import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSessionHandle;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation;
import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHandle;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Active Session</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl#getAssociatedSession <em>Associated Session</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl#getJoinedParticipants <em>Joined Participants</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl#getItemUnderDiscussion <em>Item Under Discussion</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl#getItemsCompleted <em>Items Completed</em>}</li>
 *   <li>{@link edu.ncsu.csc.wolfpoker.jazz.common.model.impl.ActiveSessionImpl#getLastOperation <em>Last Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActiveSessionImpl extends SimpleItemImpl implements ActiveSession {
	/**
	 * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected int ALL_FLAGS = 0;

	/**
	 * The cached value of the '{@link #getAssociatedSession() <em>Associated Session</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssociatedSession()
	 * @generated
	 * @ordered
	 */
	protected SessionHandle associatedSession;

	/**
	 * The flag representing whether the Associated Session reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int ASSOCIATED_SESSION_ESETFLAG = 1 << 11;

	/**
	 * The cached value of the '{@link #getJoinedParticipants() <em>Joined Participants</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJoinedParticipants()
	 * @generated
	 * @ordered
	 */
	protected EList joinedParticipants;

	/**
	 * The cached value of the '{@link #getItemUnderDiscussion() <em>Item Under Discussion</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItemUnderDiscussion()
	 * @generated
	 * @ordered
	 */
	protected ItemForEstimation itemUnderDiscussion;

	/**
	 * The flag representing whether the Item Under Discussion reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int ITEM_UNDER_DISCUSSION_ESETFLAG = 1 << 12;

	/**
	 * The cached value of the '{@link #getItemsCompleted() <em>Items Completed</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItemsCompleted()
	 * @generated
	 * @ordered
	 */
	protected EList itemsCompleted;

	/**
	 * The default value of the '{@link #getLastOperation() <em>Last Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastOperation()
	 * @generated
	 * @ordered
	 */
	protected static final int LAST_OPERATION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLastOperation() <em>Last Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastOperation()
	 * @generated
	 * @ordered
	 */
	protected int lastOperation = LAST_OPERATION_EDEFAULT;

	/**
	 * The flag representing whether the Last Operation attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected static final int LAST_OPERATION_ESETFLAG = 1 << 13;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int EOFFSET_CORRECTION = JazzPackage.Literals.ACTIVE_SESSION
			.getFeatureID(JazzPackage.Literals.ACTIVE_SESSION__ASSOCIATED_SESSION)
			- JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActiveSessionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClass eStaticClass() {
		return JazzPackage.Literals.ACTIVE_SESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionHandle getAssociatedSession() {
		if (associatedSession != null
				&& ((EObject) associatedSession).eIsProxy()) {
			InternalEObject oldAssociatedSession = (InternalEObject) associatedSession;
			associatedSession = (SessionHandle) eResolveProxy(oldAssociatedSession);
			if (associatedSession != oldAssociatedSession) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION
									+ EOFFSET_CORRECTION, oldAssociatedSession,
							associatedSession));
			}
		}
		return associatedSession;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SessionHandle basicGetAssociatedSession() {
		return associatedSession;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssociatedSession(SessionHandle newAssociatedSession) {
		SessionHandle oldAssociatedSession = associatedSession;
		associatedSession = newAssociatedSession;
		boolean oldAssociatedSessionESet = (ALL_FLAGS & ASSOCIATED_SESSION_ESETFLAG) != 0;
		ALL_FLAGS |= ASSOCIATED_SESSION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION
							+ EOFFSET_CORRECTION, oldAssociatedSession,
					associatedSession, !oldAssociatedSessionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAssociatedSession() {
		SessionHandle oldAssociatedSession = associatedSession;
		boolean oldAssociatedSessionESet = (ALL_FLAGS & ASSOCIATED_SESSION_ESETFLAG) != 0;
		associatedSession = null;
		ALL_FLAGS &= ~ASSOCIATED_SESSION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION
							+ EOFFSET_CORRECTION, oldAssociatedSession, null,
					oldAssociatedSessionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAssociatedSession() {
		return (ALL_FLAGS & ASSOCIATED_SESSION_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getJoinedParticipants() {
		if (joinedParticipants == null) {
			joinedParticipants = new EObjectResolvingEList.Unsettable(
					IContributorHandle.class, this,
					JazzPackage.ACTIVE_SESSION__JOINED_PARTICIPANTS
							+ EOFFSET_CORRECTION);
		}
		return joinedParticipants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetJoinedParticipants() {
		if (joinedParticipants != null)
			((InternalEList.Unsettable) joinedParticipants).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetJoinedParticipants() {
		return joinedParticipants != null
				&& ((InternalEList.Unsettable) joinedParticipants).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ItemForEstimation getItemUnderDiscussion() {
		if (itemUnderDiscussion != null
				&& ((EObject) itemUnderDiscussion).eIsProxy()) {
			InternalEObject oldItemUnderDiscussion = (InternalEObject) itemUnderDiscussion;
			itemUnderDiscussion = (ItemForEstimation) eResolveProxy(oldItemUnderDiscussion);
			if (itemUnderDiscussion != oldItemUnderDiscussion) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION
									+ EOFFSET_CORRECTION,
							oldItemUnderDiscussion, itemUnderDiscussion));
			}
		}
		return itemUnderDiscussion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ItemForEstimation basicGetItemUnderDiscussion() {
		return itemUnderDiscussion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setItemUnderDiscussion(ItemForEstimation newItemUnderDiscussion) {
		ItemForEstimation oldItemUnderDiscussion = itemUnderDiscussion;
		itemUnderDiscussion = newItemUnderDiscussion;
		boolean oldItemUnderDiscussionESet = (ALL_FLAGS & ITEM_UNDER_DISCUSSION_ESETFLAG) != 0;
		ALL_FLAGS |= ITEM_UNDER_DISCUSSION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION
							+ EOFFSET_CORRECTION, oldItemUnderDiscussion,
					itemUnderDiscussion, !oldItemUnderDiscussionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetItemUnderDiscussion() {
		ItemForEstimation oldItemUnderDiscussion = itemUnderDiscussion;
		boolean oldItemUnderDiscussionESet = (ALL_FLAGS & ITEM_UNDER_DISCUSSION_ESETFLAG) != 0;
		itemUnderDiscussion = null;
		ALL_FLAGS &= ~ITEM_UNDER_DISCUSSION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION
							+ EOFFSET_CORRECTION, oldItemUnderDiscussion, null,
					oldItemUnderDiscussionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetItemUnderDiscussion() {
		return (ALL_FLAGS & ITEM_UNDER_DISCUSSION_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public List getItemsCompleted() {
		if (itemsCompleted == null) {
			itemsCompleted = new EObjectContainmentEList.Unsettable(
					ItemForEstimation.class, this,
					JazzPackage.ACTIVE_SESSION__ITEMS_COMPLETED
							+ EOFFSET_CORRECTION);
		}
		return itemsCompleted;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetItemsCompleted() {
		if (itemsCompleted != null)
			((InternalEList.Unsettable) itemsCompleted).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetItemsCompleted() {
		return itemsCompleted != null
				&& ((InternalEList.Unsettable) itemsCompleted).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLastOperation() {
		return lastOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastOperation(int newLastOperation) {
		int oldLastOperation = lastOperation;
		lastOperation = newLastOperation;
		boolean oldLastOperationESet = (ALL_FLAGS & LAST_OPERATION_ESETFLAG) != 0;
		ALL_FLAGS |= LAST_OPERATION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					JazzPackage.ACTIVE_SESSION__LAST_OPERATION
							+ EOFFSET_CORRECTION, oldLastOperation,
					lastOperation, !oldLastOperationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLastOperation() {
		int oldLastOperation = lastOperation;
		boolean oldLastOperationESet = (ALL_FLAGS & LAST_OPERATION_ESETFLAG) != 0;
		lastOperation = LAST_OPERATION_EDEFAULT;
		ALL_FLAGS &= ~LAST_OPERATION_ESETFLAG;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					JazzPackage.ACTIVE_SESSION__LAST_OPERATION
							+ EOFFSET_CORRECTION, oldLastOperation,
					LAST_OPERATION_EDEFAULT, oldLastOperationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLastOperation() {
		return (ALL_FLAGS & LAST_OPERATION_ESETFLAG) != 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ACTIVE_SESSION__ITEMS_COMPLETED:
			return ((InternalEList) getItemsCompleted()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION:
			if (resolve)
				return getAssociatedSession();
			return basicGetAssociatedSession();
		case JazzPackage.ACTIVE_SESSION__JOINED_PARTICIPANTS:
			return getJoinedParticipants();
		case JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION:
			if (resolve)
				return getItemUnderDiscussion();
			return basicGetItemUnderDiscussion();
		case JazzPackage.ACTIVE_SESSION__ITEMS_COMPLETED:
			return getItemsCompleted();
		case JazzPackage.ACTIVE_SESSION__LAST_OPERATION:
			return new Integer(getLastOperation());
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eSet(int featureID, Object newValue) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION:
			setAssociatedSession((SessionHandle) newValue);
			return;
		case JazzPackage.ACTIVE_SESSION__JOINED_PARTICIPANTS:
			getJoinedParticipants().clear();
			getJoinedParticipants().addAll((Collection) newValue);
			return;
		case JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION:
			setItemUnderDiscussion((ItemForEstimation) newValue);
			return;
		case JazzPackage.ACTIVE_SESSION__ITEMS_COMPLETED:
			getItemsCompleted().clear();
			getItemsCompleted().addAll((Collection) newValue);
			return;
		case JazzPackage.ACTIVE_SESSION__LAST_OPERATION:
			setLastOperation(((Integer) newValue).intValue());
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void eUnset(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION:
			unsetAssociatedSession();
			return;
		case JazzPackage.ACTIVE_SESSION__JOINED_PARTICIPANTS:
			unsetJoinedParticipants();
			return;
		case JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION:
			unsetItemUnderDiscussion();
			return;
		case JazzPackage.ACTIVE_SESSION__ITEMS_COMPLETED:
			unsetItemsCompleted();
			return;
		case JazzPackage.ACTIVE_SESSION__LAST_OPERATION:
			unsetLastOperation();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean eIsSet(int featureID) {
		switch (featureID - EOFFSET_CORRECTION) {
		case JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION:
			return isSetAssociatedSession();
		case JazzPackage.ACTIVE_SESSION__JOINED_PARTICIPANTS:
			return isSetJoinedParticipants();
		case JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION:
			return isSetItemUnderDiscussion();
		case JazzPackage.ACTIVE_SESSION__ITEMS_COMPLETED:
			return isSetItemsCompleted();
		case JazzPackage.ACTIVE_SESSION__LAST_OPERATION:
			return isSetLastOperation();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class baseClass) {
		if (baseClass == ActiveSessionHandle.class) {
			switch (baseFeatureID) {
			default:
				return -1;
			}
		}
		if (baseClass == ActiveSession.class) {
			switch (baseFeatureID - EOFFSET_CORRECTION) {
			case JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION:
				return JazzPackage.ACTIVE_SESSION__ASSOCIATED_SESSION
						+ EOFFSET_CORRECTION;
			case JazzPackage.ACTIVE_SESSION__JOINED_PARTICIPANTS:
				return JazzPackage.ACTIVE_SESSION__JOINED_PARTICIPANTS
						+ EOFFSET_CORRECTION;
			case JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION:
				return JazzPackage.ACTIVE_SESSION__ITEM_UNDER_DISCUSSION
						+ EOFFSET_CORRECTION;
			case JazzPackage.ACTIVE_SESSION__ITEMS_COMPLETED:
				return JazzPackage.ACTIVE_SESSION__ITEMS_COMPLETED
						+ EOFFSET_CORRECTION;
			case JazzPackage.ACTIVE_SESSION__LAST_OPERATION:
				return JazzPackage.ACTIVE_SESSION__LAST_OPERATION
						+ EOFFSET_CORRECTION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (lastOperation: "); //$NON-NLS-1$
		if ((ALL_FLAGS & LAST_OPERATION_ESETFLAG) != 0)
			result.append(lastOperation);
		else
			result.append("<unset>"); //$NON-NLS-1$
		result.append(')');
		return result.toString();
	}

} //ActiveSessionImpl
