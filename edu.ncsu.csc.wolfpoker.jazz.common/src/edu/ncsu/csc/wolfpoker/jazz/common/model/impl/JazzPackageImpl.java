/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model.impl;

import com.ibm.team.repository.common.model.RepositoryPackage;

import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSessionHandle;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation;
import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzFactory;
import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHandle;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Vote;
import edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class JazzPackageImpl extends EPackageImpl implements JazzPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionHandleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionHistoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sessionHistoryHandleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeSessionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass activeSessionHandleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass voteEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass itemForEstimationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass voteHistoryItemEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private JazzPackageImpl() {
		super(eNS_URI, JazzFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link JazzPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static JazzPackage init() {
		if (isInited)
			return (JazzPackage) EPackage.Registry.INSTANCE
					.getEPackage(JazzPackage.eNS_URI);

		// Obtain or create and register package
		JazzPackageImpl theJazzPackage = (JazzPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof JazzPackageImpl ? EPackage.Registry.INSTANCE
				.get(eNS_URI) : new JazzPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		RepositoryPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theJazzPackage.createPackageContents();

		// Initialize created meta-data
		theJazzPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theJazzPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(JazzPackage.eNS_URI, theJazzPackage);
		return theJazzPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSession() {
		return sessionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSession_Text() {
		return (EAttribute) sessionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_Participants() {
		return (EReference) sessionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_CreatedBy() {
		return (EReference) sessionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_SessionCoordinator() {
		return (EReference) sessionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSession_DateCreated() {
		return (EAttribute) sessionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSession_DateLastModified() {
		return (EAttribute) sessionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSession_Description() {
		return (EAttribute) sessionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_Items() {
		return (EReference) sessionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_ProjectArea() {
		return (EReference) sessionEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSession_SessionHistory() {
		return (EReference) sessionEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSession_Active() {
		return (EAttribute) sessionEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSession_Archived() {
		return (EAttribute) sessionEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSession_CardsValueLowerBound() {
		return (EAttribute) sessionEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSession_CardsValueUpperBound() {
		return (EAttribute) sessionEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionHandle() {
		return sessionHandleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionHistory() {
		return sessionHistoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSessionHistory_Date() {
		return (EAttribute) sessionHistoryEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSessionHistory_HistoryType() {
		return (EAttribute) sessionHistoryEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSessionHistory_User() {
		return (EReference) sessionHistoryEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSessionHistory_OldValue() {
		return (EAttribute) sessionHistoryEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSessionHistory_NewValue() {
		return (EAttribute) sessionHistoryEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSessionHistoryHandle() {
		return sessionHistoryHandleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveSession() {
		return activeSessionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveSession_AssociatedSession() {
		return (EReference) activeSessionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveSession_JoinedParticipants() {
		return (EReference) activeSessionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveSession_ItemUnderDiscussion() {
		return (EReference) activeSessionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActiveSession_ItemsCompleted() {
		return (EReference) activeSessionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActiveSession_LastOperation() {
		return (EAttribute) activeSessionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActiveSessionHandle() {
		return activeSessionHandleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVote() {
		return voteEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVote_Participant() {
		return (EReference) voteEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVote_Vote() {
		return (EAttribute) voteEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getItemForEstimation() {
		return itemForEstimationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getItemForEstimation_Item() {
		return (EReference) itemForEstimationEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getItemForEstimation_CurrentVotes() {
		return (EReference) itemForEstimationEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getItemForEstimation_VoteHistory() {
		return (EReference) itemForEstimationEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getItemForEstimation_SavedVote() {
		return (EAttribute) itemForEstimationEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getItemForEstimation_RoundNo() {
		return (EAttribute) itemForEstimationEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVoteHistoryItem() {
		return voteHistoryItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVoteHistoryItem_Session() {
		return (EAttribute) voteHistoryItemEClass.getEStructuralFeatures().get(
				0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVoteHistoryItem_Votes() {
		return (EReference) voteHistoryItemEClass.getEStructuralFeatures().get(
				1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVoteHistoryItem_VoteRound() {
		return (EAttribute) voteHistoryItemEClass.getEStructuralFeatures().get(
				2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JazzFactory getJazzFactory() {
		return (JazzFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		sessionEClass = createEClass(SESSION);
		createEAttribute(sessionEClass, SESSION__TEXT);
		createEReference(sessionEClass, SESSION__PARTICIPANTS);
		createEReference(sessionEClass, SESSION__CREATED_BY);
		createEReference(sessionEClass, SESSION__SESSION_COORDINATOR);
		createEAttribute(sessionEClass, SESSION__DATE_CREATED);
		createEAttribute(sessionEClass, SESSION__DATE_LAST_MODIFIED);
		createEAttribute(sessionEClass, SESSION__DESCRIPTION);
		createEReference(sessionEClass, SESSION__ITEMS);
		createEReference(sessionEClass, SESSION__PROJECT_AREA);
		createEReference(sessionEClass, SESSION__SESSION_HISTORY);
		createEAttribute(sessionEClass, SESSION__ACTIVE);
		createEAttribute(sessionEClass, SESSION__ARCHIVED);
		createEAttribute(sessionEClass, SESSION__CARDS_VALUE_LOWER_BOUND);
		createEAttribute(sessionEClass, SESSION__CARDS_VALUE_UPPER_BOUND);

		sessionHandleEClass = createEClass(SESSION_HANDLE);

		sessionHistoryEClass = createEClass(SESSION_HISTORY);
		createEAttribute(sessionHistoryEClass, SESSION_HISTORY__DATE);
		createEAttribute(sessionHistoryEClass, SESSION_HISTORY__HISTORY_TYPE);
		createEReference(sessionHistoryEClass, SESSION_HISTORY__USER);
		createEAttribute(sessionHistoryEClass, SESSION_HISTORY__OLD_VALUE);
		createEAttribute(sessionHistoryEClass, SESSION_HISTORY__NEW_VALUE);

		sessionHistoryHandleEClass = createEClass(SESSION_HISTORY_HANDLE);

		activeSessionEClass = createEClass(ACTIVE_SESSION);
		createEReference(activeSessionEClass,
				ACTIVE_SESSION__ASSOCIATED_SESSION);
		createEReference(activeSessionEClass,
				ACTIVE_SESSION__JOINED_PARTICIPANTS);
		createEReference(activeSessionEClass,
				ACTIVE_SESSION__ITEM_UNDER_DISCUSSION);
		createEReference(activeSessionEClass, ACTIVE_SESSION__ITEMS_COMPLETED);
		createEAttribute(activeSessionEClass, ACTIVE_SESSION__LAST_OPERATION);

		activeSessionHandleEClass = createEClass(ACTIVE_SESSION_HANDLE);

		voteEClass = createEClass(VOTE);
		createEReference(voteEClass, VOTE__PARTICIPANT);
		createEAttribute(voteEClass, VOTE__VOTE);

		itemForEstimationEClass = createEClass(ITEM_FOR_ESTIMATION);
		createEReference(itemForEstimationEClass, ITEM_FOR_ESTIMATION__ITEM);
		createEReference(itemForEstimationEClass,
				ITEM_FOR_ESTIMATION__CURRENT_VOTES);
		createEReference(itemForEstimationEClass,
				ITEM_FOR_ESTIMATION__VOTE_HISTORY);
		createEAttribute(itemForEstimationEClass,
				ITEM_FOR_ESTIMATION__SAVED_VOTE);
		createEAttribute(itemForEstimationEClass, ITEM_FOR_ESTIMATION__ROUND_NO);

		voteHistoryItemEClass = createEClass(VOTE_HISTORY_ITEM);
		createEAttribute(voteHistoryItemEClass, VOTE_HISTORY_ITEM__SESSION);
		createEReference(voteHistoryItemEClass, VOTE_HISTORY_ITEM__VOTES);
		createEAttribute(voteHistoryItemEClass, VOTE_HISTORY_ITEM__VOTE_ROUND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RepositoryPackage theRepositoryPackage = (RepositoryPackage) EPackage.Registry.INSTANCE
				.getEPackage(RepositoryPackage.eNS_URI);

		// Add supertypes to classes
		sessionEClass.getESuperTypes().add(theRepositoryPackage.getAuditable());
		sessionEClass.getESuperTypes().add(this.getSessionHandle());
		sessionHandleEClass.getESuperTypes().add(
				theRepositoryPackage.getAuditableHandle());
		sessionHistoryEClass.getESuperTypes().add(
				theRepositoryPackage.getSimpleItem());
		sessionHistoryEClass.getESuperTypes().add(
				this.getSessionHistoryHandle());
		sessionHistoryHandleEClass.getESuperTypes().add(
				theRepositoryPackage.getSimpleItemHandle());
		activeSessionEClass.getESuperTypes().add(
				theRepositoryPackage.getSimpleItem());
		activeSessionEClass.getESuperTypes().add(this.getActiveSessionHandle());
		activeSessionHandleEClass.getESuperTypes().add(
				theRepositoryPackage.getSimpleItemHandle());
		voteEClass.getESuperTypes().add(theRepositoryPackage.getHelper());
		itemForEstimationEClass.getESuperTypes().add(
				theRepositoryPackage.getHelper());
		voteHistoryItemEClass.getESuperTypes().add(
				theRepositoryPackage.getHelper());

		// Initialize classes and features; add operations and parameters
		initEClass(
				sessionEClass,
				Session.class,
				"Session", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(
				getSession_Text(),
				ecorePackage.getEString(),
				"text", "yadda", 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(
				getSession_Participants(),
				theRepositoryPackage.getContributorHandleFacade(),
				null,
				"participants", null, 0, -1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSession_CreatedBy(),
				theRepositoryPackage.getContributorHandleFacade(),
				null,
				"createdBy", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSession_SessionCoordinator(),
				theRepositoryPackage.getContributorHandleFacade(),
				null,
				"sessionCoordinator", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getSession_DateCreated(),
				ecorePackage.getELong(),
				"dateCreated", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getSession_DateLastModified(),
				ecorePackage.getELong(),
				"dateLastModified", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getSession_Description(),
				ecorePackage.getEString(),
				"description", "yadda", 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEReference(
				getSession_Items(),
				theRepositoryPackage.getItemHandleFacade(),
				null,
				"items", null, 0, -1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSession_ProjectArea(),
				theRepositoryPackage.getItemHandleFacade(),
				null,
				"projectArea", null, 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSession_SessionHistory(),
				this.getSessionHistoryHandle(),
				null,
				"sessionHistory", null, 0, -1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getSession_Active(),
				ecorePackage.getEBoolean(),
				"active", "false", 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(
				getSession_Archived(),
				ecorePackage.getEBoolean(),
				"archived", "false", 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(
				getSession_CardsValueLowerBound(),
				ecorePackage.getEInt(),
				"cardsValueLowerBound", "0", 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$
		initEAttribute(
				getSession_CardsValueUpperBound(),
				ecorePackage.getEInt(),
				"cardsValueUpperBound", "100", 0, 1, Session.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$ //$NON-NLS-2$

		initEClass(
				sessionHandleEClass,
				SessionHandle.class,
				"SessionHandle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(
				sessionHistoryEClass,
				SessionHistory.class,
				"SessionHistory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(
				getSessionHistory_Date(),
				ecorePackage.getELong(),
				"date", null, 0, 1, SessionHistory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getSessionHistory_HistoryType(),
				ecorePackage.getEString(),
				"historyType", null, 0, 1, SessionHistory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getSessionHistory_User(),
				theRepositoryPackage.getContributorHandleFacade(),
				null,
				"user", null, 0, 1, SessionHistory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getSessionHistory_OldValue(),
				ecorePackage.getEString(),
				"oldValue", null, 0, 1, SessionHistory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getSessionHistory_NewValue(),
				ecorePackage.getEString(),
				"newValue", null, 0, 1, SessionHistory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(
				sessionHistoryHandleEClass,
				SessionHistoryHandle.class,
				"SessionHistoryHandle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(
				activeSessionEClass,
				ActiveSession.class,
				"ActiveSession", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getActiveSession_AssociatedSession(),
				this.getSessionHandle(),
				null,
				"associatedSession", null, 0, 1, ActiveSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getActiveSession_JoinedParticipants(),
				theRepositoryPackage.getContributorHandleFacade(),
				null,
				"joinedParticipants", null, 0, -1, ActiveSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getActiveSession_ItemUnderDiscussion(),
				this.getItemForEstimation(),
				null,
				"itemUnderDiscussion", null, 0, 1, ActiveSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getActiveSession_ItemsCompleted(),
				this.getItemForEstimation(),
				null,
				"itemsCompleted", null, 0, -1, ActiveSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getActiveSession_LastOperation(),
				ecorePackage.getEInt(),
				"lastOperation", null, 0, 1, ActiveSession.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(
				activeSessionHandleEClass,
				ActiveSessionHandle.class,
				"ActiveSessionHandle", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$

		initEClass(
				voteEClass,
				Vote.class,
				"Vote", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getVote_Participant(),
				theRepositoryPackage.getContributorHandleFacade(),
				null,
				"participant", null, 0, 1, Vote.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getVote_Vote(),
				ecorePackage.getEInt(),
				"vote", null, 0, 1, Vote.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(
				itemForEstimationEClass,
				ItemForEstimation.class,
				"ItemForEstimation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEReference(
				getItemForEstimation_Item(),
				theRepositoryPackage.getItemHandleFacade(),
				null,
				"item", null, 0, 1, ItemForEstimation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getItemForEstimation_CurrentVotes(),
				this.getVote(),
				null,
				"currentVotes", null, 0, -1, ItemForEstimation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getItemForEstimation_VoteHistory(),
				this.getVoteHistoryItem(),
				null,
				"voteHistory", null, 0, -1, ItemForEstimation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getItemForEstimation_SavedVote(),
				ecorePackage.getEInt(),
				"savedVote", null, 0, 1, ItemForEstimation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getItemForEstimation_RoundNo(),
				ecorePackage.getEInt(),
				"roundNo", null, 0, 1, ItemForEstimation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		initEClass(
				voteHistoryItemEClass,
				VoteHistoryItem.class,
				"VoteHistoryItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS); //$NON-NLS-1$
		initEAttribute(
				getVoteHistoryItem_Session(),
				theRepositoryPackage.getUUID(),
				"session", null, 0, 1, VoteHistoryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEReference(
				getVoteHistoryItem_Votes(),
				this.getVote(),
				null,
				"votes", null, 0, -1, VoteHistoryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$
		initEAttribute(
				getVoteHistoryItem_VoteRound(),
				ecorePackage.getEInt(),
				"voteRound", null, 0, 1, VoteHistoryItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED); //$NON-NLS-1$

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// teamPackage
		createTeamPackageAnnotations();
		// queryableProperty
		createQueryablePropertyAnnotations();
		// teamAttribute
		createTeamAttributeAnnotations();
		// com.ibm.team.codegen.helperTypeMarker
		createComAnnotations();
	}

	/**
	 * Initializes the annotations for <b>teamPackage</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createTeamPackageAnnotations() {
		String source = "teamPackage"; //$NON-NLS-1$		
		addAnnotation(this, source, new String[] {
				"clientBasePackage", "edu.ncsu.csc.wolfpoker", //$NON-NLS-1$ //$NON-NLS-2$
				"clientPackageSuffix", "common.model" //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

	/**
	 * Initializes the annotations for <b>queryableProperty</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createQueryablePropertyAnnotations() {
		String source = "queryableProperty"; //$NON-NLS-1$			
		addAnnotation(getSession_Text(), source, new String[] {
				"unique", "false" //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSession_CreatedBy(), source, new String[] {
				"unique", "false" //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSession_SessionCoordinator(), source, new String[] {
				"unique", "false" //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSession_Description(), source, new String[] {
				"unique", "false" //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSession_Items(), source, new String[] {
				"unique", "false" //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSession_ProjectArea(), source, new String[] {
				"unique", "false" //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getActiveSession_AssociatedSession(), source,
				new String[] { "unique", "false" //$NON-NLS-1$ //$NON-NLS-2$
				});
	}

	/**
	 * Initializes the annotations for <b>teamAttribute</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createTeamAttributeAnnotations() {
		String source = "teamAttribute"; //$NON-NLS-1$				
		addAnnotation(getSession_Text(), source, new String[] {
				"dbStringSize", "SMALL" //$NON-NLS-1$ //$NON-NLS-2$
		});
		addAnnotation(getSession_Description(), source, new String[] {
				"dbStringSize", "SMALL" //$NON-NLS-1$ //$NON-NLS-2$
		});
	}

	/**
	 * Initializes the annotations for <b>com.ibm.team.codegen.helperTypeMarker</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createComAnnotations() {
		String source = "com.ibm.team.codegen.helperTypeMarker"; //$NON-NLS-1$												
		addAnnotation(voteEClass, source, new String[] {});
		addAnnotation(itemForEstimationEClass, source, new String[] {});
		addAnnotation(voteHistoryItemEClass, source, new String[] {});
	}

} //JazzPackageImpl
