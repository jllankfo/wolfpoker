/**
 * Licensed Materials - Property of IBM
 * (c) Copyright IBM Corporation 2008. All Rights Reserved.
 * 
 * Note to U.S. Government Users Restricted Rights:
 * Use, duplication or disclosure restricted by GSA ADP Schedule
 * Contract with IBM Corp.
 */
package edu.ncsu.csc.wolfpoker.jazz.common.model;

import com.ibm.team.repository.common.model.SimpleItemHandle;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Session History Handle</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ncsu.csc.wolfpoker.jazz.common.model.JazzPackage#getSessionHistoryHandle()
 * @model
 * @generated
 */
public interface SessionHistoryHandle extends SimpleItemHandle {
} // SessionHistoryHandle
