package edu.ncsu.csc.wolfpoker.jazz.service;

import com.ibm.team.repository.common.IContext;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.repository.common.UUID;
import com.ibm.team.repository.service.AbstractRepositoryComponentInitializer;
import com.ibm.team.repository.service.IRepositoryInitializerContext;

import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class WolfPokerInitializer extends
		AbstractRepositoryComponentInitializer {

	public void initializeNewRepository(
			IRepositoryInitializerContext initializerContext)
			throws TeamRepositoryException {
		
		UUID uuid = UUID.generate();
		Session session = (Session) IWolfPokerService.PLANNING_POKER_SESSION_TYPE.createItem();
		session.setText("Test Session");
		session.setContextId(IContext.PUBLIC);
		initializerContext.addNewAuditable(session, uuid);
	}

}
