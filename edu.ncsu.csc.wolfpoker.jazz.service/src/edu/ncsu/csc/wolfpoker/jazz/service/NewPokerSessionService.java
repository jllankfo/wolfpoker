package edu.ncsu.csc.wolfpoker.jazz.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;

import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.process.internal.common.query.BaseProjectAreaQueryModel;
import com.ibm.team.process.internal.common.query.BaseProjectAreaQueryModel.ProjectAreaQueryModel;
import com.ibm.team.repository.common.IContext;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.IItem;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.repository.common.UUID;
import com.ibm.team.repository.common.model.Item;
import com.ibm.team.repository.common.model.ItemHandle;
import com.ibm.team.repository.common.model.query.BaseContributorQueryModel;
import com.ibm.team.repository.common.model.query.BaseContributorQueryModel.ContributorQueryModel;
import com.ibm.team.repository.common.query.IItemQuery;
import com.ibm.team.repository.common.query.IItemQueryPage;
import com.ibm.team.repository.common.service.IQueryService;
import com.ibm.team.repository.service.AbstractService;
import com.ibm.team.repository.service.IMailerService;
import com.ibm.team.repository.service.IRepositoryItemService;
import com.ibm.team.repository.service.MailSender;
import com.ibm.team.workitem.common.internal.model.query.BaseWorkItemQueryModel;
import com.ibm.team.workitem.common.internal.model.query.BaseWorkItemQueryModel.WorkItemQueryModel;
import com.ibm.team.workitem.common.model.AttributeType;
import com.ibm.team.workitem.common.model.AttributeTypes;
import com.ibm.team.workitem.common.model.IAttribute;
import com.ibm.team.workitem.common.model.IEnumeration;
import com.ibm.team.workitem.common.model.ILiteral;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.Identifier;
import com.ibm.team.workitem.service.IWorkItemServer;

import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation;
import edu.ncsu.csc.wolfpoker.jazz.common.model.JazzFactory;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Vote;
import edu.ncsu.csc.wolfpoker.jazz.common.model.VoteHistoryItem;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseActiveSessionQueryModel;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseActiveSessionQueryModel.ActiveSessionQueryModel;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseSessionQueryModel;
import edu.ncsu.csc.wolfpoker.jazz.common.model.query.BaseSessionQueryModel.SessionQueryModel;

public class NewPokerSessionService extends AbstractService implements
		IWolfPokerService {

	public synchronized Session postNewSession(SessionParameters params)
			throws TeamRepositoryException {

		Session session = (Session) PLANNING_POKER_SESSION_TYPE.createItem();
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		session.setItemId(UUID.generate());
		long time = (new Date()).getTime();

		session.setDateCreated(time);
		List historyList = session.getSessionHistory();
		addHistory(params, session, time, repositoryItemService,
				SessionHistoryChanges.CREATED);

		return saveSession(params, session, time, true);
	}

	public synchronized void postDeleteSession(UUIDParam params)
			throws TeamRepositoryException {

		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		repositoryItemService.deleteItem(getSessionFromRepository(params.uuid));
	}

	public synchronized Session postSaveSession(SaveSessionParameters params)
			throws TeamRepositoryException {
		SessionParameters sessionParameters = new SessionParameters();
		sessionParameters.createrUserID = params.createrUserID;
		sessionParameters.description = params.description;
		sessionParameters.itemsUUID = params.itemsID;
		sessionParameters.name = params.name;
		sessionParameters.participantsUserID = params.participantsUserID;
		sessionParameters.projectAreaName = params.projectArea;
		sessionParameters.sessionCoordinatorUserID = params.sessionCoordinatorUserID;

		sessionParameters.cardsValuelowerBound = params.cardsValuelowerBound;
		sessionParameters.cardsValueUpperBound = params.cardsValueUpperBound;

		Session session = getSessionFromRepository(params.sessionID);
		session = (Session) session.getWorkingCopy();

		// //TODO remove after test
		// try {
		// System.err.println("Execute email test");
		// notifySessionChanges(null,session);
		// } catch (Exception e) {
		// java.io.FileWriter writer;
		// try {
		// writer = new java.io.FileWriter("myfile.txt");
		// java.io.BufferedWriter out = new java.io.BufferedWriter(writer);
		// out.write(e.getMessage());
		// out.close();
		// } catch (IOException e1) {
		// // TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		//			
		// }

		// **********************

		return saveSession(sessionParameters, session, (new Date()).getTime(),
				false);
	}

	private Session getSessionFromRepository(String sessionUUID)
			throws TeamRepositoryException {
		IQueryService queryService = getService(IQueryService.class);
		SessionQueryModel queryModel = BaseSessionQueryModel.SessionQueryModel.ROOT;
		IItemQuery itemQuery = IItemQuery.FACTORY.newInstance(queryModel);
		itemQuery.filter(queryModel.itemId()._eq(itemQuery.newUUIDArg()));
		IItemQueryPage itemQueryPage = queryService.queryItems(itemQuery,
				new Object[] { UUID.valueOf(sessionUUID) }, 1);

		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		IItem[] items = repositoryItemService.fetchItems(itemQueryPage
				.handlesAsArray(), null);

		return (Session) items[0];
	}

	private ActiveSession getAssociatedActiveSession(String sessionUUID)
			throws TeamRepositoryException {
		IQueryService queryService = getService(IQueryService.class);
		ActiveSessionQueryModel queryModel = BaseActiveSessionQueryModel.ActiveSessionQueryModel.ROOT;

		IItemQuery itemQuery = IItemQuery.FACTORY.newInstance(queryModel);
		itemQuery.filter(queryModel.associatedSession().itemId()._eq(
				itemQuery.newUUIDArg()));
		IItemQueryPage itemQueryPage = queryService.queryItems(itemQuery,
				new Object[] { UUID.valueOf(sessionUUID) }, 1);

		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		IItem[] items = repositoryItemService.fetchItems(itemQueryPage
				.handlesAsArray(), null);

		return (ActiveSession) items[0];
	}

	private ActiveSession getActiveSession(String activeSeessionUUID)
			throws TeamRepositoryException {
		IQueryService queryService = getService(IQueryService.class);
		ActiveSessionQueryModel queryModel = BaseActiveSessionQueryModel.ActiveSessionQueryModel.ROOT;

		IItemQuery itemQuery = IItemQuery.FACTORY.newInstance(queryModel);
		itemQuery.filter(queryModel.itemId()._eq(itemQuery.newUUIDArg()));
		IItemQueryPage itemQueryPage = queryService.queryItems(itemQuery,
				new Object[] { UUID.valueOf(activeSeessionUUID) }, 1);

		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		IItem[] items = repositoryItemService.fetchItems(itemQueryPage
				.handlesAsArray(), null);
		if (items.length == 1)
			return (ActiveSession) items[0];
		else
			return null;
	}

	private Session saveSession(SessionParameters params, Session session,
			long time, boolean isNew) throws TeamRepositoryException {

		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		if (!isNew && !session.getText().equals(params.name)) {
			addHistory(params, session, time, repositoryItemService,
					SessionHistoryChanges.NAME);
		}
		session.setText(params.name);

		session.setCreatedBy(getContributorByID(params.createrUserID));

		/* set the cards values' lower and upper bound */
		session.setCardsValueLowerBound(params.cardsValuelowerBound);
		session.setCardsValueUpperBound(params.cardsValueUpperBound);

		if (!isNew
				&& !params.sessionCoordinatorUserID
						.equalsIgnoreCase(((IContributor) repositoryItemService
								.fetchItem(session.getSessionCoordinator(),
										null)).getUserId())) {
			addHistory(params, session, time, repositoryItemService,
					SessionHistoryChanges.COORDINATOR);
		}
		session
				.setSessionCoordinator(getContributorByID(params.sessionCoordinatorUserID));

		session.setDateLastModified(time);
		if (!isNew
				&& !params.description.equalsIgnoreCase(session
						.getDescription())) {
			addHistory(params, session, time, repositoryItemService,
					SessionHistoryChanges.DESCRIPTION);
		}
		session.setDescription(params.description);

		List<IContributor> participants = session.getParticipants();
		if (!isNew && participants.size() != params.participantsUserID.length) {
			addHistory(params, session, time, repositoryItemService,
					SessionHistoryChanges.PARTICIPANTS);
		}
		participants.clear();
		for (int i = 0; i < params.participantsUserID.length; i++) {
			participants.add(getContributorByID(params.participantsUserID[i]));
		}

		session.setProjectArea(getProjectAreaByName(params.projectAreaName));

		List<Item> itemsToBeEstimated = session.getItems();
		if (!isNew && itemsToBeEstimated.size() != params.itemsUUID.length) {
			addHistory(params, session, time, repositoryItemService,
					SessionHistoryChanges.ITEMS);
		}
		itemsToBeEstimated.clear();

		for (int i = 0; i < params.itemsUUID.length; i++) {

			itemsToBeEstimated
					.add((Item) getWorkItemByUUID(params.itemsUUID[i]));
		}
		session.setContextId(IContext.PUBLIC);
		session = repositoryItemService.saveItem(session);

		// notify session changes to the user
		//TODO disable not implement function
		/*
		try {
			notifySessionChanges(params, session);
		} catch (MessagingException e) {
			// write to a file
			try {
				e.printStackTrace();
				java.io.FileWriter writer = new java.io.FileWriter("myfile.txt");
				java.io.BufferedWriter out = new java.io.BufferedWriter(writer);
				out.write(e.getMessage());
				out.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		*/

		return session;
	}

	/**
	 * TODO Test email functionality on server
	 * 
	 * @param params
	 * @param session
	 * @throws TeamRepositoryException
	 * @throws MessagingException
	 */
	private void notifySessionChanges(SessionParameters params, Session session)
			throws TeamRepositoryException, MessagingException {
		/*
		 * //compare params and session variables to determine whether any
		 * user(s) have been added or deleted List<IContributor>
		 * oldContributorList = session.getParticipants(); String[]
		 * newContributorList = (String[])params.participantsUserID.clone();
		 * System.arraycopy(params.participantsUserID, 0, newContributorList, 0,
		 * params.participantsUserID.length); ArrayList<Integer> indicesToRemove
		 * = new ArrayList<Integer>(); //List<IContributor>
		 * 
		 * //loop through both old and new participant lists and remove common
		 * items. That way, new items will be left in the newList //and the old
		 * ones in the oldList for (int i=0; i<params.participantsUserID.length;
		 * i++) { if
		 * (oldContributorList.contains(getContributorByID(newContributorList
		 * [i]))) //need to cast this properly {
		 * oldContributorList.remove(getContributorByID(newContributorList[i]));
		 * //remove item from this list too indicesToRemove.add((Integer)i); } }
		 * 
		 * for (int i=0; i<indicesToRemove.size();i++) {
		 * //newContributorList.[indicesToRemove[i]] }
		 */
		// send email to the people in order to notify people
		IMailerService mailerService = getService(IMailerService.class);
		MailSender sender = mailerService.getDefaultSender();

		String emailaddress = "kstsai@ncsu.edu";
		String message = "test:message";
		String subject = "test:subject";
		mailerService.sendMail(sender, emailaddress, subject, message, null);
		// System.err.println("End email");
		// mailerService.sendMail(mailerService.getDefaultSender(),
		// "raza.abbas@gmail.com", "Jazz Test Email", "Test", "rsyed@ncsu.edu");

	}

	private void addHistory(SessionParameters params, Session session,
			long time, IRepositoryItemService repositoryItemService,
			String historyType) throws TeamRepositoryException {

		List historyList = session.getSessionHistory();
		SessionHistory history = JazzFactory.eINSTANCE.createSessionHistory();
		history = (SessionHistory) history.getWorkingCopy();
		history.setDate(time);
		history.setItemId(UUID.generate());
		history.setUser(getAuthenticatedContributor());
		history.setHistoryType(historyType);

		if (historyType.equalsIgnoreCase(SessionHistoryChanges.NAME)) {
			history.setOldValue(session.getText());
			history.setNewValue(params.name);
		} else if (historyType
				.equalsIgnoreCase(SessionHistoryChanges.COORDINATOR)) {
			history.setOldValue(((IContributor) repositoryItemService
					.fetchItem(session.getSessionCoordinator(), null))
					.getName());
			history.setNewValue(getContributorByID(
					params.sessionCoordinatorUserID).getName());
		} else if (historyType
				.equalsIgnoreCase(SessionHistoryChanges.DESCRIPTION)) {
			history.setOldValue(session.getDescription());
			history.setNewValue(params.description);
		} else if (historyType
				.equalsIgnoreCase(SessionHistoryChanges.PARTICIPANTS)) {
			List<IContributor> participants = session.getParticipants();
			String value = "";
			for (Iterator iterator = participants.iterator(); iterator
					.hasNext();) {
				IContributor contributor = (IContributor) repositoryItemService
						.fetchItem((IItemHandle) iterator.next(), null);
				value += contributor.getName() + ", ";
			}
			history.setOldValue(value);
			value = "";
			for (int i = 0; i < params.participantsUserID.length; i++) {
				IContributor contributor = getContributorByID(params.participantsUserID[i]);
				value += contributor.getName() + ", ";
			}
			history.setNewValue(value);
		} else if (historyType.equalsIgnoreCase(SessionHistoryChanges.ITEMS)) {
			List participants = session.getItems();

			String value = "";
			for (Iterator iterator = participants.iterator(); iterator
					.hasNext();) {
				IWorkItem item = (IWorkItem) repositoryItemService.fetchItem(
						(IItemHandle) iterator.next(), null);
				value += item.getId() + ", ";
			}
			history.setOldValue(value);
			value = "";
			for (int i = 0; i < params.itemsUUID.length; i++) {
				IWorkItem workItem = getWorkItemByUUID(params.itemsUUID[i]);
				value += workItem.getId() + ", ";
			}
			history.setNewValue(value);
		}else if (historyType.equalsIgnoreCase(SessionHistoryChanges.VOTE))
		{
			IWorkItem workItem = getWorkItemByUUID(params.itemsUUID[0]);
			workItem.getWorkingCopy();
			history.setHistoryType(workItem.getHTMLSummary().toString() + " vote");
			history.setOldValue(params.oldVote+"");
			history.setNewValue(params.vote+"");
			history.setContextId(IContext.PUBLIC);
			repositoryItemService.saveItem(history);
			historyList.add(history);
			repositoryItemService.saveItem(session);
			return;
		}
		history.setContextId(IContext.PUBLIC);
		repositoryItemService.saveItem(history);
		historyList.add(history);
	}

	private IWorkItem getWorkItemByUUID(String uuid)
			throws TeamRepositoryException {
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		IQueryService queryService = getService(IQueryService.class);

		WorkItemQueryModel workItemQueryModel = BaseWorkItemQueryModel.WorkItemQueryModel.ROOT;
		IItemQuery workItemQuery = IItemQuery.FACTORY
				.newInstance(workItemQueryModel);
		workItemQuery.filter(workItemQueryModel.itemId()._eq(
				workItemQuery.newUUIDArg()));

		IItemQueryPage participantsIDQueryPage = queryService.queryItems(
				workItemQuery, new Object[] { UUID.valueOf(uuid) },
				IQueryService.ITEM_QUERY_MAX_PAGE_SIZE);
		IItem[] sessionItems = repositoryItemService.fetchItems(
				participantsIDQueryPage.handlesAsArray(), null);

		return (IWorkItem) sessionItems[0];
	}

	private IProjectArea getProjectAreaByName(String name)
			throws TeamRepositoryException {
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		IQueryService queryService = getService(IQueryService.class);

		ProjectAreaQueryModel projectAreaQueryModel = BaseProjectAreaQueryModel.ProjectAreaQueryModel.ROOT;
		IItemQuery projectQuery = IItemQuery.FACTORY
				.newInstance(projectAreaQueryModel);
		projectQuery.filter(projectAreaQueryModel.name()._eq(
				(projectQuery.newStringArg())));
		IItemQueryPage projectQueryPage = queryService.queryItems(projectQuery,
				new Object[] { name }, 1);
		IItem[] projectArea = repositoryItemService.fetchItems(projectQueryPage
				.handlesAsArray(), null);

		return (IProjectArea) projectArea[0];
	}

	private IContributor getContributorByID(String userID)
			throws TeamRepositoryException {

		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		IQueryService queryService = getService(IQueryService.class);

		ContributorQueryModel queryModel = BaseContributorQueryModel.ContributorQueryModel.ROOT;
		IItemQuery itemQuery = IItemQuery.FACTORY.newInstance(queryModel);
		itemQuery.filter(queryModel.userId()._eq(itemQuery.newStringArg()));
		IItemQueryPage itemQueryPage = queryService
				.queryItems(itemQuery, new Object[] { userID },
						IQueryService.ITEM_QUERY_MAX_PAGE_SIZE);
		IItem[] items = repositoryItemService.fetchItems(itemQueryPage
				.handlesAsArray(), null);

		return (IContributor) items[0];
	}

	public synchronized Session[] getAllSessionsByProjectArea(
			SessionsByProjectAreaParam param) throws TeamRepositoryException {
		IProjectArea projectArea = getProjectAreaByName(param.projectName);

		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		IQueryService queryService = getService(IQueryService.class);

		SessionQueryModel queryModel = BaseSessionQueryModel.SessionQueryModel.ROOT;
		IItemQuery itemQuery = IItemQuery.FACTORY.newInstance(queryModel);
		itemQuery.filter(queryModel.projectArea().itemId()._eq(
				itemQuery.newUUIDArg()));

		IItemQueryPage itemQueryPage = queryService.queryItems(itemQuery,
				new Object[] { projectArea.getItemId() },
				IQueryService.ITEM_QUERY_MAX_PAGE_SIZE);

		IItem[] items = repositoryItemService.fetchItems(itemQueryPage
				.handlesAsArray(), null);
		ArrayList<Session> sessions = new ArrayList<Session>();
		for (IItem item : items) {
			if (item instanceof Session) {
				sessions.add((Session) item);
			}
		}

		return sessions.toArray(new Session[sessions.size()]);
	}

	//public synchronized VoteHistoryItem[] getHistoryByWorkItem(UUIDParam params)
	//{
	//	IRepositoryItemService repositoryService = getService(IRepositoryItemService.class);
	//	IQueryService queryService = getService(IQueryService.class);
	//	
	//}
	public synchronized ActiveSession postStartSession(UUIDParam params)
			throws TeamRepositoryException {

		// TODO to put a check for coordinator
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		Session session = getSessionFromRepository(params.uuid);
		session = (Session) session.getWorkingCopy();
		if (!session.isActive()) {
			ActiveSession activeSession = JazzFactory.eINSTANCE
					.createActiveSession();
			activeSession = (ActiveSession) activeSession.getWorkingCopy();
			activeSession.setAssociatedSession(session);
			activeSession.setItemId(UUID.generate());
			activeSession.setLastOperation(LastActiveSessionOperation.CREATED);

			IContributorHandle contributor = getAuthenticatedContributor();
			List joinedParticipants = activeSession.getJoinedParticipants();
			joinedParticipants.add(contributor);

			activeSession.setContextId(IContext.PUBLIC);
			activeSession = repositoryItemService.saveItem(activeSession);
			session.setActive(true);

			session.setContextId(IContext.PUBLIC);
			repositoryItemService.saveItem(session);
			return activeSession;
		} else {
			return null;
		}
	}
	
	public synchronized boolean postArchiveSession(UUIDParam params) throws TeamRepositoryException
	{
		//TODO: put a check for coordinator
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		Session session = getSessionFromRepository(params.uuid);
		session = (Session) session.getWorkingCopy();
		session.setArchived(true);
		repositoryItemService.saveItem(session);
		return true;
		
	}

	public synchronized ActiveSession postJoinSession(UUIDParam params)
			throws TeamRepositoryException {

		ActiveSession activeSession = getAssociatedActiveSession(params.uuid);
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);

		if (activeSession != null) {
			activeSession = (ActiveSession) activeSession.getWorkingCopy();
			IContributorHandle contributor = getAuthenticatedContributor();
			List joinedParticipants = activeSession.getJoinedParticipants();
			joinedParticipants.add(contributor);
			activeSession.setModified(new Timestamp((new Date()).getTime()));
			activeSession
					.setLastOperation(LastActiveSessionOperation.PARTICIPANT_JOINED);
			activeSession.setContextId(IContext.PUBLIC);
			activeSession = repositoryItemService.saveItem(activeSession);
			return activeSession;
		}

		return null;
	}

	public synchronized void postStartVoting(StoryVoteParam params)
			throws TeamRepositoryException {

		ActiveSession activeSession = getActiveSession(params.activeSessionUUID);
		IWorkItem workItem = getWorkItemByUUID(params.workItemUUID);
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);

		if (activeSession != null && workItem != null) {
			if (isToBeEstimated(activeSession, workItem, repositoryItemService)) {
				activeSession = (ActiveSession) activeSession.getWorkingCopy();
				ItemForEstimation iud = JazzFactory.eINSTANCE
						.createItemForEstimation();
				iud.setItem(workItem);
				iud.setRoundNo(1);
				activeSession.setItemUnderDiscussion(iud);
				activeSession
						.setModified(new Timestamp((new Date()).getTime()));
				activeSession
						.setLastOperation(LastActiveSessionOperation.VOTE_STARTED);
				activeSession.setContextId(IContext.PUBLIC);
				repositoryItemService.saveItem(activeSession);
			}
		}
	}

	public synchronized void postShowResult(StoryVoteParam params)
			throws TeamRepositoryException {

		ActiveSession activeSession = getActiveSession(params.activeSessionUUID);
		IWorkItem workItem = getWorkItemByUUID(params.workItemUUID);
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);

		if (activeSession != null && workItem != null) {
			if (isToBeEstimated(activeSession, workItem, repositoryItemService)) {
				activeSession = (ActiveSession) activeSession.getWorkingCopy();
				activeSession
						.setModified(new Timestamp((new Date()).getTime()));
				activeSession
						.setLastOperation(LastActiveSessionOperation.SHOW_RESULT);
				activeSession.setContextId(IContext.PUBLIC);
				repositoryItemService.saveItem(activeSession);
			}
		}
	}

	private boolean isToBeEstimated(ActiveSession activeSession,
			IWorkItem workItem, IRepositoryItemService repositoryItemService)
			throws TeamRepositoryException {
		// / workItem has to be part of session
		Session session = (Session) repositoryItemService.fetchItem(
				activeSession.getAssociatedSession(), null);

		List items = session.getItems();
		for (Iterator iterator = items.iterator(); iterator.hasNext();) {
			ItemHandle itemHandle = (ItemHandle) iterator.next();
			if (itemHandle.getItemId().equals(workItem.getItemId()))
				;
			return true;
		}

		return false;
	}

	public synchronized void postStartRevoting(StoryVoteParam params)
			throws TeamRepositoryException {

		ActiveSession activeSession = getActiveSession(params.activeSessionUUID);
		IWorkItem workItem = getWorkItemByUUID(params.workItemUUID);
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);

		if (activeSession != null && workItem != null) {
			if (workItem.getItemId().equals(
					activeSession.getItemUnderDiscussion().getItem()
							.getItemId())) {
				activeSession = (ActiveSession) activeSession.getWorkingCopy();

				ItemForEstimation iud = activeSession.getItemUnderDiscussion();
				iud.setRoundNo(iud.getRoundNo() + 1);

				List history = iud.getVoteHistory();

				VoteHistoryItem newHistoryItem = JazzFactory.eINSTANCE
						.createVoteHistoryItem();
				newHistoryItem.setVoteRound(history.size() + 1);
				List curentVotes = iud.getCurrentVotes();
				for (Iterator iterator = curentVotes.iterator(); iterator
						.hasNext();) {
					Vote vote = (Vote) iterator.next();
					Vote historyItemVote = JazzFactory.eINSTANCE.createVote();
					historyItemVote.setVote(vote.getVote());
					historyItemVote.setParticipant(vote.getParticipant());
					newHistoryItem.getVotes().add(historyItemVote);
				}

				iud.getCurrentVotes().clear();
				history.add(newHistoryItem);

				activeSession
						.setModified(new Timestamp((new Date()).getTime()));
				activeSession
						.setLastOperation(LastActiveSessionOperation.REVOTE);
				activeSession.setContextId(IContext.PUBLIC);
				repositoryItemService.saveItem(activeSession);
			}
		}
	}

	public synchronized boolean postVote(VoteParam params)
			throws TeamRepositoryException {

		ActiveSession activeSession = getActiveSession(params.activeSessionUUID);
		IWorkItem workItem = getWorkItemByUUID(params.workItemUUID);
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);

		if (activeSession != null && workItem != null
				&& activeSession.getItemUnderDiscussion() != null) {
			activeSession = (ActiveSession) activeSession.getWorkingCopy();
			ItemForEstimation iud = activeSession.getItemUnderDiscussion();
			IWorkItem wud = (IWorkItem) repositoryItemService.fetchItem(iud
					.getItem(), null);
			if (workItem.getId() == wud.getId()) {

				List votes = iud.getCurrentVotes();

				Vote newVote = JazzFactory.eINSTANCE.createVote();
				newVote.setVote(params.vote);
				newVote.setParticipant( getAuthenticatedContributor());

				votes.add(newVote);
				activeSession
						.setModified(new Timestamp((new Date()).getTime()));
				activeSession.setLastOperation(LastActiveSessionOperation.VOTE);
				activeSession.setContextId(IContext.PUBLIC);
				repositoryItemService.saveItem(activeSession);
				return true;
			}
		}
		return false;
	}
	/**
	 * Let the moderator to cancel the whole vote
	 * author Samuel
	 */
	public boolean postVoteGameCancel(VoteParam params)
		throws TeamRepositoryException {
		ActiveSession activeSession = getActiveSession(params.activeSessionUUID);
		IWorkItem workItem = getWorkItemByUUID(params.workItemUUID);
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		
		if (activeSession != null && workItem != null) {

			activeSession = (ActiveSession) activeSession.getWorkingCopy();
			activeSession
					.setLastOperation(LastActiveSessionOperation.VOTEGAMECANCEL);
			activeSession.setContextId(IContext.PUBLIC);
			repositoryItemService.saveItem(activeSession);
			return true;
		}		
		return false;
	}	
	/**
	 * Let the participant to cancel the vote
	 * @param params
	 * @return
	 * @throws TeamRepositoryException
	 * author Samuel
	 */
	public synchronized boolean postVoteCancel(VoteParam params)
		throws TeamRepositoryException {

		ActiveSession activeSession = getActiveSession(params.activeSessionUUID);
		IWorkItem workItem = getWorkItemByUUID(params.workItemUUID);
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		
		if (activeSession != null && workItem != null
				&& activeSession.getItemUnderDiscussion() != null) {
			activeSession = (ActiveSession) activeSession.getWorkingCopy();
			ItemForEstimation iud = activeSession.getItemUnderDiscussion();
			IWorkItem wud = (IWorkItem) repositoryItemService.fetchItem(iud
					.getItem(), null);
			if (workItem.getId() == wud.getId()) {
		
				List votes = iud.getCurrentVotes();
		
				for (Iterator iterator = votes.iterator(); iterator.hasNext();) {
					Vote vote = (Vote) iterator.next();
					if( vote.getParticipant().getItemId() ==  getAuthenticatedContributor().getItemId() ){
						votes.remove(vote);
						break;
					}
				}
				
				activeSession
						.setModified(new Timestamp((new Date()).getTime()));
				activeSession.setLastOperation(LastActiveSessionOperation.VOTECANCEL);
				activeSession.setContextId(IContext.PUBLIC);
				repositoryItemService.saveItem(activeSession);
				return true;
			}
		}
		return false;
		}
	
	public synchronized void postSaveVote(VoteParam params)
			throws TeamRepositoryException {

		try {
			ActiveSession activeSession = getActiveSession(params.activeSessionUUID);

			IWorkItem workItem = getWorkItemByUUID(params.workItemUUID);
			IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);

			if (activeSession != null && workItem != null
					&& activeSession.getItemUnderDiscussion() != null) {
				activeSession = (ActiveSession) activeSession.getWorkingCopy();
				ItemForEstimation iud = activeSession.getItemUnderDiscussion();
				IWorkItem wud = (IWorkItem) repositoryItemService.fetchItem(iud
						.getItem(), null);
				if (workItem.getId() == wud.getId()) {
					activeSession.getItemUnderDiscussion().setSavedVote(
							params.vote);

					ItemForEstimation itemToSave = JazzFactory.eINSTANCE
							.createItemForEstimation();
					itemToSave.setItem(wud);
					itemToSave.setRoundNo(iud.getRoundNo());
					itemToSave.setSavedVote(iud.getSavedVote());

					// TODO yarora to handle saving of history also
					List oldHistoryList = iud.getVoteHistory();

					List newHistoryList = itemToSave.getVoteHistory();

					for (Iterator iterator = oldHistoryList.iterator(); iterator
							.hasNext();) {
						VoteHistoryItem oldHistoryItem = (VoteHistoryItem) iterator
								.next();
						VoteHistoryItem newHistoryItem = JazzFactory.eINSTANCE
								.createVoteHistoryItem();

						// TODO hack to remove this piece of code, adding this
						// code makes
						// the saving of history work. Cant figure out why.
						// Should be checked with
						// next version of code
						newHistoryItem.setInternalId(UUID.generate());
						newHistoryItem.setSession(UUID.generate());
						// end hack

						newHistoryItem.setVoteRound(oldHistoryItem
								.getVoteRound());

						List curentVotes = oldHistoryItem.getVotes();
						for (Iterator iterator1 = curentVotes.iterator(); iterator1
								.hasNext();) {
							Vote vote = (Vote) iterator1.next();
							Vote historyItemVote = JazzFactory.eINSTANCE
									.createVote();
							historyItemVote.setVote(vote.getVote());
							historyItemVote.setParticipant(vote
									.getParticipant());
							newHistoryItem.getVotes().add(historyItemVote);
						}

						newHistoryList.add(newHistoryItem);
					}

					activeSession.getItemsCompleted().add(itemToSave);

					activeSession.setModified(new Timestamp((new Date())
							.getTime()));
					activeSession
							.setLastOperation(LastActiveSessionOperation.VOTE_SAVED);
					activeSession.setContextId(IContext.PUBLIC);
					repositoryItemService.saveItem(activeSession);
					
					SessionParameters historyParams = new SessionParameters();
					historyParams.itemsUUID = new String[]{params.workItemUUID};
					historyParams.vote = params.vote;
					historyParams.oldVote = params.oldVote;
					Session session = (Session) repositoryItemService.fetchItem(
							activeSession.getAssociatedSession(), null);
					session = (Session) session.getWorkingCopy();

					addHistory(historyParams,session,System.currentTimeMillis(),repositoryItemService,SessionHistoryChanges.VOTE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public synchronized boolean postSaveActiveSession(UUIDParam params)
			throws TeamRepositoryException {
		ActiveSession activeSession = getActiveSession(params.uuid);
		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);

		if (activeSession != null) {
			activeSession = (ActiveSession) activeSession.getWorkingCopy();
			Session session = (Session) repositoryItemService.fetchItem(
					activeSession.getAssociatedSession(), null);
			session = (Session) session.getWorkingCopy();

			List completedItems = activeSession.getItemsCompleted();
			IWorkItemServer workItemServer = getService(IWorkItemServer.class);
			IProjectArea projectArea = (IProjectArea) repositoryItemService
					.fetchItem(session.getProjectArea(), null);
			IAttribute attribute = workItemServer.findAttribute(projectArea,
					"com.ibm.team.apt.attribute.complexity", null);
			try{
				String type = attribute.getAttributeType();
				AttributeType attributeType = AttributeTypes.getAttributeType(type);
	
				for (Iterator iterator = completedItems.iterator(); iterator
						.hasNext();) {
					ItemForEstimation itemSaved = (ItemForEstimation) iterator
							.next();
	
					IWorkItem workItem = (IWorkItem) repositoryItemService
							.fetchItem(itemSaved.getItem(), null);
					workItem = (IWorkItem) workItem.getWorkingCopy();
					
					Identifier literalID = resolveComplexityName(itemSaved.getSavedVote(), attribute, workItemServer);
					
					if (literalID != null) {
						if (workItem.hasAttribute(attribute)) {
							workItem.setValue(attribute, literalID);
						} else {
							workItem.addCustomAttribute(attribute);
							workItem.setValue(attribute,literalID);
						}
					}
					workItemServer.saveWorkItem2(workItem, null, null);
				}
			
				// TODO yogesh to archive it
				// session.setArchived(true);
				// session.setActive(false);
				session.setContextId(IContext.PUBLIC);
				repositoryItemService.saveItem(session);
				
			}catch(NullPointerException npe){
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * I Hate doing it like this but its a quick fix. Instead of assuming that the complexity enum neatly has id's of 1,2,3 etc
	 * We are going to assume that the name is 1 pt, 2 pts, etc. This seems like the safer of the two assumptions. Although
	 * We shouldn't be doing this at all.
	 * @param savedVote
	 * @param attribute 
	 * @param workItemServer 
	 * @return
	 */
	public static Identifier resolveComplexityName(int savedVote, IAttribute attribute, IWorkItemServer workItemServer) {
		
		try {
			String name = "";
			if (savedVote == 1) name = "1 pt";
			else name = savedVote + " pts";
			
			Identifier literalID;
			IEnumeration enumeration = workItemServer.resolveEnumeration(attribute, null); // or IWorkitemCommon
			List literals = enumeration.getEnumerationLiterals();
			for (Iterator it= literals.iterator(); it.hasNext();) {
				ILiteral iLiteral = (ILiteral) it.next();
				if (iLiteral.getName().equals(name)) {
					return iLiteral.getIdentifier2();
				}
			}
			
		}catch (Exception e) {
			//Just return null
		}
		return null;
	}
	

	public synchronized void postCancelActiveSession(UUIDParam params)
			throws TeamRepositoryException {

		try {
			ActiveSession activeSession = getActiveSession(params.uuid);
			IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
			if (activeSession != null) {
				repositoryItemService.deleteItem(activeSession);
				Session session = (Session) repositoryItemService.fetchItem(
						activeSession.getAssociatedSession(), null);
				session = (Session) session.getWorkingCopy();
				session.setActive(false);
				session.setContextId(IContext.PUBLIC);
				repositoryItemService.saveItem(session);
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void postLeaveSession(UUIDParam params)
			throws TeamRepositoryException {
		try {
			ActiveSession activeSession = getActiveSession(params.uuid);
			IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);

			if (activeSession != null) {
				activeSession = (ActiveSession) activeSession.getWorkingCopy();
				IContributorHandle contributor = getAuthenticatedContributor();
				List joinedParticipants = activeSession.getJoinedParticipants();
				for (Iterator iterator = joinedParticipants.iterator(); iterator
						.hasNext();) {
					IContributorHandle alreadyJoined = (IContributorHandle) iterator
							.next();
					if (alreadyJoined.getItemId().getUuidValue().equals(
							contributor.getItemId().getUuidValue())) {
						iterator.remove();
						activeSession.setModified(new Timestamp((new Date())
								.getTime()));
						activeSession
								.setLastOperation(LastActiveSessionOperation.PARTICIPANT_LEFT);
						activeSession.setContextId(IContext.PUBLIC);
						activeSession = repositoryItemService
								.saveItem(activeSession);
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public IWorkItem[] getWorkItemsByIteration(UUIDParam iterationUUID)
			throws TeamRepositoryException {
		IQueryService queryService = getService(IQueryService.class);

		WorkItemQueryModel model = WorkItemQueryModel.ROOT;
		IItemQuery query = IItemQuery.FACTORY.newInstance(model);
		query.filter(model.target().itemId()._eq(query.newUUIDArg()));

		IItemQueryPage itemQueryPage = queryService.queryItems(query,
				new Object[] { UUID.valueOf(iterationUUID.uuid) },
				Integer.MAX_VALUE);

		IRepositoryItemService repositoryItemService = getService(IRepositoryItemService.class);
		IItem[] items = repositoryItemService.fetchItems(itemQueryPage
				.handlesAsArray(), null);

		ArrayList<IWorkItem> workItem = new ArrayList<IWorkItem>();
		for (IItem item : items) {
			if (item instanceof IWorkItem) {
				workItem.add((IWorkItem) item);
			}
		}

		return workItem.toArray(new IWorkItem[workItem.size()]);
	}
	
}
