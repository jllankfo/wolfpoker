package edu.ncsu.csc.wolfpoker.jazz.service;

import java.util.HashSet;
import java.util.Set;

import junit.framework.Assert;

import org.eclipse.core.runtime.IProgressMonitor;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ibm.team.foundation.common.text.XMLString;
import com.ibm.team.process.client.IProcessClientService;
import com.ibm.team.process.client.IProcessItemService;
import com.ibm.team.process.common.IProcessDefinition;
import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.client.TeamPlatform;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.client.IAuditableClient;
import com.ibm.team.workitem.client.IWorkItemClient;
import com.ibm.team.workitem.client.WorkItemOperation;
import com.ibm.team.workitem.client.WorkItemWorkingCopy;
import com.ibm.team.workitem.common.model.ICategoryHandle;
import com.ibm.team.workitem.common.model.IWorkItem;

import edu.ncsu.csc.wolfpoker.jazz.clientlib.IWolfPokerClientLib;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.SessionParameters;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;


public class JerrodTest {
	private static final String TEST_PROJECT_PLUGIN_ID = "edu.ncsu.csc.wolfpoker.jazz.service.test";
	private static final String PROJECT_AREA_NAME = "Scrum";
	private static final String ADMIN = "TestJazzProjectAdmin1";
	private static final String TEST_USER = "TestJazzUser1";
	private static final String POKER_SESSION_PARTICIPANT1 = "sessionParticipant";
	private static final String POKER_SESSION_PARTICIPANT2 = "sessionParticipant2";

	private static String repositoryURI = "https://localhost:9443/jazz/";
	private static ITeamRepository teamRepository;
	private static IProcessItemService processItemService;
	private static IWorkItemClient workItemClient;
	private static IProcessClientService processClient;
	private static IAuditableClient auditableClient;

	private static IContributor testUser;
	private static IContributor participantUser1;
	private static IContributor participantUser2;

	private static String categoryName = "defect";
	private static String typeIdentifier = "task";
	private static IProjectArea projectArea;
	private static IWorkItem workItem1;
	private static IWorkItem workItem2;
	private static IWolfPokerClientLib library;
	
	private Set<Session> createdSession =  new HashSet<Session>();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TeamPlatform.startup();
		
		teamRepository = TeamPlatform.getTeamRepositoryService()
				.getTeamRepository(repositoryURI);

		library = (IWolfPokerClientLib) teamRepository
				.getClientLibrary(IWolfPokerClientLib.class);
		if (library == null) 
			System.out.println("WTF");

		workItemClient = (IWorkItemClient) teamRepository
				.getClientLibrary(IWorkItemClient.class);

		if (workItemClient == null)
			System.out.println("WTF2");
		processItemService = (IProcessItemService) teamRepository
				.getClientLibrary(IProcessItemService.class);

		processClient = (IProcessClientService) teamRepository
				.getClientLibrary(IProcessClientService.class);

		auditableClient = (IAuditableClient) teamRepository
				.getClientLibrary(IAuditableClient.class);

		teamRepository.registerLoginHandler(new LoginHandler(ADMIN, ADMIN));

		teamRepository.login(null);

	}
	
	@Test
	public void createSession() throws TeamRepositoryException {
	
		Session session = createPokerSession();
		
		Assert.assertNotNull("Session could not be created", session);
		
		createdSession.add(session);
	}
	
	private Session createPokerSession() throws TeamRepositoryException {
		SessionParameters sessionParameters = new SessionParameters();
		sessionParameters.name = "session name";
		sessionParameters.description = "description";
		sessionParameters.createrUserID = teamRepository.loggedInContributor()
				.getUserId();

		sessionParameters.itemsUUID = new String[] {
				"1","2"};

		sessionParameters.participantsUserID = new String[] {
				"TestJazzUser1","TestJazzUser2" };
		sessionParameters.projectAreaName = "Scrum";
		sessionParameters.sessionCoordinatorUserID = "TestJazzUser1";
		Session session = library.createNewSession(sessionParameters);
		
		return session;
	}

}
