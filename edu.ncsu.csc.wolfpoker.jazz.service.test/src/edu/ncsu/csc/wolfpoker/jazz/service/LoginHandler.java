package edu.ncsu.csc.wolfpoker.jazz.service;

import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.client.ITeamRepository.ILoginHandler;
import com.ibm.team.repository.client.ITeamRepository.ILoginHandler.ILoginInfo;

class LoginHandler implements ILoginHandler, ILoginInfo {
	
	private String userID;
	private String password;
	
	public LoginHandler(String userId, String password) {
		this.userID= userId;
		this.password = password;
	}
	
	public String getUserId() {
		return userID;
	}
	
	public String getPassword() {
		return password;
	}
	
	public ILoginInfo challenge(ITeamRepository repository) {
		return this;
	}
}