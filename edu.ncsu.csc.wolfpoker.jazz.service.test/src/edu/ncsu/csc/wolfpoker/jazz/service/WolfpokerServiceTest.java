package edu.ncsu.csc.wolfpoker.jazz.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.ibm.team.foundation.common.text.XMLString;
import com.ibm.team.process.client.IProcessClientService;
import com.ibm.team.process.client.IProcessItemService;
import com.ibm.team.process.common.IDescription;
import com.ibm.team.process.common.IProcessDefinition;
import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.process.common.ProcessContentKeys;
import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.client.TeamPlatform;
import com.ibm.team.repository.common.FileLocator;
import com.ibm.team.repository.common.IContent;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.LineDelimiter;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.repository.common.model.Contributor;
import com.ibm.team.repository.common.model.RepositoryFactory;
import com.ibm.team.workitem.client.IAuditableClient;
import com.ibm.team.workitem.client.IWorkItemClient;
import com.ibm.team.workitem.client.WorkItemOperation;
import com.ibm.team.workitem.client.WorkItemWorkingCopy;
import com.ibm.team.workitem.common.model.ICategoryHandle;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.IWorkItemHandle;
import com.ibm.team.workitem.common.model.IWorkItemType;

import edu.ncsu.csc.wolfpoker.jazz.clientlib.IWolfPokerClientLib;
import edu.ncsu.csc.wolfpoker.jazz.clientlib.ItemChangeListener;
import edu.ncsu.csc.wolfpoker.jazz.clientlib.ItemChangeNotifier;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.SessionParameters;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistoryHandle;

public class WolfpokerServiceTest {

	private static final String TEST_PROJECT_PLUGIN_ID = "edu.ncsu.csc.wolfpoker.jazz.service.test";
	private static final String PROJECT_AREA_NAME = "hello3";
	private static final String ADMIN = "ADMIN";
	private static final String TEST_USER = "jazzTestUser";
	private static final String POKER_SESSION_PARTICIPANT1 = "sessionParticipant";
	private static final String POKER_SESSION_PARTICIPANT2 = "sessionParticipant2";

	private static String repositoryURI = "https://localhost:9443/jazz/";
	private static ITeamRepository teamRepository;
	private static IProcessItemService processItemService;
	private static IWorkItemClient workItemClient;
	private static IProcessClientService processClient;
	private static IAuditableClient auditableClient;

	private static IContributor testUser;
	private static IContributor participantUser1;
	private static IContributor participantUser2;

	private static String categoryName = "defect";
	private static String typeIdentifier = "task";
	private static IProjectArea projectArea;
	private static IWorkItem workItem1;
	private static IWorkItem workItem2;
	private static IWolfPokerClientLib library;
	
	private Set<Session> createdSession =  new HashSet<Session>();
	private static IProcessDefinition projectDefinition;
	
	private static class WorkItemInitialization extends WorkItemOperation {

		private String summary;
		private ICategoryHandle category;

		public WorkItemInitialization(String summary, ICategoryHandle category) {
			super("Initializing Work Item");
			this.summary = summary;
			this.category = category;
		}

		@Override
		protected void execute(WorkItemWorkingCopy workingCopy,
				IProgressMonitor monitor) throws TeamRepositoryException {
			IWorkItem workItem = workingCopy.getWorkItem();
			workItem.setHTMLSummary(XMLString.createFromPlainText(summary));
			workItem.setCategory(category);
		}
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		TeamPlatform.startup();
		
		teamRepository = TeamPlatform.getTeamRepositoryService()
				.getTeamRepository(repositoryURI);

		library = (IWolfPokerClientLib) teamRepository
				.getClientLibrary(IWolfPokerClientLib.class);

		workItemClient = (IWorkItemClient) teamRepository
				.getClientLibrary(IWorkItemClient.class);

		processItemService = (IProcessItemService) teamRepository
				.getClientLibrary(IProcessItemService.class);

		processClient = (IProcessClientService) teamRepository
				.getClientLibrary(IProcessClientService.class);

		auditableClient = (IAuditableClient) teamRepository
				.getClientLibrary(IAuditableClient.class);

		teamRepository.registerLoginHandler(new LoginHandler(ADMIN, ADMIN));

		teamRepository.login(null);

		projectArea = getProjectArea(PROJECT_AREA_NAME);
		workItem1 = createWorkItem("Summary 1");
		workItem2 = createWorkItem("Summary 2");
		testUser = getUser(TEST_USER);
		participantUser1 = getUser(POKER_SESSION_PARTICIPANT1);
		participantUser2 = getUser(POKER_SESSION_PARTICIPANT2);

		teamRepository.logout();
		teamRepository.registerLoginHandler(new LoginHandler(TEST_USER, ""));
		teamRepository.login(null);
	}

	@SuppressWarnings("unchecked")
	private static IContributor getUser(String name)
			throws TeamRepositoryException {


		IContributor newUser = null;
		List<IContributor> allContributors = teamRepository.contributorManager()
				.fetchAllContributors(null);
		for (Iterator<IContributor> iterator = allContributors.iterator(); iterator.hasNext();) {
			IContributor contributor = iterator.next();
			if (contributor.getName().equalsIgnoreCase(name)) {
				newUser = contributor;
				break;
			}
		}
		if (newUser == null) {
			newUser = RepositoryFactory.eINSTANCE.createContributor();
			newUser = (Contributor) newUser.getWorkingCopy();
			newUser.setUserId(name);
			newUser.setName(name);
			newUser.setEmailAddress(name);
			newUser = teamRepository.contributorManager().saveContributor(
					newUser, null);
		}
		return newUser;
	}

	private static IWorkItem createWorkItem(String summary)
			throws TeamRepositoryException {

		IWorkItemType workItemType = workItemClient.findWorkItemType(
				projectArea, typeIdentifier, null);
		List<String> path = Arrays.asList(projectArea.getName().split("/"));

		ICategoryHandle category = workItemClient.findCategoryByNamePath(
				projectArea, path, null);
		WorkItemInitialization operation = new WorkItemInitialization(summary,
				category);

		IWorkItemHandle handle = operation.run(workItemType, null);
		IWorkItem workItem = auditableClient.resolveAuditable(handle,
				IWorkItem.FULL_PROFILE, null);

		return workItem;
	}

	private static IProjectArea getProjectArea(String projectAreaName)
			throws TeamRepositoryException, UnsupportedEncodingException {

		URI uri = URI.create(PROJECT_AREA_NAME.replaceAll(" ", "%20"));
		IProjectArea projectArea = (IProjectArea) processClient
				.findProcessArea(uri, null, null);
		
		if (projectArea == null) {
			projectArea = setupProjectArea(PROJECT_AREA_NAME, "");

			workItemClient.createCategory(projectArea, categoryName, null);
		 }
		return projectArea;
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		teamRepository.logout();
		teamRepository.registerLoginHandler(new LoginHandler(ADMIN, ADMIN));
		teamRepository.login(null);
		if(projectArea!=null)
			processItemService.delete(projectArea, true, null);
		if(projectDefinition!=null)
			processItemService.delete(projectDefinition, true, null);
		if(testUser!=null)
			teamRepository.contributorManager().deleteContributor(testUser, null);
		if(participantUser1!=null)
		teamRepository.contributorManager().deleteContributor(participantUser1,
				null);
		if(participantUser1!=null)
		teamRepository.contributorManager().deleteContributor(participantUser2,
				null);

		teamRepository.logout();
		// TeamPlatform.shutdown();
	}

	@After
	public void cleanup() {
		for (Iterator<Session> iterator = createdSession.iterator(); iterator.hasNext();) {
			Session session = iterator.next();
			try {
				library.deleteSession(session);
			} catch (TeamRepositoryException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void createSession() throws TeamRepositoryException {
	
		Session session = createPokerSession();
		
		Assert.assertNotNull("Session could not be created", session);
		
		createdSession.add(session);
	}

	@Test
	public void getAllSessionByProjectArea() throws TeamRepositoryException {
		createdSession.add(createPokerSession());
		createdSession.add(createPokerSession());
		
		Session[] session = library.getAllSessionsByProjectArea(projectArea.getName());
		Assert.assertTrue(session.length > 0);
		
		for (int i = 0; i < session.length; i++) {
			IProjectArea retrievedProjectArea = (IProjectArea) teamRepository
					.itemManager().fetchCompleteItem(
							session[i].getProjectArea(), IItemManager.DEFAULT,
							null);
			Assert
					.assertEquals(
							"The project area of retrieved session is not same as argument sent to API",
							projectArea.getName(), retrievedProjectArea
									.getName());
		}
	}
	
	@Test
	public void deleteSession() throws TeamRepositoryException {
		Session session = createPokerSession();
		String createdSessionUUID = session.getItemId().getUuidValue();
		
		library.deleteSession(session);
		
		Session[] allSession = library.getAllSessionsByProjectArea(projectArea.getName());
		
		for (int i = 0; i < allSession.length; i++) {
//			
			Assert.assertFalse(
							"Session Not Deleted Properly",
							allSession[i].getItemId().getUuidValue().equals(createdSessionUUID));
		}
	}
	
	@Test
	public void startSessionTest() throws TeamRepositoryException {
		Session session = createPokerSession();
		createdSession.add(session);
		
		ActiveSession activeSession = library.startSession(session);
		Assert.assertTrue("Session could not be started", activeSession!=null);
	}
	
	@Test
	public void joinSessionTest() throws TeamRepositoryException {
		Session session = createPokerSession();
		createdSession.add(session);

		library.startSession(session);

		ActiveSession activeSession = library.joinSession(session);
		List joinedPartList = activeSession.getJoinedParticipants();
		Assert.assertTrue("Session could not be joined", joinedPartList.size()>0);
		Assert.assertTrue("Session could not be joined", activeSession!=null);
	}
	
	@Test
	public void startVotingTest() throws TeamRepositoryException {
		Session session = createPokerSession();
		createdSession.add(session);
		ActiveSession activeSession = library.startSession(session);
		library.startVoting(activeSession, workItem1);
	}
	
	@Test
	public void voteTest() throws TeamRepositoryException {
		Session session = createPokerSession();
		createdSession.add(session);
		ActiveSession activeSession = library.startSession(session);
		library.startVoting(activeSession, workItem1);
		library.vote(activeSession, workItem1, 10);
	}
	
	@Test
	public void saveVoteTest() throws TeamRepositoryException {
		Session session = createPokerSession();
		createdSession.add(session);
		ActiveSession activeSession = library.startSession(session);
		library.startVoting(activeSession, workItem1);
		library.vote(activeSession, workItem1, 10);
		library.saveVote(activeSession, workItem1, 10,"0");
	}
	

	@Test
	public void saveActiveSessionTest() throws TeamRepositoryException {
		Session session = createPokerSession();
		createdSession.add(session);
		ActiveSession activeSession = library.startSession(session);
		library.startVoting(activeSession, workItem1);
		library.vote(activeSession, workItem1, 10);
		library.saveVote(activeSession, workItem1, 10,"0");
		library.saveActiveSession(activeSession);
	}
	
	class Listener implements ItemChangeListener {

		public void itemChaged(ActiveSession itemOrig, ActiveSession itemNew) {
			System.out.println("itemChanged");
		}

		public void itemNotFound() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	@Test
	public void listenerTest() throws TeamRepositoryException {
		Session session = createPokerSession();
		createdSession.add(session);
		ActiveSession activeSession = library.startSession(session);
		
		ItemChangeNotifier notifier = new ItemChangeNotifier(activeSession,
				new Listener(), teamRepository);
		notifier.startListening();
		library.startVoting(activeSession, workItem1);
		
		try {
			Thread.sleep(1000);
			library.joinSession(session);
			Thread.sleep(1000);
			library.vote((ActiveSession) activeSession, workItem1, 10);
			Thread.sleep(1000);
			library.saveVote((ActiveSession) activeSession, workItem1, 10,"0");
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		notifier.stopListening();
	}
	
	@Test
	public void historyTest() throws TeamRepositoryException {
		Session session = createPokerSession();
		createdSession.add(session);
		List historyList = session.getSessionHistory();
		
		Assert.assertTrue("Creation History not present", historyList.size()==1);
		
		SessionParameters sessionParameters = new SessionParameters();
		sessionParameters.name = "session name2";
		sessionParameters.description = "description new";
		sessionParameters.createrUserID = teamRepository.loggedInContributor()
				.getUserId();

		sessionParameters.itemsUUID = new String[] {
				workItem1.getItemId().getUuidValue() };

		sessionParameters.participantsUserID = new String[] {
				participantUser1.getUserId()};
		sessionParameters.projectAreaName = projectArea.getName();
		sessionParameters.sessionCoordinatorUserID = participantUser2.getUserId();
		session = library.saveSession(session.getItemId().getUuidValue(),
				sessionParameters);

		historyList = session.getSessionHistory();
		
		for (Iterator iterator = historyList.iterator(); iterator.hasNext();) {
			SessionHistoryHandle historyHandle = (SessionHistoryHandle) iterator.next();
			SessionHistory history = (SessionHistory) teamRepository
					.itemManager().fetchCompleteItem(historyHandle,
							IItemManager.DEFAULT, null);
			System.out.println(history.getHistoryType());			
		}
	}

	
	private Session createPokerSession() throws TeamRepositoryException {
		SessionParameters sessionParameters = new SessionParameters();
		sessionParameters.name = "session name";
		sessionParameters.description = "description";
		sessionParameters.createrUserID = teamRepository.loggedInContributor()
				.getUserId();

		sessionParameters.itemsUUID = new String[] {
				workItem1.getItemId().getUuidValue(),
				workItem2.getItemId().getUuidValue() };

		sessionParameters.participantsUserID = new String[] {
				participantUser1.getUserId(), participantUser2.getUserId() };
		sessionParameters.projectAreaName = projectArea.getName();
		sessionParameters.sessionCoordinatorUserID = testUser.getUserId();
		Session session = library.createNewSession(sessionParameters);
		
		return session;
	}

	private static IProjectArea saveProjectArea(IProjectArea projectArea) throws TeamRepositoryException {
		IProcessItemService clientService = processItemService;
		return (IProjectArea) clientService.save(projectArea, null);
	}
	
	private static IProjectArea createProjectAreaWorkingCopy(IProjectArea projectArea) throws TeamRepositoryException {
		IProcessItemService clientService = processItemService;
		return (IProjectArea) clientService.getMutableCopy(projectArea);
	}

	private static IProjectArea setupProjectArea(String projectAreaName, String projectAreaSummary) throws TeamRepositoryException, UnsupportedEncodingException {	
		IProcessDefinition definition = createProcessDefinition();
		IProjectArea projectArea = createProjectArea(projectAreaName, projectAreaSummary);
				
		projectArea = createProjectAreaWorkingCopy(projectArea);
		projectArea.setProcessDefinition(definition);
		
		projectArea = saveProjectArea(projectArea);
		projectArea = processItemService.initialize(projectArea, null);
		Assert.assertTrue(projectArea.isInitialized());
		
		return projectArea;
	}

	private static IProcessDefinition createProcessDefinition() throws UnsupportedEncodingException, TeamRepositoryException {
		projectDefinition = createProcessDefinition("com.ibm.team.process.test");
		return projectDefinition;
	}

	@SuppressWarnings("unchecked")
	private static IProcessDefinition createProcessDefinition(String processId) throws TeamRepositoryException, UnsupportedEncodingException {
		IProcessDefinition processDefinition= getMutableProcessDefinition(processId);
		processDefinition.setName("Simple Test Process");
		
		IDescription description = processDefinition.getDescription();
		description.setSummary("Simple process for testing");
		
		processDefinition.setProcessId(processId);
		
		Map<String, IContent> definitionData = processDefinition.getProcessData();		
		definitionData.put(ProcessContentKeys.PROCESS_SPECIFICATION_KEY, createProcessDefinitionSpecificationContent());
		definitionData.put(ProcessContentKeys.PROCESS_STATE_KEY, createProcessDefinitionStateContent());
		
		return (IProcessDefinition) processItemService.save(processDefinition, null);
	}
	
	private static IContent createProcessDefinitionSpecificationContent() throws TeamRepositoryException {
		return createContentFromFile("testData/test_specification.xml");
	}

	private static IContent createProcessDefinitionStateContent() throws TeamRepositoryException {
		return createContentFromFile("testData/test_state.xml");
	}

	private static IProcessDefinition getMutableProcessDefinition(String processId) throws TeamRepositoryException {
		IProcessItemService service = processItemService;
		IProcessDefinition processDefinition = service.findProcessDefinition(processId, IProcessClientService.ALL_PROPERTIES, null);
		if (processDefinition != null) {
			processDefinition = (IProcessDefinition) service.getMutableCopy(processDefinition);
		} else {
			processDefinition= service.createProcessDefinition();
		}
		processDefinition.setProcessId(processId);
		return processDefinition;
	}
			
	private static IProjectArea createProjectArea(String name, String summary) {
		IProjectArea projectArea= processItemService.createProjectArea();
		projectArea.setName(name);

		IDescription description = projectArea.getDescription();
		description.setSummary(summary);
		return projectArea;
	}
	
	private final static IContent createContentFromFile(String filePath, String bundleSymbolicName) throws TeamRepositoryException {
		URL fileURL = FileLocator.find(bundleSymbolicName, new Path(filePath));
		if (fileURL != null) {
			try {
				InputStream stream = fileURL.openStream();
				IContent c =  teamRepository.contentManager().storeContent(IContent.CONTENT_TYPE_TEXT, IContent.ENCODING_US_ASCII, LineDelimiter.LINE_DELIMITER_NONE, stream, null, null);
				return c;
			} catch (IOException e) {
				throw new TeamRepositoryException(e);
			}
		}
		return null;
	}
	
	private final static IContent createContentFromFile(String filePath) throws TeamRepositoryException {
		return createContentFromFile(filePath, TEST_PROJECT_PLUGIN_ID);
	}
}
