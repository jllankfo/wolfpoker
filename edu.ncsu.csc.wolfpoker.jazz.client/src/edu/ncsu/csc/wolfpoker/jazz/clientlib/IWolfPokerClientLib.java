package edu.ncsu.csc.wolfpoker.jazz.clientlib;

import com.ibm.team.process.common.IIteration;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.IWorkItemHandle;

import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.SessionParameters;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public interface IWolfPokerClientLib {
	Session[] getAllSessionsByProjectArea(String projectAreaName)
			throws TeamRepositoryException;

	IWorkItem[] getWorkItemsByIteration(IIteration iteration)
			throws TeamRepositoryException;

	Session createNewSession(SessionParameters sessionParameters) throws TeamRepositoryException;

	void deleteSession(Session session) throws TeamRepositoryException;

	Session saveSession(String sessionID, SessionParameters params) throws TeamRepositoryException;

	ActiveSession startSession(Session session) throws TeamRepositoryException;

	ActiveSession joinSession(Session session) throws TeamRepositoryException;

	void leaveSession(ActiveSession session) throws TeamRepositoryException;

	void startVoting(ActiveSession activeSession, IWorkItemHandle workItem)
			throws TeamRepositoryException;

	void showResult(ActiveSession activeSession, IWorkItemHandle workItem)
			throws TeamRepositoryException;

	void startRevoting(ActiveSession activeSession, IWorkItemHandle workItem)
			throws TeamRepositoryException;

	void vote(ActiveSession activeSession, IWorkItemHandle workItem, int vote)
			throws TeamRepositoryException;
	
	/**
	 * Moderator cancel the whole voting
	 * @param activeSession
	 * @param workItem
	 * @throws TeamRepositoryException
	 */
	void voteGameCancel(ActiveSession activeSession, IWorkItemHandle workItem)
		throws TeamRepositoryException;
	/**
	 * Individual cancel their own vote
	 * @param activeSession
	 * @param workItem
	 * @throws TeamRepositoryException
	 */
	void voteCancel(ActiveSession activeSession, IWorkItemHandle workItem)
		throws TeamRepositoryException;

	void saveVote(ActiveSession activeSession, IWorkItem workItem, int vote, String oldVote)
			throws TeamRepositoryException;

	boolean saveActiveSession(ActiveSession activeSession)
			throws TeamRepositoryException;

	void cancelActiveSession(ActiveSession activeSession)
			throws TeamRepositoryException;
	
	boolean setSessionArchived(Session session) throws TeamRepositoryException;
}
