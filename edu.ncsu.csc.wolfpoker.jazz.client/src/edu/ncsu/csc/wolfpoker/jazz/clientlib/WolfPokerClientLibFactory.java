package edu.ncsu.csc.wolfpoker.jazz.clientlib;

import com.ibm.team.repository.client.util.ClientLibraryFactory;
import com.ibm.team.repository.client.util.IClientLibraryContext;

public class WolfPokerClientLibFactory extends ClientLibraryFactory {

	@Override
	public Object createClientLibrary(IClientLibraryContext context) {
		return new WolfPokerClientLib(context);
	}

}
