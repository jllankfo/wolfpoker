package edu.ncsu.csc.wolfpoker.jazz.clientlib;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.swt.widgets.Display;

import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.ItemNotFoundException;
import com.ibm.team.repository.common.TeamRepositoryException;

import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;


public class ItemChangeNotifier {
	private Timer timer;
	private ActiveSession activeSession;
	private ItemChangeListener listener;
	private ITeamRepository repository;
	
	
	public ItemChangeNotifier(ActiveSession session,
			ItemChangeListener listener, ITeamRepository repository) {
		this.activeSession = session;
		this.listener = listener;
		this.repository = repository;
		timer = new Timer(); 
	}
	
	private void pool() {

		try {
			final ActiveSession newSession = (ActiveSession) repository.itemManager().fetchCompleteItem(
					activeSession, IItemManager.UNSHARED, null);
			if (newSession.getModified().after((activeSession.getModified()))) {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						listener.itemChaged(activeSession, newSession);
					}
				});
			}
			activeSession = newSession; 
		} catch (TeamRepositoryException tre) {
			if (tre instanceof ItemNotFoundException) {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						stopListening();
						listener.itemNotFound();
					}
				});
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void startListening() {
		timer.schedule(new TimerTask(){

			@Override
			public void run() {
				pool();
			}}, 500, 500);
	}
	
	public void stopListening() {
		timer.cancel();
	}
}
