package edu.ncsu.csc.wolfpoker.jazz.clientlib;

import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;

public interface ItemChangeListener {
	void itemChaged(ActiveSession itemOrig, ActiveSession itemNew);
	
	void itemNotFound();
}
