package edu.ncsu.csc.wolfpoker.jazz.clientlib;

import com.ibm.team.process.common.IIteration;
import com.ibm.team.repository.client.util.IClientLibraryContext;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.IWorkItemHandle;

import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.SaveSessionParameters;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.SessionParameters;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.StoryVoteParam;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.UUIDParam;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.VoteParam;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class WolfPokerClientLib implements IWolfPokerClientLib {

	private IClientLibraryContext context;

	public WolfPokerClientLib(IClientLibraryContext context) {
		this.context = context;
	}

	private IWolfPokerService getService() throws TeamRepositoryException {
		IWolfPokerService service = (IWolfPokerService) context
				.getServiceInterface(IWolfPokerService.class);
		if (service == null) {
			throw new TeamRepositoryException("Unable to get IWolfPokerService");
		}
		return service;
	}

	public Session[] getAllSessionsByProjectArea(String projectAreaName)
			throws TeamRepositoryException {

		IWolfPokerService.SessionsByProjectAreaParam sessionParam = 
				new IWolfPokerService.SessionsByProjectAreaParam();
		sessionParam.projectName = projectAreaName;
		return getService().getAllSessionsByProjectArea(sessionParam);
	}

	public Session createNewSession(SessionParameters sessionParams)
			throws TeamRepositoryException {
		Session session =  getService().postNewSession(sessionParams);
		return session;
	}

	public void deleteSession(Session session) throws TeamRepositoryException {
		UUIDParam params =  new UUIDParam();
		params.uuid = session.getItemId().getUuidValue();
		getService().postDeleteSession(params);
	}

	public Session saveSession(String sessionID, SessionParameters params)
			throws TeamRepositoryException {
		SaveSessionParameters saveSessionParams = new SaveSessionParameters();
		saveSessionParams.createrUserID = params.createrUserID;
		saveSessionParams.description = params.description;
		saveSessionParams.itemsID = params.itemsUUID;
		saveSessionParams.name = params.name;
		saveSessionParams.participantsUserID = params.participantsUserID;
		saveSessionParams.projectArea = params.projectAreaName;
		saveSessionParams.sessionCoordinatorUserID = params.sessionCoordinatorUserID;
		saveSessionParams.sessionID = sessionID;
		saveSessionParams.cardsValuelowerBound = params.cardsValuelowerBound;
		saveSessionParams.cardsValueUpperBound = params.cardsValueUpperBound;
		return getService().postSaveSession(saveSessionParams);
	}
	
	public ActiveSession startSession(Session session) throws TeamRepositoryException {
		UUIDParam params =  new UUIDParam();
		params.uuid = session.getItemId().getUuidValue();
		return getService().postStartSession(params);
	}

	public ActiveSession joinSession(Session session) throws TeamRepositoryException {
		UUIDParam params =  new UUIDParam();
		params.uuid = session.getItemId().getUuidValue();

		return getService().postJoinSession(params);
	}

	public void startVoting(ActiveSession activeSession, IWorkItemHandle workItem)
			throws TeamRepositoryException {
		
		StoryVoteParam params = new StoryVoteParam();
		params.activeSessionUUID = activeSession.getItemId().getUuidValue();
		params.workItemUUID = workItem.getItemId().getUuidValue();
		getService().postStartVoting(params);
	}
	
	public void showResult(ActiveSession activeSession, IWorkItemHandle workItem)
		throws TeamRepositoryException {
		StoryVoteParam params = new StoryVoteParam();
		params.activeSessionUUID = activeSession.getItemId().getUuidValue();
		params.workItemUUID = workItem.getItemId().getUuidValue();
		getService().postShowResult(params);
	}

	public void vote(ActiveSession activeSession, IWorkItemHandle workItem, int vote)
			throws TeamRepositoryException {
		
		VoteParam params = new VoteParam();
		params.activeSessionUUID = activeSession.getItemId().getUuidValue();
		params.workItemUUID = workItem.getItemId().getUuidValue();
		params.vote = vote;
		getService().postVote(params);
	}
	
	public void voteGameCancel(ActiveSession activeSession,
			IWorkItemHandle workItem) throws TeamRepositoryException {
		VoteParam params = new VoteParam();
		params.activeSessionUUID = activeSession.getItemId().getUuidValue();
		params.workItemUUID = workItem.getItemId().getUuidValue();
		getService().postVoteGameCancel(params);		
		
	}	
	
	public void voteCancel(ActiveSession activeSession, IWorkItemHandle workItem)
			throws TeamRepositoryException {

		VoteParam params = new VoteParam();
		params.activeSessionUUID = activeSession.getItemId().getUuidValue();
		params.workItemUUID = workItem.getItemId().getUuidValue();
		getService().postVoteCancel(params);
	}

	public void saveVote(ActiveSession activeSession, IWorkItem workItem,
			int vote,String oldVote) throws TeamRepositoryException {
		VoteParam params = new VoteParam();
		params.activeSessionUUID = activeSession.getItemId().getUuidValue();
		params.workItemUUID = workItem.getItemId().getUuidValue();
		params.vote = vote;
		params.oldVote = oldVote;
		getService().postSaveVote(params);		
	}

	public boolean saveActiveSession(ActiveSession activeSession)
			throws TeamRepositoryException {
		
		UUIDParam params =  new UUIDParam();
		params.uuid = activeSession.getItemId().getUuidValue();

		return getService().postSaveActiveSession(params);
	}

	public void cancelActiveSession(ActiveSession activeSession)
			throws TeamRepositoryException {
		UUIDParam params =  new UUIDParam();
		params.uuid = activeSession.getItemId().getUuidValue();

		getService().postCancelActiveSession(params);
	}

	public void startRevoting(ActiveSession activeSession,
			IWorkItemHandle workItem) throws TeamRepositoryException {
		StoryVoteParam params = new StoryVoteParam();
		params.activeSessionUUID = activeSession.getItemId().getUuidValue();
		params.workItemUUID = workItem.getItemId().getUuidValue();
		getService().postStartRevoting(params);
	}

	public void leaveSession(ActiveSession activeSession)
			throws TeamRepositoryException {
		UUIDParam params =  new UUIDParam();
		params.uuid = activeSession.getItemId().getUuidValue();

		getService().postLeaveSession(params);
		
	}

	public IWorkItem[] getWorkItemsByIteration(IIteration iteration)
			throws TeamRepositoryException {
		UUIDParam params =  new UUIDParam();
		params.uuid = iteration.getItemId().getUuidValue();
		return getService().getWorkItemsByIteration(params);
	}

	public boolean setSessionArchived(Session session)
			throws TeamRepositoryException {
		UUIDParam params = new UUIDParam();
		params.uuid = session.getItemId().getUuidValue();
		return getService().postArchiveSession(params);
		
	}
}
