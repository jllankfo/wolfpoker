package edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer;

import java.util.HashMap;
import java.util.Map;

import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.UUID;

import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazzclient.views.HandleToObject;

/**
 * The purpose of this class is to cache calls to the repository and pre-populate the session wrapper with the 
 * creator and the coordinator
 * @author ADMINIBM
 *
 */
public class SessionWrapperManager {

	public Map<UUID, IContributor> cache;
	
	public SessionWrapperManager() {
		cache = new HashMap<UUID,IContributor>();
	}
	
	/**
	 * On cache miss this method will contact the repository, so it should not be run inside of a UI thread
	 * @param session
	 * @return
	 */
	public SessionWrapper prefetch(Session session) {
		SessionWrapper wrapper = new SessionWrapper(session);
		
		IContributorHandle creatorHandle = session.getCreatedBy();
		IContributorHandle coordinatorHandle = session.getSessionCoordinator();
		
		wrapper.setCreator(fetch(creatorHandle));
		wrapper.setCoordinator(fetch(coordinatorHandle));
		
		return wrapper;
	}
	
	private IContributor fetch(IContributorHandle handle) {
		
		UUID id = handle.getItemId();
		
		if (cache.containsKey(id)) { //Cache hit
			return cache.get(id);
		} else {
			//Contacts foundation
			IContributor item = HandleToObject.convert(handle);
			cache.put(id,item);
			return item;
		}
	}
}
