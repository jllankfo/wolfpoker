package edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer;

import java.util.ArrayList;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;

import com.ibm.team.process.rcp.ui.teamnavigator.ConnectedProjectAreaRegistry;
import com.ibm.team.repository.client.ITeamRepository;

import edu.ncsu.csc.wolfpoker.jazz.client.util.PokerSessionQuery;
import edu.ncsu.csc.wolfpoker.jazz.clientlib.IWolfPokerClientLib;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class PokerSessionFetchJob extends Job {

	private final WolfpokerPageManager manager;
	private PokerSessionQuery query;
	private String projectName = "";
	
	public PokerSessionFetchJob(WolfpokerPageManager manager, PokerSessionQuery query) {
		super("PokerJob");
		this.manager = manager;
		this.query = query;
	}


	/**
	 * Does a get on the poker sessions and runs it through a predefined filter.
	 * Eventually we should filter them server side to avoid fetching all
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {

		try {
			ITeamRepository repository = (ITeamRepository) query.getProjectArea().getOrigin();
			IWolfPokerClientLib library = (IWolfPokerClientLib) repository
					.getClientLibrary(IWolfPokerClientLib.class);
			Object[] pokerSession = library.getAllSessionsByProjectArea(ConnectedProjectAreaRegistry.getDefault().getProjectAreaName(query.getProjectArea()));
			final ArrayList<SessionWrapper> sessions = new ArrayList<SessionWrapper>();

			SessionWrapperManager wrapperManager = new SessionWrapperManager();
			for (Object o : pokerSession) {
				Session session = (Session) o;
				if (query.getFilter().filter(session, repository)){
					//Preset the creator/coordinator so we don't have to fetch in the ui thread
					//Also caches contributors for the life of the wrapper manager
					SessionWrapper wrapper = wrapperManager.prefetch(session);
					sessions.add(wrapper);
				}
			}
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					manager.setResults(sessions);
					manager.setDescription(projectName + " - " + query.getName());
				}
			});

		} catch(Exception e) {
			return Status.CANCEL_STATUS;
		}
		

		return Status.OK_STATUS;
	}

	public PokerSessionQuery getQuery() {
		return query;
	}
	
	public void setProjectName(String name) {
		this.projectName = name;
	}
}
