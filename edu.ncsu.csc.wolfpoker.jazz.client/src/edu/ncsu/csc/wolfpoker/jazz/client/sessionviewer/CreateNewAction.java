package edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuCreator;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.ibm.team.process.common.IDevelopmentLine;
import com.ibm.team.process.common.IDevelopmentLineHandle;
import com.ibm.team.process.common.IIteration;
import com.ibm.team.process.common.IIterationHandle;
import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.repository.common.UUID;

import edu.ncsu.csc.wolfpoker.jazzclient.views.NewSessionMenuAdapter;
import edu.ncsu.csc.wolfpoker.jazzclient.views.PokerEditorInput;

/**
 * This is the action for creating a new poker session
 * It needs to be knowledgeable of the current project area, and will open an editor for that project area
 * @author ADMINIBM
 *
 */
public class CreateNewAction extends Action implements IMenuCreator {

	Menu _menu;
	IProjectArea area;
	
	public CreateNewAction() {
		super("",IAction.AS_DROP_DOWN_MENU);

	}
	@Override
	public void run() {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		try {
			page.openEditor(new PokerEditorInput(area, null, null,false),
				"edu.ncsu.csc.wolfpoker.jazzclient.views.NewPokerSession");
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void dispose() {
		if (_menu != null) {
			_menu.dispose();
			_menu = null;
		}
	}

	@Override
	public Menu getMenu(Control parent) {
		if (_menu != null)
			_menu.dispose();
		_menu = new Menu(parent);
		popuplateMenu(_menu);
		return _menu;
	}

	@Override
	public IMenuCreator getMenuCreator() {
		return this;
	}
	@Override
	public Menu getMenu(Menu parent) {
		if (_menu != null)
			_menu.dispose();
		_menu = new Menu(parent);
		popuplateMenu(_menu);
		return _menu;
	}
	private void popuplateMenu(Menu menu) {
		MenuItem newItem = new MenuItem(menu, SWT.NONE);
		newItem.setText("New");
		newItem.addSelectionListener(new NewSessionMenuAdapter(area));
		
		MenuItem newFromIt = new MenuItem(menu, SWT.CASCADE);
		newFromIt.setText("New from iteration");
		
		try {
			addIterationMenus(menu,newFromIt,area);
		}catch(Exception e){
			//TODO
			e.printStackTrace();
		}
		
	}

	
	private void addIterationMenus(Menu menu, MenuItem item, IProjectArea area) {
		IDevelopmentLineHandle[] devLineHandles = area.getDevelopmentLines();
		
		List<IDevelopmentLine> devLines;
		Map<UUID, Menu> handleToMenu = new HashMap<UUID, Menu>();
		
		Menu subMenu = new Menu(menu);
		try {
			ITeamRepository repository = (ITeamRepository)devLineHandles[0].getOrigin();
			devLines = fetchBulk(repository,Arrays.asList(devLineHandles));
			Set<IIterationHandle> iterationHandles = new HashSet<IIterationHandle>();
			for (IDevelopmentLine devLine : devLines) {
				
				if (devLine.getIterations() == null || devLine.getIterations().length == 0) continue;
				//Create a menu and menu item for each development line
				Menu devLineMenu = new Menu(subMenu);
				handleToMenu.put(devLine.getItemId(), devLineMenu);
				iterationHandles.addAll(Arrays.asList(devLine.getIterations()));
				
				MenuItem devLineMenuItem = new MenuItem(subMenu, SWT.CASCADE);
				devLineMenuItem.setText(devLine.getName());
				devLineMenuItem.setMenu(devLineMenu);
			}
			
			addIterationsToMenu(handleToMenu, iterationHandles, area, repository);
			
		} catch (TeamRepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		item.setMenu(subMenu);
	}
	
	private void addIterationsToMenu(Map<UUID, Menu> handleToMenu,
			Set<IIterationHandle> iterationHandles,IProjectArea area, ITeamRepository repository) throws TeamRepositoryException {
		
		List<IIteration> iterations = fetchBulk(repository,Arrays.asList(iterationHandles.toArray()));
		Set<IIterationHandle> childIterations = new HashSet<IIterationHandle>();
		for (IIteration iteration : iterations) { 
			Menu menu = handleToMenu.get(iteration.getDevelopmentLine().getItemId());
			if (menu == null) continue;
			
			//This is a parent iteration so create another nesting
			if (iteration.getChildren().length != 0) {
				childIterations.addAll(Arrays.asList(iteration.getChildren()));
				Menu parentItMenu = new Menu(menu);
				handleToMenu.put(iteration.getItemId(), parentItMenu);
				
				MenuItem parentItMenuItem = new MenuItem(menu, SWT.CASCADE);
				parentItMenuItem.setText(iteration.getName());
				parentItMenuItem.setMenu(parentItMenu);
				
				//Add the current iteration to its own menu
				MenuItem newItem = new MenuItem(parentItMenu, SWT.NONE);
				newItem.setText(iteration.getName());
				newItem.addSelectionListener(new NewSessionMenuAdapter(area, iteration));
			
			}else {
				//If the iteration has a parent then we need to use that menu instead
				if (iteration.getParent() != null) menu = handleToMenu.get(iteration.getParent().getItemId());
				MenuItem newItem = new MenuItem(menu, SWT.NONE);
				newItem.setText(iteration.getName());
				newItem.addSelectionListener(new NewSessionMenuAdapter(area, iteration));
			}
			
		}
		if (childIterations.size() != 0) addIterationsToMenu(handleToMenu, childIterations, area, repository);
	}

	public <T> java.util.List<T> fetchBulk(ITeamRepository repository, java.util.List<?> handles) throws TeamRepositoryException {
		@SuppressWarnings("unchecked")
		java.util.List<T> list = repository.itemManager().fetchCompleteItems(handles, IItemManager.DEFAULT,null);
		return list;
	}
	

	public void setProjectArea(IProjectArea area) {
		this.area = area;
	}

}
