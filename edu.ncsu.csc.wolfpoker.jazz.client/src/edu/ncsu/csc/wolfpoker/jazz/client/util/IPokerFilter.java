package edu.ncsu.csc.wolfpoker.jazz.client.util;

import java.util.Iterator;
import java.util.List;

import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.model.ItemHandle;

import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazzclient.views.HandleToObject;

/**
 * To add another query, add an additional filter here, and create and enum for it in PokerSessionQueries.
 * It will be added to both views automatically
 * @author ADMINIBM
 *
 */
public abstract class IPokerFilter {
	String name;
	
	public abstract boolean filter(Session session, ITeamRepository repository);

	public IPokerFilter(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	
	public static IPokerFilter ALL = new IPokerFilter("All poker sessions") {

		@Override
		public boolean filter(Session session, ITeamRepository repository) {
			return true;
		}
		
	};
	
	public static IPokerFilter CREATED = new IPokerFilter("My created poker sessions") {

		@Override
		public boolean filter(Session session, ITeamRepository repository) {
			IContributor creator = HandleToObject.convert((ItemHandle) session
					.getCreatedBy(), IItemManager.REFRESH);

			if (creator.getUserId().equals(repository.getUserId())) 
				return true;
			else
				return false;
			}
		
	};
	
	public static IPokerFilter UPCOMING = new IPokerFilter("Upcoming poker sessions") {

		@Override
		public boolean filter(Session session, ITeamRepository repository) {
			List<IContributor> participants = session.getParticipants();

			IContributor coordinator = HandleToObject.convert(session
					.getSessionCoordinator());

			if (coordinator.getUserId().equals(repository.getUserId())) {
				if (!session.isArchived())
					return true;
			}

			for (Iterator<IContributor> iterator = participants.iterator(); iterator
					.hasNext();) {
				IContributorHandle item = iterator.next();
				IContributor contributor = HandleToObject.convert(
						(ItemHandle) item, IItemManager.REFRESH);

				if (contributor.getUserId().equals(repository.getUserId())) {
					if (!session.isArchived())
						return true;
				}
			}
			
			return false;
		}
		
	};
	
	public static IPokerFilter ARCHIVED = new IPokerFilter("Archived poker sessions") {

		@Override
		public boolean filter(Session session, ITeamRepository repository) {
			if (session.isArchived())	
				return true;
			return false;
		}
		
	};
}
