package edu.ncsu.csc.wolfpoker.jazz.client.util;

import com.ibm.team.process.common.IProjectAreaHandle;



public class PokerSessionQuery {
	
	private String name;
	private IPokerFilter filter;
	private IProjectAreaHandle projectArea;
	
	private PokerSessionQuery(String name, IPokerFilter filter, IProjectAreaHandle projectArea) {
		this.name=name;
		this.filter = filter;
		this.projectArea = projectArea;
	}
	
	public String getName() {
		return name;
	}
	
	public IPokerFilter getFilter() {
		return filter;
	}
	
	public IProjectAreaHandle getProjectArea() {
		return projectArea;
	}
	
	
	/**
	 * Add new queries here
	 * @param projectArea
	 * @return
	 */
	public static PokerSessionQuery[] getQueries(IProjectAreaHandle projectArea) {
		PokerSessionQuery created = new PokerSessionQuery("Created", IPokerFilter.CREATED, projectArea); 
		PokerSessionQuery upcoming = new PokerSessionQuery("Upcoming", IPokerFilter.UPCOMING,  projectArea);
		PokerSessionQuery archived = new PokerSessionQuery("Archived", IPokerFilter.ARCHIVED,  projectArea);
		
		return new PokerSessionQuery[] {created,upcoming,archived};
		
		
		
	}
}
