package edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.IPageSite;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.rcp.ui.internal.ImagePool;

import edu.ncsu.csc.wolfpoker.jazz.clientlib.IWolfPokerClientLib;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazzclient.views.HandleToObject;
import edu.ncsu.csc.wolfpoker.jazzclient.views.PokerEditorInput;
import edu.ncsu.csc.wolfpoker.jazzclient.views.PokerSessionUtil;

public class PokerResultsPage extends Page {

	private Hashtable<String, IEditorInput> openSessionList = new Hashtable<String, IEditorInput>();
	private Image sessionImage;

	@Override
	public void init(IPageSite pageSite) {
		super.init(pageSite);
		sessionImage = AbstractUIPlugin.imageDescriptorFromPlugin("edu.ncsu.csc.wolfpoker.jazz.client", "img/sessionIcon.jpg").createImage();
	}

	@Override
	public void dispose() {
		sessionImage.dispose();
		super.dispose();
	}

	TableViewer viewer;
	
	@Override
	public void createControl(Composite parent) {
		// define the TableViewer
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
		      | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		// create the columns 
		createColumns(viewer);
		// make lines and header visible
		final Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true); 
		
		table.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseDoubleClick(MouseEvent e) {
				TableItem item =  table.getSelection()[0];
				Session session = ((SessionWrapper) item.getData()).getSession();
				openSession(session);
			}
			
			
		});
		
		createTableMenu(table);
		
		viewer.setContentProvider(ArrayContentProvider.getInstance());
		viewer.setInput(new ArrayList<SessionWrapper>());
		

	}

	private void createTableMenu(final Table table) {
		Menu menu = new Menu(table); //where table1 is your table
		
		//Open menu item
		MenuItem open = new MenuItem(menu, SWT.PUSH);
		open.setText("Open");
		open.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				TableItem[] items = table.getSelection();
				
				for (TableItem item : items) {
					Session session = ((SessionWrapper) item.getData()).getSession();
					openSession(session);
				}
			}
			
		});
		//End open
		
		//Archive menu item
		MenuItem archive = new MenuItem(menu, SWT.PUSH);
		archive.setText("Archive");
		archive.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				Shell shell = Display.getCurrent().getActiveShell();
				MessageBox mb = new MessageBox(shell,SWT.ICON_QUESTION | SWT.YES | SWT.NO);
				
				mb.setText("Are you sure?");
				mb.setMessage("Are you sure you want to archive the selected sessions?");
				int answer = mb.open();
				
				if (answer == SWT.YES){
					TableItem[] items = table.getSelection();
					for (TableItem item : items) {
						Session session = ((SessionWrapper) item.getData()).getSession();
						archiveSession(session);
					}
				}
			}
			
		});
		//End archive
		
		//Delete menu item
		MenuItem delete = new MenuItem(menu, SWT.PUSH);
		delete.setText("Delete");
		delete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				Shell shell = Display.getCurrent().getActiveShell();
				MessageBox mb = new MessageBox(shell,SWT.ICON_QUESTION | SWT.YES | SWT.NO);
				
				mb.setText("Are you sure?");
				mb.setMessage("Are you sure you want to delete the selected sessions?");
				int answer = mb.open();
				
				if (answer == SWT.YES){
					TableItem[] items = table.getSelection();
					for (TableItem item : items) {
						Session session = ((SessionWrapper) item.getData()).getSession();
						deleteSession(session);
					}
				}
			}
		});
		//End delete
		
		table.setMenu(menu);
	}

	private void createColumns(TableViewer viewer) {
		
		//Image
		TableViewerColumn image = new TableViewerColumn(viewer,SWT.NONE);
		image.getColumn().setWidth(20);
		//image.getColumn().setText("Poker Session");
		image.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public Image getImage(Object element) {
				
				return sessionImage;
			}

			@Override
			public String getText(Object element) {
				return "";
			}
			
		});
		
		//Name
		TableViewerColumn name = new TableViewerColumn(viewer,SWT.NONE);
		name.getColumn().setWidth(400);
		name.getColumn().setText("Poker Session");
		name.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				// TODO Auto-generated method stub
				return ((SessionWrapper) element).getSession().getText();
			}
			
		});
		
		//Creator
		TableViewerColumn creator = new TableViewerColumn(viewer,SWT.NONE);
		creator.getColumn().setWidth(100);
		creator.getColumn().setText("Creator");
		creator.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				
				try{
					SessionWrapper wrapper = (SessionWrapper) element;
					return wrapper.getCreator().getName();
				}catch(Exception e)
				{
					return "";
				}
				
			}
			
		});
		
		//Coordinator
		//TODO This needs to be async
		TableViewerColumn coordinator = new TableViewerColumn(viewer,SWT.NONE);
		coordinator.getColumn().setWidth(100);
		coordinator.getColumn().setText("Coordinator");
		coordinator.setLabelProvider(new ColumnLabelProvider() {

			@Override
			public String getText(Object element) {
				try{
					SessionWrapper wrapper = (SessionWrapper) element;
					return wrapper.getCoordinator().getName();
				}catch(Exception e)
				{
					return "";
				}
				
			}
			
		});
		
		

	}
	
	@Override
	public Control getControl() {
		return viewer.getControl();
	}

	@Override
	public void setFocus() {
		viewer.getTable().setFocus();
	}

	public void setResults(List<SessionWrapper> sessions) {
		viewer.setInput(sessions);
		viewer.refresh();
		
	}
	

	
	
	@Override
	public void setActionBars(IActionBars actionBars) {
		// TODO Auto-generated method stub
		Action a = new Action(){
			
		};
		a.setImageDescriptor(ImagePool.ACCESS_GROUP);
		actionBars.getToolBarManager().add(a);
		//super.setActionBars(actionBars);
	}
	
	public void clearResults() {
		viewer.getTable().clearAll();
	}

	

	/**
	 * This method will keep track of open sessions and only allow a session to be opened once
	 * @param session
	 */
	private void openSession(Session session) {

		performSessionListMaintenance();
		session = HandleToObject.convert(session.getItemHandle(),IItemManager.REFRESH);
		if (!openSessionList.containsKey(session.getText())) {

			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

			ITeamRepository repository = (ITeamRepository) session.getOrigin();

			try {
				IProjectArea projectArea = (IProjectArea) repository.itemManager().fetchCompleteItem(
						session.getProjectArea(), IItemManager.REFRESH, null);

				boolean readOnly = PokerSessionUtil.isParticipant(session) || session.isArchived();
				IEditorInput editorInput = new PokerEditorInput(projectArea, null, session, readOnly);

				page.openEditor(editorInput,
						"edu.ncsu.csc.wolfpoker.jazzclient.views.NewPokerSession");
				openSessionList.put(session.getText(), editorInput);

			} catch (PartInitException e) {
				e.printStackTrace();
			} catch (TeamRepositoryException e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean archiveSession(Session session) {
		
		ITeamRepository repository = (ITeamRepository) session.getOrigin();

		try {
			IWolfPokerClientLib library = (IWolfPokerClientLib) repository.getClientLibrary(IWolfPokerClientLib.class);

			if (!PokerSessionUtil.isParticipant(session)) {
				return library.setSessionArchived(session);
				
			}

		} catch (TeamRepositoryException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	private void deleteSession(Session session) {
		
		ITeamRepository repository = (ITeamRepository) session.getOrigin();

		try {
			IWolfPokerClientLib library = (IWolfPokerClientLib) repository.getClientLibrary(IWolfPokerClientLib.class);

			if (PokerSessionUtil.isCreator(session)) {
				library.deleteSession(session);
				closeSessionIfOpen(session);
				
			}

		} catch (TeamRepositoryException e) {
			e.printStackTrace();
		}
	}

	
	private void closeSessionIfOpen(Session ses) {
		if (openSessionList.containsKey(ses.getText())) {
			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			page.closeEditor(page.findEditor(openSessionList.get(ses.getText())), false);
			openSessionList.remove(ses.getText());
		}
	}
	
	/**
	 * Determine which sessions are currently open
	 */
	private void performSessionListMaintenance() {
		String session;
		IEditorInput editorInput;
		IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

		for (Enumeration<String> e = openSessionList.keys(); e.hasMoreElements();) {
			session = e.nextElement();
			editorInput = openSessionList.get(session);
			if (page.findEditor(editorInput) == null) {
				openSessionList.remove(session);
			}
		}
	}
}
