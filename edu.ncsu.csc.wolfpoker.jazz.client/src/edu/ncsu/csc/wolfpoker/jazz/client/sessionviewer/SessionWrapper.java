package edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer;

import com.ibm.team.repository.common.IContributor;

import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class SessionWrapper {

	Session session;
	IContributor coordinator;
	IContributor creator;
	
	public SessionWrapper(Session session) {
		this.session = session;
	}

	public Session getSession() {
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public IContributor getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(IContributor coordinator) {
		this.coordinator = coordinator;
	}

	public IContributor getCreator() {
		return creator;
	}

	public void setCreator(IContributor creator) {
		this.creator = creator;
	}
}
