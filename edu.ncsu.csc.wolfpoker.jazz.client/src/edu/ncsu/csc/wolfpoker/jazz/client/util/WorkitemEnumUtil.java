package edu.ncsu.csc.wolfpoker.jazz.client.util;

import java.util.Iterator;
import java.util.List;

import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.client.IWorkItemClient;
import com.ibm.team.workitem.common.model.IAttributeHandle;
import com.ibm.team.workitem.common.model.IEnumeration;
import com.ibm.team.workitem.common.model.ILiteral;
import com.ibm.team.workitem.common.model.Identifier;

/**
 * Thanks to the original author for this code.
 * From: http://rsjazz.wordpress.com/2012/08/20/manipulationg-work-item-enumeration-values/
 * @author Ralph Schoon
 *
 */
public class WorkitemEnumUtil {
	
	@SuppressWarnings("rawtypes") 
	public static ILiteral getLiteralbyID(Identifier findLliteralID, IAttributeHandle iAttribute, ITeamRepository repository) throws TeamRepositoryException {
		IWorkItemClient workItemClient = (IWorkItemClient) repository.getClientLibrary(IWorkItemClient.class);
		IEnumeration enumeration = workItemClient.resolveEnumeration(iAttribute, null);

		List literals = enumeration.getEnumerationLiterals();
		for (Iterator iterator = literals.iterator(); iterator.hasNext();) {
			ILiteral iLiteral = (ILiteral) iterator.next();
			if (iLiteral.getIdentifier2().equals(findLliteralID)) {
				return iLiteral;
			}
		}
		return null;
	}

	@SuppressWarnings("rawtypes") 
	public static Identifier getLiteralEqualsString(String name, IAttributeHandle ia, ITeamRepository repository) throws TeamRepositoryException {
		IWorkItemClient workItemClient = (IWorkItemClient) repository.getClientLibrary(IWorkItemClient.class);
	
		Identifier literalID = null;
		IEnumeration enumeration = workItemClient.resolveEnumeration(ia, null); // or IWorkitemCommon
		List literals = enumeration.getEnumerationLiterals();
		for (Iterator iterator = literals.iterator(); iterator.hasNext();) {
			ILiteral iLiteral = (ILiteral) iterator.next();
			if (iLiteral.getName().equals(name)) {
				literalID = iLiteral.getIdentifier2();
				break;
			}
		}
		return literalID;
	}
}
