package edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.part.Page;
import org.eclipse.ui.progress.UIJob;

import com.ibm.team.process.common.IProjectAreaHandle;
import com.ibm.team.process.rcp.ui.teamnavigator.ConnectedProjectAreaRegistry;
import com.ibm.team.process.rcp.ui.teamnavigator.IConnectedProjectAreaRegistryChangeEvent;
import com.ibm.team.process.rcp.ui.teamnavigator.IConnectedProjectAreaRegistryListener;

import edu.ncsu.csc.wolfpoker.jazz.client.util.PokerSessionQuery;

public class InitialPokerPage extends Page {
	
	ScrolledComposite scrollArea;
	final WolfpokerPageManager manager;
	Composite viewArea;
	
	//This map contains mapping between project areas and links, so we can remove them if the project areas are deleted.
	Map<IProjectAreaHandle, Link[]> knownProjectAreas = new HashMap<IProjectAreaHandle,Link[]>();
	
	public InitialPokerPage(WolfpokerPageManager manager) {
		this.manager = manager;
	}

	@Override
	public void createControl(Composite parent) {
		scrollArea = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		viewArea = new Composite(scrollArea, SWT.NONE);
		scrollArea.setContent(viewArea);
		scrollArea.setExpandHorizontal(true);
		scrollArea.setExpandVertical(true);
		
		GridLayout layout= new GridLayout();
		layout.marginHeight= 5;
		layout.marginWidth= 5;
		
		viewArea.setLayout(layout);
		viewArea.setBackground(viewArea.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		//Populate with links for the connected project areas
		
		@SuppressWarnings("unchecked")
		List<IProjectAreaHandle> projectAreas = ConnectedProjectAreaRegistry.getDefault().getConnectedProjectAreas(null,false);
	    for (IProjectAreaHandle projectArea : projectAreas)
	    	createProjectAreaLink(projectArea);
	    
	    scrollArea.setMinSize(viewArea.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	    ConnectedProjectAreaRegistry.getDefault().addListener(fProjectAreaChangeListener);
		
	}

	@Override
	public Control getControl() {
		return scrollArea;
	}

	@Override
	public void setFocus() {
		scrollArea.setFocus();
		
	}
	
	private void createProjectAreaLink(final IProjectAreaHandle projectArea) {
		
		String paName = ConnectedProjectAreaRegistry.getDefault().getProjectAreaName(projectArea);
		
		List<Link> links = new ArrayList<Link>();
		for (final PokerSessionQuery query : PokerSessionQuery.getQueries(projectArea)) {
			Link link = new Link(viewArea, SWT.NONE);
			link.setText("<A>Open " + query.getName().toLowerCase() + " sessions ("+ paName +")</A>");
			link.setBackground(viewArea.getBackground());
			link.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
		
					PokerSessionFetchJob job = new PokerSessionFetchJob(manager, query);					
					try{
						manager.runJob(job);
					}catch(Exception e1) {
						e1.printStackTrace();
						//TODO
					}
					
				}
				
			});
			GridDataFactory.fillDefaults().grab(true, false).applyTo(link);
			links.add(link);
		}
		
		//Blank link
		Link link = new Link(viewArea, SWT.NONE);
		link.setBackground(viewArea.getBackground());
		GridDataFactory.fillDefaults().grab(true, false).applyTo(link);
		links.add(link);
		
		Link[] linkArray = new Link[links.size()];
		links.toArray(linkArray);
		knownProjectAreas.put(projectArea, linkArray);

		
	}
	
	private void removeLinks(Link[] links) {
		for (Link link : links)
			link.dispose();
		
	}
	
	
	//Taken from the workitem view, this listener will update the initial view if the project areas change
	private IConnectedProjectAreaRegistryListener fProjectAreaChangeListener = new IConnectedProjectAreaRegistryListener() {
		@SuppressWarnings("unchecked")
		public void connectedProjectAreaRegistryChanged(IConnectedProjectAreaRegistryChangeEvent changeEvent) {
			
			(new UIJob("PokerUpdate") {
				
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					
					if (viewArea.isDisposed())
						return Status.OK_STATUS;
					
					
					List<IProjectAreaHandle> currentProjectAreas = ConnectedProjectAreaRegistry.getDefault().getConnectedProjectAreas(null, false);
					
					for (Iterator<IProjectAreaHandle> iterator= currentProjectAreas.iterator(); iterator.hasNext();) {
						IProjectAreaHandle projectArea= iterator.next();
						if (!knownProjectAreas.keySet().contains(projectArea))
							createProjectAreaLink(projectArea);
					}
					for (Iterator<IProjectAreaHandle> iterator= knownProjectAreas.keySet().iterator(); iterator.hasNext();) {
						IProjectAreaHandle projectArea= iterator.next();
						if (!currentProjectAreas.contains(projectArea))
							removeLinks(knownProjectAreas.get(projectArea));
					}
					viewArea.layout();
					scrollArea.setMinSize(viewArea.computeSize(SWT.DEFAULT, SWT.DEFAULT));
					return Status.OK_STATUS;

				}

		
			}).schedule();
		};
	};

	
}
