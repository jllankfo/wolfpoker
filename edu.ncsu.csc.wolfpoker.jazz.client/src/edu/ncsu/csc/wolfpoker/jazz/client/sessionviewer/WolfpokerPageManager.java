package edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer;


import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.part.PageBookView;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.rcp.ui.internal.ImagePool;

import edu.ncsu.csc.wolfpoker.jazz.client.util.PokerSessionQuery;


/**
 * This is a view class for poker sessions based off of the workitems view
 * Author: Jerrod Lankford
 */

public class WolfpokerPageManager extends PageBookView {
	
	//Pages
	PokerResultsPage resultsPage;
	InitialPokerPage initialPage;
	PokerSessionFetchJob lastJob;
	
	//Actions
	Action backAction;
	Action refreshAction;
	CreateNewAction createAction;
	
	ImageDescriptor createImage;
	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "edu.ncsu.csc.wolfpoker.jazz.client.views.WolfpokerView";

	/**
	 * The constructor.
	 */
	public WolfpokerPageManager() {
		
	}

	
	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		
	}

	@Override
	protected IPage createDefaultPage(PageBook book) {
		
		//Initialize the results page but don't show it
		resultsPage = new PokerResultsPage();
		initPage(resultsPage);
		resultsPage.createControl(book);
		
		initialPage = new InitialPokerPage(this);
		initPage(initialPage);
		initialPage.createControl(book);
		
		return initialPage;
	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		createImage = AbstractUIPlugin.imageDescriptorFromPlugin("edu.ncsu.csc.wolfpoker.jazz.client", "img/create.png");
		setContentDescription("No results to display");
		setUpActionBar();

	}

	private void setUpActionBar() {
	
		
		IToolBarManager manager = getViewSite().getActionBars().getToolBarManager();
		
		backAction = new Action() {
			public void run(){
				showInitialPage();
			}
		};
		
		backAction.setText("Back");
		backAction.setToolTipText("Back to initial view");
		backAction.setImageDescriptor(ImagePool.HISTORY_BACK_ENABLED_ICON);
		backAction.setEnabled(false);
		
		refreshAction = new Action() {
			@Override
			public void run() {
				//rerun job
				if (lastJob != null)
					lastJob.schedule();

			}
		};
		
		refreshAction.setText("Refresh");
		refreshAction.setToolTipText("Refresh current query");
		refreshAction.setImageDescriptor(ImagePool.REFRESH_ACTION_ICON);
		refreshAction.setEnabled(false);
		
		createAction = new CreateNewAction();
		createAction.setImageDescriptor(createImage);
		createAction.setEnabled(false);
		
		manager.add(createAction);
		manager.add(backAction);
		manager.add(refreshAction);
	}


	@Override
	protected PageRec doCreatePage(IWorkbenchPart part) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected void doDestroyPage(IWorkbenchPart part, PageRec pageRecord) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected IWorkbenchPart getBootstrapPart() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean isImportant(IWorkbenchPart part) {
		// TODO Auto-generated method stub
		return false;
	}

	public PokerResultsPage getResultsPage() {
		return resultsPage;
	}
	
	public void showInitialPage() {
		setContentDescription("No results to display");
		getPageBook().showPage(initialPage.getControl());
		lastJob = null;
		
		//Disable buttons
		refreshAction.setEnabled(false);
		createAction.setEnabled(false);
		backAction.setEnabled(false);
		resultsPage.clearResults();
	}

	public void runJob(PokerSessionFetchJob job) throws TeamRepositoryException {
		lastJob = job;
		getPageBook().showPage(resultsPage.getControl());
		refreshAction.setEnabled(true);
		createAction.setEnabled(true);
		backAction.setEnabled(true);
		
		//Doing the fetch here means we only fetch the project area on the initial results creation
		if (job != null) {
			PokerSessionQuery query = job.getQuery();
			ITeamRepository repo = (ITeamRepository) query.getProjectArea().getOrigin();
			IProjectArea projectArea = (IProjectArea) repo.itemManager().fetchCompleteItem(query.getProjectArea(), IItemManager.UNSHARED,null);
			createAction.setProjectArea(projectArea);
			job.setProjectName(projectArea.getName());
			setContentDescription("Running " + job.getQuery().getName());
		}
		
		job.schedule();
		
	}
	
	public void setResults(List<SessionWrapper> sessions) {
		resultsPage.setResults(sessions);
	}
	
	public void setDescription(String desc) {
		setContentDescription(desc);
	}
	
}