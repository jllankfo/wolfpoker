package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.progress.UIJob;

import com.ibm.team.jface.JazzResources;
import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.IExtensibleItem;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.client.IWorkItemClient;
import com.ibm.team.workitem.common.expression.IQueryableAttribute;
import com.ibm.team.workitem.common.model.IAttribute;
import com.ibm.team.workitem.common.model.IEnumeration;
import com.ibm.team.workitem.common.model.ILiteral;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.IWorkItemHandle;
import com.ibm.team.workitem.common.model.IWorkItemType;
import com.ibm.team.workitem.common.model.Identifier;
import com.ibm.team.workitem.common.model.ItemProfile;
import com.ibm.team.workitem.rcp.ui.WorkItemUI;

import edu.ncsu.csc.wolfpoker.jazz.client.util.WorkitemEnumUtil;
import edu.ncsu.csc.wolfpoker.jazz.clientlib.IWolfPokerClientLib;
import edu.ncsu.csc.wolfpoker.jazz.clientlib.ItemChangeListener;
import edu.ncsu.csc.wolfpoker.jazz.clientlib.ItemChangeNotifier;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.LastActiveSessionOperation;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ItemForEstimation;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Vote;

public class PokerSession extends EditorPart {

	private static final int WORKITEM_NAME_COLUMN_WIDTH = 200;
	private static final int WORKITME_NAME_COLUMN = 2;
	private static final String SAVE_VOTE = "Save Vote";
	private static final String WORKITEM_UNDER_CONSIDERATION = "Workitem:";
	private static final int NO_CARD_COLUMNS = 5;
	private static final String IMG_DEFAULT_DECK = "/img/deck1.jpg";
	private static final String IMG_VOTED_DECK = "/img/deck.jpg";
	private static final String IMG_CARD_PATH = "/img/";
	private static final String POKER_TABLE = "Poker Table";
	private static final String PLANNING_POKER_SESSION = "Active Poker Session";
	private static final String PLANNING_POKER_SESSION_LEFT = "Poker Session (Coordinator Left)";
	private static final RGB FONT_COLOR = new RGB(25, 76, 127);
	private static final RGB WHITE_COLOR = new RGB(255, 255, 255);

	private static final String SESSION_NAME = "Session Name:";
	private static final String SESSION_PARTICIPANTS = "Participants";
	private static final String SAVE_BUTTON = "Save";
	private static final String CANCEL_BUTTON = "Cancel";
	private static final String WORKITEMS = "Work Item";
	private static final String ID = "ID";
	private static final String WORKITEM_NAME = "Work Item";
	private static final String ESTIMATE = "Estimate";
	private static final int ESTIMATE_COLUMN_INDEX = 3;
	private static final int TABLE_HEIGHT = 145;
	private static final int NO_WI_COLUMNS = 4;
	private static final String PARTICIPANTS_COLUMN_NAME = "Participant Name";
	private static final String JOINED_COLUMN_NAME = "Joined";
	private static final String JOIN = "Y";
	private static final String NOT_JOIN = "N";
	private static final String START_VOTE = "Start Voting";
	private static final String SHOW_VOTES = "Show Votes";
	private static final String REVOTE = "Revote";
	private static final String CANCEL_VOTE = "Cancel Voting";
	private static final String CARDS = "Cards";
	private static final int HIGHLIGHT_CARD_BORDER = 3;
	private static final int HIGHLIGHT_CARD_COLOR = 0x000000FF;

	private FormToolkit toolkit;
	private ScrolledForm form;
	private Color fontColor;
	private Button saveButton;
	private Button cancelButton;
	private Session session;
	private IProjectArea projectArea;
	private ITeamRepository repository;
	private Table workItemTable;
	private PokerSession editor;
	private ActiveSession activeSession;
	private Map<String, TableItem> participantJoinedMap = new HashMap<String, TableItem>();
	private Map<String, TableItem> workItemMap = new HashMap<String, TableItem>();
	private Map<String, Button> participantCardMap = new HashMap<String, Button>();
	private Table participantsTable;
	private ItemChangeNotifier changeNotifier;
	private IWolfPokerClientLib library;
	private Section voteSection;
	private Composite voteComposite;
	private Button startVoting;
	private Image defaultDeckImage;
	private Image votedDeckImage;
	private Button showVotesButton;
	private Button revoteButton;
	private boolean isParticipant;
	private Composite cardComposite;
	private Section cardSection;
	private Color whiteColor;
	private Button saveVoteButton;
	private Button cancelVoteButton;
	private static int[] cards;
	private static final int QUESTION_MARK = -1;

	private void initSWTcomponent(Composite parent) {
		toolkit = new FormToolkit(parent.getDisplay());
		form = toolkit.createScrolledForm(parent);
		form.setText(PLANNING_POKER_SESSION);

		form.getBody().setLayout(new GridLayout(2, true));
		fontColor = new Color(parent.getDisplay(), FONT_COLOR);
		whiteColor = new Color(parent.getDisplay(), WHITE_COLOR);
		defaultDeckImage = getImage(IMG_DEFAULT_DECK);
		votedDeckImage = getImage(IMG_VOTED_DECK);

		initHeader();
		initWorkItemsSection();
		initParticipantsSection();
		initVoteSection();
		initCardSesction();

		changeNotifier = new ItemChangeNotifier(activeSession,
				new ItemChangeListener() {

					public void itemChaged(ActiveSession itemOrig,
							ActiveSession itemNew) {
						activeSession = itemNew;
						if (isParticipant) {
							refreshUIforParticipant();
						} else {
							refreshUI();
						}

					}

					public void itemNotFound() {
						showErrorDialog();
					}

				}, repository);

		changeNotifier.startListening();
	}

	private void showErrorDialog() {
		MessageBox messageBox = new MessageBox(form.getBody()
				.getShell(), SWT.ICON_ERROR | SWT.OK);
		messageBox
				.setMessage("The coordinator has left the session");
		messageBox.setText("No coordinator");
		messageBox.open();

		form.setText(PLANNING_POKER_SESSION_LEFT);

	}

	private void initHeader() {
		Composite composite = toolkit.createComposite(form.getBody());
		composite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2,
				1));
		composite.setLayout(new GridLayout(4, false));
		toolkit.createLabel(composite, SESSION_NAME).setForeground(fontColor);

		Text sessionName = toolkit.createText(composite, session.getText());
		sessionName.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
		sessionName.setEditable(false);
		if (!isParticipant) {
			saveButton = toolkit.createButton(composite, SAVE_BUTTON, SWT.PUSH);
			saveButton.setLayoutData(new GridData(70, -1));
			((GridData) saveButton.getLayoutData()).verticalAlignment = SWT.TOP;
			saveButton.addSelectionListener(new SaveButtonListener());
		}

		cancelButton = toolkit.createButton(composite, CANCEL_BUTTON, SWT.PUSH);
		cancelButton.setLayoutData(new GridData(70, -1));
		((GridData) cancelButton.getLayoutData()).verticalAlignment = SWT.TOP;
		cancelButton.addSelectionListener(new CancelButtonListener());
		
		

	}

	private void initWorkItemsSection() {
		Section workItemSection = toolkit.createSection(form.getBody(),
				Section.TITLE_BAR | Section.EXPANDED);
		workItemSection.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
				false));
		workItemSection.setText(WORKITEMS);
		Composite workItemComposite = toolkit.createComposite(workItemSection);

		workItemComposite.setLayout(new GridLayout(2, false));
		workItemSection.setClient(workItemComposite);

		workItemTable = toolkit.createTable(workItemComposite, SWT.SINGLE
				| SWT.BORDER | SWT.FULL_SELECTION);
		workItemTable.setLinesVisible(true);
		workItemTable.setHeaderVisible(true);
		GridData data = new GridData(SWT.FILL, SWT.TOP, true, false);
		data.heightHint = TABLE_HEIGHT;
		data.verticalSpan = 5;
		workItemTable.setLayoutData(data);

		/* initialize the table column */
		TableColumn column = new TableColumn(workItemTable, SWT.CENTER);
		column.setText(" ");
		column.setMoveable(false);
		column.setResizable(true);

		column = new TableColumn(workItemTable, SWT.CENTER);
		column.setText(ID);
		column.setMoveable(false);
		column.setResizable(true);
		column.addListener(SWT.Selection, new TableSorter(workItemTable,
				column, true, false));

		column = new TableColumn(workItemTable, SWT.LEFT);
		column.setText(WORKITEM_NAME);
		column.setMoveable(false);
		column.setResizable(true);
		column.addListener(SWT.Selection, new TableSorter(workItemTable,
				column, false, false));
		column.setWidth(WORKITEM_NAME_COLUMN_WIDTH);

		column = new TableColumn(workItemTable, SWT.CENTER);
		column.setText(ESTIMATE);
		column.setMoveable(false);
		column.setResizable(false);
		column.addListener(SWT.Selection, new TableSorter(workItemTable,
				column, true, false));

		/* Create a resource manager for the table's icons */
		final LocalResourceManager resourceManager = new LocalResourceManager(
				JFaceResources.getResources(), workItemTable);

		/* Get the workItems from the session */
		java.util.List workItems = session.getItems();
		final IWorkItemClient workItemClient = (IWorkItemClient) repository
				.getClientLibrary(IWorkItemClient.class);

		for (Iterator iterator = workItems.iterator(); iterator.hasNext();) {
			IWorkItemHandle workItemHandle = (IWorkItemHandle) iterator.next();
			try {
				final IWorkItem workItem = HandleToObject
						.convert(workItemHandle);

				final TableItem item = new TableItem(workItemTable, SWT.CENTER);
				workItemMap.put(workItem.getItemId().getUuidValue(), item);
				item.setData(workItem);
				UIJob job = new UIJob("Loading icons") {
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						try {
							IWorkItemType type = workItemClient
									.findWorkItemType(
											workItem.getProjectArea(), workItem
													.getWorkItemType(), monitor);
							ImageDescriptor imageDescriptor = WorkItemUI
									.getImageDescriptor(type.getIconURL());
							Image image = JazzResources.getImageWithDefault(
									resourceManager, imageDescriptor);
							item.setImage(0, image);
						} catch (TeamRepositoryException e) {
							e.printStackTrace();
						}
						return Status.OK_STATUS;
					}
				};
				job.setUser(false);
				job.schedule();
				item.setText(1, workItem.getId() + "");
				item.setText(2, workItem.getHTMLSummary().toString());
				String estimate = "";
				try {
					estimate = getComplexityValue(workItem);
					item.setText(ESTIMATE_COLUMN_INDEX, estimate + "");
				} catch (IllegalStateException ise) {
					item.setText(ESTIMATE_COLUMN_INDEX, "");
				}

			} catch (TeamRepositoryException e) {
				e.printStackTrace();
			}
		}
		for (int i = 0; i < NO_WI_COLUMNS; i++) {
			workItemTable.getColumn(i).pack();
			if (i == WORKITME_NAME_COLUMN)
				workItemTable.getColumn(i).setWidth(WORKITEM_NAME_COLUMN_WIDTH);
		}
		workItemTable.addMouseListener(new TableItemDoubleClickListener());

		if (!isParticipant) {
			startVoting = toolkit.createButton(workItemComposite, START_VOTE,
					SWT.PUSH);
			startVoting.setLayoutData(new GridData(100, -1));
			((GridData) startVoting.getLayoutData()).verticalAlignment = SWT.TOP;
			startVoting.addSelectionListener(new StartVotingListener());

			showVotesButton = toolkit.createButton(workItemComposite,
					SHOW_VOTES, SWT.PUSH);
			showVotesButton.setLayoutData(new GridData(100, -1));
			((GridData) showVotesButton.getLayoutData()).verticalAlignment = SWT.TOP;
			showVotesButton.setEnabled(false);
			showVotesButton.addSelectionListener(new ShowVotesListener());

			revoteButton = toolkit.createButton(workItemComposite, REVOTE,
					SWT.PUSH);
			revoteButton.setLayoutData(new GridData(100, -1));
			((GridData) revoteButton.getLayoutData()).verticalAlignment = SWT.TOP;
			revoteButton.setEnabled(false);
			revoteButton.addSelectionListener(new RevoteListener());

			saveVoteButton = toolkit.createButton(workItemComposite, SAVE_VOTE,
					SWT.PUSH);
			saveVoteButton.setLayoutData(new GridData(100, -1));
			((GridData) saveVoteButton.getLayoutData()).verticalAlignment = SWT.TOP;
			saveVoteButton.setEnabled(false);
			saveVoteButton.addSelectionListener(new SaveVoteListener());

			cancelVoteButton = toolkit.createButton(workItemComposite,
					CANCEL_VOTE, SWT.PUSH);
			cancelVoteButton.setLayoutData(new GridData(100, -1));
			((GridData) cancelVoteButton.getLayoutData()).verticalAlignment = SWT.TOP;
			cancelVoteButton.setEnabled(false);
			cancelVoteButton.addSelectionListener(new CancelVotesListener());
			
		}
	}

	private class TableItemDoubleClickListener extends MouseAdapter {

		public void mouseDoubleClick(MouseEvent e) {

			if (e.widget == workItemTable
					&& workItemTable.getSelectionCount() > 0) {
				TableItem doubleClickedItem;
				// assume that the first one they selected is what they
				// double-clicked on
				doubleClickedItem = workItemTable.getSelection()[0];
				Object workItem = doubleClickedItem.getData();
				WorkItemUI.openEditor(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage(), workItem);
			}

		}
	}

	String getComplexityValue(IWorkItem workItem)
			throws TeamRepositoryException {
		IWorkItemClient workItemClient = (IWorkItemClient) repository
				.getClientLibrary(IWorkItemClient.class);

		IAttribute attribute = workItemClient.findAttribute(projectArea,
				"com.ibm.team.apt.attribute.complexity", null);
		/*
		 * TODO it has null pointer exception if the project is not scrum, so I
		 * add "attribute !=null" 
		 * by Samuel
		 */
		if (attribute != null && workItem.hasAttribute(attribute)) {
			Identifier<ILiteral> obj = (Identifier<ILiteral>) workItem
					.getValue(attribute);
			ILiteral literal = WorkitemEnumUtil.getLiteralbyID(obj, attribute, repository);
			return literal.getName();
		} else {
			return "";
		}

	}

	private void initParticipantsSection() {

		Section participantSection = toolkit.createSection(form.getBody(),
				Section.TITLE_BAR | Section.EXPANDED);
		participantSection.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
				false));

		participantSection.setText(SESSION_PARTICIPANTS);

		Composite participantComposite = toolkit
				.createComposite(participantSection);

		participantComposite.setLayout(new GridLayout(2, false));
		participantSection.setClient(participantComposite);

		participantsTable = toolkit.createTable(participantComposite,
				SWT.SINGLE | SWT.BORDER);
		participantsTable.setLinesVisible(true);
		participantsTable.setHeaderVisible(true);
		GridData data = new GridData(SWT.FILL, SWT.TOP, true, false);
		data.heightHint = TABLE_HEIGHT;
		participantsTable.setLayoutData(data);

		TableColumn column = new TableColumn(participantsTable, SWT.CENTER);
		column.setText(PARTICIPANTS_COLUMN_NAME);
		column.setMoveable(false);
		column.setResizable(false);
		column = new TableColumn(participantsTable, SWT.CENTER);
		column.setText(JOINED_COLUMN_NAME);
		column.setMoveable(false);
		column.setResizable(false);

		initParticipants();

		for (int i = 0; i < 2; i++) {
			participantsTable.getColumn(i).pack();
		}
		participantsTable.addMouseListener(new UserDoubleClickListener());
	}

	private void initParticipants() {
		java.util.List participants = session.getParticipants();
		for (Iterator iterator = participants.iterator(); iterator.hasNext();) {

			IContributorHandle contributorHandle = (IContributorHandle) iterator
					.next();

			IContributor contributor;
			contributor = HandleToObject.convert(contributorHandle);

			TableItem item = new TableItem(participantsTable, SWT.NONE);
			item.setText(0, contributor.getName());
			item.setText(1, NOT_JOIN);
			item.setData(contributor);
			participantJoinedMap.put(contributor.getItemId().getUuidValue(),
					item);

		}

		refreshJoinedParticipants();
	}

	/**
	 * DoubleClickListener for opening the user editor by double-click the user
	 */
	public class UserDoubleClickListener extends MouseAdapter {
		public void mouseDoubleClick(MouseEvent e) {
			if (e.widget == participantsTable
					&& participantsTable.getSelectionCount() > 0) {
				TableItem doubleClickedItem;
				// assume that the first one they selected is what they
				// double-clicked on
				doubleClickedItem = participantsTable.getSelection()[0];
				Object participant = doubleClickedItem.getData();
				WorkItemUI.openEditor(PlatformUI.getWorkbench()
						.getActiveWorkbenchWindow().getActivePage(),
						participant);
			}
		}
	}

	private void initVoteSection() {
		voteSection = toolkit.createSection(form.getBody(), Section.TITLE_BAR
				| Section.EXPANDED);
		voteSection.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		voteSection.setText(POKER_TABLE);
		ScrolledComposite scroll = new ScrolledComposite(voteSection,
				SWT.V_SCROLL);
		scroll.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, true, false));
		scroll.setBackground(whiteColor);

		voteComposite = toolkit.createComposite(scroll);
		scroll.setContent(voteComposite);
		voteComposite.setLayout(new GridLayout(1, false));
		voteSection.setClient(scroll);
		voteSection.setVisible(false);
	}

	private void initCardSesction() {
		cardSection = toolkit.createSection(form.getBody(), Section.TITLE_BAR
				| Section.EXPANDED);
		cardSection.setText(CARDS);
		cardSection
				.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

		ScrolledComposite scroll = new ScrolledComposite(cardSection,
				SWT.V_SCROLL | SWT.H_SCROLL);
		scroll.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		scroll.setBackground(whiteColor);
		cardComposite = toolkit.createComposite(scroll);
		scroll.setContent(cardComposite);
		cardSection.setClient(scroll);
		cardComposite.setLayout(new GridLayout(NO_CARD_COLUMNS, false));
		cardSection.setVisible(false);
	}

	private void refreshUI() {
		switch (activeSession.getLastOperation()) {
		case LastActiveSessionOperation.PARTICIPANT_JOINED:
			refreshJoinedParticipants();
			break;

		case LastActiveSessionOperation.VOTE:
			refreshVotedParticipants();
			break;

		case LastActiveSessionOperation.VOTECANCEL:
			refreshVotedParticipants();
			break;

		case LastActiveSessionOperation.PARTICIPANT_LEFT:
			refreshJoinedParticipants();
			break;
		case LastActiveSessionOperation.VOTE_SAVED:
			showSavedResult();
			break;
		default:
			break;
		}
	}

	private void refreshUIforParticipant() {
		switch (activeSession.getLastOperation()) {
		case LastActiveSessionOperation.PARTICIPANT_JOINED:
			refreshJoinedParticipants();
			break;
		case LastActiveSessionOperation.PARTICIPANT_LEFT:
			refreshJoinedParticipants();
			// to do check for administrator
			break;
		case LastActiveSessionOperation.VOTE_STARTED:
			cardSection.setVisible(true);
			refreshCardsView(false);
			refreshVotesView();
			break;
		case LastActiveSessionOperation.REVOTE:
			refreshCardsView(false);
			refreshVotesView();
			break;
		case LastActiveSessionOperation.VOTE:
			refreshVotedParticipants();
			break;
		case LastActiveSessionOperation.VOTECANCEL:
			refreshVotedParticipants();
			break;
		case LastActiveSessionOperation.SHOW_RESULT:
			voteSection.setVisible(true);
			cardSection.setVisible(true);
			showVotesInHistoryArea();
			clearComposite(cardComposite);
			break;
		case LastActiveSessionOperation.VOTE_SAVED:
			showSavedResult();
			refresUIOnSaveVote();
			break;
		case LastActiveSessionOperation.VOTEGAMECANCEL:
			clearComposite(cardComposite);
			break;
		default:
			break;
		}
	}

	private void refreshVotesView() {
		IItemHandle item = activeSession.getItemUnderDiscussion().getItem();

		if (item != null) {
			IWorkItem workItem;
			workItem = HandleToObject.convert(item);
			startNextVote(workItem);
		}
	}

	private void revote() {
		ItemForEstimation iud = activeSession.getItemUnderDiscussion();
		IWorkItem workItem;
		try {
			workItem = HandleToObject.convert(iud.getItem());

			library.startRevoting(activeSession, workItem);
			startNextVote(workItem);
		} catch (TeamRepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void showVotesInHistoryArea() {
		ItemForEstimation iud = activeSession.getItemUnderDiscussion();
		List votes = iud.getCurrentVotes();

		int low = getLow(votes.iterator());
		int high = getHigh(votes.iterator());

		for (Iterator voteIter = votes.iterator(); voteIter.hasNext();) {
			Vote vote = (Vote) voteIter.next();
			Button card = participantCardMap.get(vote.getParticipant()
					.getItemId().getUuidValue());

			ImageData imageData = (loadCardImage(vote.getVote()))
					.getImageData();

			/*
			 * Highlight the card when 1. Highest vote 2. Lowest vote 3.
			 * Question mark (Always) except when votes are all the same value
			 * (it means that highest value equals lowest value
			 */
			if (((high != low) && (vote.getVote() == high || vote.getVote() == low))
					|| vote.getVote() == QUESTION_MARK)
				highlightBorder(imageData);

			Image image = new Image(form.getDisplay(), imageData);
			card.setImage(image);

			card.pack(true);
		}
	}

	private int getLow(Iterator iterator) {
		int low = Integer.MAX_VALUE;
		while (iterator.hasNext()) {
			Vote vote = (Vote) iterator.next();
			if (vote.getVote() < low && (vote.getVote() != -1))
				low = vote.getVote();
		}
		return low;
	}

	private int getHigh(Iterator iterator) {
		int high = Integer.MIN_VALUE;
		while (iterator.hasNext()) {
			Vote vote = (Vote) iterator.next();
			if (vote.getVote() > high)
				high = vote.getVote();
		}
		return high;
	}

	private void highlightBorder(ImageData imgData) {
		// go across the top & bottom
		for (int x = 0; x < imgData.width; x++) {
			for (int y = 0; y < HIGHLIGHT_CARD_BORDER; y++) {
				imgData.setPixel(x, y, HIGHLIGHT_CARD_COLOR);
				imgData.setPixel(imgData.width - x - 1, imgData.height - y - 1,
						HIGHLIGHT_CARD_COLOR);
			}
		}
		// go down the sides
		for (int y = 0; y < imgData.height; y++) {
			for (int x = 0; x < HIGHLIGHT_CARD_BORDER; x++) {
				imgData.setPixel(x, y, HIGHLIGHT_CARD_COLOR);
				imgData.setPixel(imgData.width - x - 1, imgData.height - y - 1,
						HIGHLIGHT_CARD_COLOR);
			}
		}
	}

	private void clearComposite(Composite composite) {
		Control[] controls = composite.getChildren();
		for (int j = 0; j < controls.length; j++) {
			controls[j].setVisible(false);
			controls[j].dispose();
		}
		composite.pack(true);
		composite.redraw();
	}

	private void startVotingOnWorkItem(IWorkItem workItem) {

		cardSection.setVisible(true);
		startNextVote(workItem);
		refreshCardsView(false);
	}

	private void showSavedResult() {

		participantCardMap.clear();
		voteSection.setVisible(true);
		int savedVote = activeSession.getItemUnderDiscussion().getSavedVote();

		IItemHandle item = activeSession.getItemUnderDiscussion().getItem();
		IWorkItem workItem = HandleToObject.convert(item);

		Composite currentVotesComposite = toolkit
				.createComposite(voteComposite);
		currentVotesComposite.setLayout(new GridLayout(1, false));
		currentVotesComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP,
				true, false));

		Composite voteHeaderComposite = toolkit
				.createComposite(currentVotesComposite);
		voteHeaderComposite.setLayout(new GridLayout(1, false));
		voteHeaderComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
				false));

		activeSession = HandleToObject.convert(activeSession);

		String text = WORKITEM_UNDER_CONSIDERATION + "Vote Saved:";

		toolkit.createLabel(voteHeaderComposite, text).setForeground(fontColor);

		Label workItemLabel = toolkit.createLabel(voteHeaderComposite, workItem
				.getHTMLSummary().getPlainText(), SWT.WRAP);

		GridData labelLayoutData = new GridData(SWT.FILL, SWT.TOP, true, false);
		workItemLabel.setLayoutData(labelLayoutData);

		Composite votesOfParticipantsComposite = toolkit
				.createComposite(currentVotesComposite);
		votesOfParticipantsComposite.setLayout(new GridLayout(NO_CARD_COLUMNS,
				false));
		Composite cardWithName = toolkit
				.createComposite(votesOfParticipantsComposite);
		cardWithName.setLayout(new GridLayout(1, false));

		Button startSessionButton = toolkit.createButton(cardWithName, "",
				SWT.PUSH);
		toolkit.createLabel(cardWithName, "Saved Vote");

		startSessionButton.setImage(loadCardImage(savedVote));

		currentVotesComposite.moveAbove(null);
		voteComposite.pack(true);
	}

	private void startNextVote(IWorkItem workItem) {
		participantCardMap.clear();
		voteSection.setVisible(true);

		Composite currentVotesComposite = toolkit
				.createComposite(voteComposite);
		currentVotesComposite.setLayout(new GridLayout(1, false));
		currentVotesComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP,
				true, false));

		Composite voteHeaderComposite = toolkit
				.createComposite(currentVotesComposite);
		voteHeaderComposite.setLayout(new GridLayout(1, false));
		voteHeaderComposite.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true,
				false));

		activeSession = HandleToObject.convert(activeSession);

		String text = WORKITEM_UNDER_CONSIDERATION + "Round:"
				+ activeSession.getItemUnderDiscussion().getRoundNo() + ":";
		toolkit.createLabel(voteHeaderComposite, text).setForeground(fontColor);
		Label workItemLabel = toolkit.createLabel(voteHeaderComposite, workItem
				.getHTMLSummary().getPlainText(), SWT.WRAP);
		GridData labelLayoutData = new GridData(SWT.FILL, SWT.TOP, true, false);
		workItemLabel.setLayoutData(labelLayoutData);

		Composite votesOfParticipantsComposite = toolkit
				.createComposite(currentVotesComposite);
		votesOfParticipantsComposite.setLayout(new GridLayout(NO_CARD_COLUMNS,
				false));
		List participantsList = activeSession.getJoinedParticipants();
		for (Iterator iterator = participantsList.iterator(); iterator
				.hasNext();) {
			IContributorHandle participantHandle = (IContributorHandle) iterator
					.next();
			IContributor contributor = HandleToObject
					.convert(participantHandle);
			Composite cardWithName = toolkit
					.createComposite(votesOfParticipantsComposite);
			cardWithName.setLayout(new GridLayout(1, false));
			Button startSessionButton = toolkit.createButton(cardWithName, "",
					SWT.PUSH);
			participantCardMap.put(contributor.getItemId().getUuidValue(),
					startSessionButton);
			toolkit.createLabel(cardWithName, contributor.getName());

			startSessionButton.setImage(defaultDeckImage);
		}
		currentVotesComposite.moveAbove(null);
		voteComposite.pack(true);
	}

	private void refreshJoinedParticipants() {

		Collection allParticipants = participantJoinedMap.values();
		for (Iterator iterator = allParticipants.iterator(); iterator.hasNext();) {
			TableItem tableItem = (TableItem) iterator.next();
			tableItem.setText(1, NOT_JOIN);
		}
		List joinedParticipants = activeSession.getJoinedParticipants();
		for (Iterator iterator = joinedParticipants.iterator(); iterator
				.hasNext();) {
			IContributorHandle contributor = (IContributorHandle) iterator
					.next();
			TableItem tableItem = participantJoinedMap.get(contributor
					.getItemId().getUuidValue());
			if (tableItem != null)
				tableItem.setText(1, JOIN);
		}
		for (int i = 0; i < 2; i++) {
			participantsTable.getColumn(i).pack();
		}
	}

	private void refreshVotedParticipants() {
		ItemForEstimation iud = activeSession.getItemUnderDiscussion();
		List votes = iud.getCurrentVotes();

		for (Iterator iterator = participantCardMap.values().iterator(); iterator
				.hasNext();) {
			Button card = (Button) iterator.next();
			card.setImage(defaultDeckImage);
		}
		for (Iterator voteIter = votes.iterator(); voteIter.hasNext();) {
			Vote vote = (Vote) voteIter.next();
			Button card = participantCardMap.get(vote.getParticipant()
					.getItemId().getUuidValue());
			card.setImage(votedDeckImage);
			card.pack(true);
		}

		if (votes.size() == participantCardMap.size())
			showVotes();
	}

	/**
	 * To generate the voting card view and add functionality to the card
	 * 
	 * @param saveVote
	 *            TRUE for moderator to save the vote FALSE for participant to
	 *            submit his/her vote
	 */
	private void refreshCardsView(final boolean saveVote) {
		clearComposite(cardComposite);
		IItemHandle item = activeSession.getItemUnderDiscussion().getItem();

		if (item != null) {
			IWorkItem curIud;
			curIud = HandleToObject.convert(item);

			final Composite voteSectionHeaderComposite = toolkit
					.createComposite(cardComposite);
			final GridLayout layout = new GridLayout(1, false);
			voteSectionHeaderComposite.setLayout(layout);
			GridData headerData = new GridData();
			headerData.horizontalSpan = NO_CARD_COLUMNS;
			voteSectionHeaderComposite.setLayoutData(headerData);

			toolkit.createLabel(voteSectionHeaderComposite,
					WORKITEM_UNDER_CONSIDERATION).setForeground(fontColor);
			final Label workItemLabel = toolkit.createLabel(
					voteSectionHeaderComposite, curIud.getHTMLSummary()
							.getPlainText(), SWT.WRAP);
			final GridData data = new GridData(SWT.FILL, SWT.TOP, true, false);
			data.widthHint = 500;
			workItemLabel.setLayoutData(data);

			for (int i = 0; i < cards.length; i++) {

				final int card = cards[i];

				/* Don't allow save question mark */
				if (saveVote && card == QUESTION_MARK) {
					continue;
				}

				Composite eachCardComposite = toolkit
						.createComposite(cardComposite);
				eachCardComposite.setLayout(new GridLayout(1, false));
				eachCardComposite.setLayoutData(new GridData());

				Button cardButton = toolkit.createButton(eachCardComposite, "",
						SWT.PUSH);

				cardButton.setImage(loadCardImage(card));

				cardButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						if (saveVote) {
							saveVote(card);
						} else {
							submitVote(card);
						}

					}

				});

				Hyperlink exampleLink = toolkit.createHyperlink(
						eachCardComposite, "Similar", 0);
				exampleLink.addHyperlinkListener(new HyperlinkAdapter() {
					@Override
					public void linkActivated(HyperlinkEvent e) {
						new FindSimilarButtonListener(repository, projectArea,
								card).widgetSelected(null);
					}
				});
			}
			cardComposite.pack(true);
		}

	}

	private void refresUIOnSaveVote() {
		ItemForEstimation iud = activeSession.getItemUnderDiscussion();

		IWorkItem workItem;
		workItem = HandleToObject.convert(iud.getItem());
		int card = iud.getSavedVote();
		TableItem tableItem = workItemMap.get(workItem.getItemId()
				.getUuidValue());
		tableItem.setText(ESTIMATE_COLUMN_INDEX, "" + card);
		workItemTable.getColumn(ESTIMATE_COLUMN_INDEX).pack();
		if (!isParticipant)
			startVoting.setEnabled(true);

		clearComposite(cardComposite);
	}

	private void saveVote(int card) {
		ItemForEstimation iud = activeSession.getItemUnderDiscussion();

		IWorkItem workItem;
		try {
			
			workItem = HandleToObject.convert(iud.getItem());
			String oldValue = "";
			try{
				oldValue = getComplexityValue(workItem);
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			library.saveVote(activeSession, workItem, card,oldValue);
			TableItem tableItem = workItemMap.get(workItem.getItemId()
					.getUuidValue());
			tableItem.setText(ESTIMATE_COLUMN_INDEX, "" + card);
			workItemTable.getColumn(ESTIMATE_COLUMN_INDEX).pack();
			startVoting.setEnabled(true);
		} catch (TeamRepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		clearComposite(cardComposite);
	}

	private void submitVote(final int card) {
		try {
			ItemForEstimation iud = activeSession.getItemUnderDiscussion();
			
			IWorkItem workItem = HandleToObject.convert(iud.getItem());
			
			library.vote(activeSession, workItem, card);
			
			clearComposite(cardComposite);

			final Composite voteSectionHeaderComposite = toolkit
					.createComposite(cardComposite);
			final GridLayout layout = new GridLayout(1, false);
			voteSectionHeaderComposite.setLayout(layout);
			GridData headerData = new GridData();
			headerData.horizontalSpan = NO_CARD_COLUMNS;
			voteSectionHeaderComposite.setLayoutData(headerData);

			toolkit.createLabel(voteSectionHeaderComposite,
					WORKITEM_UNDER_CONSIDERATION).setForeground(fontColor);
			final Label workItemLabel = toolkit.createLabel(
					voteSectionHeaderComposite, workItem.getHTMLSummary()
							.getPlainText(), SWT.WRAP);
			final GridData data = new GridData(SWT.FILL, SWT.TOP, true, false);
			data.widthHint = 500;
			workItemLabel.setLayoutData(data);

			Button cardButton = toolkit.createButton(cardComposite, "",
					SWT.PUSH);

			cardButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					cancelVote();
					refreshCardsView(false);
				}
			});
			cardButton.setToolTipText("Click it again to cancel the vote");
			cardButton.setImage(loadCardImage(card));

			Button cancelButton = toolkit.createButton(cardComposite, "",
					SWT.PUSH);
			cancelButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					cancelVote();
					refreshCardsView(false);
				}
			});

			String imagePath = IMG_CARD_PATH + "cancel_vote.jpg";
//			InputStream cardStream = getImage(imagePath);
			Image image =getImage(imagePath);
			cancelButton.setImage(image);

			cardComposite.pack(true);
		} catch (TeamRepositoryException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Call InputStream to load the image from file and draw the number on it
	 * according to the given card value
	 * 
	 * @param button
	 *            Button
	 * @param card
	 *            card value
	 */
	private Image loadCardImage(int card) {
		String imagePath = IMG_CARD_PATH + "background.jpg";
		if (card == QUESTION_MARK) {
			imagePath = IMG_CARD_PATH + "question_mark.jpg";
		}

		Image image = getImage(imagePath);
		if (card != QUESTION_MARK) {
			GC gc = new GC(image);
			gc.drawString(card + "", 3, 1);

			gc.dispose();
		}

		return image;
	}

	private Image getImage(String imagePath) {
		ImageDescriptor desc = ImageDescriptor.createFromFile(PokerSession.class, imagePath);
		Image img = desc.createImage();
		return img;
	}

	private void cancelVote() {
		try {
			ItemForEstimation iud = activeSession.getItemUnderDiscussion();

			IWorkItem workItem = HandleToObject.convert(iud.getItem());
			library.voteCancel(activeSession, workItem);

		} catch (TeamRepositoryException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
		session = ((PokerSessionInput) input).getSession();
		activeSession = ((PokerSessionInput) input).getActiveSession();
		projectArea = ((PokerSessionInput) input).getProjectArea();
		repository = (ITeamRepository) projectArea.getOrigin();
		isParticipant = !PokerSessionUtil.isCoordinator(session);
		editor = this;
		library = (IWolfPokerClientLib) repository
				.getClientLibrary(IWolfPokerClientLib.class);

		/* Load the card values from Project Area Enumeration */
		cards = getCardValues();
	}

	/**
	 * Load the card values list from enumeration
	 * 
	 * @return
	 */
	private int[] getCardValues() {

		IWorkItemClient workItemCleint = (IWorkItemClient) repository
				.getClientLibrary(IWorkItemClient.class);

		IProgressMonitor monitor = new NullProgressMonitor();
		IAttribute myAttribute;
		try {
			myAttribute = workItemCleint.findAttribute(projectArea,
					"com.ibm.team.apt.attribute.complexity", monitor);

			if (myAttribute != null) {
				IEnumeration<ILiteral> enumeration = (IEnumeration<ILiteral>) workItemCleint
						.resolveEnumeration(myAttribute, monitor);
				java.util.List<ILiteral> literals = enumeration
						.getEnumerationLiterals();
				int[] card = new int[literals.size() + 1];
				/* Put Question_Mark into array */
				card[0] = QUESTION_MARK;
				int index = 1;
				for (ILiteral literal : literals) {
					String value = literal.getName();
					value = value.trim();
					char[] valueChar = value.toCharArray();
					String number = "";
					for (int i = 0; i < valueChar.length; i++) {
						if (Character.isDigit(valueChar[i])) {
							number += valueChar[i];
						}
					}
					card[index++] = Integer.parseInt(number);
				}

				return card;
			}
		} catch (TeamRepositoryException e) {

			e.printStackTrace();
		}
		/*
		 * If can't find the complexity attribute or any exception then give
		 * default value
		 */
		int[] card = { -1, 0, 1, 2, 3, 5, 8, 13, 20, 40, 100 };
		return card;
	}

	@Override
	public boolean isDirty() {
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void createPartControl(Composite parent) {
		initSWTcomponent(parent);
	}

	@Override
	public void setFocus() {
	}

	@Override
	public void dispose() {

		changeNotifier.stopListening();
		// TODO to put a message box for this
		cancelSession();
		super.dispose();
	}

	private void cancelSession() {
		try {
			changeNotifier.stopListening();
			if (activeSession != null) {
				if (isParticipant) {
					library.leaveSession(activeSession);
				} else {
					library.leaveSession(activeSession);
					library.cancelActiveSession(activeSession);
					activeSession = null;
				}
			}
		} catch (TeamRepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean isTobeEstimated(IWorkItem workItem)
			throws TeamRepositoryException {
		String estimate = getComplexityValue(workItem);

		List itemsCompleted = activeSession.getItemsCompleted();
		for (Iterator iterator = itemsCompleted.iterator(); iterator.hasNext();) {
			ItemForEstimation item = (ItemForEstimation) iterator.next();
			if (item.getItem().getItemId().getUuidValue().equals(
					workItem.getItemId().getUuidValue())) {
				return voteAgainDialog();
			}
		}
		return true;
	}

	private boolean voteAgainDialog() {
		MessageBox messageBox = new MessageBox(form.getBody().getShell(),
				SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		messageBox
				.setMessage("Item has already been estimated. Do you want to re-estimate it?");
		messageBox.setText("Already Estimated");
		int response = messageBox.open();
		if (response == SWT.YES) {
			return true;
		} else {
			return false;
		}
	}

	private void showVotes() {
		ItemForEstimation iud = activeSession.getItemUnderDiscussion();

		if (!isParticipant) {

			try {
				IWorkItem workItem = HandleToObject.convert(iud.getItem());
				library.showResult(activeSession, workItem);
			} catch (TeamRepositoryException ex) {

				ex.printStackTrace();
			}
			showVotesButton.setEnabled(false);
			revoteButton.setEnabled(true);
			saveVoteButton.setEnabled(true);
			startVoting.setEnabled(false);
		}
		showVotesInHistoryArea();

		clearComposite(cardComposite);

	}

	private void cancelVotesGame() {
		try {
			ItemForEstimation iud = activeSession.getItemUnderDiscussion();

			IWorkItem workItem = HandleToObject.convert(iud.getItem());
			library.voteGameCancel(activeSession, workItem);

		} catch (TeamRepositoryException e1) {
			e1.printStackTrace();
		}
	}

	private final class SaveVoteListener extends SelectionAdapter {
		public void widgetSelected(SelectionEvent e) {
			refreshCardsView(true);
			showVotesButton.setEnabled(false);
			revoteButton.setEnabled(false);
			saveVoteButton.setEnabled(false);
			cancelVoteButton.setEnabled(false);
		}
	}

	private final class RevoteListener extends SelectionAdapter {
		public void widgetSelected(SelectionEvent e) {
			revote();
			refreshCardsView(false);
			showVotesButton.setEnabled(true);
			revoteButton.setEnabled(false);
			saveVoteButton.setEnabled(false);
		}
	}

	private final class ShowVotesListener extends SelectionAdapter {
		public void widgetSelected(SelectionEvent e) {

			showVotes();
		}
	}

	private final class CancelVotesListener extends SelectionAdapter {
		public void widgetSelected(SelectionEvent e) {
			cancelVotesGame();
			clearComposite(cardComposite);
			cardComposite.pack(true);
			startVoting.setEnabled(true);
			showVotesButton.setEnabled(false);
			revoteButton.setEnabled(false);
			saveVoteButton.setEnabled(false);
			cancelVoteButton.setEnabled(false);
		}
	}

	private final class StartVotingListener extends SelectionAdapter {
		public void widgetSelected(SelectionEvent e) {
			IWorkItem workItem = (IWorkItem) ((TableItem) workItemTable
					.getSelection()[0]).getData();

			try {
				if (activeSession.getJoinedParticipants().size() <= 1) {
					MessageBox messageBox = new MessageBox(form.getBody()
							.getShell(), SWT.ICON_ERROR | SWT.OK);
					messageBox
							.setMessage("No participant has yet joined the session or everyone has left");
					messageBox.setText("No participants");
					messageBox.open();
					return;
				} else if (isTobeEstimated(workItem)) {
					library.startVoting(activeSession, workItem);
					startVotingOnWorkItem(workItem);
					startVoting.setEnabled(false);
					showVotesButton.setEnabled(true);
					revoteButton.setEnabled(false);
					saveVoteButton.setEnabled(false);
					cancelVoteButton.setEnabled(true);
				}
			} catch (TeamRepositoryException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	
	private ItemProfile<IWorkItem> getProfile(IQueryableAttribute attribute) {
		if (!attribute.isStateExtension())
			return IWorkItem.SMALL_PROFILE.createExtension(attribute.getIdentifier());
		return IWorkItem.SMALL_PROFILE.createExtension(IWorkItem.CUSTOM_ATTRIBUTES_PROPERTY, IExtensibleItem.TIMESTAMP_EXTENSIONS_QUERY_PROPERTY);
	}

	private class SaveButtonListener extends SelectionAdapter {
		public void widgetSelected(SelectionEvent e) {
			try {
				//If fail to save the active session then popup a error message
				if( !library.saveActiveSession(activeSession) ){
					openErrorMessageDialog();
				}
			} catch (TeamRepositoryException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		private void openErrorMessageDialog() {
			MessageBox messageBox = new MessageBox(form.getBody().getShell(),
					SWT.ICON_ERROR);
			messageBox
					.setMessage("Unable to save this game, voting result can only save to scrum project area");
			messageBox.setText("Unable to save");
			messageBox.open();
		}
	}

	/**
	 * End the game and show the result
	 */
	private class CancelButtonListener extends SelectionAdapter {
		public void widgetSelected(SelectionEvent e) {
			cancelSession();
			PlatformUI.getWorkbench().getActiveWorkbenchWindow()
					.getActivePage().closeEditor(editor, false);
		}
	}
}
