package edu.ncsu.csc.wolfpoker.jazzclient.views;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.ibm.team.process.common.IProjectArea;

import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class PokerSessionInput implements IEditorInput {

	private IProjectArea projectArea;
	private Session session;
	private ActiveSession activeSession;

	public PokerSessionInput(IProjectArea projectArea, Session session, ActiveSession activeSession) {
		this.projectArea = projectArea;
		this.session = session;
		this.activeSession = activeSession;
	}

	public boolean exists() {
		return false;
	}

	public ImageDescriptor getImageDescriptor() {
		return null;
	}

	public String getName() {
		return "Active Session";
	}

	public IPersistableElement getPersistable() {
		return null;
	}

	public String getToolTipText() {
		return "Active Session";
	}

	public Object getAdapter(Class adapter) {
		return null;
	}

	public IProjectArea getProjectArea() {
		return projectArea;
	}

	public Session getSession() {
		return session;
	}

	public ActiveSession getActiveSession() {
		return activeSession;
	}
}
