package edu.ncsu.csc.wolfpoker.jazzclient.views;

import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.repository.common.TeamRepositoryException;

public class HandleToObject {

	@SuppressWarnings("unchecked")
	public static final <T> T convert(IItemHandle handle) {
		try {
			ITeamRepository repo = (ITeamRepository) handle.getOrigin();
			return (T) repo.itemManager().fetchCompleteItem(handle,
					IItemManager.REFRESH, null);
		} catch (TeamRepositoryException e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public static final <T> T convert(IItemHandle handle, int flag) {
		try {
			ITeamRepository repo = (ITeamRepository) handle.getOrigin();
			return (T) repo.itemManager().fetchCompleteItem(handle, flag, null);
		} catch (TeamRepositoryException e) {
			e.printStackTrace();
			return null;
		}
	}

}
