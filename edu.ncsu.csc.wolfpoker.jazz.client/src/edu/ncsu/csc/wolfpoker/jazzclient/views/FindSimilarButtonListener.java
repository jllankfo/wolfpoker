package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;

import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.client.IAuditableClient;
import com.ibm.team.workitem.client.IQueryClient;
import com.ibm.team.workitem.client.IWorkItemClient;
import com.ibm.team.workitem.common.IAuditableCommon;
import com.ibm.team.workitem.common.expression.AttributeExpression;
import com.ibm.team.workitem.common.expression.Expression;
import com.ibm.team.workitem.common.expression.IQueryableAttribute;
import com.ibm.team.workitem.common.expression.IQueryableAttributeFactory;
import com.ibm.team.workitem.common.expression.QueryableAttributes;
import com.ibm.team.workitem.common.expression.SelectClause;
import com.ibm.team.workitem.common.expression.Statement;
import com.ibm.team.workitem.common.expression.Term;
import com.ibm.team.workitem.common.model.AttributeOperation;
import com.ibm.team.workitem.common.model.IAttribute;
import com.ibm.team.workitem.common.model.IEnumeration;
import com.ibm.team.workitem.common.model.ILiteral;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.Identifier;
import com.ibm.team.workitem.common.query.IQueryDescriptor;
import com.ibm.team.workitem.common.query.QueryTypes;
import com.ibm.team.workitem.rcp.ui.QueriesUI;

import edu.ncsu.csc.wolfpoker.jazz.client.util.WorkitemEnumUtil;

/**
 * Create a query to find similar votes
 */
public class FindSimilarButtonListener extends SelectionAdapter {
	private final ITeamRepository repository;
	private final IProjectArea projectArea;
	private final int cardValue;

	public FindSimilarButtonListener(ITeamRepository repository, IProjectArea projectArea, int cardValue) {
		super();
		this.repository = repository;
		this.projectArea = projectArea;
		this.cardValue = cardValue;
	}

	public void widgetSelected(SelectionEvent e) {
		final IQueryClient queryClient = (IQueryClient) repository.getClientLibrary(IQueryClient.class);

		try {
			Expression expr = makeExpression();

			SelectClause selectClause = new SelectClause();
			Statement stmt = new Statement(selectClause, expr);

			String queryName = "Example stories with " + cardValue + " points";
			final IQueryDescriptor query = queryClient.createQuery(projectArea, QueryTypes.WORK_ITEM_QUERY, queryName,
					stmt);
			
			UIJob job = new UIJob("Showing already created query: " + queryName) {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					try {
						queryClient.save(query,monitor);
						QueriesUI.showQueryResults(PlatformUI.getWorkbench().getActiveWorkbenchWindow(), query);
						queryClient.delete(query, monitor);
					} catch (TeamRepositoryException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					return Status.OK_STATUS;
				}
			};
			job.schedule();


		} catch (TeamRepositoryException e1) {
			e1.printStackTrace();
		}

	}

	/**
	 * I Hate doing it like this but its a quick fix. Instead of assuming that the complexity enum neatly has id's of 1,2,3 etc
	 * We are going to assume that the name is 1 pt, 2 pts, etc. This seems like the safer of the two assumptions. Although
	 * We shouldn't be doing this at all.
	 * @param savedVote
	 * @param attribute 
	 * @param workItemServer 
	 * @return
	 */
	public static Identifier resolveComplexityName(int savedVote, IAttribute attribute, IWorkItemClient workItemServer) {
		
		try {
			String name = "";
			if (savedVote == 1) name = "1 pt";
			else name = savedVote + " pts";
			
			Identifier literalID;
			IEnumeration enumeration = workItemServer.resolveEnumeration(attribute, null); // or IWorkitemCommon
			List literals = enumeration.getEnumerationLiterals();
			for (Iterator it= literals.iterator(); it.hasNext();) {
				ILiteral iLiteral = (ILiteral) it.next();
				if (iLiteral.getName().equals(name)) {
					return iLiteral.getIdentifier2();
				}
			}
			
		}catch (Exception e) {
			//Just return null
		}
		return null;
	}
	
	private Expression makeExpression() throws TeamRepositoryException {
		IAuditableClient auditableClient = (IAuditableClient) repository.getClientLibrary(IAuditableClient.class);
		IQueryableAttributeFactory factory = QueryableAttributes.getFactory(IWorkItem.ITEM_TYPE);

		IQueryableAttribute pointsAttr = findAttribute("com.ibm.team.apt.attribute.complexity", factory,
				auditableClient, null);
		
		IWorkItemClient workItemClient = (IWorkItemClient) repository
				.getClientLibrary(IWorkItemClient.class);
		IAttribute attribute = workItemClient.findAttribute(projectArea,
				"com.ibm.team.apt.attribute.complexity", null);

		WorkitemEnumUtil.getLiteralEqualsString((cardValue == 1 ? "1 pt" : cardValue + " pts"), attribute, repository);
		
		
		AttributeExpression pointsExpr = new AttributeExpression(pointsAttr, AttributeOperation.EQUALS, Identifier
				.create(ILiteral.class, cardValue + ""));

		Term outer = new Term();
		outer.add(pointsExpr);

		return outer;
	}

	private IQueryableAttribute findAttribute(String attributeId, IQueryableAttributeFactory factory,
			IAuditableCommon auditableCommon, IProgressMonitor monitor) throws TeamRepositoryException {
		return factory.findAttribute(projectArea, attributeId, auditableCommon, monitor);
	}
}