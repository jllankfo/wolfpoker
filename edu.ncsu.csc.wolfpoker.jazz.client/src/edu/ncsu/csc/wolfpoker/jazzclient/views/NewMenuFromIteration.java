package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

import com.ibm.team.process.common.IIteration;
import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.IWorkItemHandle;

import edu.ncsu.csc.wolfpoker.jazz.clientlib.IWolfPokerClientLib;

public class NewMenuFromIteration extends Action {

	private IProjectArea projectArea;
	private IIteration iteration;
	private ITeamRepository repository;

	public NewMenuFromIteration(IProjectArea projectArea, IIteration iteration) {
		this.projectArea = projectArea;
		this.iteration = iteration;
		this.repository = (ITeamRepository) projectArea.getOrigin();
	}

	@Override
	public String getText() {
		return iteration.getName();
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isHandled() {
		return true;
	}

	@Override
	public void run() {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();

		try {
			if (iteration != null) {
				IWolfPokerClientLib library = (IWolfPokerClientLib) repository
						.getClientLibrary(IWolfPokerClientLib.class);
				IWorkItem[] workItems = library
						.getWorkItemsByIteration(iteration);

				List<IWorkItemHandle> list = new ArrayList<IWorkItemHandle>();

				for (int i = 0; i < workItems.length; i++) {
					list.add(workItems[i]);
				}
				page.openEditor(new PokerEditorInput(projectArea, list,
					null, false),
					"edu.ncsu.csc.wolfpoker.jazzclient.views.NewPokerSession");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
