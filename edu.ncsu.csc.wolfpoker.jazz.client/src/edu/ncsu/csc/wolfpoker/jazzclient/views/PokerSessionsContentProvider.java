package edu.ncsu.csc.wolfpoker.jazzclient.views;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ITreePathContentProvider;
import org.eclipse.jface.viewers.TreePath;

import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.process.rcp.ui.teamnavigator.DomainSubtreeRoot;

import edu.ncsu.csc.wolfpoker.jazz.client.util.PokerSessionQuery;

public class PokerSessionsContentProvider extends ArrayContentProvider
		implements ITreePathContentProvider {

	PokerSessionsContentProvider() {
	}


	public Object[] getChildren(TreePath parentPath) {
		if (parentPath.getLastSegment() instanceof DomainSubtreeRoot) {

			DomainSubtreeRoot sub = (DomainSubtreeRoot) parentPath
					.getLastSegment();

			IProjectArea projectArea = (IProjectArea) sub.getCategoryElement();
			
		return PokerSessionQuery.getQueries(projectArea);
		}else{
			return new Object[0];
		}
	}

	public TreePath[] getParents(Object element) {
		return new TreePath[0];
	}

	public void dispose() {
	}


	@Override
	public boolean hasChildren(TreePath path) {
		if (path.getLastSegment() instanceof DomainSubtreeRoot)
			return true;
		else if (path.getLastSegment() instanceof PokerSessionQuery)
			return false;
		
		return false;
	}

}
