package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.repository.common.TeamRepositoryException;

import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;
import edu.ncsu.csc.wolfpoker.jazz.common.model.SessionHistory;

public class PokerSessionHistoryFormPage extends FormPage {

	private Session session;
	private IProjectArea projectArea;
	private ITeamRepository repository;
	private ScrolledForm form;
	private FormToolkit toolkit;
	private Color fontColor;
	private static final RGB FONT_COLOR = new RGB(25, 76, 127);

	public PokerSessionHistoryFormPage(FormEditor editor, String id,
			String title) {
		super(editor, id, title);

	}
	
	@Override
	public void init(IEditorSite site, IEditorInput input) {
		setInput(input);
		session = ((PokerEditorInput) input).getSession();
		projectArea = ((PokerEditorInput) input).getProjectArea();
		repository = (ITeamRepository) projectArea.getOrigin();
	}
	
	@Override
	protected void createFormContent(IManagedForm managedForm) {
		form = managedForm.getForm();
		toolkit = managedForm.getToolkit();
		fontColor = new Color(form.getDisplay(), FONT_COLOR);

		form.setText("History Details");
		form.getBody().setLayout(new GridLayout(2, false));
		initHeader();
		try {
			initHistorySection();
		} catch (TeamRepositoryException e) {
			 //TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	private void initHeader() {
		Composite composite = toolkit.createComposite(form.getBody());
		composite.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true,
				false, 2, 1));
		composite.setLayout(new GridLayout(4, false));
		toolkit.createLabel(composite, "Session Name:").setForeground(fontColor);

		Text sessionName = toolkit.createText(composite, session.getText());

		sessionName.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true,
				false));
		sessionName.setEditable(false);
	}
	
	private void initHistorySection() throws TeamRepositoryException {
		
		Section mainHistorySection = toolkit.createSection(form.getBody(),
				Section.TITLE_BAR | Section.EXPANDED);
		mainHistorySection.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true,
				false));
		mainHistorySection.setText("History Details");
		Composite mainHistoryComposite = toolkit.createComposite(mainHistorySection);
		mainHistoryComposite.setLayout(new GridLayout(1, false));
		mainHistorySection.setClient(mainHistoryComposite);
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy 'at' HH:mm:ss zzz");
		
		List historyList = session.getSessionHistory();
		for (ListIterator iterator = historyList.listIterator(historyList.size()); iterator.hasPrevious();) {
			SessionHistory history = (SessionHistory) repository.itemManager()
					.fetchCompleteItem((IItemHandle) iterator.previous(),
							IItemManager.REFRESH, null);

			Section historySection = toolkit.createSection(
					mainHistoryComposite, Section.TWISTIE | Section.EXPANDED);
			Composite historyComposite = toolkit
					.createComposite(historySection);
			historySection.setClient(historyComposite);

			historySection.setText(getUser(history.getUser()).getName() + " "
					+ df.format(new Date(history.getDate())));

			historyComposite.setLayout(new GridLayout(2, false));

			toolkit.createLabel(historyComposite, "Changed:").setForeground(fontColor);
			toolkit.createLabel(historyComposite, history.getHistoryType());
			if(!history.getHistoryType().equalsIgnoreCase(IWolfPokerService.SessionHistoryChanges.CREATED))
			{
				toolkit.createLabel(historyComposite, "Old Value:").setForeground(fontColor);
				toolkit.createLabel(historyComposite, history.getOldValue());
				
				toolkit.createLabel(historyComposite, "New Value:").setForeground(fontColor);
				toolkit.createLabel(historyComposite,history.getNewValue());
			}

		}
	}

	private IContributor getUser(IContributorHandle user) throws TeamRepositoryException {
		return (IContributor) repository.itemManager().fetchCompleteItem(
				(IItemHandle) user, IItemManager.REFRESH, null);
	}
}
