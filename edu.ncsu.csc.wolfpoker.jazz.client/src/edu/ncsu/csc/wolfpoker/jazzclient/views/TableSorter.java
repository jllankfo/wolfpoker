package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class TableSorter implements Listener {
	public static final Collator collator = Collator.getInstance(Locale.getDefault());

	private final Table table;
	private final TableColumn column;
	private final int columnIndex;
	private TableItem newItem;
	private final boolean treatAsInteger;
	private boolean treatAsImage;
	private int direction = -1;
	private int tableDirection = SWT.DOWN;

	public TableSorter(Table table, TableColumn column, boolean treatAsInteger, boolean treatAsImage) {
		this.table = table;
		this.column = column;
		this.treatAsInteger = treatAsInteger;
		this.treatAsImage = treatAsImage;
		columnIndex = table.indexOf(column);
	}

	public void handleEvent(Event e) {
		direction=direction*-1;
		if(tableDirection==SWT.DOWN)
			tableDirection = SWT.UP;
		List<TableItem> items = makeList();
		sortList(items);
		resetList(items);
		table.setSortDirection(tableDirection);
		table.setSortColumn(column);
	}

	private List<TableItem> makeList() {
		TableItem[] items = table.getItems();
		List<TableItem> itemList = new ArrayList<TableItem>(items.length);
		for (TableItem tableItem : items) {
			itemList.add(tableItem);
		}
		return itemList;
	}

	private void sortList(List<TableItem> items) {
		Collections.sort(items, new Comparator<TableItem>() {
			public int compare(TableItem o1, TableItem o2) {
				String text1;
				String text2;
				if (treatAsImage) {
					text1 = "" + o1.getImage().hashCode();
					text2 = "" + o2.getImage().hashCode();
				} else {
					text1 = o1.getText(columnIndex);
					text2 = o2.getText(columnIndex);
				}
				
				int comparison = 0;
				try {
					if (treatAsInteger || treatAsImage)
						comparison = Integer.valueOf(text1).compareTo(Integer.valueOf(text2));
					else
						comparison = collator.compare(text1, text2);
				} catch (NumberFormatException e) {
					comparison = collator.compare(text1, text2);
				}
				return comparison * direction;
			}
		});
	}

	private void resetList(List<TableItem> items) {
		int numColumns = table.getColumnCount();
		for (TableItem item : items) {
			List<String> stringValues = new ArrayList<String>(numColumns);
			List<Image> imageValues = new ArrayList<Image>(numColumns);
			for (int itemIndex = 0; itemIndex < numColumns; itemIndex++) {
				stringValues.add(item.getText(itemIndex));
				imageValues.add(item.getImage(itemIndex));
			}
			Object itemData = item.getData();
			int itemStyle = item.getStyle();
			item.dispose();
			newItem = new TableItem(table, itemStyle);
			newItem.setText(stringValues.toArray(new String[stringValues.size()]));
			newItem.setImage(imageValues.toArray(new Image[numColumns]));
			newItem.setData(itemData);
		}
	}

}
