package edu.ncsu.csc.wolfpoker.jazzclient.views;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.BaseLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.IViewerLabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;

import edu.ncsu.csc.wolfpoker.jazz.client.util.PokerSessionQuery;

public class PokerSessionsLabelProvider extends BaseLabelProvider implements
		IViewerLabelProvider, ILabelProvider, ITreePathLabelProvider {

	private static final String IMG_SESSION_ICON = "/img/sessionIcon.jpg";
	private Image sessionIcon;

	public PokerSessionsLabelProvider() {
		ImageDescriptor desc = ImageDescriptor.createFromFile(PokerSession.class, IMG_SESSION_ICON);
		sessionIcon = desc.createImage();
	}

	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		updateLabel(label, elementPath.getLastSegment());
	}

	public void updateLabel(ViewerLabel label, Object element) {
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	
	public String getText(Object element) {
		if (element instanceof PokerSessionQuery)
			return ((PokerSessionQuery) element).getName();
		return "";
	}

	public Image getImage(Object element) {
		return sessionIcon;
	}
}