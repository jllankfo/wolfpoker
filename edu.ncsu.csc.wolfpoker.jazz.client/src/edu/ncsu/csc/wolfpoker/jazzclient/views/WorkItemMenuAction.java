package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.repository.common.IItemHandle;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.IWorkItemHandle;

public class WorkItemMenuAction implements IObjectActionDelegate {

	private ISelection fSelection;
	
	public WorkItemMenuAction() {
		super();
	}

	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
	}

	public void selectionChanged(IAction action, ISelection selection) {
		fSelection= selection;
	}

	public void run(IAction action) {
		Iterator iterator= ((IStructuredSelection) fSelection).iterator();
		final List<IWorkItemHandle> handles= new ArrayList<IWorkItemHandle>();
		IProjectArea projectArea = null;
		while (iterator.hasNext()) {
			Object workItem= (IWorkItemHandle) iterator.next();
			if (workItem instanceof IWorkItemHandle)
			{
				IWorkItem workItemComplete = HandleToObject.convert((IItemHandle) workItem);
						
				projectArea = HandleToObject.convert(
								workItemComplete.getProjectArea()); 
							
				handles.add((IWorkItemHandle) workItem);
			}
		}
		
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		
		try {
			page.openEditor(new PokerEditorInput(projectArea, handles,
					null, false),
					"edu.ncsu.csc.wolfpoker.jazzclient.views.NewPokerSession");
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
