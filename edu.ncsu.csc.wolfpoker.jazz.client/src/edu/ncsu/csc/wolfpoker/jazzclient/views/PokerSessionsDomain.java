package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreePathContentProvider;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.ui.IWorkbenchPartSite;
import org.eclipse.ui.PlatformUI;

import com.ibm.team.process.common.IDevelopmentLine;
import com.ibm.team.process.common.IDevelopmentLineHandle;
import com.ibm.team.process.common.IIteration;
import com.ibm.team.process.common.IIterationHandle;
import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.process.rcp.ui.teamnavigator.Category;
import com.ibm.team.process.rcp.ui.teamnavigator.Domain;
import com.ibm.team.process.rcp.ui.teamnavigator.DomainSubtreeRoot;
import com.ibm.team.process.rcp.ui.teamnavigator.ProjectAreaCategory;
import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.repository.common.UUID;

import edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer.PokerSessionFetchJob;
import edu.ncsu.csc.wolfpoker.jazz.client.sessionviewer.WolfpokerPageManager;
import edu.ncsu.csc.wolfpoker.jazz.client.util.PokerSessionQuery;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class PokerSessionsDomain extends Domain {
	// TODO If we want to change who opens a session, this is where we should do
	// it
	// This is related to setting up the menu options.
	private PokerSessionsLabelProvider labelProvider;
	private ITreePathContentProvider contentProvider;
	
	@Override
	public boolean open(IWorkbenchPartSite site, final IStructuredSelection selection) {
		if (selection.getFirstElement() instanceof PokerSessionQuery) {
			try {
				PokerSessionQuery query = (PokerSessionQuery) selection.getFirstElement();
				WolfpokerPageManager manager = (WolfpokerPageManager) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView("edu.ncsu.csc.wolfpoker.jazz.client.views.WolfpokerView");
				PokerSessionFetchJob job = new PokerSessionFetchJob(manager,query);
				manager.runJob(job);
			}catch(Exception e) {
				e.printStackTrace();
			}
			return true;
		}
		
		return super.open(site,selection);
	}

	@Override
	public boolean contains(Object element) {
		if (element instanceof PokerSessionQuery)
			return true;
		else
			return false;
	}

	@Override
	public ITreePathContentProvider getContentProvider() {
		if (contentProvider == null)
			contentProvider = new PokerSessionsContentProvider();
		return contentProvider;
	}

	@Override
	public ITreePathLabelProvider getLabelProvider() {
		if (labelProvider == null)
			labelProvider = new PokerSessionsLabelProvider();
		return labelProvider;
	}

	@Override
	public boolean supportsCategory(Category category) {
		if (category instanceof ProjectAreaCategory) {
			return true;

		} else {
			return false;
		}
	}

	@Override
	public void contributeContextMenuActions(final IMenuManager manager, final IStructuredSelection selection) {

		if (selection.size() == 1) {
			// single element selected
			if (selection.getFirstElement() instanceof DomainSubtreeRoot) {
				DomainSubtreeRoot sub = (DomainSubtreeRoot) selection.getFirstElement();
				IProjectArea projectArea = (IProjectArea) sub.getCategoryElement();

				createMainPlannnigPokerMenus(manager, selection, projectArea);
			} else if (selection.getFirstElement() instanceof Session) {
			
			}
		}
	}

	private void createMainPlannnigPokerMenus(final IMenuManager manager, final IStructuredSelection selection,
			IProjectArea projectArea) {

		MenuManager subMenu = new MenuManager("New from Iteration");
		addIterationMenus(subMenu, projectArea);

		manager.add(subMenu);
		NewMenuAction newMenu = new NewMenuAction(projectArea);
		manager.add(newMenu);
	}

	private void addIterationMenus(MenuManager subMenu, IProjectArea area) {
		IDevelopmentLineHandle[] devLineHandles = area.getDevelopmentLines();
		
		List<IDevelopmentLine> devLines;
		Map<UUID, MenuManager> handleToMenu = new HashMap<UUID, MenuManager>();
		try {
			ITeamRepository repository = (ITeamRepository)devLineHandles[0].getOrigin();
			devLines = fetchBulk(repository,Arrays.asList(devLineHandles));
			Set<IIterationHandle> iterationHandles = new HashSet<IIterationHandle>();
			for (IDevelopmentLine devLine : devLines) {
				MenuManager menu = new MenuManager(devLine.getName());
				handleToMenu.put(devLine.getItemId(), menu);
				iterationHandles.addAll(Arrays.asList(devLine.getIterations()));
				subMenu.add(menu);

			}
			
			addIterationsToMenu(handleToMenu, iterationHandles, area, repository);
			
		} catch (TeamRepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	private void addIterationsToMenu(Map<UUID, MenuManager> handleToMenu,
			Set<IIterationHandle> iterationHandles,IProjectArea area, ITeamRepository repository) throws TeamRepositoryException {
		
		List<IIteration> iterations = fetchBulk(repository,Arrays.asList(iterationHandles.toArray()));
		Set<IIterationHandle> childIterations = new HashSet<IIterationHandle>();
		for (IIteration iteration : iterations) { 
			MenuManager menu = handleToMenu.get(iteration.getDevelopmentLine().getItemId());
			if (menu == null) continue;
			if (iteration.getChildren().length != 0) {
				childIterations.addAll(Arrays.asList(iteration.getChildren()));
				MenuManager parentIt = new MenuManager(iteration.getName());
				handleToMenu.put(iteration.getItemId(), parentIt);
				menu.add(parentIt);
				NewMenuFromIteration itMenu = new NewMenuFromIteration(area,iteration);
				parentIt.add(itMenu);
			
			}else {
				//If the iteration has a parent then we need to use that menu instead
				NewMenuFromIteration itMenu = new NewMenuFromIteration(area,iteration);
				if (iteration.getParent() != null) menu = handleToMenu.get(iteration.getParent().getItemId());
				menu.add(itMenu);
			}
			
		}
		if (childIterations.size() != 0) addIterationsToMenu(handleToMenu, childIterations, area, repository);
	}

	public <T> java.util.List<T> fetchBulk(ITeamRepository repository, java.util.List<?> handles) throws TeamRepositoryException {
		@SuppressWarnings("unchecked")
		java.util.List<T> list = repository.itemManager().fetchCompleteItems(handles, IItemManager.DEFAULT,null);
		return list;
	}
}
