package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

public class PokerSessionCustomizeCardValuesDialog extends Dialog {

	// TODO A work in progress.
	// This isn't being used anywhere. It's meant for customizing the values
	// that will be used in a given session. The default values need to be
	// pulled from the process config, then the user can customize them from
	// there.
	// Finishing this will require a change to the database.
	// Feel free to rip it apart. It's old.
	private static final int[] cardValues = { 0, 1, 2, 3, 5, 8, 13, 20, 40, 100, -1 };;
	private Button[] cardButtons;
	private HashMap<Integer, Boolean> cardsSelect;
	private List<Integer> cardValuesList;

	public PokerSessionCustomizeCardValuesDialog(Shell parent, List cvl) {
		super(parent);

		cardButtons = new Button[cardValues.length];
		cardsSelect = new HashMap<Integer, Boolean>();
		this.cardValuesList = cvl;

		/* default all cards selected */
		if (cardValuesList == null) {
			cardValuesList = new ArrayList<Integer>();
			for (int i = 0; i < cardValues.length; i++) {
				cardsSelect.put(cardValues[i], true);
				cardValuesList.add(cardValues[i]);
			}
		} else {
			for (Integer value : cardValuesList) {
				cardsSelect.put(value, true);
			}
		}
	}

	protected Control createDialogArea(Composite parent) {

		// Create a composite to hold the label
		Composite composite = new Composite(parent, SWT.NONE);
		GridData data = new GridData(GridData.FILL_BOTH);
		data.horizontalSpan = 2;
		composite.setLayoutData(data);
		composite.setLayout(new FillLayout());
		this.createButtons(parent);

		return composite;
	}

	protected void createButtons(Composite parent) {

		for (int j = 0; j < cardButtons.length; j++) {
			cardButtons[j] = createButton(parent, j + 2, "", true);

			if (cardsSelect.get(cardValues[j])) {
				cardButtons[j].setText(cardValues[j] + "check");
			} else {
				cardButtons[j].setText(cardValues[j] + "uncheck");
			}
		}

	}

	protected void buttonPressed(int buttonId) {

		// TODO Changed the icon of pressed button

		if (cardsSelect.get(cardValues[buttonId - 2])) {
			cardsSelect.put(cardValues[buttonId - 2], false);
			cardButtons[buttonId - 2].setText(cardValues[buttonId - 2] + ":uncheck");

		} else {
			cardsSelect.put(cardValues[buttonId - 2], true);
			cardButtons[buttonId - 2].setText(cardValues[buttonId - 2] + ":check");
		}
	}

	public boolean close() {
		getcardValuesList();
		return super.close();
	}

	public List getcardValuesList() {
		cardValuesList.clear();
		for (int i = 0; i < cardValues.length; i++) {
			if (cardsSelect.get(cardValues[i])) {
				cardValuesList.add(cardValues[i]);
			}
		}

		return cardValuesList;
	}
}
