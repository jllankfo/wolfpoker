package edu.ncsu.csc.wolfpoker.jazzclient.views;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.editor.FormEditor;

import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class PokerSessionEditor extends FormEditor {

	public static final String PROP_SESSION_DETAILS_NAME = "Session Details";
	public static final String PROP_SESSION_DETAILS_ID = "Session Details";
	public static final String PROP_HISTORY_DETAILS_NAME = "History Details";
	public static final String PROP_HISTORY_DETAILS_ID = "History Details";
	
	
	private boolean dirty;
	private PokerSessionEditorFormPage sessionEditorPage;

	@Override
	public void doSave(IProgressMonitor monitor) {
		if(dirty)
			sessionEditorPage.saveSession();
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setSite(site);
		setInput(input);
	}

	@Override
	public boolean isDirty() {
		return dirty;
	}

	public void setDirty(boolean dirty) {
		this.dirty = dirty;
		firePropertyChange(IEditorPart.PROP_DIRTY);
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void setFocus() {
	}

	@Override
	protected void addPages() {
		try {
			sessionEditorPage = new PokerSessionEditorFormPage(this, PROP_SESSION_DETAILS_ID, PROP_SESSION_DETAILS_NAME);
			addPage(sessionEditorPage);
			Session session = ((PokerEditorInput)getEditorInput()).getSession();
			if(session!=null)
				setPartName(((PokerEditorInput)getEditorInput()).getName());
				addPage(new PokerSessionHistoryFormPage(this, PROP_HISTORY_DETAILS_ID, PROP_HISTORY_DETAILS_NAME));
		} catch (PartInitException e) {
			e.printStackTrace();
		}
	}
}