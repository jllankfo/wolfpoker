package edu.ncsu.csc.wolfpoker.jazzclient.views;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

import com.ibm.team.process.common.IProjectArea;

public class NewMenuAction extends Action {

	private IProjectArea projectArea;

	public NewMenuAction(IProjectArea projectArea) {
		this.projectArea = projectArea;
	}

	@Override
	public String getText() {
		return "New";
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean isHandled() {
		return true;
	}

	@Override
	public void run() {
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();

		try {
			page.openEditor(new PokerEditorInput(projectArea, null, null,
					false),
					"edu.ncsu.csc.wolfpoker.jazzclient.views.NewPokerSession");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
