package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.workitem.common.model.IWorkItemHandle;

import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class PokerEditorInput implements IEditorInput {
	private IProjectArea projectArea;
	private Session session;
	private boolean readOnly;
	private List<IWorkItemHandle> workItems;

	public PokerEditorInput(IProjectArea projectArea, List<IWorkItemHandle> workItems,
			Session session, boolean readOnly) {
		this.projectArea = projectArea;
		this.session = session;
		this.readOnly = readOnly;
		this.workItems = workItems;
	}

	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getName() {
		if (session != null)
			return session.getText();
		else
			return "Planning Poker";
	}

	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getToolTipText() {
		// TODO Auto-generated method stub
		if (session != null)
			return session.getText();
		else
			return "Planning Poker";
	}

	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	public IProjectArea getProjectArea() {
		return projectArea;
	}

	public Session getSession() {
		return session;
	}

	public boolean getReadOnly() {
		return readOnly;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public List<IWorkItemHandle> getWorkItems() {
		return workItems;
	}

}
