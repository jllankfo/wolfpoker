package edu.ncsu.csc.wolfpoker.jazzclient.views;

import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.TeamRepositoryException;

import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class PokerSessionUtil {
	
	public final static boolean isParticipant(Session session) {
		try{
			boolean readOnly = true;
			ITeamRepository repository = (ITeamRepository) session.getOrigin();
			
			IContributor creator = (IContributor) repository.itemManager()
					.fetchCompleteItem(session.getCreatedBy(),
							IItemManager.REFRESH, null);
	
			IContributor coordinator = (IContributor) repository.itemManager()
					.fetchCompleteItem(session.getSessionCoordinator(),
							IItemManager.REFRESH, null);
			if (creator.getUserId().equals(repository.getUserId())
					|| coordinator.getUserId().equals(repository.getUserId()))
				readOnly = false;
	
			return readOnly;
		}catch(TeamRepositoryException e){
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean isCoordinator(Session session) {
		if (session == null) return false;
		try {
			boolean coord = false;
			ITeamRepository repository = (ITeamRepository) session.getOrigin();

			IContributor coordinator = (IContributor) repository.itemManager().fetchCompleteItem(
					session.getSessionCoordinator(), IItemManager.REFRESH, null);
			if (coordinator.getUserId().equals(repository.getUserId()))
				coord = true;

			return coord;
		} catch (TeamRepositoryException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static boolean isCreator(Session session) {
		if (session == null) return false;
		try {
			boolean isCreator = false;
			ITeamRepository repository = (ITeamRepository) session.getOrigin();

			IContributor creator = (IContributor) repository.itemManager().fetchCompleteItem(
					session.getCreatedBy(), IItemManager.REFRESH, null);
			if (creator.getUserId().equals(repository.getUserId()))
				isCreator = true;

			return isCreator;
		} catch (TeamRepositoryException e) {
			e.printStackTrace();
			return false;
		}
	}
}
