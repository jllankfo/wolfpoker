package edu.ncsu.csc.wolfpoker.jazzclient.views;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubProgressMonitor;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.progress.UIJob;

import com.ibm.team.jface.JazzResources;
import com.ibm.team.process.common.IProjectArea;
import com.ibm.team.process.common.ITeamArea;
import com.ibm.team.process.common.ITeamAreaHandle;
import com.ibm.team.process.rcp.ui.TeamContributorSelectionDialog;
import com.ibm.team.repository.client.IItemManager;
import com.ibm.team.repository.client.ITeamRepository;
import com.ibm.team.repository.common.IContributor;
import com.ibm.team.repository.common.IContributorHandle;
import com.ibm.team.repository.common.IExtensibleItem;
import com.ibm.team.repository.common.TeamRepositoryException;
import com.ibm.team.workitem.client.IQueryClient;
import com.ibm.team.workitem.client.IWorkItemClient;
import com.ibm.team.workitem.common.expression.AttributeExpression;
import com.ibm.team.workitem.common.expression.IQueryableAttribute;
import com.ibm.team.workitem.common.expression.IQueryableAttributeFactory;
import com.ibm.team.workitem.common.expression.QueryableAttributes;
import com.ibm.team.workitem.common.expression.Term;
import com.ibm.team.workitem.common.model.AttributeOperation;
import com.ibm.team.workitem.common.model.IAttribute;
import com.ibm.team.workitem.common.model.ILiteral;
import com.ibm.team.workitem.common.model.IWorkItem;
import com.ibm.team.workitem.common.model.IWorkItemHandle;
import com.ibm.team.workitem.common.model.IWorkItemType;
import com.ibm.team.workitem.common.model.Identifier;
import com.ibm.team.workitem.common.model.ItemProfile;
import com.ibm.team.workitem.common.query.IQueryResult;
import com.ibm.team.workitem.common.query.IResolvedResult;
import com.ibm.team.workitem.rcp.ui.WorkItemUI;
import com.ibm.team.workitem.rcp.ui.workitempicker.WorkItemSelectionDialog;

import edu.ncsu.csc.wolfpoker.jazz.client.util.WorkitemEnumUtil;
import edu.ncsu.csc.wolfpoker.jazz.clientlib.IWolfPokerClientLib;
import edu.ncsu.csc.wolfpoker.jazz.common.IWolfPokerService.SessionParameters;
import edu.ncsu.csc.wolfpoker.jazz.common.model.ActiveSession;
import edu.ncsu.csc.wolfpoker.jazz.common.model.Session;

public class PokerSessionEditorFormPage extends FormPage {

	private static final int LARGE_STRING_LENGTH = 300;

	private static final String DATE_FORMAT = "MM/dd/yyyy 'at' HH:mm:ss zzz";

	private static final String PLANNING_POKER_SESSION = "Planning Poker Session";

	private static final String SESSION_COORDINATOR = "Session Coordinator:*";

	private static final String DATE_LAST_MODIFIED = "Date Last Modified:";

	private static final String FIND = "Find";

	private static final String ALL_ITEMS = "All Items";

	private static final String SELECTED_ITEMS = "Selected Items";
	
	private static final String SELECT_RANGE = "Select Range";

	private static final String ITEMS_TO_BE_ESTIMATED = "Items to be Estimated*";

	private static final String ALL_USERS = "Users in this Project Area and Team Areas";

	private static final String SELECTED_USERS = "Selected Users";

	private static final String SESSION_PARTICIPANTS = "Session Participants*";

	private static final String REMOVE = "Remove";

	private static final String ADD = "Add";

	private static final String SAVE_BUTTON = "Save";

	private static final String DETAILS = "Details";

	private static final String DESCRIPTION = "Description:";

	private static final String DATE_CREATED = "Date Created:";

	private static final String PROJECT_AREA = "Project Area:";

	private static final String CREATED_BY = "Created By:";

	private static final String SESSION_NAME = "Session Name*";

	private static final RGB FONT_COLOR = new RGB(25, 76, 127);

	private static final String CANCEL_BUTTON = "Cancel";

	private static final String START_BUTTON = "Start Game";
	
	private static final String JOIN_BUTTON = "Join Game";
	
	private static final String EMPTY = "  ";
	
	private static final String WORKITEM_NAME = "Work Item";
	
	private static final String WORKITEM_ID = "ID";
	
	private static final int WORKITEM_NAME_COLUMN_WIDTH = 200;

	private static final int WORKITEM_ICON_COLUMN_WIDTH = 25;
	
	private static final int WORKITEM_ID_COLUMN_WIDTH = 30;

	private static final String WORKITEM_ESTIMATE = "Estimate";

	private static final int WORKITEM_ESTIMATE_COLUMN_WIDTH = 70;
	
	private FormToolkit toolkit;

	private ScrolledForm form;

	private Text sessionName;

	private Button saveButton;

	private IProjectArea projectArea;

	private ITeamRepository repository;

	private IProgressMonitor monitor;

	private Color fontColor;

	private IContributor createrUser;

	private Button cancelButton;

	private Table selectedItemTable;

	private Table itemTable;

	private List selectedParticipantList;

	private List participantList;

	private Label dateCreatedLabel;

	private Label dateLastModifiedLabel;

	private Text description;

	private IWorkbenchPage workbankPage;

	private PokerSessionEditor editor;

	private Combo sessionCoordinatorCombo;

	private Session session;

	private Label createrUserLabel;

	private boolean readOnly;

	private Label sessionCoordinatorText;

	private Button startButton;

	private java.util.List<IWorkItemHandle> alreadySelectedWorkItems;
	
	/**
	 * To initial and construct the SWT components
	 */
	private void initSWTcomponent(IManagedForm managedForm) {
		form = managedForm.getForm();
		toolkit = managedForm.getToolkit();

		form.setText(PLANNING_POKER_SESSION);

		form.getBody().setLayout(new GridLayout(2, false));
		fontColor = new Color(form.getDisplay(), FONT_COLOR);

		initHeader();

		if (session == null) {
			try {
				createrUser = repository.contributorManager()
						.fetchContributorByUserId(repository.getUserId(),
								monitor);
			} catch (TeamRepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			initDetailSection(createrUser.getName());
		} else {
			initDetailSection("");
		}

		initItemsSection();

		initParticipantsSection();

		loadSessionDetails();
		setClean();
	}

	private void loadSessionDetails() {
		if (session != null) {
			setPartName(session.getText());
			if (readOnly) {
				sessionName.setEditable(false);
			}
			sessionName.setText(session.getText());

			try {
				createrUser = (IContributor) repository.itemManager()
						.fetchCompleteItem(session.getCreatedBy(),
								IItemManager.REFRESH, null);

				createrUserLabel.setText(createrUser.getName());
				createrUserLabel.pack(true);
				SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
				dateCreatedLabel.setText(df.format(session.getDateCreated()));
				dateCreatedLabel.pack(true);
				dateLastModifiedLabel.setText(df.format(session
						.getDateLastModified()));
				dateLastModifiedLabel.pack(true);

				selectedItemTable.removeAll();
				java.util.List items = session.getItems();
			
				java.util.List<IWorkItem> fullItems = fetchBulk(items);
				
				for (IWorkItem workItem : fullItems) {
					if (!isResolved(workItem)) {
						if (!readOnly) {
							removeFromWorkItemTable(itemTable, workItem);
						}

						addToWorkItemTable(selectedItemTable, workItem);
					}
				}

				selectedParticipantList.removeAll();
				java.util.List<IContributor> contributors = fetchBulk(session.getParticipants());
				for (IContributor contributor : contributors) {
					if (!readOnly) {
						if (participantList.indexOf(contributor.getName()) >= 0)
							participantList.remove(contributor.getName());
					}
					selectedParticipantList.add(contributor.getName());
					selectedParticipantList.setData(contributor.getName(), contributor);
				}

				if (session.getSessionCoordinator() != null) {
					IContributor sessionCoordinator = (IContributor) repository
							.itemManager().fetchCompleteItem(
									session.getSessionCoordinator(),
									IItemManager.DEFAULT, null);
					if (readOnly) {
						sessionCoordinatorText.setText(sessionCoordinator
								.getName());
						sessionCoordinatorText.pack(true);
					} else {
						sessionCoordinatorCombo.select(sessionCoordinatorCombo
								.indexOf(sessionCoordinator.getName()));
					}
				}
				description.setText(session.getDescription());

			} catch (TeamRepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void initDetailSection(String createrUserName) {
		Section detailSection = toolkit.createSection(form.getBody(),
				Section.TITLE_BAR | Section.EXPANDED);
		detailSection.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, false,
				false, 1, 2));
		detailSection.setText(DETAILS);

		Composite detailComposite = toolkit.createComposite(detailSection);
		detailComposite.setLayout(new GridLayout(2, false));
		detailSection.setClient(detailComposite);

		toolkit.createLabel(detailComposite, CREATED_BY).setForeground(
				fontColor);

		createrUserLabel = toolkit
				.createLabel(detailComposite, createrUserName);

		toolkit.createLabel(detailComposite, PROJECT_AREA).setForeground(
				fontColor);

		toolkit.createLabel(detailComposite, projectArea.getName());

		toolkit.createLabel(detailComposite, DATE_CREATED).setForeground(
				fontColor);
		dateCreatedLabel = toolkit.createLabel(detailComposite, "");

		toolkit.createLabel(detailComposite, DATE_LAST_MODIFIED).setForeground(
				fontColor);
		dateLastModifiedLabel = toolkit.createLabel(detailComposite, "");

		toolkit.createLabel(detailComposite, SESSION_COORDINATOR)
				.setForeground(fontColor);
		if (readOnly) {
			sessionCoordinatorText = toolkit.createLabel(detailComposite, "");
		} else {
			sessionCoordinatorCombo = new Combo(detailComposite, SWT.DROP_DOWN
					| SWT.READ_ONLY);
			sessionCoordinatorCombo.setLayoutData(new GridData(SWT.FILL,
					SWT.DEFAULT, true, false));
			sessionCoordinatorCombo.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					setDirty();
				}
			});
		}

		toolkit.createLabel(detailComposite, DESCRIPTION).setForeground(
				fontColor);

		toolkit.createLabel(detailComposite, "");

		description = toolkit.createText(detailComposite, "");
		if (readOnly) {
			description.setEditable(false);
		}

		description.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true,
				false, 2, 1));

		((GridData) description.getLayoutData()).widthHint = 250;
		((GridData) description.getLayoutData()).heightHint = 100;
		description.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				setDirty();
			}
		});
	}


	private void initItemsSection() {
		Section itemsSection = toolkit.createSection(form.getBody(),
				Section.TITLE_BAR | Section.EXPANDED);
		itemsSection.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true,
				false));
		itemsSection.setText(ITEMS_TO_BE_ESTIMATED);

		Composite itemsComposite = toolkit.createComposite(itemsSection);

		itemsComposite.setLayout(new GridLayout(3, false));
		itemsSection.setClient(itemsComposite);

		toolkit.createLabel(itemsComposite, SELECTED_ITEMS).setForeground(
				fontColor);

		if (!readOnly) {
			Label allItems = toolkit.createLabel(itemsComposite, ALL_ITEMS);
			allItems.setLayoutData(new GridData(SWT.DEFAULT, SWT.DEFAULT,
					false, false, 2, 1));
			allItems.setForeground(fontColor);
		} else {
			toolkit.createLabel(itemsComposite, "");
			toolkit.createLabel(itemsComposite, "");
		}

		selectedItemTable = toolkit.createTable(itemsComposite, SWT.MULTI
				| SWT.BORDER | SWT.FULL_SELECTION );
		selectedItemTable.setLinesVisible(true);
		selectedItemTable.setHeaderVisible(true);
		
		TableColumn column = new TableColumn(selectedItemTable, SWT.CENTER);
		column.setText(EMPTY);
		column.setMoveable(false);
		column.setResizable(false);
		column.addListener(SWT.Selection, new TableSorter(selectedItemTable, column, true, true));
		column.setWidth(WORKITEM_ICON_COLUMN_WIDTH);
		
		column = new TableColumn(selectedItemTable, SWT.CENTER);
		column.setText(WORKITEM_ID);
		column.setMoveable(false);
		column.setResizable(true);
		column.addListener(SWT.Selection, new TableSorter(selectedItemTable,column,true,false));
		column.setWidth(WORKITEM_ID_COLUMN_WIDTH);
		
		column = new TableColumn(selectedItemTable, SWT.LEFT);
		column.setText(WORKITEM_NAME);
		column.setMoveable(false);
		column.setResizable(true);
		column.addListener(SWT.Selection, new TableSorter(selectedItemTable, column, false, false));
		column.setWidth(WORKITEM_NAME_COLUMN_WIDTH);
		
		column = new TableColumn(selectedItemTable,SWT.LEFT);
		column.setText(WORKITEM_ESTIMATE);
		column.setMoveable(false);
		column.setResizable(true);
		column.setWidth(WORKITEM_ESTIMATE_COLUMN_WIDTH);
		
		selectedItemTable.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT,
				true, true, 1, 4));
		((GridData) selectedItemTable.getLayoutData()).heightHint = 100;
		((GridData) selectedItemTable.getLayoutData()).widthHint = 150;

		if (!readOnly) {
			itemTable = toolkit.createTable(itemsComposite, SWT.MULTI
					| SWT.BORDER | SWT.FULL_SELECTION );
			
			itemTable.setLinesVisible(true);
			itemTable.setHeaderVisible(true);
			
			TableColumn itemColumn = new TableColumn(itemTable, SWT.CENTER);
			itemColumn.setText(EMPTY);
			itemColumn.setMoveable(false);
			itemColumn.setResizable(true);
			itemColumn.addListener(SWT.Selection, new TableSorter(itemTable, itemColumn, true, true));
			itemColumn.setWidth(WORKITEM_ICON_COLUMN_WIDTH);
			
			itemColumn = new TableColumn(itemTable,SWT.CENTER);
			itemColumn.setText(WORKITEM_ID);
			itemColumn.setMoveable(false);
			itemColumn.setResizable(true);
			itemColumn.addListener(SWT.Selection,new TableSorter(itemTable,itemColumn,true,false));
			itemColumn.setWidth(WORKITEM_ID_COLUMN_WIDTH);
			
			itemColumn = new TableColumn(itemTable, SWT.LEFT);
			itemColumn.setText(WORKITEM_NAME);
			itemColumn.setMoveable(false);
			itemColumn.setResizable(true);
			itemColumn.addListener(SWT.Selection, new TableSorter(itemTable, itemColumn, false, false));
			itemColumn.setWidth(WORKITEM_NAME_COLUMN_WIDTH);
			
			itemTable.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true,
					true, 1, 4));
			((GridData) itemTable.getLayoutData()).heightHint = 100;
			((GridData) itemTable.getLayoutData()).widthHint = 150;

			Button add = toolkit.createButton(itemsComposite, ADD, SWT.PUSH);
			add.setLayoutData(new GridData(75, -1));
			((GridData) add.getLayoutData()).verticalAlignment = SWT.TOP;

			Button remove = toolkit.createButton(itemsComposite, REMOVE,
					SWT.PUSH);
			remove.setLayoutData(new GridData(75, -1));
			((GridData) remove.getLayoutData()).verticalAlignment = SWT.TOP;

			Button find = toolkit.createButton(itemsComposite, FIND,
					SWT.PUSH);
			find.setLayoutData(new GridData(75, -1));
			((GridData) find.getLayoutData()).verticalAlignment = SWT.TOP;

			
			Button selectRange = toolkit.createButton(itemsComposite,SELECT_RANGE,SWT.PUSH);
			selectRange.setLayoutData(new GridData(75,-1));
			((GridData) selectRange.getLayoutData()).verticalAlignment = SWT.TOP;
			
			add.addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
					int lenMoved = moveBetweenTables(selectedItemTable, itemTable);
					if (lenMoved> 0)
						setDirty();
				}
			});

			remove.addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
					int lenMoved = moveBetweenTables(itemTable, selectedItemTable);
					if (lenMoved> 0)
						setDirty();
				}
			});

			
			find.addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
		            final IWorkItemHandle[] workItemHandles = WorkItemSelectionDialog
		                    .getWorkItems(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		           
		            try {
		            	java.util.List<IWorkItem> workItems = fetchBulk(Arrays.asList(workItemHandles));
		            	for (IWorkItem workItem : workItems) { 
		            		if (isAlreadyIn(itemTable, workItem))
		            			removeFromWorkItemTable(itemTable, workItem);
		            		
		            		if (!isAlreadyIn(selectedItemTable, workItem))
		            			addToWorkItemTable(selectedItemTable, workItem);
		            	}
		            } catch(TeamRepositoryException e1) {
		            	java.util.List<String> errors = new ArrayList<String>();
		            	errors.add(e1.getMessage());
		            	showErrorDialog(errors);
		            }
				}

			});
			
			selectRange.addSelectionListener(new SelectionAdapter(){
				public void widgetSelected(SelectionEvent e)
				{				
					InputDialog id = new InputDialog(form.getBody().getShell(), "Range", "Input a range of work item ids.", "0", new RangeValidator());
					if (id.open() == Window.OK)
					{
						String values[] = id.getValue().split("-");
						
						int vals[] = new int[] {Integer.parseInt(values[0]),Integer.parseInt(values[1])};
						
						//Get table item indexs with a work item id in the range
						TableItem[] items = itemTable.getItems();
						ArrayList<Integer> selectItems = new ArrayList<Integer>();
						for(int a=0;a<items.length;a++)
						{
							int workItemId = -1;
							try{
								workItemId = Integer.parseInt(items[a].getText(1));
							}catch(NumberFormatException e1)
							{
								System.err.println("Error casting workitem id.");
							}
							if (vals[0]<= workItemId && workItemId <= vals[1])
								selectItems.add(itemTable.indexOf(items[a]));
						}
						
						//Cast the object array to an integer array
						int selectInts[] = new int[selectItems.size()];
						int c=0;
						for(Integer b : selectItems)
						{
							selectInts[c] = b;
							c++;
						}
						itemTable.select(selectInts);
					}
				}
			});
			itemTable.addMouseListener(new ItemsDoubleClickListener());
			refreshItemsToEstimate();
			addAlreadySelectedWorkItems();
		}
		
		selectedItemTable.addMouseListener(new ItemsDoubleClickListener());
	}

	private void addAlreadySelectedWorkItems() {
		try {
			if (alreadySelectedWorkItems != null && alreadySelectedWorkItems.size() != 0) {
				java.util.List<IWorkItem> workItems = fetchBulk(alreadySelectedWorkItems);
				for (IWorkItem workItem : workItems) {
					if (!isResolved(workItem)) {
						addToWorkItemTable(selectedItemTable, workItem);
						removeFromWorkItemTable(itemTable, workItem);
					}
				}
			}
		} catch (TeamRepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void refreshItemsToEstimate() {
		itemTable.removeAll();
		IWorkItemClient workItemClient = (IWorkItemClient) repository
				.getClientLibrary(IWorkItemClient.class);

		IQueryClient queryClient = workItemClient.getQueryClient();

		IQueryableAttributeFactory factory = QueryableAttributes
				.getFactory(IWorkItem.ITEM_TYPE);

		IQueryableAttribute projectAreaAttribute;
		//"com.ibm.team.apt.attribute.complexity"
		try {
			projectAreaAttribute = factory.findAttribute(projectArea,
					IWorkItem.PROJECT_AREA_PROPERTY, queryClient
							.getAuditableCommon(), monitor);
			AttributeExpression projectAreaExpression = new AttributeExpression(
					projectAreaAttribute, AttributeOperation.EQUALS,
					projectArea);

			Term term = new Term();
			term.add(projectAreaExpression);

			ItemProfile<IWorkItem> profile = getProfile(projectAreaAttribute)
					.createExtension(IWorkItem.CREATOR_PROPERTY);

			IQueryResult<IResolvedResult<IWorkItem>> result = queryClient
					.getResolvedExpressionResults(projectArea, term, profile);

			while (result.hasNext(monitor)) {
				IWorkItem workItem = result.next(monitor).getItem();

				if (!isResolved(workItem)) {

					if (isAlreadyIn(selectedItemTable, workItem) == false)
						addToWorkItemTable(itemTable, workItem);

				}
			}

		} catch (TeamRepositoryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	private ItemProfile<IWorkItem> getProfile(IQueryableAttribute attribute) {
		if (!attribute.isStateExtension())
			return IWorkItem.SMALL_PROFILE.createExtension(attribute
					.getIdentifier());
		return IWorkItem.SMALL_PROFILE.createExtension(
				IWorkItem.CUSTOM_ATTRIBUTES_PROPERTY,
				IExtensibleItem.TIMESTAMP_EXTENSIONS_QUERY_PROPERTY);
	}

	private void initParticipantsSection() {

		Section participantSection = toolkit.createSection(form.getBody(),
				Section.TITLE_BAR | Section.EXPANDED);
		participantSection.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT,
				true, true));
		participantSection.setText(SESSION_PARTICIPANTS);

		Composite participantComposite = toolkit
				.createComposite(participantSection);

		participantComposite.setLayout(new GridLayout(3, false));
		participantSection.setClient(participantComposite);

		toolkit.createLabel(participantComposite, SELECTED_USERS)
				.setForeground(fontColor);

		if (!readOnly) {
			Label allUsers = toolkit.createLabel(participantComposite,
					ALL_USERS);

			allUsers.setLayoutData(new GridData(SWT.DEFAULT, SWT.DEFAULT,
					false, false, 2, 1));
			allUsers.setForeground(fontColor);
		} else {
			toolkit.createLabel(participantComposite, "");
			toolkit.createLabel(participantComposite, "");
		}

		selectedParticipantList = new List(participantComposite, SWT.BORDER
				| SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
		selectedParticipantList.setLayoutData(new GridData(SWT.FILL,
				SWT.DEFAULT, true, true, 1, 2));
		((GridData) selectedParticipantList.getLayoutData()).heightHint = 100;
		((GridData) selectedParticipantList.getLayoutData()).widthHint = 150;
		
		selectedParticipantList
			.addMouseListener(new UserDoubleClickListener());

		if (!readOnly) {
			participantList = new List(participantComposite, SWT.BORDER
					| SWT.H_SCROLL | SWT.V_SCROLL | SWT.MULTI);
			participantList.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT,
					true, true, 1, 2));
			((GridData) participantList.getLayoutData()).heightHint = 100;
			((GridData) participantList.getLayoutData()).widthHint = 150;

			participantList.addMouseListener(new UserDoubleClickListener());
			refreshParticipants();
			
			Button add = toolkit.createButton(participantComposite, ADD,
					SWT.PUSH);
			add.setLayoutData(new GridData(70, -1));
			((GridData) add.getLayoutData()).verticalAlignment = SWT.TOP;
			Button remove = toolkit.createButton(participantComposite, REMOVE,
					SWT.PUSH);
			remove.setLayoutData(new GridData(70, -1));
			((GridData) remove.getLayoutData()).verticalAlignment = SWT.TOP;
			add.addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
					String[] selectedUser = participantList.getSelection();
					if (selectedUser.length > 0)
						setDirty();

					for (int i = 0; i < selectedUser.length; i++) {
						selectedParticipantList.add(selectedUser[i]);
						participantList.remove(selectedUser[i]);
					}
				}
			});

			remove.addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
					String[] toRemoveUser = selectedParticipantList
							.getSelection();
					if (toRemoveUser.length > 0)
						setDirty();
					for (int i = 0; i < toRemoveUser.length; i++) {
						participantList.add(toRemoveUser[i]);
						selectedParticipantList.remove(toRemoveUser[i]);
					}
				}
			});

			Button addMore = toolkit.createButton(participantComposite,
					"Find users...", SWT.PUSH);
			addMore.setLayoutData(new GridData(70, -1));
			((GridData) add.getLayoutData()).verticalAlignment = SWT.TOP;

			addMore.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					addFoundUser();
				}
			});


		}
	}

	private void refreshParticipants() {

		try {
			Map<String, IContributor> participants = new HashMap<String, IContributor>();
			java.util.List<ITeamAreaHandle> teamAreaHandles = (java.util.List<ITeamAreaHandle>) projectArea
					.getTeamAreaHierarchy().getTeamAreas();

			java.util.Set<IContributorHandle> memberHandles = new HashSet<IContributorHandle>();
			// Contributors from all child team areas
			java.util.List<ITeamArea> teamAreas = fetchBulk(teamAreaHandles);
			for (ITeamArea teamArea : teamAreas) {
				memberHandles.addAll(Arrays.asList(teamArea.getMembers()));
				memberHandles.addAll(Arrays.asList(teamArea.getAdministrators()));
			}

			// Contributors in the project area
			
			memberHandles.addAll(Arrays.asList(projectArea.getMembers()));
			memberHandles.addAll(Arrays.asList(projectArea.getAdministrators()));
			
			java.util.List<IContributor> members = fetchBulk(Arrays.asList(memberHandles.toArray()));
			for (IContributor member : members) {
				participants.put(member.getName(), member);
			}
			
			// Sort the list of entries
			Set<Entry<String, IContributor>> entries = participants.entrySet();
			java.util.List<Entry<String, IContributor>> entryList = new ArrayList<Entry<String, IContributor>>();
			entryList.addAll(entries);
			Collections.sort(entryList,
					new Comparator<Entry<String, IContributor>>() {
						public int compare(Entry<String, IContributor> o1,
								Entry<String, IContributor> o2) {
							return TableSorter.collator.compare(o1.getKey(), o2
									.getKey());
						}
					});

			String currentUser = "";
			if (repository != null) currentUser = repository.getUserId();
			int index=0;
			// Putting them into the actual list
			for (Entry<String, IContributor> entry : entryList) {
				selectedParticipantList.add(entry.getKey());
				participantList.setData(entry.getKey().toString(), entry
						.getValue());
				String user = entry.getKey();
				sessionCoordinatorCombo.add(user);
				if (currentUser.equals(user)) index=sessionCoordinatorCombo.getItemCount()-1;
				
			}
			
			sessionCoordinatorCombo.select(index);
		} catch (TeamRepositoryException e) {
			e.printStackTrace();
		}
	}

	private void initHeader() {
		Composite composite = toolkit.createComposite(form.getBody());
		composite.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true,
				false, 2, 1));
		composite.setLayout(new GridLayout(5, false));
		toolkit.createLabel(composite, SESSION_NAME).setForeground(fontColor);

		sessionName = toolkit.createText(composite, "");

		sessionName.setLayoutData(new GridData(SWT.FILL, SWT.DEFAULT, true,
				false));
		if (!readOnly) {
			sessionName.addModifyListener(new ModifyListener() {
				public void modifyText(ModifyEvent e) {
					setDirty();
				}
			});

			saveButton = toolkit.createButton(composite, SAVE_BUTTON, SWT.PUSH);
			cancelButton = toolkit.createButton(composite, CANCEL_BUTTON,
					SWT.PUSH);
			if (PokerSessionUtil.isCoordinator(session)){
			startButton = toolkit.createButton(composite, START_BUTTON,
					SWT.PUSH);
			}else
			{
				startButton = toolkit.createButton(composite, JOIN_BUTTON, SWT.PUSH);
			}
			saveButton.setLayoutData(new GridData(50, -1));
			cancelButton.setLayoutData(new GridData(50, -1));
			startButton.setLayoutData(new GridData(70, -1));

			cancelButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					workbankPage.closeEditor(editor, false);
				}
			});

			saveButton.addSelectionListener(new SelectionAdapter() {

				public void widgetSelected(SelectionEvent e) {
					saveSession();
				}

			});

			startButton.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					startSession();
				}
			});
		}else{
			//If the session is active and user isnt creator/coordinator
			if (PokerSessionUtil.isParticipant(session) && !session.isArchived())
			{
				startButton = toolkit.createButton(composite, JOIN_BUTTON, SWT.PUSH);
				startButton.setLayoutData(new GridData(70, -1));

				startButton.addSelectionListener(new SelectionAdapter() {
					public void widgetSelected(SelectionEvent e) {
						startSession();
					}
				});
			}
		}
	}

	void saveSession() {
		java.util.List<String> errorList = verify();
		if (errorList.size() > 0) {
			// display notification to the user
			showErrorDialog(errorList);
			return;
		}

		IWolfPokerClientLib library = (IWolfPokerClientLib) repository
				.getClientLibrary(IWolfPokerClientLib.class);
		SessionParameters sessionParameters = new SessionParameters();

		sessionParameters.createrUserID = createrUser.getUserId();
		sessionParameters.description = description.getText();

		ArrayList<String> items = new ArrayList<String>();
		IWorkItem[] selectedItems = getWorkitemsOfTable(selectedItemTable);
		for (int i = 0; i < selectedItems.length; i++) {
			items.add(selectedItems[i].getItemId().getUuidValue());
		}
		sessionParameters.itemsUUID = items.toArray(new String[items.size()]);

		sessionParameters.name = sessionName.getText();

		ArrayList<String> participants = new ArrayList<String>();

		String[] selectedParticipants = selectedParticipantList.getItems();
		for (int j = 0; j < selectedParticipants.length; j++) {
			IContributor contributor = (IContributor) participantList
					.getData(selectedParticipants[j]);
			participants.add(contributor.getUserId());
		}
		sessionParameters.participantsUserID = participants
				.toArray(new String[participants.size()]);

		if (sessionCoordinatorCombo.getSelectionIndex() >= 0) {
			sessionParameters.sessionCoordinatorUserID = ((IContributor) participantList
					.getData(sessionCoordinatorCombo
							.getItem(sessionCoordinatorCombo
									.getSelectionIndex()))).getUserId();
		}
		sessionParameters.projectAreaName = projectArea.getName();

		try {
			if (session == null) {
				session = library.createNewSession(sessionParameters);
			} else {

				session = library.saveSession(session.getItemId()
						.getUuidValue(), sessionParameters);

			}
		} catch (TeamRepositoryException e1) {
			e1.printStackTrace();
			// TODO Add a notification to the user here - or something,
			// otherwise session becomes null
		}
		setPartName(session.getText());

		SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
		dateCreatedLabel.setText(df.format(new Date(session.getDateCreated())));
		dateCreatedLabel.pack(true);
		dateLastModifiedLabel.setText(df.format(new Date(session
				.getDateLastModified())));

		dateLastModifiedLabel.pack(true);

		if (getEditor().findPage(PokerSessionEditor.PROP_HISTORY_DETAILS_ID) != null)
			getEditor().removePage(1);

		((PokerEditorInput) getEditor().getEditorInput()).setSession(session);

		try {
			getEditor().addPage(
					new PokerSessionHistoryFormPage(getEditor(),
							PokerSessionEditor.PROP_HISTORY_DETAILS_ID,
							PokerSessionEditor.PROP_HISTORY_DETAILS_NAME));
		} catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Update button to say join instead of start or vice versa if coordinator changes
		
		if (PokerSessionUtil.isCoordinator(session))
			startButton.setText(START_BUTTON);
		else
			startButton.setText(JOIN_BUTTON);
		setClean();
	}

	private void startSession() {

		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();

		ITeamRepository repository = (ITeamRepository) session.getOrigin();

		try {
			IProjectArea projectArea = (IProjectArea) repository.itemManager()
					.fetchCompleteItem(session.getProjectArea(),
							IItemManager.REFRESH, null);

			IWolfPokerClientLib library = (IWolfPokerClientLib) repository
					.getClientLibrary(IWolfPokerClientLib.class);
			/* To get the latest session */
			Session newSession = (Session) repository.itemManager()
					.fetchCompleteItem(session, IItemManager.REFRESH, null);
			if (!newSession.isActive() && PokerSessionUtil.isCoordinator(newSession)) {

				ActiveSession activeSession = library.startSession(newSession);
				page.openEditor(new PokerSessionInput(projectArea, newSession,
						activeSession),
						"edu.ncsu.csc.wolfpoker.jazzclient.views.PokerSession");

			} else if (newSession.isActive()){
				ActiveSession activeSession = library.joinSession(newSession);
				page
						.openEditor(new PokerSessionInput(projectArea,
								newSession, activeSession),
								"edu.ncsu.csc.wolfpoker.jazzclient.views.PokerSession");
			}else
			{
				MessageBox messageBox = new MessageBox(form.getBody()
						.getShell(), SWT.ICON_ERROR | SWT.OK);
				messageBox
						.setMessage("Session is not active.");
				messageBox.setText("Not active");
				messageBox.open();
			}

		} catch (PartInitException e) {
			e.printStackTrace();
		} catch (TeamRepositoryException e) {
			e.printStackTrace();
		}
	}

	private void showErrorDialog(java.util.List<String> errorList) {
		final Shell dialog = new Shell(SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM);
		dialog.setText("Error Encountered!");
		dialog.setBounds(this.form.getBounds().width / 2,
				this.form.getBounds().height / 2, 280, 150);
		dialog.setLayout(new GridLayout(1, true));

		final String newLineBullet = "\n\t\u2022";
		String message = "The following errors were encountered: ";
		for (String error : errorList) {
			message += newLineBullet + error;
		}
		final Label label = new Label(dialog, SWT.WRAP);
		label.setText(message);

		final Button buttonOK = new Button(dialog, SWT.PUSH);
		buttonOK.setText("     OK     ");
		Listener listener = new Listener() {
			public void handleEvent(Event event) {
				dialog.close();
			}
		};
		buttonOK.addListener(SWT.Selection, listener);
		buttonOK.setLayoutData(new GridData(GridData.CENTER, GridData.CENTER,
				true, true));

		dialog.open();
	}

	/**
	 * To determined whether a single workitem has been resolved.
	 * 
	 * @param workItem
	 * @return true or false
	 */
	private boolean isResolved(IWorkItem workItem) {
		try {
			return (workItem.getResolutionDate() != null);
		} catch (IllegalStateException ise) {
			return false;
		}
	}

	private java.util.List<String> verify() {
		java.util.List<String> errorList = new ArrayList<String>();
		if (sessionName.getText() == "" || sessionName.getText() == null)
			errorList.add("Enter a session name");
		if (selectedParticipantList.getItemCount() == 0)
			errorList.add("Add at least one participant.");
		if (selectedItemTable.getItemCount() == 0)
			errorList.add("Add at least one item to be estimated.");
		if (sessionCoordinatorCombo.getSelectionIndex() == -1)
			errorList.add("Select a coordinator.");
		return errorList;
	}

	private void setDirty() {
		editor.setDirty(true);
	}

	private void setClean() {
		editor.setDirty(false);
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) {
		setInput(input);
		session = ((PokerEditorInput) input).getSession();
		projectArea = ((PokerEditorInput) input).getProjectArea();
		
		
		if (projectArea == null) {
			throw new RuntimeException("Invalid Project area. Are you logged in?");
		}
		repository = (ITeamRepository) projectArea.getOrigin();
		alreadySelectedWorkItems = ((PokerEditorInput) input).getWorkItems();
		readOnly = ((PokerEditorInput) input).getReadOnly();
	}

	public PokerSessionEditorFormPage(FormEditor editor, String id, String title) {
		super(editor, id, title);
		monitor = new NullProgressMonitor();
		workbankPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage();
		this.editor = (PokerSessionEditor) editor;
		setClean();
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		initSWTcomponent(managedForm);
	}

	public class ItemsDoubleClickListener extends MouseAdapter {

		public void mouseDoubleClick(MouseEvent e) {

			IWorkItem doubleClickedItem;
			if (e.widget == selectedItemTable) {
				doubleClickedItem = getSelectedItem(selectedItemTable);
			} else {
				doubleClickedItem = getSelectedItem(itemTable);
			}

			if(doubleClickedItem!=null)
				WorkItemUI.openEditor(workbankPage, doubleClickedItem);
		}
	}

	/**
	 * DoubleClickListener for opening the user editor by double-click the user
	 */
	public class UserDoubleClickListener extends MouseAdapter {
		public void mouseDoubleClick(MouseEvent e) {
			Object input = null;
			String selectUser;
			if (e.widget == participantList) {
				selectUser = participantList.getItem(participantList
						.getFocusIndex());
				input = participantList.getData(selectUser);
			} else if (e.widget == selectedParticipantList) {
				selectUser = selectedParticipantList
						.getItem(selectedParticipantList.getFocusIndex());
				input = selectedParticipantList.getData(selectUser);
			} else {
				selectUser = null;
			}
			
			WorkItemUI.openEditor(workbankPage, input);
		}
	}

	private int moveBetweenTables(Table to, Table from) {
		IWorkItem[] selectedItems = getSelectedWorkitemsOfTable(from);
		
		for (int i = 0; i < selectedItems.length; i++) {
			IWorkItem workItem = selectedItems[i];
			addToWorkItemTable(to, workItem);
		}
		
		int[] selected = from.getSelectionIndices();
		from.remove(selected);
		return selected.length;
	}
	
	private void addToWorkItemTable(Table workItemTable, final IWorkItem workItem) {
		
		final IWorkItemClient workItemClient = (IWorkItemClient) repository
				.getClientLibrary(IWorkItemClient.class);
		final LocalResourceManager resourceManager = new LocalResourceManager(
				JFaceResources.getResources(), workItemTable);
		final TableItem item = new TableItem(workItemTable, SWT.CENTER);
		
		UIJob job = new UIJob("Loading icons") {
		
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					if (!item.isDisposed()) {
						IWorkItemType type = workItemClient.findWorkItemType(
								workItem.getProjectArea(), workItem
										.getWorkItemType(), monitor);
						ImageDescriptor imageDescriptor = WorkItemUI
								.getImageDescriptor(type.getIconURL());
						Image image = JazzResources.getImageWithDefault(
								resourceManager, imageDescriptor);
						item.setImage(0, image);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return Status.OK_STATUS;
			}
		};

		job.setUser(false);
		job.schedule();
		
		item.setText(1,workItem.getId()+"");
		item.setText(2, workItem.getHTMLSummary().toString());
		item.setText(3,getComplexityValue(workItem));
		item.setData(workItem);
		
	}
	
	private void removeFromWorkItemTable(Table workItemTable, IWorkItem workItem) {
		TableItem[] tableItems = workItemTable.getItems();
		for (int i = 0; i < tableItems.length; i++) {
			IWorkItem foundWorkItems = (IWorkItem) tableItems[i].getData();
			if (foundWorkItems.getId() == workItem.getId()) {
				workItemTable.remove(i);
				break;
			}

		}
	}

	private IWorkItem[] getWorkitemsOfTable(Table workItemTable) {
		TableItem[] tableItems = workItemTable.getItems();
		
		IWorkItem[] workItems = new IWorkItem[tableItems.length];
		for (int i = 0; i < tableItems.length; i++) {
			workItems[i] = (IWorkItem) tableItems[i].getData();
		}
		return workItems;
	}


	private IWorkItem[] getSelectedWorkitemsOfTable(Table workItemTable) {
		TableItem[] tableItems = workItemTable.getSelection();
		
		IWorkItem[] workItems = new IWorkItem[tableItems.length];
		for (int i = 0; i < tableItems.length; i++) {
			workItems[i] = (IWorkItem) tableItems[i].getData();
		}
		return workItems;
	}
	
	private IWorkItem getSelectedItem(Table workItemTable) {
		TableItem[] tableItems = workItemTable.getSelection();
		if (tableItems.length > 0) {
			return (IWorkItem) tableItems[0].getData();
		}
		return null;
	}

	private boolean isAlreadyIn(Table workItemTable, IWorkItem workItem) {
		TableItem[] tableItems = workItemTable.getItems();
		for (int i = 0; i < tableItems.length; i++) {
			IWorkItem foundWorkItems = (IWorkItem) tableItems[i].getData();
			if (foundWorkItems.getId() == workItem.getId()) {
				return true;
			}

		}
		
		return false;
	}
	
	private void addFoundUser() {
		final Shell shell = new Shell(SWT.APPLICATION_MODAL | SWT.DIALOG_TRIM);
		shell.setText("Add users to the Poker Session");
		final TeamContributorSelectionDialog dialog = new TeamContributorSelectionDialog(
				shell, repository);
		try {
			if (dialog.open() == Window.OK) {
				IContributorHandle[] selected = dialog.getContributorResult();
				java.util.List<IContributor> contributors = fetchBulk(Arrays.asList(selected));
				for (IContributor c : contributors) {
					String name = c.getName();
					
					if(participantList.indexOf(name)>=0)
						participantList.remove(name);
					
					selectedParticipantList.add(name);
					participantList.setData(name.toString(), c);
				}
			}
		} catch (TeamRepositoryException exception) {
			exception.printStackTrace();
		} finally {
			dialog.close();
		}
	}
	
	public String getComplexityValue(IWorkItem workItem)
	{
		IWorkItemClient workItemClient = (IWorkItemClient) repository
		.getClientLibrary(IWorkItemClient.class);

		IAttribute attribute = null;
		try {
			attribute = workItemClient.findAttribute(projectArea,
			"com.ibm.team.apt.attribute.complexity", null);
		} catch (TeamRepositoryException e) {
			e.printStackTrace();
		}

		try {
			if (attribute != null && workItem.hasAttribute(attribute)) {
				Identifier<ILiteral> obj = (Identifier<ILiteral>) workItem
				.getValue(attribute);
				
				//Fetch the enum name, instead of the id
				ILiteral literal = WorkitemEnumUtil.getLiteralbyID(obj, attribute, repository);
				return literal.getName();
			} else {
				return "";
			}
		} catch (TeamRepositoryException e) {
			return "";
		}

	}
	
	private class RangeValidator implements IInputValidator
	{

		public String isValid(String newText) {
			String check[] = newText.split("-");
			if (check.length != 2)
				return "Range must be in the form of num-num (eg. 1-5)";
			try{
				Integer.parseInt(check[0]);
				Integer.parseInt(check[1]);
			}catch(NumberFormatException e)
			{
				return "Each end of the range must be a numeric value.";
			}
			return null;
		}
		
	}
	
	
	public <T> java.util.List<T> fetchBulk(java.util.List<?> handles) throws TeamRepositoryException {
		@SuppressWarnings("unchecked")
		java.util.List<T> list = repository.itemManager().fetchCompleteItems(handles, IItemManager.DEFAULT,new SubProgressMonitor(monitor, 500));
		return list;
	}
}



